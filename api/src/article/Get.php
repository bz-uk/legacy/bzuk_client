<?php
namespace botgarApi\article;

use botgarApi\image\ImageModel;
use Slim\Slim;

class Get {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($articleId) {
      $conf = $this->app->container->get('configuration');
      $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT * FROM article where id=" . $articleId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setBody($sql);
            $this->app->response->setStatus(404);
            return;
        }
        $row = $result->fetch(\PDO::FETCH_ASSOC);
        $output = [
            'id' => $row['id'],
            'title' => $row['title'],
	          'title_en' => $row['title_en'],
            'code' => $row['code'],
            'type' => $row['type'],
            'author' => $row['author'],
            'date_created' => $row['date_created'],
            'date_changed' => $row['date_changed'],
            'image_id' => $row['image_id'],
            'tag_id' => $row['tag_id'],
            'map_marker_id' => $row['map_marker_id'],
            'map_overlay_id' => $row['map_overlay_id'],
            'blocks' => []
        ];

        $sql = "SELECT * FROM article_block where article_id=" . $articleId . ' ORDER BY `order`';
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setBody($sql);
            $this->app->response->setStatus(404);
            return;
        }
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $output['blocks'][] = [
                'id' => $row['id'],
                'content' => $row['content'],
		'content_en' => $row['content_en'],
                'backgroud_color' => $row['backgroud_color']
            ];
        }

        //marker
        $output['markers'] = [];
	       if ($output['map_marker_id'] != null && $output['map_marker_id'] > 0) {
            $sql = "SELECT * FROM map_marker where id=" . $output['map_marker_id'];
            $result = $db->query($sql);
            if ($result) {
              while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                  $output['markers'][] = $row;
              }
            }
        }
        //image
        if ($output['image_id'] != null && $output['image_id'] > 0) {
          $sql = "SELECT * FROM image where id=" . $output['image_id'];
          $result = $db->query($sql);
          if ($result) {
              $imgRow = $result->fetch(\PDO::FETCH_ASSOC);
              if ($imgRow['url'] == null ) {
                $imgRow['url'] = 'http://admin-botgar.jedinecnakava.sk/api/api/images/' . $imgRow['id'];
              }
              $output['image'] = $imgRow;
          }
        }

        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
