angular.module('adminApp').controller('plantSettingsDialogController', ['$scope', '$mdDialog', '$log', 'plant', 'webpService', 'plantSettings', function($scope, $mdDialog, $log, plant, webpService, plantSettings) {
    $scope.settings = plantSettings;
    $scope.plant = plant;
    $scope.selectedImages = [];
    if (plantSettings.selectedImages) {
        for (var i = 0; i<=plantSettings.selectedImages.length;i++) {
            var imgId = plantSettings.selectedImages[i]
            $scope.selectedImages[imgId] = true;
        }
    }

    $scope.webp = webpService.supported() ? '/webp' : '';

    $scope.save = function() {
        if (!$scope.settings.backgroundColor) {
            alert('Barva pozadí nemuŽe zustat prázdná');
            return;
        }
        $mdDialog.hide($scope.settings);
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.onImageSelected = function(image) {
        var imageId = parseInt(image.id);
        $scope.settings.selectedImages = [image.id];
        $scope.selectedImages = {};
        $scope.selectedImages[imageId] = true;
    }

    $scope.imageIsLandscape = function (img) {
        return img.width && img.height && parseInt(img.width) > parseInt(img.height);
    }

    $scope.getImagePreviewWidth = function (img) {
        if (!$scope.imageIsLandscape(img)) {
            return 250;
        }
        return 512;
    }

    $scope.getImageUrl = function (image) {
        return image.url + '/max-width/' + $scope.getImagePreviewWidth(image) + $scope.webp;
    }

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);
