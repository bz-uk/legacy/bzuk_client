'use strict';
const _ = require('lodash');
const debug = require('debug')('userRolePermission');

//const bookshelf = require('./../../database/local').bookshelf
const models = require('./../../database/local').sequelize.models;
const SequelizeInstance = require('sequelize/lib/instance');
const User = require('./../../appModels/user');
const Role = require('./../../appModels/role');
const Permission = require('./../../appModels/permission');
const UserPermissions = require('./../../permissions/userPermisions');

module.exports = class UserRolePermission {
    constructor(context) {
        for (let key of Object.keys(context)) {
            Object.defineProperty(this, key, {
                get: function () {
                    return context[key];
                }
            });
        }
    }

    static fetchFor(user) {
        debug('fetch for ', user.id);
        let context = {};
        if (user instanceof SequelizeInstance) {
            context.user = user;
        } else {
            context.user = models.User.build(user);
        }
        return Role.loadById(context.user.role_id)
        .then((role) => {
            context.role = role;
            return Permission.loadByRoleId(role.id);
        })
        .then((permissions) => {
            context.permissions = permissions;
            return new UserRolePermission(context);
        })
    }


    /**
     *
     * @return {Promise<R>|Promise.<TResult>|Promise<R2|R1>}
     */
    fetch() {
        let context = {};
        if (this.userData instanceof SequelizeInstance) {
            context.user = this.userData;
        } else {
            context.user = models.User.build(this.userData);
        }
        return Role.loadById(context.user.role_id)
        .then((role) => {
            context.role = role;
            return Permission.loadByRoleId(role.id);
        })
        .then((permissions) => {
            context.permissions = permissions;
            return context;
        })
    }
}