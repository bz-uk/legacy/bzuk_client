<?php
namespace botgarApi\authors;

use Slim\Slim;

class DeleteOne {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message, $debug = null) {
      $response = ['status'=>'error', 'message'=> $message];
      if ($debug !== null) {
        $response['debug'] = $debug;
      }
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run($id) {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);
                
        if (!is_numeric($id)) {
            return $this->returnInvalidResponse('invalid id');
        }
        $sql = 'UPDATE authors SET is_deleted=1 WHERE id=' . $id;
        $result = $db->query($sql);
        if (!$result) {
            return $this->returnInvalidResponse('Unable to delete author');
        }        
        $response = ['status'=> 'ok', 'message' => 'Author deleted'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
