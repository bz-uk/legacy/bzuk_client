var authorsService = function ($log, $resource, $rootScope, baseService, $q) {
    var authorsResource = $resource('/api/api/index.php/authors', {}, {
        'getAll': {
            method: 'GET',
            isArray: true
        },
        'create': {
            method: 'POST',
        }
    });
    var authorResource = $resource('/api/api/index.php/authors/:id', {}, {
        'delete': {
            method: 'DELETE',
        },
        'save': {
            method: 'PUT',
        }
    });
    var authors;

    var svc = {
        onAuthorsLoadedError: function (scope, callback) {
            this.registerHandler('authors-laod-error-event', scope, callback)
        },
        onAllAuthorsLoaded: function (scope, callback) {
            this.registerHandler('authors-loaded-event', scope, callback)
        },
        onAuthorCreateError: function (scope, callback) {
            this.registerHandler('author-create-error-event', scope, callback)
        },
        onAuthorCreated: function (scope, callback) {
            this.registerHandler('author-created-event', scope, callback)
        },
        onAuthorSaved: function (scope, callback) {
            this.registerHandler('author-saved-event', scope, callback)
        },
        onAuthorDeleted: function (scope, callback) {
            this.registerHandler('author-deleted-event', scope, callback)
        },
        loadAll: function (forceReload) {
            if (!forceReload && authors !== undefined) {
                $rootScope.$emit('authors-loaded-event', authors);
                return;
            }
            authorsResource.getAll({},
                function (data) {
                    var authorsData = angular.copy(data);
                    delete authorsData.$promise;
                    delete authorsData.resolved;
                    authors = authorsData
                    $rootScope.$emit('authors-loaded-event', authorsData);
                },
                function (err) {
                    $log.error('Unable to load authors: ', err);
                    $rootScope.$emit('authors-laod-error-event', err);
                })
        },
        loadAllAsync: function (forceReload) {
            if (!forceReload && authors !== undefined) {
                return $q(function (resolve, reject) {
                    resolve(authors);
                }).promise;
            }
            return authorsResource.getAll({},
                function (data) {
                    var authorsData = angular.copy(data);
                    delete authorsData.$promise;
                    delete authorsData.resolved;
                    return authorsData;
                },
                function (err) {
                    $log.error('Unable to load authors: ', err);
                }).$promise;
        },
        delete: function (author) {
            return authorResource.delete({id: author.id}, function (data) {
                $rootScope.$emit('author-deleted-event', data);
                $rootScope.$emit('ok-message', 'Autor by smazán');
            }, function (err) {
                alert('Nepodarilo se smazt autora ' + err.message);
            });
        },
        saveAuthor: function (id, author) {
            return authorResource.save({id: author.id}, author, function (data) {
                $rootScope.$emit('author-saved-event', data);
                $rootScope.$emit('ok-message', 'Autor by uložen');
            }, function (err) {
                alert('Nepodarilo se uložit autora ' + err.message);
            });
        },
        create: function (name, note) {
            if (note === undefined) {
                note = '';
            }
            var q = authorsResource.create({}, {name: name, note: note},
                function (data) {
                    var newAuthor = angular.copy(data);
                    if (authors === undefined) {
                        authors = [];
                    }
                    authors.push(newAuthor.author)
                    $rootScope.$emit('author-created-event', newAuthor.author);
                    console.log('reslving', newAuthor.author)
                    return newAuthor.author;
                },
                function (err) {
                    $log.error('Unable to create author: ', err);
                    $rootScope.$emit('author-create-error-event', err);
                })
            console.log('returning promise ', q.$promise)
            return q.$promise;
        }
    }
    return angular.extend({}, baseService, svc)
}
authorsService.$inject = ['$log', '$resource', '$rootScope', 'baseService', '$q'];
angular.module('adminApp').factory('authorsService', authorsService);