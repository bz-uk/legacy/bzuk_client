angular.module('adminApp').factory('categoriesService', ['$log', '$resource', '$rootScope', '$q', function ($log, $resource, $rootScope, $q) {
    var categoriesResource = $resource('/api/api/index.php/plants-categories', {}, {
        'getAll': {method: 'GET', isArray:true}
    });
    var singleCategoryResource = $resource('/api/api/index.php/plants-categories/:categoryId', {}, {
        'delete': {method: 'DELETE'}
    });

    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }

    var allPlantsCategories = []
    return {
        //start: hanlders
        onCategoriesLoaded: function (scope, callback) {
            registerHandler('categories-loaded-event', scope, callback)
        },
        onCategoryDeleted: function (scope, callback) {
            registerHandler('category-deleted-event', scope, callback)
        },
        //end: hanlders
        deleteCategory: function(category) {
            $log.debug('Deleting category', category);
            return singleCategoryResource.delete({categoryId: category.id}, category, function(data) {
                $rootScope.$emit('category-deleted-event', category);
                $rootScope.$emit('ok-message', 'Kategorie smazána');
            });
        },
        getAllCategoriesAsync: function (query, force) {
            if (allPlantsCategories.length > 0 && true !== true) {
                return $q(function(resolve, reject) {
                    resolve(allPlantsCategories);
                }).promise;
            }
            return categoriesResource.getAll({}, {query: query}, function (categories) {
                delete categories.$promise;
                delete categories.$resolved;
                allPlantsCategories = angular.copy(categories)
                $rootScope.$emit('categories-loaded-event', allPlantsCategories);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status === 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to get plant tags', err);
            }).$promise;
        }
    }
}]);
