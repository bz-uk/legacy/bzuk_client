'use strict';
const debug = require('debug')('image-upload');
const sharp = require('sharp');
const process = require('process');
const path = require('path');
const fs = require('fs');
const os = require('os');
const mkdirp = require('mkdirp');
const HttpStatusCodes = require('http-status-codes');

const configuration = require('./../../../common/config');
const knex = require('./../../../database/local').knex;
const BaseRoute = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const UnauthorizedError = require('./../../../error/unauthorized');
const Resizer = require('./../../../common/image/resizer');
const fsPromise = require('./../../../common/fs');
const routeUtil = require('./../../../util/route');


const UPLAOD_TARGET_PATH = 'data/images/';
const ORIGINALS_PATH = 'originals';
const WIDTH = 2048;

module.exports = class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/images-preview/:imageId',
            name: 'fullWidthPreview'
        }
    }

    main(req, res) {
        debug('Params: ', req.params)
        if (!req.params.imageid) {
            res.status(HttpStatusCodes.BAD_REQUEST).json({
                errorCode: 'invalid_value',
                message: 'invalid value',
            });
            return Promise.resolve();
        }

        return Promise.all([fsPromise.fileExists(path.join(process.cwd(), UPLAOD_TARGET_PATH, WIDTH + '', req.params.imageid + '.jpg')),
            fsPromise.fileExists(path.join(process.cwd(), UPLAOD_TARGET_PATH, WIDTH + '', req.params.imageid + '.png')),
            fsPromise.fileExists(path.join(process.cwd(), UPLAOD_TARGET_PATH, WIDTH + '', req.params.imageid + '.jpeg')),])
        .then((exists) => {
            if (!exists[0] && !exists[1]) {
                debug('file does not exists.')
                return sequelize.models.ImageCompat.findOne({where: {id: req.params.imageid}})
                .then((image) => {
                    let extension = path.extname(image.name).replace(/\./, '');
                    debug('resizing...');

                    return Resizer.resize(path.join(process.cwd(), UPLAOD_TARGET_PATH, 'originals', image.name), extension, image.id, [WIDTH], UPLAOD_TARGET_PATH)
                    .then(() => {
                        return {
                            name: req.params.imageid + '.' + extension,
                            extension: extension
                        }
                    })
                })
            } else {
                let extension = ((exists[0] && 'jpg') || (exists[1] && 'png') || (exists[2] && 'jpeg'));
                return {name: req.params.imageid + '.' + extension, extension: extension};
                debug('file already exists.')
            }
        })
        .then((image) => {
            debug('returning image');
            routeUtil.serveFileInline(res, path.join(process.cwd(), UPLAOD_TARGET_PATH, WIDTH + '', image.name), image.extension);
        })
    }
}