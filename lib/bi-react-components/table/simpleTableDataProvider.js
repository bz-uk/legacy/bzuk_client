'use strict';
import BaseDataProvider from './baseDataProvider';
import _ from 'lodash';

import TableDataProvider from './../service/tableDataProvider';

const TABLE_DATA_ROUTE_URL = '/table-data';

export default class SimpleTableDataProvider extends BaseDataProvider {
    constructor(backendEndPointUrl, options) {
        super();
        let defaultOptions      = {};
        this.options            = _.assign(defaultOptions, options);
        this.backendEndPointUrl = backendEndPointUrl;
        if (options && options.service && options.service instanceof TableDataProvider) {
            this.service = options.service;
        } else {
            this.service = new TableDataProvider(backendEndPointUrl.replace(/\/$/, '') + TABLE_DATA_ROUTE_URL, {
                eventBus: this.options.eventBus,
                logger: this.options.logger
            });
        }
    }
    
    destruct() {
        this.removeAllListeners();
    }
    
    requestData(columns, filter, rowsPerPage, page) {
        var self = this;
        this.service.getRowsForTable(columns, filter, rowsPerPage, page)
        .then(function (response) {
            self.emit('data-loaded', response);
        }).catch(function (err) {
            self.emit('data-load-error', err);
        })
    }
}