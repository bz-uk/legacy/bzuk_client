angular.module('adminApp').factory('familiesService', ['$log', '$resource', '$rootScope', '$q', function ($log, $resource, $rootScope, $q) {
    var familiesResource = $resource('/api/api/index.php/plants-families', {}, {
        'getAll': {method: 'GET', isArray: true}
    });
    var familyResource = $resource('/api/api/index.php/plants-families/:familyId', {}, {
        'save': {method: 'PUT'},
        'delete': {method: 'DELETE'}
    });

    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }

    var allPlantsFamilies = [];
    return {
        //start: hanlders
        onFamiliesLoaded: function (scope, callback) {
            registerHandler('families-loaded-event', scope, callback)
        },
        onFamilyDeleted: function (scope, callback) {
            registerHandler('family-deleted-event', scope, callback)
        },
        //end: hanlders
        deleteFamily: function (family) {
            return familyResource.delete({familyId: family.id}, family, function (data) {
                $rootScope.$emit('family-deleted-event', family);
                $rootScope.$emit('ok-message', 'Čeleď smazána');
            });
        },
        getAllFamiliesAsync: function (query, force) {
            if (allPlantsFamilies.length > 0 && force !== true) {
                return $q(function (resolve, reject) {
                    resolve(allPlantsFamilies);
                }).promise;
            }
            return familiesResource.getAll({}, {query: query}, function (families) {
                allPlantsFamilies = angular.copy(families)
                delete allPlantsFamilies.$promise;
                delete allPlantsFamilies.resolved;
                $rootScope.$emit('families-loaded-event', allPlantsFamilies);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status === 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to get plants families', err);
            }).$promise;
        }
    }
}]);
