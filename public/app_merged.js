//adminApp.js
angular.module('adminApp', ['ngMaterial', 'md.data.table', 'ngMdIcons', 'ckeditor', 'ngResource', 'ngRoute', 'ui.sortable', 'uiGmapgoogle-maps', 'ngFileUpload', 'ngSanitize', 'dndLists', 'monospaced.qrcode', 'ngAudio', 'ngCookies']);
//adminController.js
angular.module('adminApp').controller('adminController', ['$scope', '$mdDialog', '$mdMedia', '$location', '$rootScope', '$log', 'notificationsService', 'settingsService',
    function($scope, $mdDialog, $mdMedia, $location, $rootScope, $log, notificationsService, settingsService) {
        $scope.flashMessages = []
        notificationsService.onMessageRemoved($scope, function() {
          $scope.flashMessages = notificationsService.getmessages()
        })
        notificationsService.onMessageAdded($scope, function() {
          $scope.flashMessages = notificationsService.getmessages()
        })
        $scope.settings = {};
        $scope.title = '';
        $scope.subtitle = '';
        $scope.brandingLogoSrc = '';
        settingsService.load(function(data) {
            $scope.title = data.brandingTitle;
            $scope.subtitle = data.brandingSubtitle;
            $scope.brandingLogoSrc = data.brandingLogo;
            console.log('branding logo src', data);
        })

        //start: error events
        $rootScope.$on('circuits-service-error-event', function(event, err) {
            $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('settings-service-error-event', function(event, err) {
            $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('circuit-save-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('circuit-create-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-loaded-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-saved-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-created-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-points-order-save-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-deleted-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('secondary-point-deleted-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('secondary-point-saved-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('plants-loaded-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('secondary-point-added-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('plant-category-save-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('plant-family-save-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('plant-saved-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        //start slides
        $rootScope.$on('primary-point-slides-loaded-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-slide-deleted-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-slide-saved-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('error-message', function(event, message) {
          $scope.onErrorEventSimple(event, message)
        })
        //end slides
        //end: error events
        //start: ok events
        $rootScope.$on('plant-saved-event', function(event, plant, message) {
          $scope.onSuccessEvent(event, message)
        })
        $rootScope.$on('ok-message', function(event, message) {
          $scope.onSuccessEvent(event, message)
        })
        //end: ok events

        $scope.showToolbarMenu = function($mdMenu, ev) {
            originatorEv = ev;
            $mdMenu.open(ev);
        }

        $scope.logout = function() {
            window.location = "/logout";
        }

        $scope.onSuccessEvent = function(event, okMessage) {
          $log.log(okMessage)
          notificationsService.addSuccessMessage(okMessage)
        }

        $scope.onErrorEventSimple = function(event, message) {
            notificationsService.addError(message)
        }

        $scope.onErrorEvent = function(event, error) {
            $log.error(error)
            notificationsService.addError(error.statusText)
        }
    }
]);
angular.module('adminApp').config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyCmL3Ck_qfaDOTsZIHje779InGh7I8-BP0', //TODO: move to cfg file
        //v: '3.20', //defaults to latest 3.X anyhow
        libraries: 'geometry,visualization'
    });
});


//routes.js
angular.module('adminApp').config(function ($routeProvider, $locationProvider, $mdThemingProvider, $mdIconProvider) {
    $mdThemingProvider.theme('default').primaryPalette('blue')
        .accentPalette('red');
    $routeProvider
    // .when('/', {
    //     templateUrl : 'main.template.html',
    //     controller : 'adminController'
    // })
    //start: articles
        .when('/articles', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/articles/:articleId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        //end: articles
        .when('/menus', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/plants/', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/galleries/', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/galleries/:galleryId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/tags/', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/categories/', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/families/', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/authors', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/plants/print/:filterCode', {
            templateUrl: 'plants/printPlantsList.template.html',
            controller: 'printPlantsListController'
        })
        .when('/plants/:plantId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/menus/:menuId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/menus/:menuId/menu-items/:menuItemId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        //start: circuits
        .when('/circuits', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/circuits/:circuitId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/circuits/:circuitId/primary-points/:pointId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        //end: circuits
        .when('/downloads', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .otherwise({redirectTo: '/plants'});

    $locationProvider.html5Mode(true);
});

angular.module('adminApp').factory('notificationsService', ['$resource', '$log', '$rootScope', '$timeout', function($resource, $log, $rootScope, $timeout) {
    var messages = []

    var _putMessage = function(msgObject) {
      messages.push(msgObject)      
      $rootScope.$emit('notifcation-message-added-event')
      $timeout(function() {
          for (var i = 0; i < messages.length; i++) {
              if (messages[i].id == msgObject.id) {
                  messages.splice(i, 1);
                  $rootScope.$emit('notifcation-message-deleted-event')
                  return;
              }
          }
      }, 3000)
    }

    return {
        addError: function(errorMessage) {
					var msgObject = {
	            id: new Date().getTime(),
	            status: 'error',
	            message: errorMessage
	        }
          _putMessage(msgObject)
        },
        addSuccessMessage: function(message) {
          var msgObject = {
	            id: new Date().getTime(),
	            status: 'success',
	            message: message
	        }
          _putMessage(msgObject)
        },
				getmessages: function() {
					return messages
				},
				onMessageRemoved: function(scope, callback) {
					var handler = $rootScope.$on('notifcation-message-deleted-event', callback);
					scope.$on('$destroy', handler);
				},
				onMessageAdded: function(scope, callback) {
					var handler = $rootScope.$on('notifcation-message-added-event', callback);
					scope.$on('$destroy', handler);
				}
    }
}]);

angular.module('adminApp').factory('settingsService', ['$resource', '$log', '$rootScope',function ($resource, $log, $rootScope) {
	var settingsResource = $resource('/api/api/index.php/settings', {}, {
		'get':  {method:'GET'}
	});
  var settings = []

	var registerHandler = function(eventName, scope, callback) {
			var handler = $rootScope.$on(eventName, callback);
			scope.$on('$destroy', handler);
	}

	return {
		getFullCkEditorSettings: function() {
			return { 
            language: 'en',
            format_tags: 'h1;h2;h3;h4',
            allowedContent: true,
            entities: false,       
            width: '100%'
        }
		},
		onError: function(scope, callback) {
				registerHandler('settings-service-error-event', scope, callback);
		},
		onSettingsLoaded: function(scope, callback) {
				registerHandler('settings-loaded-event', scope, callback);
		},
		loadAndEmitEvent: function() {
			if (settings.length > 0) {
				$rootScope.$emit('settings-loaded-event', settings);
        return;
      }
			return settingsResource.get({}, function(data) {
        settings = angular.copy(data)
        delete settings.$prmoise
        delete settings.resolved
				$rootScope.$emit('settings-loaded-event', settings);
			}, function(err) {
				var message = '';
				if (err.status == 500) {
					message = err.data;
				} else {
					message = err.data.message;
				}
				$log.error('Unable to load settings', err);
				$rootScope.$emit('settings-service-error-event', err);
			});
		},
		load: function(successCallback, errCallback) {
      if (settings.length > 0) {
        successCallback(settings)
				$rootScope.$emit('settings-loaded-event', settings);
        return;
      }
			return settingsResource.get({}, function(data) {
        settings = angular.copy(data)
        delete settings.$prmoise
        delete settings.resolved
				successCallback(settings)
				$rootScope.$emit('settings-loaded-event', settings);
			}, function(err) {
				console.log(err);
				var message = '';
				if (err.status == 500) {
					message = err.data;
				} else {
					message = err.data.message;
				}
				$log.error('Unable to load settings', err);
				errCallback(message);
				$rootScope.$emit('settings-service-error-event', err);
			});
		},
	}
}]);

angular.module('adminApp').factory('tableService', ['$resource', '$log',function ($resource, $log) {
	var tablefiltersResource = $resource('/api/api/index.php/table-filters', {}, {
		'post':  {method:'POST'}
	});
	var tablefilterResource = $resource('/api/api/index.php/table-filters/:filterId', {}, {
		'get':  {method:'GET'}
	});
  return {
		save: function(tableName, filter , successCallback, errCallback) {
			return tablefiltersResource.post({}, {tableName: tableName, filter: filter}, function(data) {
				console.log(data)
				successCallback(data.filter_code)
			}, function(err) {
				console.log(err);
				var message = '';
				if (err.status == 500) {
					message = err.data;
				} else {
					message = err.data.message;
				}
				$log.error('Unable to save filter', err);
				errCallback(message);
			});
		},
		load: function(filterId, successCallback, errCallback) {
				return tablefilterResource.get({filterId: filterId}, function(data) {
	        var filter = angular.copy(data)
	        delete filter.$prmoise
	        delete filter.resolved
					successCallback(filter)
				}, function(err) {
					console.log(err);
					var message = '';
					if (err.status == 500) {
						message = err.data;
					} else {
						message = err.data.message;
					}
					$log.error('Unable to load filter', err);
					errCallback(message);
				});
			},
	}
}]);

'use strict';
var authorsListController = function ($log, authorsService, plantsService, $mdDialog, $scope) {
    var ctrl = this;
    ctrl.model = {
        authors: [],
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }

    authorsService.onAuthorDeleted($scope, function(event, data) {
        ctrl.getAuthors(true);
    });

    // authorsService.onAuthorSaved($scope, function(event, data) {
    //     ctrl.getAuthors(true);
    // });

    ctrl.getAuthors = function (force) {
        ctrl.model.promise = authorsService.loadAllAsync(ctrl.query, force)
            .then(function(authors) {
                $log.debug(authors)
                ctrl.model.authors = authors;
            })
        //, function(authors) {$log.debug(authors); ctrl.model.authors = authors;});
    }

    ctrl.deleteAuthor = function (ev, author) {
        var confirm = $mdDialog.confirm()
            .title('Potvrzení')
            .textContent('Autor "' + author.name + '" bude natrvalo odebrán ze všech roslitn a smazán. Pokračovat?')
            .ariaLabel('Potvrzení')
            .targetEvent(ev)
            .ok('Ano')
            .cancel('Zrušit');
        $mdDialog.show(confirm).then(function () {
            authorsService.delete(author);
        }, function () {
            //
        });
    };

    ctrl.editAuthor = function (ev, author) {
        $mdDialog.show({
            controller: 'editAuthorDialogController',
            templateUrl: 'authors/editAuthorDialog.template.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            resolve: {
                author: function () {
                    return angular.copy(author)
                },
            },
            clickOutsideToClose: true,
            fullscreen: true
        })
            .then(function () {
                ctrl.getAuthors(true);
            }, function () {
                console.log('You cancelled the dialog.');
            });
    }

    ctrl.selected = [];
    ctrl.limitOptions = [5, 10, 15];
    ctrl.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    ctrl.getAuthors(true);
}

angular.module('adminApp').component('authorsList', {
    templateUrl: 'authors/authorsList.template.html',
    controller: authorsListController,
    bindings: {}
});
angular.module('adminApp').controller('editAuthorDialogController', ['$scope', '$mdDialog', 'authorsService', 'author', '$log', function ($scope, $mdDialog, authorsService, author, $log) {
    $scope.model = author

    //start: listeners
    authorsService.onAuthorSaved($scope, function (event, author) {
        $scope.hide({relaod: true})
    })
    //end: listeners

    $scope.save = function () {
        if (($scope.model.name === undefined || $scope.model.name.length === 0)) {
            alert('Jméno nemuže být prázdné!')
            return
        }
        authorsService.saveAuthor($scope.model.id, $scope.model);
    }

    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);

'use strict';
var categoriesListController = function ($log, categoriesService, plantsService, $mdDialog, $scope) {
    $log.debug('tags!');
    var ctrl = this;
    ctrl.model = {
        categories: categoriesService.tagsList,
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }

    categoriesService.onCategoryDeleted($scope, function(event, data) {
        ctrl.getCategories(true);
    });

    plantsService.onPlantCategorySaved($scope, function(event, data) {
        ctrl.getCategories(true);
    });

    ctrl.getCategories = function (force) {
        ctrl.model.promise = categoriesService.getAllCategoriesAsync(ctrl.query, force).then(function(categories) {ctrl.model.categories = categories;})
    }

    ctrl.deleteCategory = function (ev, category) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
            .title('Potvrzeni')
            .textContent('Kategorie "' + category.name + '" bude natrvalo odebrána ze všech roslitn a smazána. Pokračovat?')
            .ariaLabel('Potvrzení')
            .targetEvent(ev)
            .ok('Ano')
            .cancel('Zrušit');
        $mdDialog.show(confirm).then(function () {
            categoriesService.deleteCategory(category);
        }, function () {
            //
        });
    };

    ctrl.editCategory = function (ev, category) {
        $mdDialog.show({
            controller: 'editCategoryDialogController',
            templateUrl: 'plants/editCategoryDialog.template.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            resolve: {
                category: function () {
                    return angular.copy(category)
                },
            },
            clickOutsideToClose: true,
            fullscreen: true
        })
            .then(function () {
                $log.debug('category saved!')
                ctrl.getCategories(true);
            }, function () {
                $log.debug('You cancelled the dialog.');
            });
    }

    ctrl.selected = [];
    ctrl.limitOptions = [5, 10, 15];
    ctrl.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    ctrl.getCategories(true);
}

angular.module('adminApp').component('categoriesList', {
    templateUrl: 'categories/categoriesList.template.html',
    controller: categoriesListController,
    bindings: {}
});
'use strict'
angular.module('adminApp').controller('circuitEditController', ['$scope', '$rootScope', '$log', '$mdDialog', '$mdMedia', 'circuitsService', '$timeout', '$routeParams', 'uiGmapIsReady', 'settingsService', 'Upload', '$location', '$q',
    function($scope, $rootScope, $log, $mdDialog, $mdMedia, circuitsService, $timeout, $routeParams, uiGmapIsReady, settingsService, Upload, $location, $q) {
        //start:models
        $scope.circuit = {}

        $scope.settings = []

        $scope.files //for uploads

        $scope.saving = false
        $scope.isNew = $routeParams.circuitId == '_'

        $scope.primaryPointsTable = {
            points: [],
            query: {
                order: 'order',
                limit: 10,
                page: 1
            },
            selected: [],
        }
        $scope.secondaryPointsTable = {
                points: [],
                query: {
                    order: 'id',
                    limit: 10,
                    page: 1
                },
                selected: [],
        }
        $scope.options = { //for mce
            language: 'cz',
            format_tags: 'h1;h2',
            allowedContent: true,
            entities: false,
            removePlugins: 'templates'
            //width: '100%'
        };
        $scope.map = {
            infoWindowTemplate: 'plants/plantEditMapMarkerInfoWindow.template.html',
            markerOptions: {
                draggable: true,
                animation: 'DROP'
            },
            markerEvents: {
                dragend: function(markerModel, eventName, markerObject) {
                  //
                }
            },
            center: {
                latitude: 50.0708767569985,
                longitude: 14.4208672642708
            },
            zoom: 18,
            bounds: {
                northeast: {
                    latitude: 50.0689551446,
                    longitude: 14.4179141521
                },
                southwest: {
                    latitude: 50.0733622808,
                    longitude: 14.4247806072
                }
            },
            events: {
                click: function(mapModel, eventName, originalEventArgs) {
                    $scope.$apply(function() {
                        //console.log('click!')

                        var e = originalEventArgs[0];
                        //$scope.model.markers = []
                        $scope.markersIdCounter++
                        $scope.model.markers.push({
                            latitude: e.latLng.lat(),
                            longitude: e.latLng.lng(),
                            id: $scope.markersIdCounter,
                            title: $scope.model.name
                        })
                        console.log($scope.model.markers)
                    });
                }
            }
        }
        $scope.primaryTableDeferred
        $scope.sortableOptions = {
            stop: function(e, ui) {
                var order = [];
                for (var i = 0; i < $scope.primaryPointsTable.points.length; i++) {
                    order.push($scope.primaryPointsTable.points[i].id)
                }
                console.log(order)
                $scope.primaryTableDeferred = $q.defer();
                $scope.primaryPointsTable.promise = $scope.primaryTableDeferred.promise
                circuitsService.savePrimaryPointsOrder($scope.circuit.id, order, $scope.primaryPointsTable.query)
            },
            helper: "clone",
            forceHelperSize: true,
            forcePlaceholderSize: true
        }

        //end:models
        //start: upload
        $scope.upload = function(file) { //TODO: to service??
          if (file === undefined) {
            return;
          }
            if (!file.$error) {
                var newImg = {
                    id: undefined,
                    url: 'https://bz-uk.cz/sites/default/files/styles/medium/public/images/abutilon_megapotamicum2.jpg',
                    status: 'uploading'
                }
                $scope.circuit.image = newImg
                console.log($scope.circuit)
                Upload.upload({
                    url: '/api/api/index.php/images', //TODO: load from settings
                    data: {
                        file: file
                    }
                }).then(function(resp) {
                        console.log('upload complete')
                        console.log(resp)
                        $timeout(function() {
                            console.log('complete timeout')
                            if (resp.data.id !== undefined && parseInt(resp.data.id) > 0) {
                                console.log('done!')
                                newImg.url = resp.data.url
                                newImg.id = parseInt(resp.data.id)
                                newImg.status = 'done'
                            }
                            $scope.log = 'file: ' +
                                resp.config.data.file.name +
                                ', Response: ' + JSON.stringify(resp.data) +
                                '\n' + $scope.log;
                        });
                    },
                    function(resp) {
                        alert('Unable to uplaod image: ' + resp.data)
                        $scope.circuit.image = undefined
                    },
                    function(evt) {
                        var progressPercentage = parseInt(100.0 *
                            evt.loaded / evt.total);
                        newImg['progress'] = progressPercentage
                    });
            }

        }
        $scope.$watch('files', function() {
            $scope.upload($scope.files);
        })
        $scope.deleteImage = function(image) {
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this image?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                $scope.circuit.image = undefined
            }, function() {});
        }
        //end: upload
        //start: listeners
        settingsService.onSettingsLoaded($scope, function(event, settings){
          $scope.settings = settings
        })
        circuitsService.onAllPrimaryPointsLoaded($scope, function(event, pointsData) {
            $scope.primaryPointsTable.points = pointsData
            console.log($scope.primaryPointsTable.points)
            if ($scope.primaryTableDeferred !== undefined) {
              $scope.primaryTableDeferred.resolve()
            }
            event.stopPropagation()
        })
        circuitsService.onPrimaryPointsOrderSaved($scope, function(event, data) {
          $scope.getPrimaryPoints()
          event.stopPropagation()
        })
        circuitsService.onAllSecondaryPointsLoaded($scope, function(event, pointsData) {
            $scope.secondaryPointsTable.points = pointsData
            event.stopPropagation()
        })
        circuitsService.onCircuitDataLoaded($scope, function(event, circuitData) {
                $scope.circuit = circuitData
                console.log('circuit loaded!')
                event.stopPropagation()
            })
        circuitsService.onCircuitSaved($scope, function(event, circuitData) {
                //$scope.circuit = circuitData
                alert('Curcuit saved');
                event.stopPropagation()
            })
        circuitsService.onPrimaryPointDeleted($scope, function(event, data) {
            $scope.getPrimaryPoints() //??
            event.stopPropagation()
        })
        circuitsService.onCircuitCreated($scope, function(event, circuitData) {
          // $scope.circuit = circuitData.id
          // $scope.isNew = false
          alert('Curcuit created');
          $location.url('circuits/')
          event.stopPropagation()
        })
        //end: listeners
        //start: map logic
        $scope.getNormalizedCoord = function(coord, zoom) {
            var y = coord.y;
            var x = coord.x;

            // tile range in one direction range is dependent on zoom level
            // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
            var tileRange = 1 << zoom;

            // don't repeat across y-axis (vertically)
            if (y < 0 || y >= tileRange) {
                return null;
            }

            // repeat across x-axis
            if (x < 0 || x >= tileRange) {
                x = (x % tileRange + tileRange) % tileRange;
            }

            return {
                x: x,
                y: y
            };
        }
        $scope.initMap = function() {
            $scope.moonMapType = new google.maps.ImageMapType({
                getTileUrl: function(coord, zoom) {
                    // console.log(getProjection())
                    var normalizedCoord = $scope.getNormalizedCoord(coord, zoom);
                    if (!normalizedCoord) {
                        return null;
                    }

                    var bound = Math.pow(2, zoom);
                    var url = '//' + $scope.settings.baseUrl + 'api/map2/' + zoom + '/' + normalizedCoord.x + '/' + normalizedCoord.y + '.png'
                    return url;
                },
                tileSize: new google.maps.Size(256, 256),
                maxZoom: 18,
                minZoom: 17,
                radius: 1738000,
                name: 'Moon'
            });
        }
        uiGmapIsReady.promise(1).then(function(instances) {
            $log.log('map is ready')
            instances.forEach(function(inst) {
                var map = inst.map
                var uuid = map.uiGmap_id
                var mapInstanceNumber = inst.instance // Starts at 1.
                $scope.initMap()
                map.overlayMapTypes.push($scope.moonMapType);

                console.log('points: ' + $scope.circuit.markers.length)
                var markers = []
                var linePoints = []
                var lineSymbol = {
                  path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
                };
                var icons = []
                var offset = 0;
                for (var i = 0; i < $scope.circuit.markers.length; i++) {
                    var div = document.createElement('DIV');
                    //var window_id = i
                    var title = ''
                    if (i == 0) {
                      title = ': start'
                    } else if (i == $scope.circuit.markers.length - 1) {
                      title = ': end'
                    }
                    div.innerHTML = '<div class="numbered-marker-bubble"><div class="number">' + $scope.circuit.markers[i].id + title + '</div><div class="bottom-left-corner"></div></div>';
                    markers[i] = new RichMarker({
                        map: map,
                        position: new google.maps.LatLng($scope.circuit.markers[i].latitude, $scope.circuit.markers[i].longitude),
                        draggable: true,
                        flat: true,
                        anchor: RichMarkerPosition.BOTTOM_LEFT,
                        content: div,
                        //window_id: window_id
                    });
                    linePoints.push(new google.maps.LatLng($scope.circuit.markers[i].latitude, $scope.circuit.markers[i].longitude))
                    offset +=  100 / $scope.circuit.markers.length;
                    icons.push({
                      offset: offset + '%',
                      icon: lineSymbol,
                    })
                    // var contentString = '<div id="content">#' + $scope.markers[i].plant_id
                    // if ($scope.markers[i].title !== undefined) {
                    //   contentString += ': ' + $scope.markers[i].title
                    //   if ($scope.markers[i].title_lat !== undefined) {
                    //     contentString += '<br/>'
                    //   }
                    // }
                    // if ($scope.markers[i].title_lat !== undefined) {
                    //   contentString += '<i>'+$scope.markers[i].title_lat+'</i>'
                    // }
                    // infoWindows[i] = new google.maps.InfoWindow({
                    //     content: contentString
                    // });
                    // google.maps.event.addListener(markers[i], 'click', function(e) {
                    //   console.log(this)
                    //   infoWindows[this.window_id].open(map, markers[this.window_id])
                    // })
                }
                var flightPath = new google.maps.Polyline({
                  path: linePoints,
                  geodesic: true,
                  strokeColor: '#FF0000',
                  strokeOpacity: 0.7,
                  strokeWeight: 5,
                  icons: icons,
                });
                console.log(icons)
                flightPath.setMap(map);
            })
        })

        //end: map logic
        //start:helpers
        $scope.openCircuits = function() {
            //TODO: save check?!?
            $location.url('/circuits')
        }
        $scope.deletePrimaryPoint = function(point) {
          var confirm = $mdDialog.confirm()
              .title('Delete')
              .textContent('Do you really want to delete this primary point?')
              .ariaLabel('Delete')
              .ok('Yes')
              .cancel('Cancel');

          $mdDialog.show(confirm).then(function() {
              circuitsService.deletePrimaryPoint($scope.circuit.id,point)
          }, function() {});
        }
        $scope.save = function() {
          if ($scope.isNew) {
            circuitsService.createAndSendEvent($scope.circuit)
          } else {
            circuitsService.saveAndSendEvent($scope.circuit)
          }
        }
        $scope.getSaveLabel = function() {
            if ($scope.isNew) {
                return $scope.saving ? 'Adding...' : 'Add'
            } else {
                return $scope.saving ? 'Saving...' : 'Save'
            }
        }
        $scope.getPrimaryPoints = function() {
            $scope.primaryPointsTable.promise = circuitsService.loadAllPrimaryPoints($routeParams.circuitId, $scope.primaryPointsTable.query).$promise;
        }
        $scope.addPrimaryPoint = function() {
          console.log('add')
          $location.url('/circuits/' + $routeParams.circuitId + '/primary-points/_')
        }
        $scope.editPrimaryPoint = function(point) {
          console.log('epp!');
          $location.url('/circuits/' + $routeParams.circuitId + '/primary-points/' + point.id)
        }
        //end: helpers
        //starts:load data
        if (!$scope.isNew) {
            circuitsService.loadOne($routeParams.circuitId)
            $scope.getPrimaryPoints()
        }
        settingsService.loadAndEmitEvent()
        //end:load data
    }
]);

'use strict'
angular.module('adminApp').directive('circuitEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'circuit/circuitEdit.template.html',
		controller: 'circuitEditController'
	};
});

angular.module('adminApp').factory('circuitsService', ['$log', '$resource', '$rootScope',
    function ($log, $resource, $rootScope) {
        var circuitsResource = $resource('/api/api/index.php/circuits', {}, {
            'get': {
                method: 'GET',
                isArray: true
            },
            'save': {
                method: 'PUT'
            },
            'create': {
                method: 'POST'
            }
        });
        var primaryPointsResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points', {}, {
            'get': {
                method: 'GET',
                isArray: true
            },
            'create': {
                method: 'POST'
            }
        });
        var primaryPointsOrderResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/order', {}, {
            'save': {
                method: 'POST'
            },
        });
        var primaryPointResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/:pointId', {}, {
            'getOne': {
                method: 'GET'
            },
            'save': {
                method: 'PUT'
            },
            'delete': {
                method: 'DELETE'
            }
        });
        var primaryPointSlidesResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/:pointId/slides', {}, {
            'get': {
                method: 'GET', isArray: true
            },
            'add': {
                method: 'POST'
            }
        });
        var primaryPointSlideResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/:pointId/slides/:slideId', {}, {
            'save': {
                method: 'PUT'
            },
            'delete': {
                method: 'DELETE'
            }
        });
        var secondaryPointsResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/:primaryPointId/secondary-points', {}, {
            'get': {
                method: 'GET',
            },
            'add': {
                method: 'POST'
            }
        });
        var secondaryPointResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/:primaryPointId/secondary-points/:secondaryPointId', {}, {
            'delete': {
                method: 'DELETE',
            },
            'save': {
                method: 'PUT'
            }
        });
        var circuitResource = $resource('/api/api/index.php/circuits/:circuitId', {}, {
            'get': {
                method: 'GET'
            },
            'save': {
                method: 'PUT'
            }
        });
        var registerHandler = function (eventName, scope, callback) {
            var handler = $rootScope.$on(eventName, callback);
            scope.$on('$destroy', handler);
        }
        return {
            //subscribs
            onError: function (scope, callback) {
                registerHandler('circuits-service-error-event', scope, callback)
            },
            onAllCircuitsLoaded: function (scope, callback) {
                registerHandler('all-circuits-loaded-event', scope, callback)
            },
            onAllPrimaryPointsLoaded: function (scope, callback) {
                registerHandler('all-circuit-primary-points-loaded-event', scope, callback)
            },
            onAllSecondaryPointsLoaded: function (scope, callback) {
                registerHandler('all-circuit-secondary-points-loaded-event', scope, callback)
            },
            onCircuitDataLoaded: function (scope, callback) {
                registerHandler('circuit-data-loaded-event', scope, callback)
            },
            onCircuitSaved: function (scope, callback) {
                registerHandler('circuit-saved-event', scope, callback)
            },
            onCircuitCreated: function (scope, callback) {
                registerHandler('circuit-created-event', scope, callback)
            },
            onPrimaryPointLoaded: function (scope, callback) {
                registerHandler('primary-point-loaded-event', scope, callback)
            },
            onPrimaryPointSaved: function (scope, callback) {
                registerHandler('primary-point-saved-event', scope, callback)
            },
            onPrimaryPointCreated: function (scope, callback) {
                registerHandler('primary-point-created-event', scope, callback)
            },
            onPrimaryPointsOrderSaved: function (scope, callback) {
                registerHandler('primary-points-order-saved-event', scope, callback)
            },
            onPrimaryPointDeleted: function (scope, callback) {
                registerHandler('primary-point-deleted-event', scope, callback)
            },
            onSecondaryPointDeleted: function (scope, callback) {
                registerHandler('secondary-point-deleted-event', scope, callback)
            },
            onSecondaryPointSaved: function (scope, callback) {
                registerHandler('secondary-point-saved-event', scope, callback)
            },
            onSecondaryPointAdded: function (scope, callback) {
                registerHandler('secondary-point-added-event', scope, callback)
            },
            onPrimaryPointSlidesLoaded: function (scope, callback) {
                registerHandler('primary-point-slides-loaded-event', scope, callback)
            },
            onPrimaryPointSlideDeleted: function (scope, callback) {
                registerHandler('primary-point-slide-deleted-event', scope, callback)
            },
            // onPrimaryPointSlideEdited: function(scope, callback) {
            // registerHandler('primary-point-slide-saved-event', scope, callback)
            // },
            // onSecondaryPointDeleted: function(scope, callback) {
            // registerHandler('primary-point-slide-deleted-event', scope, callback)
            // },
            onPrimaryPointSlideEdited: function (scope, callback) {
                registerHandler('primary-point-slide-saved-event', scope, callback)
            },
            //
            loadOne: function (circuitId) {
                return circuitResource.get({
                    circuitId: circuitId
                },
                    function (data) {
                        var circuitData = angular.copy(data);
                        delete circuitData.$promise;
                        delete circuitData.resolved;
                        $rootScope.$emit('circuit-data-loaded-event', circuitData);
                    },
                    function (err) {
                        $log.error('Unable to load primary points of circuit: ', err);
                        $rootScope.$emit('circuits-service-error-event', err);
                    })
            },
            loadAllPrimaryPoints: function (circuitId, query) {
                //TODO: implement query
                return primaryPointsResource.get({
                    circuitId: circuitId
                }, function (points) {
                    var pointsList = angular.copy(points);
                    delete pointsList.$promise;
                    delete pointsList.resolved;
                    //return successCallback(circuitsList);
                    $rootScope.$emit('all-circuit-primary-points-loaded-event', pointsList);
                }, function (err) {
                    $log.error('Unable to load primary points of circuit: ', err);
                    $rootScope.$emit('circuits-service-error-event', err);
                })
            },
            loadAllSecondaryPoints: function (circuitId, primaryPointId, query) {
                //TODO: implement query
                return secondaryPointsResource.get({
                    circuitId: circuitId,
                    primaryPointId: primaryPointId
                }, function (points) {
                    var pointsList = angular.copy(points);
                    delete pointsList.$promise;
                    delete pointsList.resolved;
                    //return successCallback(circuitsList);
                    $rootScope.$emit('all-circuit-secondary-points-loaded-event', pointsList);
                }, function (err) {
                    $log.error('Unable to load primary points of circuit: ', err);
                    $rootScope.$emit('circuits-service-error-event', err);
                })
            },
            loadAllCircuits: function (query) {
                //TODO: implement query
                return circuitsResource.get({}, function (circuits) {
                    var circuitsList = angular.copy(circuits);
                    delete circuitsList.$promise;
                    delete circuitsList.resolved;
                    //return successCallback(circuitsList);
                    $rootScope.$emit('all-circuits-loaded-event', circuitsList);
                }, function (err) {
                    $log.error('Unable to load circuits: ', err);
                    $rootScope.$emit('circuits-service-error-event', err);
                })
            },
            saveAndSendEvent: function (circuit) {
                return circuitResource.save({
                    circuitId: circuit.id
                }, circuit, function (data) {
                    $rootScope.$emit('circuit-saved-event', data);
                }, function (err) {
                    console.log(err);
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to save circuit: ', err);
                    $rootScope.$emit('circuit-save-error-event', err);
                });
            },
            save: function (circuit, successCallback, errCallback) {
                return circuitResource.save({
                    circuitId: circuit.id
                }, circuit, function (data) {
                    successCallback(data);
                }, function (err) {
                    console.log(err);
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to save circuit', err);
                    errCallback(message);
                });
            },
            createAndSendEvent: function (circuit) {
                return circuitsResource.create({}, circuit, function (data) {
                    $rootScope.$emit('circuit-created-event', data);
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to create circuit: ', err);
                    $rootScope.$emit('circuit-create-error-event', err);
                })
            },
            loadOnePrimaryPoint: function (circuitId, pointId) {
                return primaryPointResource.get({
                    circuitId: circuitId,
                    pointId: pointId
                }, {}, function (pointData) {
                    $rootScope.$emit('primary-point-loaded-event', pointData);
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to load primary point: ', err);
                    $rootScope.$emit('primary-point-loaded-error-event', err);
                })
            },
            createPrimaryPoint: function (circuitId, point) {
                return primaryPointsResource.create({
                    circuitId: circuitId
                }, point, function (data) {
                    $rootScope.$emit('primary-point-created-event', data);
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to create primary point: ', err);
                    $rootScope.$emit('primary-point-created-error-event', err);
                })
            },
            savePrimaryPoint: function (circuitId, point) {
                return primaryPointResource.save({
                    circuitId: circuitId,
                    pointId: point.id
                }, point, function (data) {
                    $rootScope.$emit('primary-point-saved-event', data);
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to create primary point: ', err);
                    $rootScope.$emit('primary-point-saved-error-event', err);
                })
            },
            savePrimaryPointsOrder: function (circuitId, newOrder, tableQuery) {
                var data = {
                    order: newOrder,
                    fromIndex: (tableQuery.page - 1) * tableQuery.limit
                }
                return primaryPointsOrderResource.save({
                    circuitId: circuitId
                }, data, function (data) {
                    $rootScope.$emit('primary-points-order-saved-event', data);
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to create primary point: ', err);
                    $rootScope.$emit('primary-points-order-save-error-event', err);
                })
            },
            removeSecondaryPoint: function (circuitId, primaryPointId, secondaryPointId) {
                return secondaryPointResource.delete({
                    circuitId: circuitId,
                    primaryPointId: primaryPointId,
                    secondaryPointId: secondaryPointId
                }, function (data) {
                    $rootScope.$emit('secondary-point-deleted-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to delete secondary point: ', err)
                    $rootScope.$emit('secondary-point-deleted-error-event', err)
                })
            },
            addSecondaryPoints: function (circuitId, primaryPointId, points) {
                return secondaryPointsResource.add({
                    circuitId: circuitId,
                    primaryPointId: primaryPointId
                }, points, function (data) {
                    $rootScope.$emit('secondary-point-added-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to add secondary points: ', err)
                    $rootScope.$emit('secondary-point-added-error-event', err)
                })
            },
            updateSecondaryPoint: function (circuitId, primaryPointId, secondaryPointId, newData) {
                return secondaryPointResource.save({
                    circuitId: circuitId,
                    primaryPointId: primaryPointId,
                    secondaryPointId: secondaryPointId
                }, newData, function (data) {
                    $rootScope.$emit('secondary-point-saved-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to save secondary point: ', err)
                    $rootScope.$emit('secondary-point-saved-error-event', err)
                })
            },
            deletePrimaryPoint: function (circuitId, point) {
                return primaryPointResource.delete({
                    circuitId: circuitId,
                    pointId: point.id
                },
                    function (data) {
                        $rootScope.$emit('primary-point-deleted-event', data)
                    },
                    function (err) {
                        var message = '';
                        if (err.status == 500) {
                            message = err.data
                        } else {
                            message = err.data.message
                        }
                        $log.error('Unable to delete primary point: ', err)
                        $rootScope.$emit('primary-point-deleted-error-event', err)
                    })
            },
            //start: primary point slides
            loadPrimaryPointSlides: function (circuitId, pointId, query) {
                return primaryPointSlidesResource.get({
                    circuitId: circuitId,
                    pointId: pointId
                }, query, function (data) {
                    $rootScope.$emit('primary-point-slides-loaded-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to load primary point slides: ', err)
                    $rootScope.$emit('primary-point-slides-loaded-error-event', err)
                })
            },
            deletePrimaryPointSlide: function (circuitId, pointId, slideId) {
                return primaryPointSlideResource.delete({
                    circuitId: circuitId,
                    pointId: pointId,
                    slideId: slideId
                }, function (data) {
                    $rootScope.$emit('primary-point-slide-deleted-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to delete slide: ', err)
                    $rootScope.$emit('primary-point-slide-deleted-error-event', err)
                })
            },
            updatePrimaryPointSlide: function (circuitId, pointId, slideId, slideData) {
                return primaryPointSlideResource.save({
                    circuitId: circuitId,
                    pointId: pointId,
                    slideId: slideId
                }, slideData, function (data) {
                    $rootScope.$emit('primary-point-slide-saved-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to save slide: ', err)
                    $rootScope.$emit('primary-point-slide-saved-error-event', err)
                })
            },
            //end: primary point slides
        }
    }
]);

'use strict';
angular.module('adminApp').controller('circuitsListController', ['$scope', '$mdDialog', '$mdMedia', 'circuitsService', '$timeout', '$location', '$log',
    function($scope, $mdDialog, $mdMedia, circuitsService, $timeout, $location, $log) {
        //start models
        $scope.model = {
            circuits: circuitsService.circuitsList,
            query: {
                order: 'title',
                limit: 10,
                page: 1
            },
            selected: [],
            flashMessages: []
        }
        $scope.selected = [];
        $scope.limitOptions = [5, 10, 15];
        $scope.options = {
          rowSelection: false,
          multiSelect: false,
          autoSelect: false,
          decapitate: false,
          largeEditDialog: true,
          boundaryLinks: false,
          limitSelect: true,
          pageSelect: true
        };
        //end: models

        $scope.editCircuit = function(circuit, ev) {
					$location.url('circuits/' + circuit.id)
        };

        $scope.addCircuit = function() {
          $location.url('circuits/_')
        }

        //start: listeners
				circuitsService.onError($scope, function(event, err) {
					console.log('cntrl err - do nothing')
				})
				circuitsService.onAllCircuitsLoaded($scope, function(event, allcircuits) {
					$scope.model.circuits = allcircuits;
				})
        //end listeners
        // function success(articles) {
        //     $scope.model.circuits = articles;
        // }
        $scope.getCircuits = function() {
            $scope.model.promise = circuitsService.loadAllCircuits($scope.query).$promise;
        }
				$scope.getCircuits()

    }
]);

'use strict';
angular.module('adminApp').directive('circuitsList', function() {
	return {
		restrict : 'E',
		templateUrl : 'circuit/circuitsList.template.html',
		controller: 'circuitsListController'
	};
});
'use strict';
angular.module('adminApp').directive('downloadContent', function() {
    return {
        restrict : 'E',
        templateUrl : "content/downloadContent.template.html"
    };
});
'use strict';
angular.module('adminApp').directive('mainContent', ['$routeParams', function($routeParams) {
    return {
        restrict : 'E',
        templateUrl : "content/mainContent.template.html",
        controller: ['$scope', '$location', function($scope, $location) {
            $scope.model = {
                location: $location.path()
            }
            $scope.isMenuEdit = function(location) {
                return location == '/menus/' + $routeParams.menuId
            }
            $scope.isArticleEdit = function(location) {
                return location == '/articles/' + $routeParams.articleId
            }
            $scope.isCircuitEdit = function(location) {
                return location == '/circuits/' + $routeParams.circuitId
            }
            $scope.isCircuitPrimaryPointEdit = function(location) {
                return location == '/circuits/' + $routeParams.circuitId + '/primary-points/' + $routeParams.pointId
            }
            $scope.isMenuItemEdit = function(location) {
              return location == '/menus/' + $routeParams.menuId + "/menu-items/" + $routeParams.menuItemId
            }
            $scope.isPlantEdit = function(location) {
                return location == '/plants/' + $routeParams.plantId
            }
            $scope.isGalleryEdit = function(location) {
                return location == '/galleries/' + $routeParams.galleryId
            }
        }],
        link: function() {
            //console.log('LINKING!');
        }
    };
}]);

'use strict';
var familiesListController = function ($log, familiesService, $mdDialog, $scope) {
    var ctrl = this;
    ctrl.model = {
        families: familiesService.allPlantsFamilies,
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }

    ctrl.getFamilies = function (force) {
        ctrl.model.promise = familiesService.getAllFamiliesAsync(ctrl.query, force).then(function(families) {ctrl.model.families = families;})
    }

    ctrl.deleteFamily = function (ev, family) {
        var confirm = $mdDialog.confirm()
            .title('Potvrzeni')
            .textContent('Čeleď "' + family.name + '" bude natrvalo odebrána ze všech roslitn a smazána. Pokračovat?')
            .ariaLabel('Potvrzení')
            .targetEvent(ev)
            .ok('Ano')
            .cancel('Zrušit');
        $mdDialog.show(confirm).then(function () {
            familiesService.deleteFamily(family);
        }, function () {
            //
        });
    }

    ctrl.editFamily = function (ev, family) {
        $mdDialog.show({
            controller: 'editFamilyDialogController',
            templateUrl: 'plants/editFamilyDialog.template.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            resolve: {
                family: function () {
                    return angular.copy(family)
                },
            },
            clickOutsideToClose: true,
            fullscreen: true
        })
            .then(function () {
                $log.debug('family saved!')
                ctrl.getFamilies(true);
            }, function () {
                $log.debug('You cancelled the dialog.');
            });
    }

    ctrl.selected = [];
    ctrl.limitOptions = [5, 10, 15];
    ctrl.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    ctrl.getFamilies(true);
}

angular.module('adminApp').component('familiesList', {
    templateUrl: 'families/familiesList.template.html',
    controller: familiesListController,
    bindings: {}
});
'use strict';
angular.module('adminApp').controller('articleEditController', ['$location', '$scope', '$mdDialog', '$mdMedia', 'articlesService', '$timeout', '$routeParams', 'settingsService', 'Upload',
    function ($location, $scope, $mdDialog, $mdMedia, articlesService, $timeout, $routeParams, settingsService, Upload) {
        //start: models
        $scope.saving = false
        $scope.model = {
            id: $routeParams.articleId
        }
        $scope.flashMessages = []
        $scope.settings = []
        $scope.isNew = $routeParams.articleId == '_';
        //end: models

        $scope.save = function () {
            $scope.saving = true
            if ($scope.isNew) {
                articlesService.addAndEmitEvent($scope.model);
            } else {
                articlesService.saveAndEmitEvent($scope.model);
            }
        }
        $scope.getSaveLabel = function () {
            if ($scope.isNew) {
                return $scope.saving ? 'Vyvářím...' : 'Vytvořit'
            } else {
                return $scope.saving ? 'Ukádám...' : 'Uložit'
            }
        }
        $scope.openArticles = function () {
            $location.url('/articles')
        }
        $scope.cancel = function () {
            $scope.openArticles()
        }
        //start: upload
        $scope.upload = function (file) { //TODO: to service??
            if (file === undefined) {
                return;
            }
            if (!file.$error) {
                var newImg = {
                    id: undefined,
                    url: 'https://bz-uk.cz/sites/default/files/styles/medium/public/images/abutilon_megapotamicum2.jpg',
                    status: 'uploading'
                }
                $scope.model.image = newImg
                Upload.upload({
                    url: '/api/api/index.php/images', //TODO: load from settings
                    data: {
                        file: file
                    }
                }).then(function (resp) {
                        console.log('upload complete')
                        console.log(resp)
                        $timeout(function () {
                            console.log('complete timeout')
                            if (resp.data.id !== undefined && parseInt(resp.data.id) > 0) {
                                console.log('done!')
                                newImg.url = resp.data.url
                                newImg.id = parseInt(resp.data.id)
                                newImg.status = 'done'
                            }
                            $scope.log = 'file: ' +
                                resp.config.data.file.name +
                                ', Response: ' + JSON.stringify(resp.data) +
                                '\n' + $scope.log;
                        });
                    },
                    function (resp) {
                        alert('Unable to uplaod image: ' + resp.data)
                        $scope.model.image = undefined
                    },
                    function (evt) {
                        var progressPercentage = parseInt(100.0 *
                            evt.loaded / evt.total);
                        newImg['progress'] = progressPercentage
                    });
            }

        }
        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        })
        $scope.deleteImage = function (image) {
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this image?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function () {
                $scope.model.image = undefined
            }, function () {
            });
        }
        //end: upload

        $scope.options = {
            language: 'en',
            format_tags: 'h1;h2',
            allowedContent: true,
            entities: false,
            removePlugins: 'templates'
            //width: '100%'
        };

        //start: listeners
        articlesService.onArticleSaved($scope, function (event) {
            $scope.saving = false;
            $location.url('/articles/');
        });
        articlesService.onArticleCreated($scope, function (event) {
            $scope.saving = false;
            $location.url('/articles/');
        });
        articlesService.onArticleCreateError($scope, function (event) {
            $scope.saving = false;
        });
        articlesService.onArticleSaveError($scope, function (event) {
            $scope.saving = false;
        });
        settingsService.onSettingsLoaded($scope, function (event, settings) {
            $scope.settings = settings
        })
        //end: listeners
        if (!$scope.isNew) {
            articlesService.loadOne($scope.model.id, function (articleModel) {
                $scope.model = articleModel;
            });
        }
        settingsService.loadAndEmitEvent()
    }]);

'use strict'
angular.module('adminApp').directive('articleEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'article/articleEdit.template.html',
		controller: 'articleEditController'
	};
});

angular.module('adminApp').controller('articleEditorDailogController', ['$scope', '$mdDialog', 'articleModel', 'articlesService', function($scope, $mdDialog, articleModel, articlesService) {
	console.log(articleModel);
	
	$scope.model = articleModel;
	
    $scope.hide = function() {
      $mdDialog.hide();
    };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.save = function() {
	articlesService.save($scope.model, function() {
		$mdDialog.hide();
	},function(errMessage) {
		alert('There was some problem during article save: ' + errMessage);
	});
    
  };
  $scope.options = {
		    language: 'en',
	  		format_tags: 'h1;h2',
		    allowedContent: true,
		    entities: false,
		    width: 500
		  };
}]);
angular.module('adminApp').factory('articlesService', ['$log', '$resource', '$rootScope', function ($log, $resource, $rootScope) {
    var articlesResource = $resource('/api/api/index.php/articles', {}, {
        'get': {method: 'GET', isArray: true},
        'save': {method: 'PUT'},
        'create': {method: 'POST'}
    });
    var articleResource = $resource('/api/api/index.php/articles/:articleId', {}, {
        'save': {method: 'PUT'},
        'get': {method: 'GET'},
        'delete': {method: 'DELETE'}
    });
    var publishArticlesResource = $resource('/api/api/index.php/articles/publish', {}, {
        'publish': {method: 'POST'},
    });
    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }
    return {
        onArticleDeleted: function (scope, callback) {
            registerHandler('article-deleted', scope, callback)
        },
        onArticleDeleteError: function (scope, callback) {
            registerHandler('article-delete-error', scope, callback)
        },
        onArticleCreated: function (scope, callback) {
            registerHandler('article-created', scope, callback)
        },
        onArticleCreateError: function (scope, callback) {
            registerHandler('article-create-error', scope, callback)
        },
        onArticleSaved: function (scope, callback) {
            registerHandler('article-saved', scope, callback)
        },
        onArticleSaveError: function (scope, callback) {
            registerHandler('article-save-error', scope, callback)
        },
        publishOneArticle: function (articleId, published) {
            var data = {};
            data[articleId] = published;
            return publishArticlesResource.publish({}, {ids: data}, function (data) {
                $rootScope.$emit('article-published', articleId, published);
                if (published) {
                    $rootScope.$emit('ok-message', 'Článek by publikován');
                }else{
                    $rootScope.$emit('ok-message', 'Článek by deaktivován');
                }
            }, function (err) {
                $rootScope.$emit('error-message', 'Publikování článku zlyhalo');
                $log.error(err);
                alert(err);
            })
        },
        deleteArticleAndEmitEvent: function (article) {
            return articleResource.delete({articleId: article.id}, function (data) {
                $rootScope.$emit('article-deleted', article);
                $rootScope.$emit('ok-message', 'Článek by smazán');
            }, function (err) {
                $rootScope.$emit('article-delete-error', err);
                $rootScope.$emit('error-message', 'Smazání článku zlyhalo');
                alert(err.message);
            });
        },
        load: function (query, successCallback) {
            //TODO: implement query
            return articlesResource.get({}, function (articles) {
                var articlesList = angular.copy(articles);
                delete articlesList.$promise;
                delete articlesList.resolved;
                console.log('okay got ' + articlesList.length + ' items');
                return successCallback(articlesList);
            }, function (err) {
                $log.error('Unable to load articles: ', err);
            })
        },
        saveAndEmitEvent: function(article) {
            return articleResource.save({articleId: article.id}, article, function (data) {
                $rootScope.$emit('article-saved', data);
                $rootScope.$emit('ok-message', 'Článek uložen');
            }, function(err) {
                $rootScope.$emit('article-save-error', err);
                $rootScope.$emit('error-message', 'Článek neuložen: ' + err.message);
            })
        },
        addAndEmitEvent: function(article) {
            return articlesResource.create({}, article, function (data) {
                $rootScope.$emit('article-created', data);
                $rootScope.$emit('ok-message', 'Článek vytvořen');
            }, function(err) {
                $rootScope.$emit('article-create-error', err);
                $rootScope.$emit('error-message', 'Článek nebyl vytvořen: ' + err.message);
            })
        },
        /** @deprecated */
        save: function (article, successCallback, errCallback) {
            return articleResource.save({articleId: article.id}, article, function (data) {
//				delete data.$promise;
//				delete data.resolved;
                successCallback(data);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status == 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to save article', err);
                errCallback(message);
            });
        },
        loadOne: function (articleId, successCallback, errCallback) {
            return articleResource.get({articleId: articleId}, function (data) {
                delete data.$promise;
                delete data.resolved;
                successCallback(data);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status == 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to load article', err);
                errCallback(message);
            });
        }
    }
}]);

'use strict';
angular.module('adminApp').controller('articlesList', ['$scope', '$location', '$mdDialog', '$mdMedia', 'articlesService', '$timeout', function ($scope, $location, $mdDialog, $mdMedia, articlesService, $timeout) {
    $scope.model = {
        articles: articlesService.articlesList,
        query: {
            order: 'title',
            limit: 10,
            page: 1
        },
        selected: [],
        flashMessages: []
    }
    $scope.selected = [];
    $scope.limitOptions = [5, 10, 15];
    $scope.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    articlesService.onArticleDeleted($scope, function (event, article) {
        $scope.getArticles();
    });

    $scope.addArticle = function () {
        $location.url('/articles/_')
    }
    $scope.editArticle = function (article) {
        $location.url('articles/' + article.id)
    };
    $scope.deleteArticle = function (article, ev) {
        var confirm = $mdDialog.confirm()
            .title('Delete')
            .textContent('Do you really want to delete this article?')
            .ariaLabel('Delete')
            .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            articlesService.deleteArticleAndEmitEvent(article);
        }, function () {
            //on cancel
        });
    }
    $scope.publishArticle = function (article) {
        //article.published = !article.published
        articlesService.publishOneArticle(article.id, article.published);
    }

    $scope.getArticles = function () {
        console.log('getting...');
        $scope.model.promise = articlesService.load($scope.query, function (articles) {
            $scope.model.articles = articles
        }).$promise;
    }
    $scope.getArticles();
}]);

'use strict';
angular.module('adminApp').directive('articlesList', function() {
	return {
		restrict : 'E',
		templateUrl : "article/articlesList.template.html" 
	};
});
'use strict';
var editGalleryComponent = function ($log, $location, galleriesService, tagsService, categoriesService, $mdDialog, $scope, $routeParams, webpService, leftMenuService, $timeout) {
    var ctrl = this;
    ctrl.gallery = { //init/new value
        active: false,
        plants: {}
    };
    ctrl.tags = {};
    ctrl.categories = [];
    ctrl.plants = [];
    ctrl.stickyPlants = {};
    ctrl.previewPlants = {};
    ctrl.isNew = $routeParams.galleryId == '_';
    ctrl.modelChanged = false;
    ctrl.saving = false;
    ctrl.loading = false;

    ctrl.webp = webpService.supported() ? '/webp' : '';

    $scope.$on('$destroy', $scope.$on('$locationChangeStart', function (event, next, current) {
        if (ctrl.modelChanged === true) {
            var confirm = $mdDialog.confirm()
                .title('Opusti editaci/vytvoření galerie?')
                .textContent('Galerie obsahuje neuložené změny. Odejít?')
                .ariaLabel('Opusti editaci/vytvoření galerie?')
                .ok('Ano')
                .cancel('Ne');
            $mdDialog.show(confirm).then(function () {
                $scope.modelChanged = false;
                leftMenuService.clearOpenConfirmation();
                $location.url('/galleries');
            }, function () {
            });
            event.preventDefault();
            return false;
        }
    }));

    ctrl.onStickyChange = function () {
        _.forEach(ctrl.stickyPlants, function (sticky, plantId) {
            if (sticky) {
                if (!ctrl.gallery.plants[plantId]) {
                    ctrl.gallery.plants[plantId] = {};
                }
                ctrl.gallery.plants[plantId].sticky = sticky;
            } else {
                if (ctrl.gallery.plants[plantId]) {
                    delete ctrl.gallery.plants[plantId].sticky;
                }
            }
        })
        ctrl.refreshGallery();
    }

    ctrl.onFilterChange = function () {
        //nothing
    }

    ctrl.processTags = function (tags) {
        for (var i = 0; i < tags.length; i++) {
            ctrl.tags[parseInt(tags[i].id)] = tags[i].name;
        }
    };

    ctrl.getTagName = function (tagId) {
        var id = tagId;
        console.log(id, ctrl.tags[id]);
        return ctrl.tags[id]
    }

    ctrl.processCategories = function (categories) {
        for (var i = 0; i < categories.length; i++) {
            ctrl.categories.push({name: categories[i].name, id: parseInt(categories[i].id)});
        }
        console.log('CC', ctrl.categories);
    };

    ctrl.getCardStyle = function (plant) {
        var style = {
            'background-color': '#ffffff'
        }
        if (ctrl.gallery.plants && ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].backgroundColor) {
            style['background-color'] = ctrl.gallery.plants[plant.id].backgroundColor;
        }
        return style;
    }

    ctrl.openPlantQuickEdit = function(plant, ev) {
        $mdDialog
        .show({
            controller: 'plantQuickEditDialogController',
            templateUrl: 'gallery/plantQuickEditDialog.template.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            resolve: {
                plant: function () {
                    return plant
                },
            },
            clickOutsideToClose: true,
            fullscreen: true
        })
        .then(function () {
            ctrl.reload();
        }, function () {
            console.log('You cancelled the dialog.');
        });
    }

    ctrl.openPlantSettings = function (plant, ev) {
        $mdDialog
            .show({
                controller: 'plantSettingsDialogController',
                templateUrl: 'gallery/plantSettingsDialog.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                resolve: {
                    plant: function () {
                        return plant
                    },
                    plantSettings: function () {
                        return {
                            sticky: ctrl.stickyPlants[plant.id] || false,
                            overrideImage: ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].previewImages ? 'selected' : 'latest',
                            selectedImages: ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].previewImages ? ctrl.gallery.plants[plant.id].previewImages : [],
                            backgroundColor: ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].backgroundColor ? ctrl.gallery.plants[plant.id].backgroundColor : '#ffffff'
                        }
                    }
                },
                clickOutsideToClose: true,
                fullscreen: true
            })
            .then(function (plantSettings) {
                if (plantSettings.sticky && !ctrl.stickyPlants[plant.id]) { //for loacal dialog
                    ctrl.stickyPlants[plant.id] = plantSettings.sticky;
                } else if (!plantSettings.sticky && ctrl.stickyPlants[plant.id]) {
                    delete ctrl.stickyPlants[plant.id];
                }
                if (plantSettings.sticky && !ctrl.gallery.plants[plant.id]) { //for global model&changes detection
                    if (!ctrl.gallery.plants[plant.id]) {
                        ctrl.gallery.plants[plant.id] = {};
                    }
                    ctrl.gallery.plants[plant.id].sticky = plantSettings.sticky;
                } else if (!plantSettings.sticky && ctrl.gallery.plants[plant.id]) {
                    delete !ctrl.gallery.plants[plant.id].sticky;
                }

                if (plantSettings.overrideImage === 'latest' && ctrl.gallery.plants[plant.id]) {
                    delete ctrl.gallery.plants[plant.id].previewImages;
                }
                if (plantSettings.overrideImage === 'selected') {
                    if (!ctrl.gallery.plants[plant.id]) {
                        ctrl.gallery.plants[plant.id] = {};
                    }
                    ctrl.gallery.plants[plant.id].previewImages = plantSettings.selectedImages;
                }
                if (plantSettings.backgroundColor) {
                    if (!ctrl.gallery.plants[plant.id]) {
                        ctrl.gallery.plants[plant.id] = {};
                    }
                    ctrl.gallery.plants[plant.id].backgroundColor = plantSettings.backgroundColor;
                }
                ctrl.refreshGallery();
            }, function () {
                console.log('You cancelled the dialog.');
            });
    }

    ctrl.refreshGallery = function () {
        var output = _.cloneDeep(ctrl.plants);
        // for (var i = 0; i < output.length; i++) {
        _.forIn(output, function (plant, plantId) {
            if (ctrl.stickyPlants[plantId]) {
                plant.sticky = ctrl.stickyPlants[plantId]
            }
        })

        // }
        ctrl.previewPlants = output;
    }
    ctrl.getActiveLabel = function () {
        if (ctrl.gallery.active) {
            return "Aktivní"
        } else {
            return "Neaktivní"
        }
    }
    ctrl.getPlantName = function (plant) {
        if (plant.name_lat !== undefined && plant.name_lat.length > 0) {
            return plant.name_lat;
        }
        return '';
    }

    ctrl.imageIsLandscape = function (img) {
        return img.width && img.height && parseInt(img.width) > parseInt(img.height);
    }

    ctrl.getSaveLabel = function () {
        if ($scope.isNew) {
            return ctrl.saving ? 'Vytvářím...' : 'Vytvořit'
        } else {
            return ctrl.saving ? 'Ukládám...' : 'Uložit'
        }
    }

    ctrl.getSaveAndCloseLabel = function () {
        if (ctrl.isNew) {
            return ctrl.saving ? 'Vytvážím...' : 'Vytvořit a zavřít'
        } else {
            return ctrl.saving ? 'Ukládám...' : 'Uložit a zavřít'
        }
    }

    ctrl.getImagePreviewWidth = function (img) {
        if (!ctrl.imageIsLandscape(img)) {
            return 250;
        }
        return 512;
    }

    ctrl.getSubTitle = function (plant) {
        if (plant.name !== undefined && plant.name.length > 0) {
            return plant.name;
        }
        return plant.family_name;
    }

    ctrl.getPlantsCount = function() {
        if (ctrl.plants) {
            return _.keys(ctrl.plants).length;
        } else {
            return 0;
        }
    }

    ctrl.getImageOverride = function (plant) {
        if (ctrl.gallery.plants && ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].previewImages && ctrl.gallery.plants[plant.id].previewImages.length > 0) {
            return true;
        }
        return false;
    }
    ctrl.getImageUrl = function (plant) {
        var idx = null;
        if (ctrl.gallery.plants && ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].previewImages && ctrl.gallery.plants[plant.id].previewImages.length > 0) {
            idx = parseInt(ctrl.gallery.plants[plant.id].previewImages[0]);
        } else {
            idx = _.keys(plant.images)[0];
        }
        if (!plant.images[idx]) {
            return '';
        }
        var url = plant.images[idx].url;
        if (url.substr(0, 7) === 'http://') {
            url = '//' + url.substr(6);
        }
        return url + '/max-width/' + ctrl.getImagePreviewWidth(plant.images[idx]) + ctrl.webp;

    };

    ctrl.getImagesCount = function(plant) {
        return _.size(plant.images);
    }

    ctrl.hasMoreTahnOneImage = function(plant) {
        return _.size(plant.images) > 1;
    }

    ctrl.openGalleriesList = function () {
        if (ctrl.modelChanged === true) {
            var confirm = $mdDialog.confirm()
                .title('Přejít na seznam galerii')
                .textContent('Galerie obsahuje neuložené změny. Přejít na seznam galerii?')
                .ariaLabel('Přejít na seznam galerii')
                .ok('Ano')
                .cancel('Ne');
            $mdDialog.show(confirm).then(function () {
                leftMenuService.clearOpenConfirmation();
                $location.url('/galleries')
            }, function () {
            });
        } else {
            $location.url('/galleries')
        }
    }

    ctrl.load = function (id) {
        return galleriesService.loadGallery($routeParams.galleryId)
            .then(function (galleryData) {
                ctrl.gallery = galleryData;
                ctrl.stickyPlants = {};
                _.forEach(ctrl.gallery.plants, function (plantData, plantId) {
                    console.log(plantData);
                    if (plantData.sticky) {
                        ctrl.stickyPlants[plantId] = plantData.sticky;
                    }
                })
            })
    }

    ctrl.getTagsFromFilter = function () {
        return [185];
    }

    ctrl.save = function (okCallback) {
        $scope.saving = true
        if (ctrl.isNew) {
            //galleriesService.
        } else {
            galleriesService.update(ctrl.gallery)
                .then(function () {
                    if (okCallback) {
                        ctrl.modelChanged = false;
                        return okCallback()
                    }
                    ctrl.load($routeParams.galleryId)
                        .then(function () {
                            ctrl.saving = false
                            leftMenuService.clearOpenConfirmation();
                            ctrl.modelChanged = false;
                            ctrl.originalModel = _.cloneDeep(JSON.parse(angular.toJson(ctrl.gallery)));
                        })
                        .catch(function (err) {
                            ctrl.saving = false
                            $rootScope.$emit('error-message', 'Uložení galerie zlyhalo');
                        })
                })
        }
    }

    ctrl.saveAndClose = function () {
        ctrl.save(function () {
            ctrl.openGalleriesList();
        })
    }

    ctrl.cancel = function () {
        ctrl.openGalleriesList();
    }

    ctrl.reload = function() {
        ctrl.load($routeParams.galleryId)
        .then(function () {
            $scope.$watch(function () {
                return ctrl.gallery
            }, function (newVal, oldVal) {
                if (ctrl.originalModel === undefined) {
                    ctrl.originalModel = _.cloneDeep(newVal.toJSON());
                    return;
                }
                ctrl.modelChanged = !_.isEqual(JSON.parse(angular.toJson(newVal)), $scope.originalModel);
                if (ctrl.modelChanged) {
                    leftMenuService.setOpenConfirmation('Opravdu odejít? Všechny změny budou ztracenny.')
                } else {
                    leftMenuService.clearOpenConfirmation();
                }
            }, true);
        })
        .then(function () {
            return galleriesService.getImages(ctrl.gallery.filter).then(function (data) {
                ctrl.loading = false;
                ctrl.plants = JSON.parse(angular.toJson(data.plants));
                console.log('P', ctrl.plants);
                ctrl.onStickyChange();
                ctrl.loading = false;
                //ctrl.previewPlants = JSON.parse(angular.toJson(plants));
            });
        })
    }


    ctrl.loading = true;
    tagsService.getAllTagsAsync(null, null, function () {
    })
        .then(function (tags) {
            ctrl.processTags(tags);
            return categoriesService.getAllCategoriesAsync(null, null);
        })
        .then(function (categories) {
            ctrl.processCategories(categories);
            if (!ctrl.isNew) {
                ctrl.reload()
            }
        });
}

angular.module('adminApp').filter('plantSorter', function () {
    return function (items, field) {
        var getPlantName = function (plant) {
            if (plant.name_lat !== undefined && plant.name_lat.length > 0) {
                return plant.name_lat;
            }
            return '';
        }
        var filtered = [];
        angular.forEach(items, function (item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            if (a.sticky && !b.sticky) {
                return 1
            }
            if (!a.sticky && b.sticky) {
                return -1
            }
            var aName = getPlantName(a);
            var bName = getPlantName(b);
            //return a.localeCompare(b);
            if (aName < bName) {
                return 1;
            }
            if (aName > bName) {
                return -1
            }
            return 0;
        });
        return _.reverse(filtered);
    };
});

angular.module('adminApp').component('editGallery', {
    templateUrl: 'gallery/editGallery.template.html',
    controller: editGalleryComponent,
    bindings: {}
});
'use strict';
var galleriesListController = function ($log, $location, galleriesService, tagsService, categoriesService, $mdDialog, $scope) {
    var ctrl = this;
    ctrl.model = {
        galleries: [],
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }
    ctrl.tags = {};
    ctrl.categories = {}

    ctrl.activateGallery = function (gallery) {
        galleriesService.setActive(gallery.id, !gallery.active);
    }

    ctrl.editGallery = function (evt, gallery) {
        $location.path('/galleries/' + gallery.id);
    }

    ctrl.getGalleries = function () {
        ctrl.model.promise = galleriesService.getAllGalleries(ctrl.query).then(function (galleries) {
            ctrl.model.galleries = galleries;
        })
    }

    ctrl.selected = [];
    ctrl.limitOptions = [5, 10, 15];
    ctrl.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };

    ctrl.getFilterVerbose = function (filter) {
        var output = [];
        if (filter.tags) {
            var tagsNames = [];
            _.forEach(filter.tags, function (tagId) {
                if (ctrl.tags[tagId]) {
                    tagsNames.push(ctrl.tags[tagId]);
                }
            })
            output.push('Tagy: ' + tagsNames.join(', '));
        }
        if (filter.categories) {
            var names = [];
            _.forEach(filter.categories, function (id) {
                if (ctrl.categories[id]) {
                    names.push(ctrl.categories[id]);
                }
            })
            if (names.length > 0) {
                output.push('Kategorie: ' + names.join(', '));
            }
        }
        return output.join(', ');
    }

    ctrl.processTags = function (tags) {
        for (var i = 0; i < tags.length; i++) {
            ctrl.tags[parseInt(tags[i].id)] = tags[i].name;
        }
    }

    ctrl.processCategories = function (categories) {
        for (var i = 0; i < categories.length; i++) {
            ctrl.categories[parseInt(categories[i].id)] = categories[i].name;
        }
    }

    tagsService.getAllTagsAsync(null, null, function () {
    })
        .then(function (tags) {
            ctrl.processTags(tags);
            return categoriesService.getAllCategoriesAsync(null, null);
        })
        .then(function (categories) {
            ctrl.processCategories(categories);
            ctrl.getGalleries(true);
        })
}

angular.module('adminApp').component('galleriesList', {
    templateUrl: 'gallery/galleriesList.template.html',
    controller: galleriesListController,
    bindings: {}
});
angular.module('adminApp').controller('plantQuickEditDialogController', ['$scope', '$mdDialog', '$log', 'plant', 'webpService', 'plantsService', function ($scope, $mdDialog, $log, plant, webpService, plantsService) {
    $scope.model = {}
    $scope.allTags = [];

    $scope.save = function () {
        plantsService.updateTags($scope.model.id, $scope.model.tags)
        .then(function() {
            $mdDialog.hide();
        })
        .catch(function(err) {
            console.log('Unable to save tags: ', err);
        })
    }

    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    plantsService.getPlant(plant.id, function (plant) {
        $scope.model = plant;
    }, function (err) {
        console.log('Unable to load plant ', err);
    })

    $scope.createFilterFor = function (query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(tag) {
            return (tag.name_lowercase.indexOf(lowercaseQuery) != -1);
        }
    }

    $scope.filterSelected = true;
    $scope.tagQuerySearch = function (criteria) {
        console.log($scope.allTags)
        return $scope.allTags.filter($scope.createFilterFor(criteria))
    }

    plantsService.getAllTags(function (allTags) {
        $scope.allTags = allTags;
    }, function (err) {
        $log.error(err)
    })
}]);

angular.module('adminApp').controller('plantSettingsDialogController', ['$scope', '$mdDialog', '$log', 'plant', 'webpService', 'plantSettings', function($scope, $mdDialog, $log, plant, webpService, plantSettings) {
    $scope.settings = plantSettings;
    $scope.plant = plant;
    $scope.selectedImages = [];
    if (plantSettings.selectedImages) {
        for (var i = 0; i<=plantSettings.selectedImages.length;i++) {
            var imgId = plantSettings.selectedImages[i]
            $scope.selectedImages[imgId] = true;
        }
    }

    $scope.webp = webpService.supported() ? '/webp' : '';

    $scope.save = function() {
        if (!$scope.settings.backgroundColor) {
            alert('Barva pozadí nemuŽe zustat prázdná');
            return;
        }
        $mdDialog.hide($scope.settings);
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.onImageSelected = function(image) {
        var imageId = parseInt(image.id);
        $scope.settings.selectedImages = [image.id];
        $scope.selectedImages = {};
        $scope.selectedImages[imageId] = true;
    }

    $scope.imageIsLandscape = function (img) {
        return img.width && img.height && parseInt(img.width) > parseInt(img.height);
    }

    $scope.getImagePreviewWidth = function (img) {
        if (!$scope.imageIsLandscape(img)) {
            return 250;
        }
        return 512;
    }

    $scope.getImageUrl = function (image) {
        return image.url + '/max-width/' + $scope.getImagePreviewWidth(image) + $scope.webp;
    }

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);

angular.module('adminApp').controller('leftMenuController', ['$scope', '$mdMedia', '$location', '$window', 'leftMenuService', '$mdDialog', 'settingsService',
    function ($scope, $mdMedia, $location, $window, leftMenuService, $mdDialog, settingsService) {
        settingsService.onSettingsLoaded($scope, function (event, settings) {
            $scope.settings = settings
        })
        $scope.MENU_ARTICLES = "articles";
        $scope.MENU_CIRCUITS = "circuits";
        $scope.MENU_DOWNLOADS = "downloads";
        $scope.MENU_MENUS = "menus";
        $scope.MENU_PLANTS = "plants";
        $scope.MENU_GALLERIES = "galleries";
        $scope.MENU_TAGS = "tags";
        $scope.MENU_CATEGORIES = "categories";
        $scope.MENU_AUTHORS = "authors";
        $scope.MENU_FAMILIES = "families";
        $scope.showLabels = true;
        if ($window.localStorage.getItem('left-menu-show-labels') !== null && $window.localStorage.getItem('left-menu-show-labels') !== undefined && $window.localStorage.getItem('left-menu-show-labels') !== '') {
            try {
                $scope.showLabels = angular.fromJson($window.localStorage.getItem('left-menu-show-labels'))
            } catch (e) {
                $log.debug('Unable to get left-menu-show-labels from local storage');
            }
        }

        $scope.toggleMenuSize = function () {
            $scope.showLabels = !$scope.showLabels;
            $window.localStorage.setItem('left-menu-show-labels', angular.toJson($scope.showLabels));
        }

        $scope.open = function (path, isFullUrl) {
            if (leftMenuService.getOpenConfirmation() === undefined) {
                if (isFullUrl) {
                    $window.open(path, '_self');
                } else {
                    $location.path(path);
                }
            } else {
                var confirm = $mdDialog.confirm()
                .title('Navigace')
                .textContent(leftMenuService.getOpenConfirmation())
                .ariaLabel('Navigace')
                .ok('Ano')
                .cancel('Ne');
                $mdDialog.show(confirm).then(function () {
                    leftMenuService.clearOpenConfirmation();
                    if (isFullUrl) {
                        $window.open(path, '_self');
                    } else {
                        $location.path(path);
                    }
                }, function () {
                });
            }
        }

        $scope.toggleSideMenu = function (menuItemCode) {
            if (menuItemCode === 'dashboard') {
                $scope.open('/', true);
                return;
            }
            if (menuItemCode == $scope.MENU_ARTICLES) {
                $scope.open('/articles');
                return;
            }
            if (menuItemCode == $scope.MENU_PLANTS) {
                $scope.open('/plants');
                return;
            }
            if (menuItemCode == $scope.MENU_TAGS) {
                $scope.open('/tags');
                return;
            }
            if (menuItemCode == $scope.MENU_CATEGORIES) {
                $scope.open('/categories');
                return;
            }
            if (menuItemCode == $scope.MENU_FAMILIES) {
                $scope.open('/families');
                return;
            }
            if (menuItemCode == $scope.MENU_FAMILIES) {
                $scope.open('/families');
                return;
            }
            if (menuItemCode == $scope.MENU_AUTHORS) {
                $scope.open('/authors');
                return;
            }
            if (menuItemCode == $scope.MENU_GALLERIES) {
                $scope.open('/galleries');
                return;
            }
            if (menuItemCode == $scope.MENU_CIRCUITS) {
                $scope.open('/circuits');
                return;
            }
            if (menuItemCode == $scope.MENU_MENUS) {
                $scope.open('/menus');
                return;
            }
            if (menuItemCode == $scope.MENU_DOWNLOADS) {
                $scope.open('/downloads');
                return;
            }
        };
    }]);

'use strict';
angular.module('adminApp').directive('leftMenu', function() {
	return {
		restrict : 'E',
		templateUrl : "leftMenu/leftMenu.template.html" 
	};
});
var leftMenuService = function ($log, $rootScope, baseService) {
    var confirmationMessage;
    var svc = {
        setOpenConfirmation: function (message) {
            confirmationMessage = message;
        },
        clearOpenConfirmation: function () {
            confirmationMessage = undefined;
        },
        getOpenConfirmation: function () {
            return confirmationMessage;
        }
    }
    return angular.extend({}, baseService, svc)
}
leftMenuService.$inject = ['$log', '$rootScope', 'baseService'];
angular.module('adminApp').factory('leftMenuService', leftMenuService);
'use strict';
angular.module('adminApp').controller('menuEditController', [ '$location', '$scope', '$mdDialog', '$mdMedia', 'menusService', '$timeout', '$routeParams', function($location, $scope, $mdDialog, $mdMedia, menusService, $timeout, $routeParams) {
	$scope.model = {
        id: $routeParams.menuId
    }
    $scope.flashMessages = []

    $scope.save = function() {
        menusService.save($scope.model, function(updatedMenu) {
            var okMessage = {id: new Date().getTime(), status: 'ok', message: 'Menu was saved'};
            $scope.flashMessages.push(okMessage);
            $timeout(function() {
                for (var i = 0; i < $scope.flashMessages.length; i++) {
                    if ($scope.flashMessages[i].id == okMessage.id) {
                        $scope.flashMessages.splice(i, 1);
                        return;
                    }
                }
            }, 3000);
        }, function(err) {
            alert('Unable to seve menu. Try again later.')
        });

    }
		$scope.openMenusList = function() {
			$location.url('menus')
		}
    $scope.onItemSaved = function(item) {
        var okMessage = {id: new Date().getTime(), status: 'ok', message: 'Menu item was saved'};
        $scope.flashMessages.push(okMessage);
        $timeout(function() {
            for (var i = 0; i < $scope.flashMessages.length; i++) {
                if ($scope.flashMessages[i].id == okMessage.id) {
                    $scope.flashMessages.splice(i, 1);
                    return;
                }
            }
        }, 3000);
    }
    $scope.onItemsReordered = function() {
        var okMessage = {id: new Date().getTime(), status: 'ok', message: 'Menu items order saved'};
        $scope.flashMessages.push(okMessage);
        $timeout(function() {
            for (var i = 0; i < $scope.flashMessages.length; i++) {
                if ($scope.flashMessages[i].id == okMessage.id) {
                    $scope.flashMessages.splice(i, 1);
                    return;
                }
            }
        }, 3000);
    }

    $scope.onItemPublished = function(published) {
        var pMessage = published?"published":"unpublished"
        var okMessage = {id: new Date().getTime(), status: 'ok', message: 'Menu item was ' + pMessage};
        $scope.flashMessages.push(okMessage);
        $timeout(function() {
            for (var i = 0; i < $scope.flashMessages.length; i++) {
                if ($scope.flashMessages[i].id == okMessage.id) {
                    $scope.flashMessages.splice(i, 1);
                    return;
                }
            }
        }, 3000);
    }

    menusService.loadMenu($scope.model.id, function(menuModel) {
        $scope.model = menuModel;
    });
} ]);

'use strict';
angular.module('adminApp').directive('menuEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'menuEdit/menuEdit.template.html',
		controller: 'menuEditController',
		scope: {
			menu: '@'
		}
	};
});
'use strict'
angular.module('adminApp').controller('menuItemEditController', [ '$location', '$scope', '$mdDialog', '$mdMedia', 'menusService', '$timeout', '$routeParams', function($location, $scope, $mdDialog, $mdMedia, menusService, $timeout, $routeParams) {
  $scope.saving = false
	$scope.model = {
        id: $routeParams.menuItemId
    }
    $scope.menuId = $routeParams.menuId
    $scope.flashMessages = []

    $scope.save = function() {
        $scope.saving = true
        menusService.saveMenuItem($scope.menuId, $scope.model, function(updatedMenu) {
            var okMessage = {id: new Date().getTime(), status: 'ok', message: 'Menu item was saved'};
            $scope.flashMessages.push(okMessage);
            $timeout(function() {
                $scope.openMenuEdit()
            }, 1000);
        }, function(err) {
            $scope.saving = false
            alert('Unable to save menu. Try again later.')
        });
    }
    $scope.openMenusList = function() {
      $location.url('/menus/')
    }
		$scope.openMenuEdit = function() {
			$location.url('/menus/' + $scope.menuId)
		}
    $scope.cancel = function() {
      $scope.openMenuEdit()
    }
    $scope.getSaveLabel = function() {
      return $scope.saving?'Saving...':'Save'
    }
    $scope.options = {
  		    language: 'en',
  	  		format_tags: 'h1;h2',
  		    allowedContent: true,
  		    entities: false,
  		    //width: '100%'
  		  };

    menusService.loadMenuItem($scope.menuId, $scope.model.id, function(menuItemModel) {
        if (menuItemModel.publishable === '1') {
          menuItemModel.publishable = true
        } else {
          menuItemModel.publishable = false
        }
        if (menuItemModel.show_subtitle === '1') {
          menuItemModel.show_subtitle = true
        } else {
          menuItemModel.show_subtitle = false
        }
        $scope.model = menuItemModel;
    });
} ]);

'use strict'
angular.module('adminApp').directive('menuItemEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'menuEdit/menuItemEdit.template.html',
		controller: 'menuItemEditController'
	};
});

angular.module('adminApp').controller('menuItemEditorDailogController', ['$scope', '$mdDialog', 'menuItemModel', function($scope, $mdDialog, menuItemModel) {
	$scope.model = menuItemModel;
	
    $scope.hide = function() {
      $mdDialog.hide();
    };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.save = function() {
	  $mdDialog.hide($scope.model);
  };
  $scope.options = {
		    language: 'en',
		    allowedContent: true,
		    entities: false,
		    width: 500
		  };
}]);
'use strict';
angular.module('adminApp').controller('menuItemsListController', ['$scope', '$mdDialog', '$mdMedia', 'menusService', '$timeout', '$location', function ($scope, $mdDialog, $mdMedia, menusService, $timeout, $location) {
  $scope.orderable = false
  $scope.publishable = false
    $scope.model = {
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }
    function success(menuItems) {
      console.log(menuItems)
      $scope.orderable = menuItems.orderable === '1'?true:false
      $scope.publishable = menuItems.publishable === '1'?true:false
      $scope.enableOrdering($scope.orderable)
        for(var i = 0; i<menuItems.items.length;i++) {
            if (menuItems.items[i].published === "1") {
                menuItems.items[i].published = true;
            } else {
                menuItems.items[i].published = false;
            }
        }
        $scope.items = menuItems.items;
    }
    $scope.publicationEnabled = function() {
      return $scope.publishable
    }
    $scope.getMenuItems = function () {
        $scope.model.promise = menusService.loadMenuItems($scope.menuId, $scope.query, success).$promise;
    }
    $scope.editMenuItem = function (menuItem, ev) {
        $location.url('menus/' + $scope.menuId + '/menu-items/' + menuItem.id)
    }
    $scope.publishItem = function(menuItem) {
        $timeout(function() {
            menusService.saveMenuItem($scope.menuId, menuItem, function(data) {
                var isPublished = menuItem.published?true:false;
                $scope.onItemPublished({published: isPublished})
                $scope.getMenuItems($scope.menuId);
            });
        }, 100);
    }
    $scope.selected = [];
    $scope.limitOptions = [5, 10, 15];
    $scope.enableOrdering = function(enable) {
      if (!enable) {
          $scope.sortableOptions = {
            disabled: true
          };
          return;
      }
      $scope.sortableOptions = {
          stop: function(e, ui) {
              console.log($scope.items);
              var order = [];
              for (var i = 0; i < $scope.items.length; i++) {
                  order.push($scope.items[i].id)
              }
              menusService.saveItemsOrder($scope.menuId, order, $scope.model.query, function() {
                  $scope.onItemsReordered({})
              })
          },
          helper: "clone",
          forceHelperSize: true,
          forcePlaceholderSize: true
      }
    }

    $scope.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    $scope.getMenuItems($scope.menuId);
}]);

'use strict';
angular.module('adminApp').directive('menuItemsList', function() {
    return {
        restrict : 'E',
        templateUrl : 'menuEdit/menuItemsList.template.html',
        scope: {
            menuId: '=',
            items: '=',
            onItemsReordered: '&',
            onItemPublished: '&',
            onItemSaved: '&'
        },
        controller: 'menuItemsListController'
    };
});
angular.module('adminApp').factory('menusService', ['$log', '$resource',function ($log, $resource) {
	var menusResource = $resource('/api/api/index.php/menus', {}, {
		'get':  {method:'GET', isArray:true},
		'save': {method: 'PUT'}
	});
	var menuResource = $resource('/api/api/index.php/menu/:menuId', {}, {
		'save': {method: 'PUT'},
		'get': {method: 'GET'}
	});
	var menuItemResource = $resource('/api/api/index.php/menu/:menuId/item/:menuItemId', {}, {
		'save': {method: 'PUT'},
		'get': {method: 'GET'}
	});
	var menuItemsResource = $resource('/api/api/index.php/menu/:menuId/items', {}, {
		'get': {method: 'GET'}
	});
	var menuItemsOrderResource = $resource('/api/api/index.php/menu/:menuId/items/order', {}, {
		'save': {method: 'POST'}
	});
	return {
		load: function(query, successCallback) {
			//TODO: implement query
			return menusResource.get({}, function(menus) {
				var menusList = angular.copy(menus);
				delete menusList.$promise;
				delete menusList.resolved;
				return successCallback(menusList);
			}, function (err) {
				$log.error('Unable to load menus: ',err);
			} )
		},
		save: function(menu, successCallback, errCallback) {
			return menuResource.save({menuId: menu.id},menu, function(data) {
				successCallback(data);data
			}, function(err) {
				console.log(err);
				var message = '';
				if (err.status == 500) {
					message = err.data;
				} else {
					message = err.data.message;
				}
				$log.error('Unable to save menu', err);
				errCallback(message);
			});
		},
		loadMenuItems: function(menuId, query, successCallback) {
			//TODO: implement query
			return menuItemsResource.get({menuId: menuId}, function(menuItems) {
				var menuItems = angular.copy(menuItems);
				delete menuItems.$promise;
				delete menuItems.resolved;
				return successCallback(menuItems);
			}, function (err) {
				$log.error('Unable to load menu items: ',err);
			} )
		},
		loadMenu: function(menuId, successCallback) {
			return menuResource.get({menuId: menuId}, function(menuModel) {
				var menuModel = angular.copy(menuModel);
				delete menuModel.$promise;
				delete menuModel.resolved;
				return successCallback(menuModel);
			}, function (err) {
				$log.error('Unable to load menu: ',err);
			} )
		},
		saveItemsOrder: function(menuId, newOrder, tableQuery, okCallback) {
			var data = {
				order: newOrder,
				fromIndex: (tableQuery.page-1) * tableQuery.limit
			}
			return menuItemsOrderResource.save({menuId: menuId}, data, function() {
				return okCallback()
			})
		},
		loadMenuItem: function(menuId, menuItemId, successCallback) {
			return menuItemResource.get({menuId: menuId, menuItemId: menuItemId}, function(menuItemModel) {
				var menuItemModel = angular.copy(menuItemModel);
				delete menuItemModel.$promise;
				delete menuItemModel.resolved;
				if (menuItemModel.published === '1') {
					menuItemModel.published = true
				} else {
					menuItemModel.published = false
				}
				return successCallback(menuItemModel);
			}, function (err) {
				$log.error('Unable to load menu item: ',err);
			} )
		},
		saveMenuItem: function(menuId, menuItemModel, successCallback) {
			return menuItemResource.save({menuId: menuId, menuItemId: menuItemModel.id}, menuItemModel, function(data) {
				return successCallback(data);
			}, function (err) {
				$log.error('Unable to load menu: ',err);
			} )
		}
	}
}]);

'use strict';
angular.module('adminApp').controller('menusListController', [ '$scope', '$mdDialog', '$mdMedia', 'menusService', '$timeout', '$location', function($scope, $mdDialog, $mdMedia, menusService, $timeout, $location) {
			$scope.model = {
				circuits : menusService.menusList,
				query: {
					    order: 'name',
					    limit: 10,
					    page: 1
					  },
				selected: [],
			    flashMessages: []
			}

			$scope.editMenu = function(menu, ev) {
				$location.url('/menus/' + menu.id)
				return;

			};

			function success(articles) {
				$scope.model.menus = articles;
			}

			$scope.getMenus = function () {
				$scope.model.promise = menusService.load($scope.query, success).$promise;
			}
			$scope.selected = [];
			$scope.limitOptions = [5,10,15];
			$scope.options = {
				    rowSelection: false,
				    multiSelect: false,
				    autoSelect: false,
				    decapitate: false,
				    largeEditDialog: true,
				    boundaryLinks: false,
				    limitSelect: true,
				    pageSelect: true
				  };
			$scope.getMenus();
} ]);

'use strict';
angular.module('adminApp').directive('menusList', function() {
	return {
		restrict : 'E',
		templateUrl : 'menuEdit/menusList.template.html',
		controller: 'menusListController'
	};
});
angular.module('adminApp').controller('addTagDailogController', ['$scope', '$mdDialog', 'plantsService', function($scope, $mdDialog, plantsService) {

    $scope.model = {
      name: ''
    }

    $scope.create = function() {
      plantsService.createTag($scope.model, function(newTag) {
        $mdDialog.hide(newTag);
      }, function (err) {
        alert(err)
      })
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);

angular.module('adminApp').controller('editCategoryDialogController', ['$scope', '$mdDialog', 'plantsService', 'category', '$log', function($scope, $mdDialog, plantsService, category, $log) {
    $log.debug('D', category)
    $scope.model = category

    //start: listeners
    plantsService.onPlantCategorySaved($scope, function(event, category) {
      $scope.hide({relaod: true})
    })
    //end: listeners

    $scope.save = function() {
      plantsService.saveCategory($scope.model.id, $scope.model)
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);

angular.module('adminApp').controller('editFamilyDialogController', ['$scope', '$mdDialog', 'plantsService', 'family', '$log', function($scope, $mdDialog, plantsService, family, $log) {
    $log.debug('D', family)
    $scope.model = family

    //start: listeners
    plantsService.onPlantFamilySaved($scope, function(event, family) {
      $scope.hide({relaod: true})
    })
    //end: listeners

    $scope.save = function() {
      if (($scope.model.name === undefined || $scope.model.name.length === 0) &&
          ($scope.model.name_lat === undefined || $scope.model.name_lat.length === 0) &&
          ($scope.model.name_en === undefined || $scope.model.name_en.length === 0)) {
            alert('Name can not be empty!')
            return
          }
      plantsService.saveFamily($scope.model.id, $scope.model)
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);

angular.module('adminApp').controller('editTagDialogController', ['$scope', '$mdDialog', 'plantsService', 'tag', '$log', function($scope, $mdDialog, plantsService, tag, $log) {
    $scope.model = tag

    //start: listeners
    plantsService.onPlantTagSaved($scope, function(event, tag) {
      $scope.hide({relaod: true})
    })
    //end: listeners

    $scope.save = function() {
      plantsService.savePlantTag($scope.model.id, $scope.model)
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);

/**
 * @module
 */
var imagePreviewDailogController = function ($scope, $mdDialog, image, imagesService, $log) {
    $scope.imageUrl = image.preview_url;
    $scope.description = image.description;
    $scope.descriptionEn = image.description_en;
    $scope.authors = image.authors;
    $scope.authorsEn = image.authors_en;
    $scope.displayVerticalOffset = image.display_vertical_offset;
    $scope.display = image.display;

    $scope.save = function () {
        var data = { description: $scope.description, descriptionEn: $scope.descriptionEn, authors: $scope.authors, authorsEn: $scope.authorsEn, display: $scope.display, displayVerticalOffset: $scope.displayVerticalOffset };
        imagesService.save(image.id, data).then(function() {
            $mdDialog.hide(data);
        }).catch(function(err) {
            $log.error(err)
            alert('Unable to save image');
        })
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
};
imagePreviewDailogController.$inject = ['$scope', '$mdDialog', 'image', 'imagesService', '$log'];
angular.module('adminApp').controller('imagePreviewDailogController', imagePreviewDailogController);
'use strict'
angular.module('adminApp').controller('plantEditController', ['$location', '$rootScope', '$scope', '$mdDialog', '$mdMedia', 'menusService', '$timeout', '$routeParams', 'uiGmapIsReady', 'Upload', 'plantsService', 'settingsService', '$log', '$q', '$window', 'leftMenuService', 'ngAudio', '$interval',
    function ($location, $rootScope, $scope, $mdDialog, $mdMedia, menusService, $timeout, $routeParams, uiGmapIsReady, Upload, plantsService, settingsService, $log, $q, $window, leftMenuService, ngAudio, $interval) {
        $scope.markersIdCounter = 0
        $scope.modelChanged = false;
        // $scope.authorSearchText;
        // $scope.selectedAuthor;
        $scope.options = { //for mce
            language: 'en',
            format_tags: 'h1;h2',
            allowedContent: true,
            entities: false,
            removePlugins: 'templates'
            //width: '100%'
        };
        $scope.contentLanguageSelectorVisible = true;
        $scope.saveEnabled = true;
        $scope.formSettings = {
            selectedContentLanguage: 'cs'
        }
        $scope.$on('$locationChangeStart', function (event, next, current) {
            $log.debug('URL ', next);
            if ($scope.modelChanged === true) {
                var confirm = $mdDialog.confirm()
                    .title('Otevřít registr')
                    .textContent('Rostlina obsahuje neuložené změny. Přejít na registr?')
                    .ariaLabel('Otevřít registr')
                    .ok('Ano')
                    .cancel('Ne');

                $mdDialog.show(confirm).then(function () {
                    $scope.modelChanged = false;
                    leftMenuService.clearOpenConfirmation();
                    $location.url('/plants');
                }, function () {
                });
                event.preventDefault();
                return false;
            }
        });
        $rootScope.$on('marker_deleted', function (event, data) {
            for (var i = 0; i < $scope.model.markers.length; i++) {
                if ($scope.model.markers[i].id == data.markerId) {
                    $scope.model.markers.splice(i, 1)
                    event.stopPropagation();
                    return
                }
            }
            event.stopPropagation();
        })
        $scope.$on('save_enabled', function (event, enabled) {
            $scope.saveEnabled = enabled;
        });

        $scope.settings = []

        $scope.map = {
            infoWindowTemplate: 'plants/plantEditMapMarkerInfoWindow.template.html',
            markerOptions: {
                draggable: true,
                animation: 'DROP'
            },
            center: {
                latitude: 0,
                longitude: 0
            },
            zoom: 18,
            bounds: {
                northeast: {
                    latitude: 50.0689551446,
                    longitude: 14.4179141521
                },
                southwest: {
                    latitude: 50.0733622808,
                    longitude: 14.4247806072
                }
            },
            events: {
                click: function (mapModel, eventName, originalEventArgs) {
                    $scope.$apply(function () {
                        var e = originalEventArgs[0];
                        //$scope.model.markers = []
                        $scope.markersIdCounter++
                        $scope.model.markers.push({
                            latitude: e.latLng.lat(),
                            longitude: e.latLng.lng(),
                            id: $scope.markersIdCounter,
                            title: $scope.model.name
                        })
                    });
                }
            }
        }

        $scope.$on('$destroy', $scope.$on('show-mode-change', function (event, mode) {
            if (mode === 'show') {
                $scope.contentLanguageSelectorVisible = true;
            }
            if (mode === 'edit') {
                $scope.contentLanguageSelectorVisible = false;
            }
            event.stopPropagation();
        }));
        // authorsService.onAllAuthorsLoaded($scope, function (event, authors) {
        //     $scope.allAuthors = authors;
        // })

        //start: tags
        $scope.allTags = []

        $scope.showAllTags = function () {
            var tagNames = []
            for (var i = 0; i < $scope.allTags.length; i++) {
                tagNames.push($scope.allTags[i].name);
            }
            alert("Existing tags: \n" + tagNames.join(",\n") + "\n\nNote: this dialog will be nicer in the future")
        }

        $scope.reloadTags = function (okCallback) {
            plantsService.getAllTags(function (allTags) {
                $scope.allTags = allTags
                if (okCallback !== undefined) {
                    okCallback()
                }
            }, function (err) {
                $log.error(err)
            })
        }
        $scope.reloadTags()

        $scope.addTag = function (ev) {
            $mdDialog.show({
                controller: 'addTagDailogController',
                templateUrl: 'plants/addTagDialog.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                resolve: {},
                clickOutsideToClose: true,
                fullscreen: true
            })
                .then(function (newTag) {
                    console.log('tag created!')
                    $scope.reloadTags(function (data) {
                        console.log('collection reloaded')
                        $scope.model.tags.push(newTag)
                    })
                }, function () {
                    console.log('You cancelled the dialog.');
                });
        }

        $scope.filterSelected = true;
        $scope.tagQuerySearch = function (criteria) {
            console.log($scope.allTags)
            return $scope.allTags.filter($scope.createFilterFor(criteria))
        }

        $scope.createFilterFor = function (query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(tag) {
                console.log(tag.name_lowercase + ', pos:' + tag.name_lowercase.indexOf(lowercaseQuery))
                return (tag.name_lowercase.indexOf(lowercaseQuery) != -1);
                ;
            }
        }

        $scope.getQrCode = function () {
            return $scope.settings['registryMapUrl'] + '?plant=' + $scope.model.id;
        }
        //start: tags

        $scope.getNormalizedCoord = function (coord, zoom) {
            var y = coord.y;
            var x = coord.x;

            // tile range in one direction range is dependent on zoom level
            // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
            var tileRange = 1 << zoom;

            // don't repeat across y-axis (vertically)
            if (y < 0 || y >= tileRange) {
                return null;
            }

            // repeat across x-axis
            if (x < 0 || x >= tileRange) {
                x = (x % tileRange + tileRange) % tileRange;
            }

            return {
                x: x,
                y: y
            };
        }

        $scope.getLocationFromMap = function () {
            if ($scope.model.markers === undefined || $scope.model.markers.length == 0) {
                return 'not set'
            }
            return $scope.model.markers.length + ' markers'
        }

        $scope.initMap = function () {
            $scope.moonMapType = new google.maps.ImageMapType({
                getTileUrl: function (coord, zoom) {
                    // console.log(getProjection())
                    var normalizedCoord = $scope.getNormalizedCoord(coord, zoom);
                    if (!normalizedCoord) {
                        return null;
                    }

                    var bound = Math.pow(2, zoom);
                    // console.log('x: ' + coord.x)
                    // console.log('y:' + coord.y)
                    var url = '//' + $scope.settings.baseUrl + 'api/map2/' + zoom + '/' + normalizedCoord.x + '/' + normalizedCoord.y + '.png'
                    //var url = 'http://botgar.local/api/map/' + zoom + '/' + normalizedCoord.x + '/' + (bound - normalizedCoord.y - 1) + '.png'
                    // console.log(url)
                    return url;
                },
                tileSize: new google.maps.Size(256, 256),
                maxZoom: 18,
                minZoom: 17,
                radius: 1738000,
                name: 'Moon'
            });
        }

        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        })
        $scope.deleteImage = function (image) {
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this image?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function () {
                image.status = 'deleted'
            }, function () {
            });
        }

        $scope.getPublishedLabel = function () {
            return $scope.model.is_active ? 'Publikován' : 'Nepublikován';
        }

        $scope.removeCzAudio = function () {
            $scope.model.audio_cz = null;
            $scope.model.audio_cz_url = null;
        }

        $scope.uploadCzAudioFile = function (file, errFiles) {
            //$scope.f = file;
            //$scope.errFile = errFiles && errFiles[0];
            $scope.audioCzUpload = true;
            if (file) {
                file.upload = Upload.upload({
                    url: '/upload/audio',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        var data = response.data[0];
                        //file.result = response.data;
                        $scope.model.audio_cz = data.id;
                        $scope.model.audio_cz_url = data.url;
                        $scope.audioCzUpload = false;
                        var audioUrl = $scope.model.audio_cz_url.replace(/\{\$BASE_URL\}/, '//' + $scope.settings.baseUrl.replace(/\/$/, ''));
                        console.log('Got audio: ', audioUrl);
                        $scope.sound_cz = ngAudio.load(audioUrl);
                    },100);
                }, function (response) {
                    if (resp.data.code === 'upload_unsupported_format') {
                        alert('Unable to upload image: unsupported format.');
                    }
                    $scope.audioCzUpload = false;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                    $scope.audioCzUploadProgress = file.progress;
                });
            }
        }

        uiGmapIsReady.promise(1).then(function (instances) {
            instances.forEach(function (inst) {
                var map = inst.map
                var uuid = map.uiGmap_id
                var mapInstanceNumber = inst.instance // Starts at 1.
                console.log('ready!')
                //map.addTileOverlay(new TileOverlayOptions().tileProvider());
                $scope.initMap()
                $scope.map.center = {
                    latitude: parseFloat($scope.settings.adminMapCenterLat),
                    longitude: parseFloat($scope.settings.adminMapCenterLon)
                };
                map.overlayMapTypes.push($scope.moonMapType);
                // map.mapTypes.set('moon', $scope.moonMapType);
                // map.setMapTypeId('moon');
            })
        })

        $scope.families = []
        $scope.categories = []
        // $scope.allAuthors = []

        plantsService.getCategories(function (categories) {
            $scope.categories = categories
        })

        plantsService.getFamilies(function (families) {
            $scope.families = families
        })

        // authorsService.loadAll()


        $scope.saving = false
        $scope.isNew = $routeParams.plantId == '_'

        settingsService.load(function (data) {
            $scope.settings = data;

            if ($scope.isNew) {
                $scope.model = {
                    new_family: 'none',
                    occurrence: '',
                    name: '',
                    name_en: '',
                    name_lat: '',
                    name_de: '',
                    name_sk: '',
                    new_category: 'none',
                    authors: [],
                    is_active: false,
                    blocks: [{
                        content: 'ukazkovej text',
                        content_en: 'example text',
                        backgroud_color: '#ffffff',
                        type: 0
                    }],
                    delete_blocks: [],
                    markers: [{
                        id: 1,
                        latitude: parseFloat($scope.settings.adminMapCenterLat),
                        longitude: parseFloat($scope.settings.adminMapCenterLon),
                        title: $scope.model.name
                    }],
                    images: [],
                    tags: []
                }
            } else {
                $scope.model = {
                    id: $routeParams.plantId
                }
            }

            if (!$scope.isNew) {
                $scope.loadPlant($scope.model.id);
                $scope.$watch('model', function (newVal, oldVal) {
                    if ($scope.originalModel === undefined) {
                        $scope.originalModel = _.cloneDeep(newVal.toJSON());
                        return;
                    }
                    $scope.modelChanged = !_.isEqual(JSON.parse(angular.toJson(newVal)), $scope.originalModel);
                    if ($scope.modelChanged) {
                        leftMenuService.setOpenConfirmation('Opravdu odejít? Všechny změny budou ztracenny.')
                    } else {
                        leftMenuService.clearOpenConfirmation();
                    }
                }, true);
            } else {
                $scope.markersIdCounter = 1
                //$scope.originalModel = _.cloneDeep($scope.model);
                $scope.$watch('model', function (newVal, oldVal) {
                    if ($scope.originalModel === undefined) {
                        $scope.originalModel = _.cloneDeep(JSON.parse(angular.toJson($scope.model)));
                        return;
                    }
                    $log.debug($scope.originalModel);
                    $log.debug(JSON.parse(angular.toJson(newVal)));
                    $scope.modelChanged = !_.isEqual(JSON.parse(angular.toJson(newVal)), $scope.originalModel);
                    if ($scope.modelChanged) {
                        leftMenuService.setOpenConfirmation('Opravdu odejít? Všechny změny budou ztraceny.')
                    } else {
                        leftMenuService.clearOpenConfirmation();
                    }
                }, true);
            }

        });

        // $scope.onMapClicked = function(event) {
        //     $scope.$apply(function() {
        //
        //         var point = event.latlng
        //         $scope.markersIdCounter++
        //         $scope.model.markers.push({
        //             latitude: point.lat,
        //             longitude: point.lng,
        //             id: $scope.markersIdCounter,
        //             title: 'hello'
        //         })
        //         $log.log($scope.model.markers)
        //     })
        // }

        $scope.flashMessages = []


        $scope.saveAndClose = function () {
            $scope.save(function () {
                $scope.openPlantsList();
            })
        }

        $scope.save = function (okCallback) {
            if (($scope.model.name === '' || $scope.model.name === undefined) &&
                ($scope.model.name_en === '' || $scope.model.name_en === undefined) &&
                ($scope.model.name_lat === '' || $scope.model.name_lat === undefined)) {
                alert('Please enter at least one name is some language')
                return
            }
            $scope.saving = true;
            if ($scope.isNew) {
                plantsService.insertOne($scope.model, function (updatedPlant) {
                    $log.debug('Plant created', updatedPlant);
                    $rootScope.$emit('ok-message', 'Rostlina byla vytvořena');
                    $scope.isNew = false;
                    if (okCallback) {
                        $scope.modelChanged = false;
                        return okCallback()
                    }
                    $scope.loadPlant(updatedPlant.id).then(function () {
                        $scope.saving = false
                        leftMenuService.clearOpenConfirmation();
                        $scope.modelChanged = false;
                        $scope.originalModel = _.cloneDeep(JSON.parse(angular.toJson($scope.model)));
                    })
                }, function (err) {
                    $scope.saving = false
                    $rootScope.$emit('error-message', 'Vložení nové rostliny zlyhalo.');
                });
            } else {
                plantsService.updateOne($scope.model, function (updatedPlant) {
                    $rootScope.$emit('ok-message', 'Rostlina byla uložena');
                    leftMenuService.clearOpenConfirmation();
                    if (okCallback) {
                        $scope.modelChanged = false;
                        okCallback();
                    }
                    $scope.loadPlant($scope.model.id).then(function () {
                        $scope.saving = false;
                        leftMenuService.clearOpenConfirmation();
                        $scope.modelChanged = false;
                        $scope.originalModel = _.cloneDeep(JSON.parse(angular.toJson($scope.model)));
                    })
                }, function (err) {
                    $scope.saving = false
                    alert('Unable to save plant: ' + err)
                });
            }
        }

        $scope.openPlantsList = function () {
            if ($scope.modelChanged === true) {
                var confirm = $mdDialog.confirm()
                    .title('Přejít na seznam rostlin')
                    .textContent('Rostlina obsahuje neuložené změny. Přejít na seznam rostlin?')
                    .ariaLabel('Přejít na seznam rostlin')
                    .ok('Ano')
                    .cancel('Ne');
                $mdDialog.show(confirm).then(function () {
                    leftMenuService.clearOpenConfirmation();
                    $location.url('/plants/')
                }, function () {
                });
            } else {
                $location.url('/plants/')
            }
        }
        $scope.opnRegistryLink = function (link) {
            if ($scope.modelChanged === true) {
                var confirm = $mdDialog.confirm()
                    .title('Otevřít registr')
                    .textContent('Rostlina obsahuje neuložené změny. Přejít na registr?')
                    .ariaLabel('Otevřít registr')
                    .ok('Ano')
                    .cancel('Ne');

                $mdDialog.show(confirm).then(function () {
                    $window.open(link, '_blank');
                }, function () {
                });
            } else {
                $window.open(link, '_blank');
            }
        }
        $scope.cancel = function () {
            $scope.openPlantsList();
        }
        $scope.getSaveLabel = function () {
            if ($scope.isNew) {
                return $scope.saving ? 'Vytvářím...' : 'Vytvořit'
            } else {
                return $scope.saving ? 'Ukládám...' : 'Uložit'
            }
        }

        $scope.getSaveAndCloseLabel = function () {
            if ($scope.isNew) {
                return $scope.saving ? 'Vytvážím...' : 'Vytvořit a zavřít'
            } else {
                return $scope.saving ? 'Ukládám...' : 'Uložit a zavřít'
            }
        }

        $scope.getHhMmSs = function(seconds) {
            var h = Math.floor(seconds / 3600);
            var m = Math.floor(seconds % 3600 / 60);
            var s = Math.floor(seconds % 3600 % 60);

            return (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s);
        }

        $scope.togglePlayAudioCz = function () {
            if ($scope.audio_cz_timer) {
                $interval.cancel($scope.audio_cz_timer);
            }
            $scope.audio_cz_timer = $interval(function () {
                $scope.audio_cz_progress = $scope.getHhMmSs($scope.sound_cz.currentTime);
                $scope.audio_cz_total = $scope.getHhMmSs($scope.sound_cz.currentTime + $scope.sound_cz.remaining);
            }, 100);

            if ($scope.audio_cz_status && $scope.audio_cz_status === 'playing') {
                $interval.cancel($scope.audio_cz_timer);
                $scope.sound_cz.pause();
                $scope.audio_cz_status = 'pause';
                return;
            }
            $scope.sound_cz.play();
            $scope.audio_cz_status = 'playing';
        }

        $scope.resetAudioCzPlay = function () {
            $interval.cancel($scope.audio_cz_timer);
            $scope.sound_cz.stop();
            $scope.audio_cz_status = null;
        }

        $scope.loadPlant = function (plantId) {
            return plantsService.getPlant(plantId, function (plantModel) {
                delete plantModel.$promise;
                delete plantModel.$resolved;
                plantModel.delete_blocks = [];
                var maxId = 0
                for (var i = 0; i < plantModel.markers.length; i++) {
                    if (plantModel.markers[i].id > maxId) {
                        maxId = plantModel.markers[i].id
                    }
                }
                $scope.markersIdCounter = maxId
                $scope.model = plantModel;
                if ($scope.model.audio_cz_url) {
                    var audioUrl = $scope.model.audio_cz_url.replace(/\{\$BASE_URL\}/, '//' + $scope.settings.baseUrl.replace(/\/$/, ''));
                    console.log('Audio url ', audioUrl);
                    $scope.sound_cz = ngAudio.load(audioUrl);
                }
            });
        }


    }
]);
angular.module('adminApp').controller('infoWindowCtrl', function ($scope, $rootScope) {
    $scope.markerNote = ''
    $scope.deleteMarker = function () {
        console.log($scope.$parent.model.id) //marker id fucker!
        $rootScope.$emit('marker_deleted', {markerId: $scope.$parent.model.id})
    }
});

'use strict'
angular.module('adminApp').directive('plantEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'plants/plantEdit.template.html',
		controller: 'plantEditController'
	};
});

'use strict';
angular.module('adminApp').controller('plantsListController', ['$scope', '$log', '$location', '$mdDialog', '$mdMedia', 'articlesService', '$timeout', 'uiGmapIsReady', 'plantsService', 'settingsService', 'tableService', '$window', 'authorsService',
    function ($scope, $log, $location, $mdDialog, $mdMedia, articlesService, $timeout, uiGmapIsReady, plantsService, settingsService, tableService, $window, authorsService) {
        $scope.settings = []
        settingsService.load(function (data) {
            $scope.settings = data
        })
        $scope.visibleMarkers = []
        $scope.mapObject
        $scope.filter = {
            category: undefined,
            family: undefined,
            tags: [],
            search: undefined,
            authors: [],
            authors_filter: 'any',
            expanded: false,
            hasTextBlocks: false
        }
        $scope.visibleColumns = {
            name_lat: true,
            name: true,
            authors: false,
            family_lat: true,
            family: true,
            category: true,
            tags: true,
            published: true
        }
        if ($window.localStorage.getItem('plants-table-visible-columns') !== null && $window.localStorage.getItem('plants-table-visible-columns') !== undefined && $window.localStorage.getItem('plants-table-visible-columns') !== '') {
            try {
                $scope.visibleColumns = angular.fromJson($window.localStorage.getItem('plants-table-visible-columns'))
            } catch (e) {
                $log.debug('Unable to get visible columns. Using default');
            }
        }

        $scope.authors = [];
        $scope.categories = [];
        $scope.families = [];
        plantsService.getCategories(function (categories) {
            $scope.categories = categories
        })
        plantsService.onPlantTagRemoved($scope, function (event) {
            $scope.getPlants()
        });
        authorsService.onAllAuthorsLoaded($scope, function (event, authors) {
            $scope.authors = authors;
        });
        $scope.openMenu = function ($mdOpenMenu, ev) {
            $mdOpenMenu(ev);
        };
        $scope.updateVisibleColumns = function () {
            $window.localStorage.setItem('plants-table-visible-columns', angular.toJson($scope.visibleColumns));
        }
        $scope.editTag = function (ev, tag) {
            $mdDialog.show({
                controller: 'editTagDialogController',
                templateUrl: 'plants/editTagDialog.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                resolve: {
                    tag: function () {
                        return angular.copy(tag)
                    },
                },
                clickOutsideToClose: true,
                fullscreen: true
            })
                .then(function () {
                    console.log('tags saved!')
                    $scope.getPlants()
                }, function () {
                    console.log('You cancelled the dialog.');
                });
        }
        $scope.authorIsSelected = function(id) {
            if ($scope.filter.authors === undefined) {
                return false;
            }
            return $scope.filter.authors.indexOf(parseInt(id)) > -1;
        }
        $scope.removeTag = function (plant, tag) {
            var confirm = $mdDialog.confirm()
                .title('Remove tag')
                .textContent('Do you really want to remove this tag?')
                .ariaLabel('Remove')
                .ok('Yes')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                plantsService.removeTag(plant.id, tag.id)
            }, function () {
            });
        }
        $scope.tableTagDialog = function (ev, isAdd) {
            var tags = []
            for (var i = 0; i < $scope.tableModel.selected.length; i++) {
                if ($scope.tableModel.selected[i].tags === undefined) {
                    continue
                }
                for (var j = 0; j < $scope.tableModel.selected[i].tags.length; j++) {
                    var found = 0
                    if (tags.length > 0) {
                        for (var k = 0; k < tags.length; k++) {
                            console.log($scope.tableModel.selected[i].tags[j])
                            //console.log('matching ' + tags[j].id + ' and ' + $scope.tableModel.selected[i].tags[j].id)
                            if (tags[k].id === $scope.tableModel.selected[i].tags[j].id) {
                                found = 1
                                break
                            }
                        }
                    }
                    if (!found) {
                        tags.push($scope.tableModel.selected[i].tags[j])
                    }
                }
            }
            $mdDialog.show({
                controller: 'tableTagDialogController',
                templateUrl: 'plants/tableTagDialog.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                resolve: {
                    isAdd: function () {
                        return isAdd
                    },
                    allTags: function () {
                        return $scope.allTags
                    },
                    tags: function () {
                        return tags
                    },
                    selectedPlants: function () {
                        return $scope.tableModel.selected
                    }
                },
                clickOutsideToClose: true,
                fullscreen: true
            })
                .then(function () {
                    console.log('tags saved!')
                    $scope.tableModel.selected = []
                    $scope.getPlants() //TODO: preserve selection
                }, function () {
                    console.log('You cancelled the dialog.');
                });
        }
        $scope.allTags = []
        $scope.reloadTags = function (okCallback) {
            plantsService.getAllTags(function (allTags) {
                // console.log('tags loaded')
                $scope.allTags = allTags
                if (okCallback !== undefined) {
                    okCallback()
                }
            }, function (err) {
                console.log('Error while loading tags:')
                console.log(err)
            })
        }
        $scope.reloadTags()
        $scope.filterSelected = true;
        $scope.tagQuerySearch = function (criteria) {
            // console.log($scope.allTags)
            return $scope.allTags.filter($scope.createFilterFor(criteria))
        }

        var specialCharacters = "";
        specialCharacters += "Latin-1 Supplement\n";
        specialCharacters += "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ\n";
        specialCharacters += "Latin Extended-A\n";
        specialCharacters += "ĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſ\n";
        specialCharacters += "Latin Extended-B\n";
        specialCharacters += "ƒǺǻǼǽǾǿ\n";
        specialCharacters += "Latin Extended Additional\n";
        specialCharacters += "ẀẁẂẃẄẅỲỳ\n";


        var defaultDiacriticsRemovalMap = [{
            'base': "A",
            'letters': /(&#65;|&#9398;|&#65313;|&#192;|&#193;|&#194;|&#7846;|&#7844;|&#7850;|&#7848;|&#195;|&#256;|&#258;|&#7856;|&#7854;|&#7860;|&#7858;|&#550;|&#480;|&#196;|&#478;|&#7842;|&#197;|&#506;|&#461;|&#512;|&#514;|&#7840;|&#7852;|&#7862;|&#7680;|&#260;|&#570;|&#11375;|[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F])/g
        }, {
            'base': "AA",
            'letters': /(&#42802;|[\uA732])/g
        }, {
            'base': "AE",
            'letters': /(&#198;|&#508;|&#482;|[\u00C6\u01FC\u01E2])/g
        }, {
            'base': "AO",
            'letters': /(&#42804;|[\uA734])/g
        }, {
            'base': "AU",
            'letters': /(&#42806;|[\uA736])/g
        }, {
            'base': "AV",
            'letters': /(&#42808;|&#42810;|[\uA738\uA73A])/g
        }, {
            'base': "AY",
            'letters': /(&#42812;|[\uA73C])/g
        }, {
            'base': "B",
            'letters': /(&#66;|&#9399;|&#65314;|&#7682;|&#7684;|&#7686;|&#579;|&#386;|&#385;|[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181])/g
        }, {
            'base': "C",
            'letters': /(&#67;|&#9400;|&#65315;|&#262;|&#264;|&#266;|&#268;|&#199;|&#7688;|&#391;|&#571;|&#42814;|[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E])/g
        }, {
            'base': "D",
            'letters': /(&#68;|&#9401;|&#65316;|&#7690;|&#270;|&#7692;|&#7696;|&#7698;|&#7694;|&#272;|&#395;|&#394;|&#393;|&#42873;|&#208;|[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779\u00D0])/g
        }, {
            'base': "DZ",
            'letters': /(&#497;|&#452;|[\u01F1\u01C4])/g
        }, {
            'base': "Dz",
            'letters': /(&#498;|&#453;|[\u01F2\u01C5])/g
        }, {
            'base': "E",
            'letters': /(&#69;|&#9402;|&#65317;|&#200;|&#201;|&#202;|&#7872;|&#7870;|&#7876;|&#7874;|&#7868;|&#274;|&#7700;|&#7702;|&#276;|&#278;|&#203;|&#7866;|&#282;|&#516;|&#518;|&#7864;|&#7878;|&#552;|&#7708;|&#280;|&#7704;|&#7706;|&#400;|&#398;|[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E])/g
        }, {
            'base': "F",
            'letters': /(&#70;|&#9403;|&#65318;|&#7710;|&#401;|&#42875;|[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B])/g
        }, {
            'base': "G",
            'letters': /(&#71;|&#9404;|&#65319;|&#500;|&#284;|&#7712;|&#286;|&#288;|&#486;|&#290;|&#484;|&#403;|&#42912;|&#42877;|&#42878;|[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E])/g
        }, {
            'base': "H",
            'letters': /(&#72;|&#9405;|&#65320;|&#292;|&#7714;|&#7718;|&#542;|&#7716;|&#7720;|&#7722;|&#294;|&#11367;|&#11381;|&#42893;|[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D])/g
        }, {
            'base': "I",
            'letters': /(&#73;|&#9406;|&#65321;|&#204;|&#205;|&#206;|&#296;|&#298;|&#300;|&#304;|&#207;|&#7726;|&#7880;|&#463;|&#520;|&#522;|&#7882;|&#302;|&#7724;|&#407;|[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197])/g
        }, {
            'base': "J",
            'letters': /(&#74;|&#9407;|&#65322;|&#308;|&#584;|[\u004A\u24BF\uFF2A\u0134\u0248])/g
        }, {
            'base': "K",
            'letters': /(&#75;|&#9408;|&#65323;|&#7728;|&#488;|&#7730;|&#310;|&#7732;|&#408;|&#11369;|&#42816;|&#42818;|&#42820;|&#42914;|[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2])/g
        }, {
            'base': "L",
            'letters': /(&#76;|&#9409;|&#65324;|&#319;|&#313;|&#317;|&#7734;|&#7736;|&#315;|&#7740;|&#7738;|&#321;|&#573;|&#11362;|&#11360;|&#42824;|&#42822;|&#42880;|[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780])/g
        }, {
            'base': "LJ",
            'letters': /(&#455;|[\u01C7])/g
        }, {
            'base': "Lj",
            'letters': /(&#456;|[\u01C8])/g
        }, {
            'base': "M",
            'letters': /(&#77;|&#9410;|&#65325;|&#7742;|&#7744;|&#7746;|&#11374;|&#412;|[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C])/g
        }, {
            'base': "N",
            'letters': /(&#78;|&#9411;|&#65326;|&#504;|&#323;|&#209;|&#7748;|&#327;|&#7750;|&#325;|&#7754;|&#7752;|&#544;|&#413;|&#42896;|&#42916;|&#330;|[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4\u014A])/g
        }, {
            'base': "NJ",
            'letters': /(&#458;|[\u01CA])/g
        }, {
            'base': "Nj",
            'letters': /(&#459;|[\u01CB])/g
        }, {
            'base': "O",
            'letters': /(&#79;|&#9412;|&#65327;|&#210;|&#211;|&#212;|&#7890;|&#7888;|&#7894;|&#7892;|&#213;|&#7756;|&#556;|&#7758;|&#332;|&#7760;|&#7762;|&#334;|&#558;|&#560;|&#214;|&#554;|&#7886;|&#336;|&#465;|&#524;|&#526;|&#416;|&#7900;|&#7898;|&#7904;|&#7902;|&#7906;|&#7884;|&#7896;|&#490;|&#492;|&#216;|&#510;|&#390;|&#415;|&#42826;|&#42828;|[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C])/g
        }, {
            'base': "OE",
            'letters': /(&#338;|[\u0152])/g
        }, {
            'base': "OI",
            'letters': /(&#418;|[\u01A2])/g
        }, {
            'base': "OO",
            'letters': /(&#42830;|[\uA74E])/g
        }, {
            'base': "OU",
            'letters': /(&#546;|[\u0222])/g
        }, {
            'base': "P",
            'letters': /(&#80;|&#9413;|&#65328;|&#7764;|&#7766;|&#420;|&#11363;|&#42832;|&#42834;|&#42836;|[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754])/g
        }, {
            'base': "Q",
            'letters': /(&#81;|&#9414;|&#65329;|&#42838;|&#42840;|&#586;|[\u0051\u24C6\uFF31\uA756\uA758\u024A])/g
        }, {
            'base': "R",
            'letters': /(&#82;|&#9415;|&#65330;|&#340;|&#7768;|&#344;|&#528;|&#530;|&#7770;|&#7772;|&#342;|&#7774;|&#588;|&#11364;|&#42842;|&#42918;|&#42882;|[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782])/g
        }, {
            'base': "S",
            'letters': /(&#83;|&#9416;|&#65331;|&#7838;|&#346;|&#7780;|&#348;|&#7776;|&#352;|&#7782;|&#7778;|&#7784;|&#536;|&#350;|&#11390;|&#42920;|&#42884;|[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784])/g
        }, {
            'base': "T",
            'letters': /(&#84;|&#9417;|&#65332;|&#7786;|&#356;|&#7788;|&#538;|&#354;|&#7792;|&#7790;|&#358;|&#428;|&#430;|&#574;|&#42886;|[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786])/g
        }, {
            'base': "TH",
            'letters': /(&#222;|[\u00DE])/g
        }, {
            'base': "TZ",
            'letters': /(&#42792;|[\uA728])/g
        }, {
            'base': "U",
            'letters': /(&#85;|&#9418;|&#65333;|&#217;|&#218;|&#219;|&#360;|&#7800;|&#362;|&#7802;|&#364;|&#220;|&#475;|&#471;|&#469;|&#473;|&#7910;|&#366;|&#368;|&#467;|&#532;|&#534;|&#431;|&#7914;|&#7912;|&#7918;|&#7916;|&#7920;|&#7908;|&#7794;|&#370;|&#7798;|&#7796;|&#580;|[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244])/g
        }, {
            'base': "V",
            'letters': /(&#86;|&#9419;|&#65334;|&#7804;|&#7806;|&#434;|&#42846;|&#581;|[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245])/g
        }, {
            'base': "VY",
            'letters': /(&#42848;|[\uA760])/g
        }, {
            'base': "W",
            'letters': /(&#87;|&#9420;|&#65335;|&#7808;|&#7810;|&#372;|&#7814;|&#7812;|&#7816;|&#11378;|[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72])/g
        }, {
            'base': "X",
            'letters': /(&#88;|&#9421;|&#65336;|&#7818;|&#7820;|[\u0058\u24CD\uFF38\u1E8A\u1E8C])/g
        }, {
            'base': "Y",
            'letters': /(&#89;|&#9422;|&#65337;|&#7922;|&#221;|&#374;|&#7928;|&#562;|&#7822;|&#376;|&#7926;|&#7924;|&#435;|&#590;|&#7934;|[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE])/g
        }, {
            'base': "Z",
            'letters': /(&#90;|&#9423;|&#65338;|&#377;|&#7824;|&#379;|&#381;|&#7826;|&#7828;|&#437;|&#548;|&#11391;|&#11371;|&#42850;|[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762])/g
        }, {
            'base': "a",
            'letters': /(&#97;|&#9424;|&#65345;|&#7834;|&#224;|&#225;|&#226;|&#7847;|&#7845;|&#7851;|&#7849;|&#227;|&#257;|&#259;|&#7857;|&#7855;|&#7861;|&#7859;|&#551;|&#481;|&#228;|&#479;|&#7843;|&#229;|&#507;|&#462;|&#513;|&#515;|&#7841;|&#7853;|&#7863;|&#7681;|&#261;|&#11365;|&#592;|[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250])/g
        }, {
            'base': "aa",
            'letters': /(&#42803;|[\uA733])/g
        }, {
            'base': "ae",
            'letters': /(&#230;|&#509;|&#483;|[\u00E6\u01FD\u01E3])/g
        }, {
            'base': "ao",
            'letters': /(&#42805;|[\uA735])/g
        }, {
            'base': "au",
            'letters': /(&#42807;|[\uA737])/g
        }, {
            'base': "av",
            'letters': /(&#42809;|&#42811;|[\uA739\uA73B])/g
        }, {
            'base': "ay",
            'letters': /(&#42813;|[\uA73D])/g
        }, {
            'base': "b",
            'letters': /(&#98;|&#9425;|&#65346;|&#7683;|&#7685;|&#7687;|&#384;|&#387;|&#595;|[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253])/g
        }, {
            'base': "c",
            'letters': /(&#99;|&#9426;|&#65347;|&#263;|&#265;|&#267;|&#269;|&#231;|&#7689;|&#392;|&#572;|&#42815;|&#8580;|[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184])/g
        }, {
            'base': "d",
            'letters': /(&#100;|&#9427;|&#65348;|&#7691;|&#271;|&#7693;|&#7697;|&#7699;|&#7695;|&#273;|&#396;|&#598;|&#599;|&#42874;|&#240;|[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A\u00F0])/g
        }, {
            'base': "dz",
            'letters': /(&#499;|&#454;|[\u01F3\u01C6])/g
        }, {
            'base': "e",
            'letters': /(&#101;|&#9428;|&#65349;|&#232;|&#233;|&#234;|&#7873;|&#7871;|&#7877;|&#7875;|&#7869;|&#275;|&#7701;|&#7703;|&#277;|&#279;|&#235;|&#7867;|&#283;|&#517;|&#519;|&#7865;|&#7879;|&#553;|&#7709;|&#281;|&#7705;|&#7707;|&#583;|&#603;|&#477;|[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD])/g
        }, {
            'base': "f",
            'letters': /(&#102;|&#9429;|&#65350;|&#7711;|&#402;|&#42876;|[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C])/g
        }, {
            'base': "g",
            'letters': /(&#103;|&#9430;|&#65351;|&#501;|&#285;|&#7713;|&#287;|&#289;|&#487;|&#291;|&#485;|&#608;|&#42913;|&#7545;|&#42879;|[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F])/g
        }, {
            'base': "h",
            'letters': /(&#104;|&#9431;|&#65352;|&#293;|&#7715;|&#7719;|&#543;|&#7717;|&#7721;|&#7723;|&#7830;|&#295;|&#11368;|&#11382;|&#613;|[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265])/g
        }, {
            'base': "hv",
            'letters': /(&#405;|[\u0195])/g
        }, {
            'base': "i",
            'letters': /(&#105;|&#9432;|&#65353;|&#236;|&#237;|&#238;|&#297;|&#299;|&#301;|&#239;|&#7727;|&#7881;|&#464;|&#521;|&#523;|&#7883;|&#303;|&#7725;|&#616;|&#305;|[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131])/g
        }, {
            'base': "ij",
            'letters': /(&#307;|[\u0133])/g
        }, {
            'base': "j",
            'letters': /(&#106;|&#9433;|&#65354;|&#309;|&#496;|&#585;|[\u006A\u24D9\uFF4A\u0135\u01F0\u0249])/g
        }, {
            'base': "k",
            'letters': /(&#107;|&#9434;|&#65355;|&#7729;|&#489;|&#7731;|&#311;|&#7733;|&#409;|&#11370;|&#42817;|&#42819;|&#42821;|&#42915;|[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3])/g
        }, {
            'base': "l",
            'letters': /(&#108;|&#9435;|&#65356;|&#320;|&#314;|&#318;|&#7735;|&#7737;|&#316;|&#7741;|&#7739;|&#322;|&#410;|&#619;|&#11361;|&#42825;|&#42881;|&#42823;|[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u0142\u019A\u026B\u2C61\uA749\uA781\uA747])/g
        }, {
            'base': "lj",
            'letters': /(&#457;|[\u01C9])/g
        }, {
            'base': "m",
            'letters': /(&#109;|&#9436;|&#65357;|&#7743;|&#7745;|&#7747;|&#625;|&#623;|[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F])/g
        }, {
            'base': "n",
            'letters': /(&#110;|&#9437;|&#65358;|&#505;|&#324;|&#241;|&#7749;|&#328;|&#7751;|&#326;|&#7755;|&#7753;|&#414;|&#626;|&#329;|&#42897;|&#42917;|&#331;|[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5\u014B])/g
        }, {
            'base': "nj",
            'letters': /(&#460;|[\u01CC])/g
        }, {
            'base': "o",
            'letters': /(&#111;|&#9438;|&#65359;|&#242;|&#243;|&#244;|&#7891;|&#7889;|&#7895;|&#7893;|&#245;|&#7757;|&#557;|&#7759;|&#333;|&#7761;|&#7763;|&#335;|&#559;|&#561;|&#246;|&#555;|&#7887;|&#337;|&#466;|&#525;|&#527;|&#417;|&#7901;|&#7899;|&#7905;|&#7903;|&#7907;|&#7885;|&#7897;|&#491;|&#493;|&#248;|&#511;|&#596;|&#42827;|&#42829;|&#629;|[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275])/g
        }, {
            'base': "oe",
            'letters': /(&#339;|[\u0153])/g
        }, {
            'base': "oi",
            'letters': /(&#419;|[\u01A3])/g
        }, {
            'base': "ou",
            'letters': /(&#547;|[\u0223])/g
        }, {
            'base': "oo",
            'letters': /(&#42831;|[\uA74F])/g
        }, {
            'base': "p",
            'letters': /(&#112;|&#9439;|&#65360;|&#7765;|&#7767;|&#421;|&#7549;|&#42833;|&#42835;|&#42837;|[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755])/g
        }, {
            'base': "q",
            'letters': /(&#113;|&#9440;|&#65361;|&#587;|&#42839;|&#42841;|[\u0071\u24E0\uFF51\u024B\uA757\uA759])/g
        }, {
            'base': "r",
            'letters': /(&#114;|&#9441;|&#65362;|&#341;|&#7769;|&#345;|&#529;|&#531;|&#7771;|&#7773;|&#343;|&#7775;|&#589;|&#637;|&#42843;|&#42919;|&#42883;|[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783])/g
        }, {
            'base': "s",
            'letters': /(&#115;|&#9442;|&#65363;|&#347;|&#7781;|&#349;|&#7777;|&#353;|&#7783;|&#7779;|&#7785;|&#537;|&#351;|&#575;|&#42921;|&#42885;|&#7835;|&#383;|[\u0073\u24E2\uFF53\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B\u017F])/g
        }, {
            'base': "ss",
            'letters': /(&#223;|[\u00DF])/g
        }, {
            'base': "t",
            'letters': /(&#116;|&#9443;|&#65364;|&#7787;|&#7831;|&#357;|&#7789;|&#539;|&#355;|&#7793;|&#7791;|&#359;|&#429;|&#648;|&#11366;|&#42887;|[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787])/g
        }, {
            'base': "th",
            'letters': /(&#254;|[\u00FE])/g
        }, {
            'base': "tz",
            'letters': /(&#42793;|[\uA729])/g
        }, {
            'base': "u",
            'letters': /(&#117;|&#9444;|&#65365;|&#249;|&#250;|&#251;|&#361;|&#7801;|&#363;|&#7803;|&#365;|&#252;|&#476;|&#472;|&#470;|&#474;|&#7911;|&#367;|&#369;|&#468;|&#533;|&#535;|&#432;|&#7915;|&#7913;|&#7919;|&#7917;|&#7921;|&#7909;|&#7795;|&#371;|&#7799;|&#7797;|&#649;|[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289])/g
        }, {
            'base': "v",
            'letters': /(&#118;|&#9445;|&#65366;|&#7805;|&#7807;|&#651;|&#42847;|&#652;|[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C])/g
        }, {
            'base': "vy",
            'letters': /(&#42849;|[\uA761])/g
        }, {
            'base': "w",
            'letters': /(&#119;|&#9446;|&#65367;|&#7809;|&#7811;|&#373;|&#7815;|&#7813;|&#7832;|&#7817;|&#11379;|[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73])/g
        }, {
            'base': "x",
            'letters': /(&#120;|&#9447;|&#65368;|&#7819;|&#7821;|[\u0078\u24E7\uFF58\u1E8B\u1E8D])/g
        }, {
            'base': "y",
            'letters': /(&#121;|&#9448;|&#65369;|&#7923;|&#253;|&#375;|&#7929;|&#563;|&#7823;|&#255;|&#7927;|&#7833;|&#7925;|&#436;|&#591;|&#7935;|[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF])/g
        }, {
            'base': "z",
            'letters': /(&#122;|&#9449;|&#65370;|&#378;|&#7825;|&#380;|&#382;|&#7827;|&#7829;|&#438;|&#549;|&#576;|&#11372;|&#42851;|[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763])/g
        }];

        $scope.removeDiacritics = function (str) {
            for (var i = 0; i < defaultDiacriticsRemovalMap.length; i++) {
                str = str.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);
            }
            return str;
        }

        $scope.createFilterFor = function (query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(tag) {
                // console.log(tag.name_lowercase + ', pos:' + tag.name_lowercase.indexOf(lowercaseQuery))
                return (tag.name_lowercase.indexOf(lowercaseQuery) != -1) || $scope.removeDiacritics(tag.name_lowercase).indexOf(lowercaseQuery) != -1;
                ;
            }
        }

        plantsService.getFamilies(function (families) {
            $scope.families = families
        })
        $scope.markers = []
        if ($window.localStorage.getItem('plants-table-model') !== null && $window.localStorage.getItem('plants-table-model') !== undefined && $window.localStorage.getItem('plants-table-model') !== '') {
            $scope.tableModel = angular.fromJson($window.localStorage.getItem('plants-table-model'));
        } else {
            $scope.tableModel = {
                limitOptions: [5, 10, 15],
                query: {
                    order: 'name_lat',
                    limit: 10,
                    page: 1
                },
                selected: []
            }
            $window.localStorage.setItem('plants-table-model', angular.toJson($scope.tableModel));
        }
        $scope.onTableModelCahnged = function () {
            $window.localStorage.setItem('plants-table-model', angular.toJson($scope.tableModel));
        }
        $scope.plants = []

        $scope.getExpandButtonText = function () {
            return $scope.filter.expanded ? "less" : "more...";
        }
        $scope.toggleExpandFilter = function () {
            $scope.filter.expanded = !$scope.filter.expanded;
            console.log($scope.filter)
            $window.localStorage.setItem('plants-filter', angular.toJson($scope.filter));
        }
        $scope.editFamily = function (ev, familyId) {
            var family
            for (var i = 0; i < $scope.families.length; i++) {
                if ($scope.families[i].id == familyId) {
                    family = $scope.families[i];
                    break;
                }
            }
            $log.debug('opening dialog for ', family)
            $mdDialog.show({
                controller: 'editFamilyDialogController',
                templateUrl: 'plants/editFamilyDialog.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                resolve: {
                    family: function () {
                        return family
                    }
                },
                clickOutsideToClose: true,
                fullscreen: true
            })
                .then(function () {
                    console.log('family saved!')
                    plantsService.getFamilies(function (families) {
                        $scope.families = families
                        $scope.getPlants()
                    })
                }, function () {
                    console.log('You cancelled the dialog.');
                });
        }
        $scope.editCategory = function (ev, categoryId) {
            var category
            for (var i = 0; i < $scope.categories.length; i++) {
                if ($scope.categories[i].id == categoryId) {
                    category = $scope.categories[i];
                    break;
                }
            }
            $log.debug('opening dialog for ', category)
            $mdDialog.show({
                controller: 'editCategoryDialogController',
                templateUrl: 'plants/editCategoryDialog.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                resolve: {
                    category: function () {
                        return category
                    }
                },
                clickOutsideToClose: true,
                fullscreen: true
            })
                .then(function () {
                    console.log('category saved!')
                    plantsService.getCategories(function (categories) {
                        $scope.categories = categories
                        $scope.getPlants()
                    })
                }, function () {
                    console.log('You cancelled the dialog.');
                });
        }
        $scope.getPlants = function () {
            // console.log($scope.tableModel.query)
            var filter = angular.copy($scope.filter)
            if (!$scope.filter.expanded) {
                filter.category = undefined
                filter.family = undefined
                filter.is_blooming = false
                filter.is_growing = false
            }
            $scope.model.promise = plantsService.getAllPlants($scope.tableModel.query, filter, function (markers, rows) {
                $scope.markers = markers
                console.log('save filter')
                $window.localStorage.setItem("plants-filter", angular.toJson($scope.filter))
                $scope.redrawMrkers()
                $scope.plants = []
                for (var i = 0; i < rows.length; i++) {
                    rows[i].is_blooming = rows[i].is_blooming === '1' ? true : false
                    rows[i].is_growing = rows[i].is_growing === '1' ? true : false
                    $scope.plants.push(rows[i])
                }
            }, function (err) {
                $log.log('Unable to search in plants')
                $log.log(err)
                alert('Unable to search in plants. Try again later')
            }).$promise;
        }
        if ($window.localStorage.getItem("plants-filter") !== undefined && $window.localStorage.getItem("plants-filter") !== null && $window.localStorage.getItem("plants-filter") !== '') {
            try {
                $scope.filter = angular.fromJson($window.localStorage.getItem("plants-filter"))
                // console.log($scope.filter)
                console.log('filter loaded')
            } catch (e) {
                console.log('load filter failed')
                console.log(e)
            }
        }
        $scope.getPlants();
        authorsService.loadAll();

        $scope.search = function () {
            //$window.localStorage.setItem('plants-filter', angular.toJson($scope.filter));
            $scope.getPlants()
        }
        $scope.onSearchKeyPress = function ($event) {
            //console.log($event)
            if ($event.type === 'keypress' && $event.keyCode === 13) {
                $scope.search()
            }
        }

        $scope.editPlant = function (plantId) {
            $location.path('/plants/' + plantId)
        }

        $scope.deletePlant = function (plantId, ev) {
            // console.log('want delete ' + plantId)
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this plant?')
                .ariaLabel('Delete')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function () {
                plantsService.deletePlant(plantId, function (data) {
                    $scope.getPlants()
                }, function (err) {
                    //err
                })
            }, function () {
            });
        }

        $scope.filterSelected = true

        $scope.map = {
            markerOptions: {
                draggable: false,
            },
            center: {
                latitude: 0,
                longitude: 0
            },
            zoom: 18,
            bounds: {
                northeast: {
                    latitude: 50.0689551446,
                    longitude: 14.4179141521
                },
                southwest: {
                    latitude: 50.0733622808,
                    longitude: 14.4247806072
                }
            },
            events: {
                click: function (mapModel, eventName, originalEventArgs) {
                    $scope.$apply(function () {
                        var e = originalEventArgs[0];
                        // console.log('la:' + e.latLng.lat() + ',lo:' + e.latLng.lng())
                        // console.log(e)
                        // var e = originalEventArgs[0];
                        // console.log('latitude:' + e.latLng.lat() + ', longitude:' + e.latLng.lng())
                    });
                },
                markerEvents: {
                    dragend: function (markerModel, eventName, markerObject) {
                        // console.log(markerObject.name)
                        // var newId = 1
                        // $scope.model.markers = []
                        // $scope.model.markers.push({latitude: markerModel.position.lat(), longitude: markerModel.position.lng(), id: newId, title: oldName })
                    }
                }
            }
        }
        console.log($scope.map);
        $scope.clearFilter = function () {
            $scope.filter = {
                category: undefined,
                family: undefined,
                is_blooming: false,
                is_growing: false,
                search: undefined,
                tags: []
            }
            $window.localStorage.setItem('plants-filter', angular.toJson($scope.filter));
            $scope.getPlants()
        }

        $scope.getNormalizedCoord = function (coord, zoom) {
            var y = coord.y;
            var x = coord.x;

            // tile range in one direction range is dependent on zoom level
            // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
            var tileRange = 1 << zoom;

            // don't repeat across y-axis (vertically)
            if (y < 0 || y >= tileRange) {
                return null;
            }

            // repeat across x-axis
            if (x < 0 || x >= tileRange) {
                x = (x % tileRange + tileRange) % tileRange;
            }

            return {
                x: x,
                y: y
            };
        }
        //
        // $scope.tile2lon(x, z) {
        //             return x / Math.pow(2, z) * 360.0 - 180
        //         }
        //
        //         $scope.tile2lat(y, z) {
        //             double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z)
        //             return (Math.atan(Math.sinh(n))) * 180 / Math.PI
        //         }

        $scope.initMap = function () {
            $scope.moonMapType = new google.maps.ImageMapType({
                getTileUrl: function (coord, zoom) {
                    // console.log(getProjection())
                    var normalizedCoord = $scope.getNormalizedCoord(coord, zoom);
                    if (!normalizedCoord) {
                        return null;
                    }

                    var bound = Math.pow(2, zoom);
                    var url = '//' + $scope.settings.baseUrl + 'api/map2/' + zoom + '/' + normalizedCoord.x + '/' + normalizedCoord.y + '.png'
                    return url;
                },
                tileSize: new google.maps.Size(256, 256),
                maxZoom: 18,
                minZoom: 17,
                radius: 1738000,
                name: 'Moon'
            })

        }

        $scope.print = function () {
            // console.log('printing...')
            tableService.save('plantsList', $scope.filter, function (filterCode) {
                // console.log('We have filter ' + filterCode)
                $window.open('http://' + $scope.settings.baseUrl + 'plants/print/' + filterCode, '_blank');
            })
        }

        $scope.addPlant = function () {
            $location.url('/plants/_')
        }

        plantsService.onPlantSaved($scope, function (evnt, savedPlant) {
            $scope.getPlants()
        })


        $scope.publishPlant = function (plant) {
            plant.is_active = !plant.is_active
            //plantsService.updateOneAndSendEvent(plant)
            plantsService.publishOnePlant(plant.id, plant.is_active).then(function (event) {
                $scope.getPlants()
            }).catch(function (err) {
                $log.error(err);
                alert('Plant publishing/unpublishing failed: ' + err.message);
            })
        }

        $scope.updateAfterDrag = function (map, posA, posB) {
            var zoom = map.getZoom();
            var sw = map.getProjection().fromLatLngToPoint(posA);
            var ne = map.getProjection().fromLatLngToPoint(posB);
            console.log('============================')
            // console.log(sw)
            // console.log(ne)

            var initialResolution = 2 * Math.PI * 6378137 / 256; // == 156543.0339
            var originShift = 2 * Math.PI * 6378137 / 2.0; // == 20037508.34
            var res = 256 / (256 * Math.pow(2, zoom));

            function x2m(x) {
                return x * initialResolution - originShift
            };
            var img = {
                w: 1024,
                h: 950
            }
            //gdal_translate -of VRT -a_srs EPSG:3857 -gcp 0 0 1605139.9726388417 6458860.813369108  -gcp 9547 0 1605656.5197440498 6458860.813369108 -gcp 9547 8631 1605656.5197440498 6458439.812549225 map.png coords.vrt
            var cmd = 'gdal_translate -of VRT -a_srs EPSG:3857 -gcp 0 0 ' + x2m(sw.x) + ' ' + x2m(ne.y) + ' -gcp ' + img.w + ' 0 ' + x2m(ne.x) + ' ' + x2m(ne.y) + ' -gcp ' + img.w + ' ' + img.h + ' ' + x2m(ne.x) + ' ' + x2m(sw.y) + ' map.png coords.vrt'
            console.log(cmd)
        }
        $scope.showAllTags = function () {
            var tagNames = []
            for (var i = 0; i < $scope.allTags.length; i++) {
                tagNames.push($scope.allTags[i].name);
            }
            alert("Existing tags: \n" + tagNames.join(",\n") + "\n\nNote: this dialog will be nicer in the future")
        }

        //start: map functions
        $scope.redrawMrkers = function () {
            if ($scope.mapObject === undefined) {
                return;
            }
            var infoWindows = []
            if ($scope.visibleMarkers.length > 0) {
                for (var i = 0; i < $scope.visibleMarkers.length; i++) {
                    $scope.visibleMarkers[i].setMap(null);
                }
            }
            $scope.visibleMarkers = []
            for (var i = 0; i < $scope.markers.length; i++) {
                var div = document.createElement('DIV');
                var window_id = i
                div.innerHTML = '<div class="numbered-marker-bubble"><div class="number">' + $scope.markers[i].plant_id + '</div><div class="bottom-left-corner"></div></div>';
                $scope.visibleMarkers[i] = new RichMarker({
                    map: $scope.mapObject,
                    position: new google.maps.LatLng($scope.markers[i].latitude, $scope.markers[i].longitude),
                    draggable: false,
                    flat: true,
                    anchor: RichMarkerPosition.BOTTOM_LEFT,
                    content: div,
                    window_id: window_id
                });
                var contentString = '<div id="content">#' + $scope.markers[i].plant_id
                if ($scope.markers[i].title !== undefined) {
                    contentString += ': ' + $scope.markers[i].title
                    if ($scope.markers[i].title_lat !== undefined) {
                        contentString += '<br/>'
                    }
                }
                if ($scope.markers[i].title_lat !== undefined) {
                    contentString += '<i>' + $scope.markers[i].title_lat + '</i>'
                }
                contentString += "<br/><a href='//" + $scope.settings.baseUrl + 'plants/' + $scope.markers[i].plant_id + "'>edit</a>"
                infoWindows[i] = new google.maps.InfoWindow({
                    content: contentString
                });
                google.maps.event.addListener($scope.visibleMarkers[i], 'click', function (e) {
                    for (var j = 0; j < infoWindows.length; j++) {
                        infoWindows[j].close()
                    }
                    infoWindows[this.window_id].open($scope.mapObject, $scope.visibleMarkers[this.window_id])
                })
            }
        }

        uiGmapIsReady.promise(1).then(function (instances) {
            $.getScript("bower_components/js-rich-marker/src/richmarker-compiled.js", function () {
                //rich labels api ready
            });
            instances.forEach(function (inst) {
                var map = inst.map
                $scope.mapObject = map
                var uuid = map.uiGmap_id
                var mapInstanceNumber = inst.instance // Starts at 1.
                console.log('map ready!')
                $scope.initMap()
                map.overlayMapTypes.push($scope.moonMapType);
                //$scope.mapObject.center($scope.settings.adminMapCenterLat, $scope.settings.adminMapCenterLon)
                $scope.map.center = {
                     latitude: parseFloat($scope.settings.adminMapCenterLat),
                     longitude: parseFloat($scope.settings.adminMapCenterLon)
                };
                console.log($scope.map);

                $scope.redrawMrkers(map);
                return

                // var infoWindows = []
                // var markers = []
                // for (var i = 0; i < $scope.markers.length; i++) {
                //     var div = document.createElement('DIV');
                //     var window_id = i
                //     div.innerHTML = '<div class="numbered-marker-bubble"><div class="number">' + $scope.markers[i].plant_id + '</div><div class="bottom-left-corner"></div></div>';
                //     markers[i] = new RichMarker({
                //         map: map,
                //         position: new google.maps.LatLng($scope.markers[i].latitude, $scope.markers[i].longitude),
                //         draggable: false,
                //         flat: true,
                //         anchor: RichMarkerPosition.BOTTOM_LEFT,
                //         content: div,
                //         window_id: window_id
                //     });
                //     var contentString = '<div id="content">#' + $scope.markers[i].plant_id
                //     if ($scope.markers[i].title !== undefined) {
                //         contentString += ': ' + $scope.markers[i].title
                //         if ($scope.markers[i].title_lat !== undefined) {
                //             contentString += '<br/>'
                //         }
                //     }
                //     if ($scope.markers[i].title_lat !== undefined) {
                //         contentString += '<i>' + $scope.markers[i].title_lat + '</i>'
                //     }
                //     infoWindows[i] = new google.maps.InfoWindow({
                //         content: contentString
                //     });
                //     google.maps.event.addListener(markers[i], 'click', function (e) {
                //         // console.log(this)
                //         infoWindows[this.window_id].open(map, markers[this.window_id])
                //     })
                //     markers[i].addListener('click', function () {
                //         console.log(infoWindows)
                //         console.log(markers[i]['window_id'])
                //         infoWindows[markers[i]['window_id']].open(map, markers[i])
                //     });
                //
                //     // var zzz =  new google.maps.Marker({ //N-E
                //     // 			position: new google.maps.LatLng($scope.markers[i].latitude, $scope.markers[i].longitude),
                //     //       map: map,
                //     //       name: 'B',
                //     // 			draggable:false
                //     // 		});
                // }


                //manual fucking markers!
                //test maps
                $scope.boundA = new google.maps.LatLng(50.06977689061054, 14.419212341308594)
                $scope.markerA = new google.maps.Marker({ //S-W
                    position: $scope.boundA,
                    name: 'A',
                    icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                    map: map,
                    draggable: true
                });
                $scope.boundB = new google.maps.LatLng(50.072700056525385, 14.423895478248596)
                $scope.markerB = new google.maps.Marker({ //N-E
                    position: $scope.boundB,
                    map: map,
                    name: 'B',
                    icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png',
                    draggable: true
                });
                var imageBounds = new google.maps.LatLngBounds(
                    $scope.boundA,
                    $scope.boundB
                );
                $scope.historicalOverlay = new google.maps.GroundOverlay(
                    '//' + $scope.settings.baseUrl + 'api/map2/map_min.png',
                    imageBounds);

                $scope.historicalOverlay.setMap(map)
                //
                google.maps.event.addListener($scope.markerA, 'dragend', function () {
                    var newPointA = $scope.markerA.getPosition();
                    var newPointB = $scope.markerB.getPosition();
                    var newBounds = new google.maps.LatLngBounds(newPointA, newPointB);
                    //$scope.historicalOverlay.updateBounds(newBounds);
                    $scope.historicalOverlay.set("bounds", newBounds);
                    $scope.historicalOverlay.map_changed();
                    $scope.updateAfterDrag(map, newPointA, newPointB)
                });

                google.maps.event.addListener($scope.markerB, 'dragend', function () {
                    var newPointA = $scope.markerA.getPosition();
                    var newPointB = $scope.markerB.getPosition();
                    var newBounds = new google.maps.LatLngBounds(newPointA, newPointB);
                    // $scope.historicalOverlay.updateBounds(newBounds);
                    $scope.historicalOverlay.set("bounds", newBounds);
                    $scope.historicalOverlay.map_changed();
                    $scope.updateAfterDrag(map, newPointA, newPointB)
                });

            })
        })
    }
]);

'use strict';
angular.module('adminApp').directive('plantsList', function() {
	return {
		restrict : 'E',
		templateUrl : "plants/plantsList.template.html",
    controller: "plantsListController"
	};
});

'use strict';
angular.module('adminApp')
    .controller('printPlantsListController', ['$scope', '$location', '$mdDialog', '$mdMedia', 'articlesService', '$timeout', 'uiGmapIsReady', 'plantsService', 'settingsService', 'tableService', '$routeParams', '$log',
        function($scope, $location, $mdDialog, $mdMedia, articlesService, $timeout, uiGmapIsReady, plantsService, settingsService, tableService, $routeParams, $log) {
            $scope.settings = []
            settingsService.load(function(data) {
                $scope.settings = data
            })
            $scope.mapObject
            $scope.filterByMap = true
            $scope.$watch('filterByMap', function(newValue) {
                if (!newValue) {
                    $scope.plants = $scope.allPlants
                } else {
                    $scope.filterPlantsByBounds($scope.mapObject.getBounds())
                }
            })            
            $scope.plants = []
            $scope.tableModel = {}
            $scope.visibleColumns = {
                name_lat: true,
                name: true,
                family_lat: true,
                family: true,
                category: true
            }
            $scope.filter = {
                category: undefined,
                family: undefined,
                tags: [],
                search: undefined
            }
            $scope.openMenu = function($mdOpenMenu, ev) {
                $mdOpenMenu(ev);
            };

            $scope.categories = []
            $scope.families = []
            $scope.getCategoryName = function(categoryId) {
                for (var i = 0; i < $scope.categories.length; i++) {
                    // console.log($scope.categories[i].id)
                    if ($scope.categories[i].id == categoryId) {
                        // console.log('have cat')
                        return $scope.categories[i].name
                    }
                }
            }
            $scope.getFamilyName = function(familyId) {
                for (var i = 0; i < $scope.families.length; i++) {
                    if ($scope.families[i].id == familyId) {
                        return $scope.families[i].name
                    }
                }
            }
            plantsService.getCategories(function(categories) {
                $scope.categories = categories
            })
            plantsService.getFamilies(function(families) {
                $scope.families = families
            })
            $scope.markers = []
            $scope.tableModel = {
                limitOptions: [5, 10, 15],
                query: {
                    order: 'name_lat',
                    limit: 10,
                    page: 1
                },
                selected: []
            }
            $scope.allPlants = []
            $scope.plants = []
            $scope.$watch('tableModel', function() {
                // console.log('CH=====================')
                // console.log($scope.tableModel)
            })

            
            $scope.getPlants = function() {
                var filter = angular.copy($scope.filter)
                if (!$scope.filter.expanded) {
                    filter.category = undefined
                    filter.family = undefined
                    filter.is_blooming = false
                    filter.is_growing = false
                }
                plantsService.getAllPlants($scope.tableModel.query, filter, function(markers, rows) {
                    // console.log(markers)
                    // console.log(rows)
                    $scope.markers = markers
                    $scope.allPlants = angular.copy(rows)
                    $scope.plants = rows             
                })
            }
            $scope.filterPlantsByBounds = function(bounds) {
                //??!?!?
                var visiblePlantIds = []
                for (var i = 0; i < $scope.markers.length; i++) {
                    var pos = new google.maps.LatLng($scope.markers[i].latitude, $scope.markers[i].longitude)
                    if (bounds.contains(pos)) {
                        visiblePlantIds.push(parseInt($scope.markers[i].plant_id))
                    }
                }
                $scope.plants = []
                for (var i = 0; i < $scope.allPlants.length; i++) {
                    var plantId = $scope.allPlants[i].id + 0
                    //console.log(visiblePlantIds)                    
                    if (visiblePlantIds.indexOf(plantId) > 0) {
                        //console.log('found!')
                        $scope.plants.push($scope.allPlants[i])
                    }
                }                
            } 
            tableService.load($routeParams.filterCode, function(filter) {
                $scope.filter = JSON.parse(filter.filter_json)
                // console.log($scope.filter)
                $scope.getPlants()
            })

            $scope.map = {
                markerOptions: {
                    draggable: false,
                },
                markerEvents: {},
                center: {
                    latitude: 0,
                    longitude: 0
                },
                zoom: 18,
                bounds: {
                    northeast: {
                        latitude: 50.0689551446,
                        longitude: 14.4179141521
                    },
                    southwest: {
                        latitude: 50.0733622808,
                        longitude: 14.4247806072
                    }
                }
            }


            $scope.getNormalizedCoord = function(coord, zoom) {
                var y = coord.y;
                var x = coord.x;

                // tile range in one direction range is dependent on zoom level
                // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
                var tileRange = 1 << zoom;

                // don't repeat across y-axis (vertically)
                if (y < 0 || y >= tileRange) {
                    return null;
                }

                // repeat across x-axis
                if (x < 0 || x >= tileRange) {
                    x = (x % tileRange + tileRange) % tileRange;
                }

                return {
                    x: x,
                    y: y
                };
            }

            $scope.initMap = function() {
                $scope.moonMapType = new google.maps.ImageMapType({
                    getTileUrl: function(coord, zoom) {
                        // console.log(getProjection())
                        var normalizedCoord = $scope.getNormalizedCoord(coord, zoom);
                        if (!normalizedCoord) {
                            return null;
                        }

                        var bound = Math.pow(2, zoom);
                        var url = '//' + $scope.settings.baseUrl + 'api/map2/' + zoom + '/' + normalizedCoord.x + '/' + normalizedCoord.y + '.png'
                        return url;
                    },
                    tileSize: new google.maps.Size(256, 256),
                    maxZoom: 18,
                    minZoom: 17,
                    radius: 1738000,
                    name: 'Moon'
                });
            }

            uiGmapIsReady.promise(1).then(function(instances) {
                instances.forEach(function(inst) {
                    var map = inst.map
                    $scope.mapObject = inst.map
                    var uuid = map.uiGmap_id
                    var mapInstanceNumber = inst.instance // Starts at 1.
                    console.log('ready!')
                        //map.addTileOverlay(new TileOverlayOptions().tileProvider());
                    $scope.initMap()
                    $scope.map.center = {
                        latitude: parseFloat($scope.settings.adminMapCenterLat),
                        longitude: parseFloat($scope.settings.adminMapCenterLon)
                    }
                    map.overlayMapTypes.push($scope.moonMapType);
                    map.addListener('bounds_changed', function() {
                        // console.log('bbbbbb')                        
                    if ($scope.filterByMap) {
                        $scope.filterPlantsByBounds(map.getBounds())
                    }
                    })
                    // var infoWindows = []
                    var markers = []
                    for (var i = 0; i < $scope.markers.length; i++) {
                        var div = document.createElement('DIV');
                        var window_id = i
                        div.innerHTML = '<div class="numbered-marker-bubble"><div class="number">' + $scope.markers[i].plant_id + '</div><div class="bottom-left-corner"></div></div>';
                        markers[i] = new RichMarker({
                            map: map,
                            position: new google.maps.LatLng($scope.markers[i].latitude, $scope.markers[i].longitude),
                            draggable: false,
                            flat: true,
                            anchor: RichMarkerPosition.BOTTOM_LEFT,
                            content: div,
                            // window_id: window_id
                        });
                        // var contentString = '<div id="content">#' + $scope.markers[i].plant_id
                        // if ($scope.markers[i].title !== undefined) {
                          // contentString += ': ' + $scope.markers[i].title
                          // if ($scope.markers[i].title_lat !== undefined) {
                            // contentString += '<br/>'
                          // }
                        // }
                        // if ($scope.markers[i].title_lat !== undefined) {
                          // contentString += '<i>'+$scope.markers[i].title_lat+'</i>'
                        // }
                        // infoWindows[i] = new google.maps.InfoWindow({
                            // content: contentString
                        // });
                        // google.maps.event.addListener(markers[i], 'click', function(e) {
                          // console.log(this)
                          // infoWindows[this.window_id].open(map, markers[this.window_id])
                        // })
                      }
                })
            })
        }
    ])

angular.module('adminApp').controller('tableTagDialogController', ['$scope', '$mdDialog', 'plantsService', 'tags', 'allTags','$log', 'selectedPlants', 'isAdd', function($scope, $mdDialog, plantsService, tags, allTags, $log, selectedPlants, isAdd) {
    //start: model
    $scope.model = {
      tags: tags,
      newTags: [],
      isAdd: isAdd
    }
    //end: model

    //start: listeners
    plantsService.onTagsSaved($scope, function(event) {
       console.log('saved')
       $scope.hide({relaod: true})
    })
    //end: listeners

    //start:: helpers
    $scope.getTitle = function() {
      if (isAdd) {
        return "Add tags"
      }
      return "Remove tags"
    }
    $scope.filterSelected = true;
    $scope.tagQuerySearch = function(criteria) {
        return allTags.filter($scope.createFilterFor(criteria))
    }

    $scope.createFilterFor = function(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(tag) {
            // console.log(tag.name_lowercase + ', pos:' + tag.name_lowercase.indexOf(lowercaseQuery))
            return (tag.name_lowercase.indexOf(lowercaseQuery) != -1);;
        }
    }
    $scope.dummySearch = function() {
      return []
    }
    //end: helpers

    $scope.save = function() {
      if (isAdd) {
        plantsService.addTags(selectedPlants, $scope.model.newTags)
      } else {
        plantsService.deleteTags(selectedPlants, $scope.model.tags)
      }
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);

var authorsService = function ($log, $resource, $rootScope, baseService, $q) {
    var authorsResource = $resource('/api/api/index.php/authors', {}, {
        'getAll': {
            method: 'GET',
            isArray: true
        },
        'create': {
            method: 'POST',
        }
    });
    var authorResource = $resource('/api/api/index.php/authors/:id', {}, {
        'delete': {
            method: 'DELETE',
        },
        'save': {
            method: 'PUT',
        }
    });
    var authors;

    var svc = {
        onAuthorsLoadedError: function (scope, callback) {
            this.registerHandler('authors-laod-error-event', scope, callback)
        },
        onAllAuthorsLoaded: function (scope, callback) {
            this.registerHandler('authors-loaded-event', scope, callback)
        },
        onAuthorCreateError: function (scope, callback) {
            this.registerHandler('author-create-error-event', scope, callback)
        },
        onAuthorCreated: function (scope, callback) {
            this.registerHandler('author-created-event', scope, callback)
        },
        onAuthorSaved: function (scope, callback) {
            this.registerHandler('author-saved-event', scope, callback)
        },
        onAuthorDeleted: function (scope, callback) {
            this.registerHandler('author-deleted-event', scope, callback)
        },
        loadAll: function (forceReload) {
            if (!forceReload && authors !== undefined) {
                $rootScope.$emit('authors-loaded-event', authors);
                return;
            }
            authorsResource.getAll({},
                function (data) {
                    var authorsData = angular.copy(data);
                    delete authorsData.$promise;
                    delete authorsData.resolved;
                    authors = authorsData
                    $rootScope.$emit('authors-loaded-event', authorsData);
                },
                function (err) {
                    $log.error('Unable to load authors: ', err);
                    $rootScope.$emit('authors-laod-error-event', err);
                })
        },
        loadAllAsync: function (forceReload) {
            if (!forceReload && authors !== undefined) {
                return $q(function (resolve, reject) {
                    resolve(authors);
                }).promise;
            }
            return authorsResource.getAll({},
                function (data) {
                    var authorsData = angular.copy(data);
                    delete authorsData.$promise;
                    delete authorsData.resolved;
                    return authorsData;
                },
                function (err) {
                    $log.error('Unable to load authors: ', err);
                }).$promise;
        },
        delete: function (author) {
            return authorResource.delete({id: author.id}, function (data) {
                $rootScope.$emit('author-deleted-event', data);
                $rootScope.$emit('ok-message', 'Autor by smazán');
            }, function (err) {
                alert('Nepodarilo se smazt autora ' + err.message);
            });
        },
        saveAuthor: function (id, author) {
            return authorResource.save({id: author.id}, author, function (data) {
                $rootScope.$emit('author-saved-event', data);
                $rootScope.$emit('ok-message', 'Autor by uložen');
            }, function (err) {
                alert('Nepodarilo se uložit autora ' + err.message);
            });
        },
        create: function (name, note) {
            if (note === undefined) {
                note = '';
            }
            var q = authorsResource.create({}, {name: name, note: note},
                function (data) {
                    var newAuthor = angular.copy(data);
                    if (authors === undefined) {
                        authors = [];
                    }
                    authors.push(newAuthor.author)
                    $rootScope.$emit('author-created-event', newAuthor.author);
                    console.log('reslving', newAuthor.author)
                    return newAuthor.author;
                },
                function (err) {
                    $log.error('Unable to create author: ', err);
                    $rootScope.$emit('author-create-error-event', err);
                })
            console.log('returning promise ', q.$promise)
            return q.$promise;
        }
    }
    return angular.extend({}, baseService, svc)
}
authorsService.$inject = ['$log', '$resource', '$rootScope', 'baseService', '$q'];
angular.module('adminApp').factory('authorsService', authorsService);
var baseService = function ($log, $rootScope) {
    var base = {};
    base.registerHandler =  function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }
    return base;
}
baseService.$inject = ['$log', '$rootScope'];
angular.module('adminApp').factory('baseService', baseService);
angular.module('adminApp').factory('categoriesService', ['$log', '$resource', '$rootScope', '$q', function ($log, $resource, $rootScope, $q) {
    var categoriesResource = $resource('/api/api/index.php/plants-categories', {}, {
        'getAll': {method: 'GET', isArray:true}
    });
    var singleCategoryResource = $resource('/api/api/index.php/plants-categories/:categoryId', {}, {
        'delete': {method: 'DELETE'}
    });

    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }

    var allPlantsCategories = []
    return {
        //start: hanlders
        onCategoriesLoaded: function (scope, callback) {
            registerHandler('categories-loaded-event', scope, callback)
        },
        onCategoryDeleted: function (scope, callback) {
            registerHandler('category-deleted-event', scope, callback)
        },
        //end: hanlders
        deleteCategory: function(category) {
            $log.debug('Deleting category', category);
            return singleCategoryResource.delete({categoryId: category.id}, category, function(data) {
                $rootScope.$emit('category-deleted-event', category);
                $rootScope.$emit('ok-message', 'Kategorie smazána');
            });
        },
        getAllCategoriesAsync: function (query, force) {
            if (allPlantsCategories.length > 0 && true !== true) {
                return $q(function(resolve, reject) {
                    resolve(allPlantsCategories);
                }).promise;
            }
            return categoriesResource.getAll({}, {query: query}, function (categories) {
                delete categories.$promise;
                delete categories.$resolved;
                allPlantsCategories = angular.copy(categories)
                $rootScope.$emit('categories-loaded-event', allPlantsCategories);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status === 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to get plant tags', err);
            }).$promise;
        }
    }
}]);

angular.module('adminApp').factory('familiesService', ['$log', '$resource', '$rootScope', '$q', function ($log, $resource, $rootScope, $q) {
    var familiesResource = $resource('/api/api/index.php/plants-families', {}, {
        'getAll': {method: 'GET', isArray: true}
    });
    var familyResource = $resource('/api/api/index.php/plants-families/:familyId', {}, {
        'save': {method: 'PUT'},
        'delete': {method: 'DELETE'}
    });

    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }

    var allPlantsFamilies = [];
    return {
        //start: hanlders
        onFamiliesLoaded: function (scope, callback) {
            registerHandler('families-loaded-event', scope, callback)
        },
        onFamilyDeleted: function (scope, callback) {
            registerHandler('family-deleted-event', scope, callback)
        },
        //end: hanlders
        deleteFamily: function (family) {
            return familyResource.delete({familyId: family.id}, family, function (data) {
                $rootScope.$emit('family-deleted-event', family);
                $rootScope.$emit('ok-message', 'Čeleď smazána');
            });
        },
        getAllFamiliesAsync: function (query, force) {
            if (allPlantsFamilies.length > 0 && force !== true) {
                return $q(function (resolve, reject) {
                    resolve(allPlantsFamilies);
                }).promise;
            }
            return familiesResource.getAll({}, {query: query}, function (families) {
                allPlantsFamilies = angular.copy(families)
                delete allPlantsFamilies.$promise;
                delete allPlantsFamilies.resolved;
                $rootScope.$emit('families-loaded-event', allPlantsFamilies);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status === 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to get plants families', err);
            }).$promise;
        }
    }
}]);

angular.module('adminApp').factory('galleriesService', ['$log', '$resource', '$rootScope', '$q', function ($log, $resource, $rootScope, $q) {
    var galleriesResource = $resource('/api/api/index.php/widgets/dr-galleries', {}, {
        'getAll': {method: 'GET', isArray: true}
    });

    var galleryStatusResource = $resource('/api/api/index.php/widgets/dr-galleries/:galleryId/active', {}, {
        'setActive': {method: 'PUT'}
    });

    var galleryResource = $resource('/api/api/index.php/widgets/dr-galleries/:galleryId', {}, {
        'get': {method: 'GET'},
        'put': {method: 'PUT'}
    });

    var galleryImagesResource = $resource('/api/api/index.php/widgets/dr-galleries/images', {}, {
        'getAll': {method: 'POST'},
    });

    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }

    return {
        //start: hanlders
        onGalleriesLoaded: function (scope, callback) {
            registerHandler('galleries-loaded-event', scope, callback)
        },
        //end: hanlders
        getImages: function (filter) {
            return galleryImagesResource.getAll({}, {filter: filter}, function (images) {
            }, function (err) {
                $rootScope.$emit('error-message', 'Nepodařilo se načíst obráazky galerie');
            }).$promise;
        },
        update: function(galleryData) {
            return galleryResource.put({galleryId: galleryData.id}, galleryData, function (result) {
            }, function (err) {
                $log.error(err);
                $rootScope.$emit('error-message', 'Nepodařilo se uložit galerii');
            }).$promise;
        },
        setActive: function (galleryId, active) {
            return galleryStatusResource.setActive({galleryId: galleryId}, {active: active}, function (response) {
                var activeVerbose = active ? 'aktivována' : "deaktivována";
                $rootScope.$emit('ok-message', 'Galerie ' + activeVerbose);
            }, function (err) {
                $rootScope.$emit('error-message', 'Zmena stavu galerie zlyhala');
                $log.error(err);
            }).$promise;
        },
        loadGallery: function (galleryId) {
            return galleryResource.get({galleryId: galleryId}, function (galleryData) {
            }, function (err) {
                $log.error(err);
                $rootScope.$emit('error-message', 'Nepodařilo se načíst galerii');
            }).$promise;
        },
        getAllGalleries: function (query) {
            return galleriesResource.getAll({}, {query: query}, function (galleries) {
                delete galleries.$promise;
                delete galleries.resolved;
                $rootScope.$emit('galleries-loaded-event', galleries);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status === 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to get galleries', err);
            }).$promise;
        }
    }
}]);

var imagesService = function ($log, $resource, $rootScope) {
    var imageResource = $resource('/api/api/index.php/images/:imageId', {}, {
        'save': {
            method: 'PUT'
        },        
    });

    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }
    return {
        onImageSaveError: function (scope, callback) {
            registerHandler('image-save-error-event', scope, callback)
        },  
        /**
         * @return Promise
         */
        save: function (imageId, data) {            
        	var promise = imageResource.save({imageId: imageId}, data,
                function (data) {
                    $rootScope.$emit('image-saved-event');
                    return data;
                },
                function (err) {
                    $log.error('Unable to save image: ', err);
                    $rootScope.$emit('image-save-error-event', err);
                }).$promise;
        	return promise;
        },        
    }
}
imagesService.$inject = ['$log', '$resource', '$rootScope'];
angular.module('adminApp').factory('imagesService', imagesService);
// fucking not working
// class NewPlantsService extends baseService {
// 	constructor($log, $resource, $rootScope) {
// 		super($rootScope)
// 		this.$resource = $resource
// 		this.$log = $log
// 		this.allPlantsTags = []
// 		this.initResurces()
// 	}
//
// 	initResurces() {
// 		this.plantResource = this.$resource('/api/api/index.php/plants/:plantId', {}, {
// 			'save': {method: 'PUT'},
// 			'get': {method: 'GET'},
// 			'delete': {method: 'DELETE'}
// 		})
// 	}
//
// 	onPlantSaved(scope, callback) {
// 		this.registerHandler('plant-saved-event', scope, callback)
// 	}
//
// 	updateSinglePlant(plantModel){
// 		return this.plantResource.save({plantId: plantModel.id},plantModel, function(data) {
// 			this.emit('plant-saved-event', plantModel, 'Plant "'+plantModel.name+'" saved successfully')
// 		}, function(err) {
// 			console.log(err);
// 			var message = '';
// 			if (err.status === 500) {
// 				message = err.data;
// 			} else {
// 				message = err.data.message;
// 			}
// 			this.emit('plant-saved-error-event', plantModel);
// 		});
// 	}
// }
// NewPlantsService.$inject = ['$log', '$resource', '$rootScope'];

//angular.module('adminApp').factory('NewPlantsService', NewPlantsService)

angular.module('adminApp').factory('plantsService', ['$log', '$resource', '$rootScope',function ($log, $resource, $rootScope) {
	var plantsResource = $resource('/api/api/index.php/plants', {}, {
    'insert': {method: 'POST'},
	});
	var plantsTableDataResource = $resource('/api/api/index.php/plants/table-data', {}, {
    'getAll': {method: 'POST'},
	});
	var plantResource = $resource('/api/api/index.php/plants/:plantId', {}, {
		'save': {method: 'PUT'},
		'get': {method: 'GET'},
		'delete': {method: 'DELETE'}
	});
    var allPlantTagsResource = $resource('/api/api/index.php/plants/:plantId/tags', {}, {
        'update': {method: 'PUT'}
    });
	var tagOnPlantResource = $resource('/api/api/index.php/plants/:plantId/tags/:tagId', {}, {
		'delete': {method: 'DELETE'}
	});
  var categoriesResource = $resource('/api/api/index.php/plants-categories', {}, {
		'get': {method: 'GET', isArray:true}
	});
	var tagsOnPlantsResource = $resource('/api/api/index.php/plants/tags', {}, {
		'add': {method: 'POST'},
		'put': {method: 'PUT'}
	});
	var categoryResource = $resource('/api/api/index.php/plants-categories/:categoryId', {}, {
		'save': {method: 'PUT'}
	});
  var familiesResource = $resource('/api/api/index.php/plants-families', {}, {
		'get': {method: 'GET', isArray:true}
	});
	var familyResource = $resource('/api/api/index.php/plants-families/:familyId', {}, {
		'save': {method: 'PUT'}
	});
	var plantsTagsResource = $resource('/api/api/index.php/plants-tags', {}, {
    'getAll': {method: 'GET', isArray:true},
		'insert': {method: 'POST'}
	});
	var plantTagResource = $resource('/api/api/index.php/plants-tags/:tagId', {}, {
		'save': {method: 'PUT'}
	})
	var plantsPublishResource = $resource('/api/api/index.php/plants/publish', {}, {
		'publish': {method: 'POST'}
	})

	var registerHandler = function(eventName, scope, callback) {
			var handler = $rootScope.$on(eventName, callback);
			scope.$on('$destroy', handler);
	}

	var allPlantsTags = []
	return {
		//start: hanlders
		onPlantsLoaded: function(scope, callback) {
			 registerHandler('plants-loaded-event', scope, callback)
		},
		onPlantCategorySaved: function(scope, callback) {
			registerHandler('plant-category-saved-event', scope, callback)
		},
		onPlantFamilySaved: function(scope, callback) {
			registerHandler('plant-family-saved-event', scope, callback)
		},
		onPlantSaved: function(scope, callback) {
			registerHandler('plant-saved-event', scope, callback)
		},
		onTagsSaved: function(scope, callback) {
			registerHandler('plants-tags-saved-event', scope, callback)
		},
		onPlantTagSaved: function(scope, callback) {
			registerHandler('plant-tag-saved-event', scope, callback)
		},
		onPlantTagRemoved: function(scope, callback) {
			registerHandler('plant-tag-removed-event', scope, callback)
		},
		//end: hanlders
		publishOnePlant: function(plantId, publish) {
			var data = {};
			data[plantId] = publish;
			return plantsPublishResource.publish({}, {ids: data}).$promise;
		},
		getAllTags: function(successCallback, errCallback) {
			if (allPlantsTags.length > 0) {
				return successCallback(allPlantsTags)
			}
			return plantsTagsResource.getAll({}, function(tags) {
				allPlantsTags = angular.copy(tags)
				delete allPlantsTags.$promise;
				delete allPlantsTags.resolved;
				successCallback(allPlantsTags);
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plant tags', err);
        errCallback(message);
			})
		},
		createTag: function(model, successCallback, errCallback) {
			return plantsTagsResource.insert(model, function(data) {
				console.log(data)
				var newTag = {
					id: data.id,
					name: model.name,
					name_lowercase: model.name.toLowerCase()
				}
				allPlantsTags.push(newTag)
				successCallback(newTag);
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to add new plant tag', err);
        errCallback(message);
			})
		},
		saveCategory: function (categoryId, category) {
			return categoryResource.save({categoryId: categoryId}, category, function(data) {
				$rootScope.$emit('plant-category-saved-event', data)
				$rootScope.$emit('ok-message', 'Kategorie uložena');
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to save category', err);
				$rootScope.$emit('plant-category-save-error-event', err)
			})
		},
		saveFamily: function (familyId, family) {
			return familyResource.save({familyId: familyId}, family, function(data) {
				$rootScope.$emit('plant-family-saved-event', data)
				$rootScope.$emit('ok-message', 'Čeleď uložena');
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to save family', err);
				$rootScope.$emit('plant-family-save-error-event', err)
			})
		},
		getPlant: function(plantId, successCallback, errCallback) {
			return plantResource.get({plantId: plantId}, function(data) {
				successCallback(data);
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plants', err);
        errCallback(message);
			}).$promise;
		},
		getAllPlantsAndSendEvent: function(query, filter) {
			var postData = {
				query: query,
				filter: filter
			}
			return plantsTableDataResource.getAll({}, postData, function(data) {
				var dataList = angular.copy(data);
				delete dataList.$promise;
				delete dataList.resolved;
				$rootScope.$emit('plants-loaded-event', dataList.markers, dataList.rows)
			}, function(err) {
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plants', err);
        $rootScope.$emit('plants-loaded-error-event', err)
      })
		},
		getAllPlants: function(query, filter, successCallback, errCallback) {
			var postData = {
				query: query,
				filter: filter
			}
			console.log(postData)
			return plantsTableDataResource.getAll({}, postData, function(data) {
				var dataList = angular.copy(data);
				delete dataList.$promise;
				delete dataList.resolved;
				// for (var i=0;i<data.rows.length;i++){
				// 	data.rows[i]['is_blooming'] = data.rows[i]['is_blooming']==='1'?true:false
				// }
        successCallback(dataList.markers, dataList.rows);
      },
			function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plants', err);
        errCallback(message);
      })
		},
		deletePlant: function(id, successCallback, errCallback) {
			return plantResource.delete({plantId: id}, function(data) {
				successCallback(data)
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to delete plant', err);
        errCallback(message);
			})
		},
    getFamilies: function(successCallback, errCallback) {
      return familiesResource.get({}, function(data) {
        successCallback(data);
      },function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plant families', err);
        errCallback(message);
      }
    )
    },
    getCategories: function(successCallback, errCallback) {
      return categoriesResource.get({}, function(data) {
        successCallback(data);
      },function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plant categories', err);
        errCallback(message);
      }
    )
    },
    insertOne: function(plantModel,successCallback, errCallback){
      return plantsResource.insert(plantModel, function(data) {
        successCallback(data);
      }, function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to insert plant', err);
        errCallback(message);
      });
    },
		updateOneAndSendEvent: function(plantModel){
      return plantResource.save({plantId: plantModel.id},plantModel, function(data) {
				$rootScope.$emit('plant-saved-event', plantModel, 'Plant "'+plantModel.name+'" saved successfully')
      }, function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $rootScope.$emit('plant-saved-error-event', plantModel);
      });
    },
        updateTags: function(plantId, tags) {
            return allPlantTagsResource.update({plantId: plantId}, {tags: tags}).$promise;
        },
		addTags: function(selectedPlants, tags) {
			return tagsOnPlantsResource.add({}, {plants: selectedPlants, tags: tags}, function(data) {
				$rootScope.$emit('plants-tags-saved-event', {}, 'Tags added successfully')
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $rootScope.$emit('plants-tags-saved-error-event');
			})
		},
		removeTag: function(plantId, tagId) {
			return tagOnPlantResource.delete({plantId: plantId, tagId: tagId}, function(data){
				$rootScope.$emit('ok-message', 'Plant tag removed successfully')
				$rootScope.$emit('plant-tag-removed-event')
			}, function(err) {
				$rootScope.$emit('error-message', 'Plant tag was NOT removed')
				$rootScope.$emit('plant-tag-removed-error-event')
			})
		},
		deleteTags: function(selectedPlants, tags) {
			return tagsOnPlantsResource.put({}, {plants: selectedPlants, tags: tags}, function(data) {
				$rootScope.$emit('plants-tags-saved-event', {}, 'Tags removed successfully')
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $rootScope.$emit('plants-tags-saved-error-event');
			})
		},
		savePlantTag: function(tagId, tag) {
			return plantTagResource.save({tagId: tagId}, tag, function(data) {
				$rootScope.$emit('ok-message', 'Plant tag saved successfully')
				$rootScope.$emit('plant-tag-saved-event', tag)
			}, function(err){
				$rootScope.$emit('error-message', 'Plant tag save failed')
				$rootScope.$emit('plant-tag-saved-error-event', tag)
			})
		},
		updateOne: function(plantModel,successCallback, errCallback){
      return plantResource.save({plantId: plantModel.id},plantModel, function(data) {
        successCallback(data);
      }, function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to insert plant', err);
        errCallback(message);
      });
    }
	}
}]);

// fucking not working
// class NewPlantsService extends baseService {
// 	constructor($log, $resource, $rootScope) {
// 		super($rootScope)
// 		this.$resource = $resource
// 		this.$log = $log
// 		this.allPlantsTags = []
// 		this.initResurces()
// 	}
//
// 	initResurces() {
// 		this.plantResource = this.$resource('/api/api/index.php/plants/:plantId', {}, {
// 			'save': {method: 'PUT'},
// 			'get': {method: 'GET'},
// 			'delete': {method: 'DELETE'}
// 		})
// 	}
//
// 	onPlantSaved(scope, callback) {
// 		this.registerHandler('plant-saved-event', scope, callback)
// 	}
//
// 	updateSinglePlant(plantModel){
// 		return this.plantResource.save({plantId: plantModel.id},plantModel, function(data) {
// 			this.emit('plant-saved-event', plantModel, 'Plant "'+plantModel.name+'" saved successfully')
// 		}, function(err) {
// 			console.log(err);
// 			var message = '';
// 			if (err.status === 500) {
// 				message = err.data;
// 			} else {
// 				message = err.data.message;
// 			}
// 			this.emit('plant-saved-error-event', plantModel);
// 		});
// 	}
// }
// NewPlantsService.$inject = ['$log', '$resource', '$rootScope'];

//angular.module('adminApp').factory('NewPlantsService', NewPlantsService)

angular.module('adminApp').factory('tagsService', ['$log', '$resource', '$rootScope', '$q', function ($log, $resource, $rootScope, $q) {
    var plantsTagsResource = $resource('/api/api/index.php/plants-tags', {}, {
        'getAll': {method: 'GET', isArray: true},
        'insert': {method: 'POST'}
    });

    var singleTagResource = $resource('/api/api/index.php/plants-tags/:tagId', {}, {
        'delete': {method: 'DELETE'}
    });

    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }

    var allPlantsTags = []
    return {
        //start: hanlders
        onTagsLoaded: function (scope, callback) {
            registerHandler('tags-loaded-event', scope, callback)
        },
        onTagDeleted: function (scope, callback) {
            registerHandler('tag-deleted-event', scope, callback)
        },
        //end: hanlders
        deleteTag: function(tag) {
            $log.debug('Deleting tag', tag);
            return singleTagResource.delete({tagId: tag.id}, tag, function(data) {
                $rootScope.$emit('tag-deleted-event', tag);
            });
        },
        getAllTagsAsync: function (query, force,successCallback) {
            if (allPlantsTags.length > 0 && true !== true) {
                return $q(function(resolve, reject) {
                    resolve(successCallback(allPlantsTags));
                }).promise;
            }
            return plantsTagsResource.getAll({}, {query: query}, function (tags) {
                allPlantsTags = angular.copy(tags)
                delete allPlantsTags.$promise;
                delete allPlantsTags.resolved;
                successCallback(allPlantsTags);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status === 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to get plant tags', err);
            }).$promise;
        },
        createTag: function (model, successCallback, errCallback) {
            return plantsTagsResource.insert(model, function (data) {
                console.log(data)
                var newTag = {
                    id: data.id,
                    name: model.name,
                    name_lowercase: model.name.toLowerCase()
                }
                allPlantsTags.push(newTag)
                successCallback(newTag);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status === 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to add new plant tag', err);
                errCallback(message);
            })
        }
    }
}]);

// fucking not working
// class NewPlantsService extends baseService {
// 	constructor($log, $resource, $rootScope) {
// 		super($rootScope)
// 		this.$resource = $resource
// 		this.$log = $log
// 		this.allPlantsTags = []
// 		this.initResurces()
// 	}
//
// 	initResurces() {
// 		this.plantResource = this.$resource('/api/api/index.php/plants/:plantId', {}, {
// 			'save': {method: 'PUT'},
// 			'get': {method: 'GET'},
// 			'delete': {method: 'DELETE'}
// 		})
// 	}
//
// 	onPlantSaved(scope, callback) {
// 		this.registerHandler('plant-saved-event', scope, callback)
// 	}
//
// 	updateSinglePlant(plantModel){
// 		return this.plantResource.save({plantId: plantModel.id},plantModel, function(data) {
// 			this.emit('plant-saved-event', plantModel, 'Plant "'+plantModel.name+'" saved successfully')
// 		}, function(err) {
// 			console.log(err);
// 			var message = '';
// 			if (err.status === 500) {
// 				message = err.data;
// 			} else {
// 				message = err.data.message;
// 			}
// 			this.emit('plant-saved-error-event', plantModel);
// 		});
// 	}
// }
// NewPlantsService.$inject = ['$log', '$resource', '$rootScope'];

//angular.module('adminApp').factory('NewPlantsService', NewPlantsService)

angular.module('adminApp').factory('webpService', ['$log', '$q', function ($log, $q) {
    var hasWebP = false;

    var img = new Image();
    img.onload = function () {
        hasWebP = !!(img.height > 0 && img.width > 0);
        //ctrl.webp = ctrl.hasWebP ? '/webp' : '';
    };
    img.onerror = function () {
        hasWebP = false;
        //ctrl.webp = ctrl.hasWebP ? '/webp' : '';
    };
    img.src = 'data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=';

    return {
        supported: function () {
            return hasWebP;
        }
    }
}]);

'use strict';
var tagsListController = function ($log, tagsService, plantsService, $mdDialog, $scope) {
    $log.debug('tags!');
    var ctrl = this;
    ctrl.model = {
        tags: tagsService.tagsList,
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }

    function success(tags) {
        ctrl.model.tags = tags;
    }

    tagsService.onTagDeleted($scope, function(event, data) {
        ctrl.getTags(true);
    });

    plantsService.onPlantTagSaved($scope, function(event, data) {
        $log.debug('s', data);
        ctrl.getTags(true);
    });

    ctrl.getTags = function (force) {
        ctrl.model.promise = tagsService.getAllTagsAsync(ctrl.query, force, success);
    }

    ctrl.deleteTag = function (ev, tag) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
            .title('Potvrzeni')
            .textContent('Tag "' + tag.name + '" bude natrvalo odebrán ze všech roslitn a smazán. Pokračovat?')
            .ariaLabel('Potvrzeni')
            .targetEvent(ev)
            .ok('Ano')
            .cancel('Zrušit');
        $mdDialog.show(confirm).then(function () {
            tagsService.deleteTag(tag);
        }, function () {
            //
        });
    };

    ctrl.editTag = function (ev, tag) {
        $mdDialog.show({
            controller: 'editTagDialogController',
            templateUrl: 'plants/editTagDialog.template.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            resolve: {
                tag: function () {
                    return angular.copy(tag)
                },
            },
            clickOutsideToClose: true,
            fullscreen: true
        })
            .then(function () {
                console.log('tags saved!')
                ctrl.getTags(true);
            }, function () {
                console.log('You cancelled the dialog.');
            });
    }

    ctrl.selected = [];
    ctrl.limitOptions = [5, 10, 15];
    ctrl.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    ctrl.getTags(true);
}

angular.module('adminApp').component('tagsList', {
    templateUrl: 'tags/tagsList.template.html',
    controller: tagsListController,
    bindings: {}
});
'use strict';
angular.module('adminApp').controller('primaryPointEditController', ['$scope', '$rootScope', '$log', '$mdDialog', '$mdMedia', 'circuitsService', '$timeout', '$routeParams', 'uiGmapIsReady', 'settingsService', 'Upload', '$location',
    function($scope, $rootScope, $log, $mdDialog, $mdMedia, circuitsService, $timeout, $routeParams, uiGmapIsReady, settingsService, Upload, $location) {
        //start:models
        $scope.markersIdCounter = 0
        $scope.point = {}

        $scope.settings = []

        $scope.files //for uploads

        $scope.saving = false
        $scope.isNew = $routeParams.pointId == '_'
        $scope.circuitId = $routeParams.circuitId
        $scope.primaryPointId = $routeParams.pointId

        $scope.options = { //for mce
            language: 'cz',
            format_tags: 'h1;h2',
            allowedContent: true,
            entities: false,
            removePlugins: 'templates'
            //width: '100%'
        };
        $scope.map = {
            infoWindowTemplate: 'plants/plantEditMapMarkerInfoWindow.template.html',
            markerOptions: {
                draggable: true,
                animation: 'DROP'
            },
            markerEvents: {
                dragend: function(markerModel, eventName, markerObject) {
                  //
                }
            },
            center: {
                latitude: 50.0708767569985,
                longitude: 14.4208672642708
            },
            zoom: 18,
            bounds: {
                northeast: {
                    latitude: 50.0689551446,
                    longitude: 14.4179141521
                },
                southwest: {
                    latitude: 50.0733622808,
                    longitude: 14.4247806072
                }
            },
            events: {
                click: function(mapModel, eventName, originalEventArgs) {
                    $scope.$apply(function() {
                        //console.log('click!')

                        var e = originalEventArgs[0];
                        //$scope.model.markers = []
                        $scope.markersIdCounter++
                        $scope.point.markers = []
                        $scope.point.markers.push({
                            latitude: e.latLng.lat(),
                            longitude: e.latLng.lng(),
                            id: $scope.markersIdCounter,
                            title: $scope.point.name
                        })
                    });
                }
            }
        }
        //end:models
        //start: upload
        $scope.upload = function(file) { //TODO: to service??
          if (file === undefined) {
            return;
          }
            if (!file.$error) {
                var newImg = {
                    id: undefined,
                    url: 'https://bz-uk.cz/sites/default/files/styles/medium/public/images/abutilon_megapotamicum2.jpg',
                    status: 'uploading'
                }
                $scope.point.image = newImg
                Upload.upload({
                    url: '/api/api/index.php/images', //TODO: load from settings
                    data: {
                        file: file
                    }
                }).then(function(resp) {
                        console.log('upload complete')
                        console.log(resp)
                        $timeout(function() {
                            console.log('complete timeout')
                            if (resp.data.id !== undefined && parseInt(resp.data.id) > 0) {
                                console.log('done!')
                                newImg.url = resp.data.url
                                newImg.id = parseInt(resp.data.id)
                                newImg.status = 'done'
                            }
                            $scope.log = 'file: ' +
                                resp.config.data.file.name +
                                ', Response: ' + JSON.stringify(resp.data) +
                                '\n' + $scope.log;
                        });
                    },
                    function(resp) {
                        alert('Unable to uplaod image: ' + resp.data)
                        $scope.point.image = undefined
                    },
                    function(evt) {
                        var progressPercentage = parseInt(100.0 *
                            evt.loaded / evt.total);
                        newImg['progress'] = progressPercentage
                    });
            }

        }
        $scope.$watch('files', function() {
            $scope.upload($scope.files);
        })
        $scope.deleteImage = function(image) {
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this image?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                $scope.point.image = undefined
            }, function() {});
        }
        //end: upload
        //start: listeners
        settingsService.onSettingsLoaded($scope, function(event, settings){
          $scope.settings = settings
        })
        //??
        // circuitsService.onCircuitDataLoaded($scope, function(event, circuitData) {
        //         $scope.circuit = circuitData
        //         event.stopPropagation()
        //     })
        circuitsService.onPrimaryPointLoaded($scope, function(event, pointData) {
          $scope.point = pointData
          var maxId = 0
          for (var i=0;i<$scope.point.markers.length;i++) {
            if ($scope.point.markers[i].id > maxId) {
              maxId = $scope.point.markers[i].id
            }
          }
          $scope.markersIdCounter = maxId
          console.log('Point loaded')
          console.log($scope.point)
          event.stopPropagation()
        })
        circuitsService.onPrimaryPointSaved($scope, function(event, pointData) {
          $scope.openCircuit();
        })
        circuitsService.onPrimaryPointCreated($scope, function(event, pointData) {
          $scope.openCircuit();
        })
        //end: listeners
        //start: map logic
        $scope.getNormalizedCoord = function(coord, zoom) {
            var y = coord.y;
            var x = coord.x;

            // tile range in one direction range is dependent on zoom level
            // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
            var tileRange = 1 << zoom;

            // don't repeat across y-axis (vertically)
            if (y < 0 || y >= tileRange) {
                return null;
            }

            // repeat across x-axis
            if (x < 0 || x >= tileRange) {
                x = (x % tileRange + tileRange) % tileRange;
            }

            return {
                x: x,
                y: y
            };
        }
        $scope.initMap = function() {
            $scope.moonMapType = new google.maps.ImageMapType({
                getTileUrl: function(coord, zoom) {
                    // console.log(getProjection())
                    var normalizedCoord = $scope.getNormalizedCoord(coord, zoom);
                    if (!normalizedCoord) {
                        return null;
                    }

                    var bound = Math.pow(2, zoom);
                    var url = '//' + $scope.settings.baseUrl + 'api/map2/' + zoom + '/' + normalizedCoord.x + '/' + normalizedCoord.y + '.png'
                    return url;
                },
                tileSize: new google.maps.Size(256, 256),
                maxZoom: 18,
                minZoom: 17,
                radius: 1738000,
                name: 'Moon'
            });
        }
        uiGmapIsReady.promise(1).then(function(instances) {
            $log.log('map is ready')
            instances.forEach(function(inst) {
                var map = inst.map
                var uuid = map.uiGmap_id
                var mapInstanceNumber = inst.instance // Starts at 1.
                $scope.initMap()
                map.overlayMapTypes.push($scope.moonMapType);
            })
        })

        //end: map logic
        //start:helpers
        $scope.openCircuit = function() {
          $location.url('/circuits/' + $routeParams.circuitId)
        }
        $scope.openCircuits = function() {
            //TODO: save check?!?
            $location.url('/circuits')
        }
        $scope.save = function() {
          if ($scope.isNew) {
            circuitsService.createPrimaryPoint($routeParams.circuitId, $scope.point)
          } else {
            circuitsService.savePrimaryPoint($routeParams.circuitId, $scope.point)
          }
        }
        $scope.getSaveLabel = function() {
            if ($scope.isNew) {
                return $scope.saving ? 'Adding...' : 'Add'
            } else {
                return $scope.saving ? 'Saving...' : 'Save'
            }
        }
        //end: helpers
        //starts:load data
        if (!$scope.isNew) {
            circuitsService.loadOnePrimaryPoint($routeParams.circuitId, $routeParams.pointId)
        }
        settingsService.loadAndEmitEvent()
        //end:load data
    }
]);

'use strict'
angular.module('adminApp').directive('primaryPointEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'circuit/primary-points/primaryPointEdit.template.html',
		controller: 'primaryPointEditController'
	};
});

'use strict';
angular.module('adminApp').controller('primaryPointSlidesTableController', ['$scope', '$rootScope', '$log', '$mdDialog', '$mdMedia', 'circuitsService', '$timeout', '$routeParams', 'uiGmapIsReady', 'settingsService', 'plantsService', 'Upload', '$location',
    function($scope, $rootScope, $log, $mdDialog, $mdMedia, circuitsService, $timeout, $routeParams, uiGmapIsReady, settingsService, plantsService, Upload, $location) {
        $log.debug('loading slides for ' + $scope.circuitId + '/' + $scope.primaryPointId)
            //start: models
        $scope.slidesTable = {
                slides: [],
                query: {
                    order: 'id',
                    limit: 10,
                    page: 1
                },
                selected: [],
            }
            //end: models
            //start listeners
        circuitsService.onPrimaryPointSlidesLoaded($scope, function(event, data) {
            $scope.slidesTable.slides = data
        })
        circuitsService.onPrimaryPointSlideDeleted($scope, function(event, data) {
            $scope.getSlides()
        })
        circuitsService.onPrimaryPointSlideEdited($scope, function(event, data) {
                $scope.getSlides()
            })
            //end listeners
            //start: helpers
        $scope.getSlides = function() {
            $scope.slidesTable.promise = circuitsService.loadPrimaryPointSlides($scope.circuitId, $scope.primaryPointId, $scope.slidesTable.query).$promise
        }
        $scope.deleteSlide = function(slide) {
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this slide?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {
                circuitsService.deletePrimaryPointSlide($scope.circuitId, $scope.primaryPointId, slide.id)
            }, function() {});
        }
        $scope.publish = function(slide) {
                slide.active = !slide.active
                circuitsService.updatePrimaryPointSlide($scope.circuitId, $scope.primaryPointId, slide.id, slide)
            }
            //end: helpers
            //start: loading data
        $scope.getSlides()
            //end: loading data
    }
]);

'use strict'
angular.module('adminApp').directive('primaryPointSlidesTable', function() {
	return {
		restrict : 'E',
		templateUrl : 'circuit/primary-points/primaryPointSlidesTable.template.html',
		controller: 'primaryPointSlidesTableController',
		scope: {
      circuitId: "@circuitId",
			primaryPointId: "@primaryPointId"
    }
	};
});

'use strict';
angular.module('adminApp').controller('secondaryPointsTableController', ['$scope', '$rootScope', '$log', '$mdDialog', '$mdMedia', 'circuitsService', '$timeout', '$routeParams', 'uiGmapIsReady', 'settingsService', 'plantsService', 'Upload', '$location',
    function($scope, $rootScope, $log, $mdDialog, $mdMedia, circuitsService, $timeout, $routeParams, uiGmapIsReady, settingsService, plantsService, Upload, $location) {
      $log.debug('loading sec. points for ' + $scope.circuitId + '/' + $scope.primaryPointId)
      //start: models
      $scope.mode = 'list'
      $scope.filter = {
          category: undefined,
          family: undefined,
          tags: [],
          search: undefined
      }
      $scope.secondaryPointsTable = {
          points: [],
          query: {
              order: 'id',
              limit: 10,
              page: 1
          },
          selected: [],
      }
      $scope.plantsTable = {
          plants: [],
          query: {
              order: 'id',
              limit: 10,
              page: 1
          },
          selected: [],
      }
      $scope.categories = []
      $scope.families = []
      //end: models
      //start listeners
      circuitsService.onAllSecondaryPointsLoaded($scope, function(event, secondaryPointsList) {
        $log.debug('secondary points loaded: ' + secondaryPointsList.tableData.length)
        $scope.secondaryPointsTable.points = secondaryPointsList.tableData
        //secondaryPointsList.markers??
      })
      circuitsService.onSecondaryPointDeleted($scope, function(event, data) {
          $scope.getPoints()
      })
      circuitsService.onSecondaryPointSaved($scope, function(event, data) {
          $scope.getPoints()
      })
      plantsService.onPlantsLoaded($scope, function(event, markers, plants) {
        $scope.plantsTable.plants = plants
      })
      circuitsService.onSecondaryPointAdded($scope, function(data) {
        $scope.mode = 'list'
        $scope.getPoints()
      })
      //end listeners
      //start: helpers
      $scope.search = function() {
        $scope.getPlants()
      }
      $scope.getAddButtonLabel = function() {
          if ($scope.plantsTable.selected.length === 0) {
            return 'Add'
          } else {
            return 'Add ' +  $scope.plantsTable.selected.length + ' plants'
          }
      }
      $scope.addNew = function() {
        $scope.mode = 'add'
        $scope.getPlants()
      }
      $scope.cancelAdd = function() {
        $scope.mode = 'list'
        $scope.getPoints()
      }
      $scope.getPoints = function() {
          $scope.secondaryPointsTable.promise = circuitsService.loadAllSecondaryPoints($scope.circuitId, $scope.primaryPointId, $scope.secondaryPointsTable.query).$promise
      }
      $scope.deletePoint = function(point) {
          circuitsService.removeSecondaryPoint($scope.circuitId, $scope.primaryPointId, point.id)
      }
      $scope.publish = function(point) {
        point.active = !point.active
        circuitsService.updateSecondaryPoint($scope.circuitId, $scope.primaryPointId, point.id, point)
      }
      $scope.getPlants = function() {
          var filter = angular.copy($scope.filter)
          $scope.plantsTable.promise = plantsService.getAllPlantsAndSendEvent($scope.plantsTable.query, filter).$promise;
      }
      $scope.onSearchKeyPress = function() {
        $scope.getPlants()
      }
      $scope.add = function() {
        circuitsService.addSecondaryPoints($scope.circuitId, $scope.primaryPointId, $scope.plantsTable.selected)
      }
      //end: helpers
      //start: loading data
      $scope.getPoints()
      plantsService.getCategories(function(categories) {
          $scope.categories = categories
      })
      plantsService.getFamilies(function(families) {
          $scope.families = families
      })
      //end: loading data
    }
]);

'use strict'
angular.module('adminApp').directive('secondaryPointsTable', function() {
	return {
		restrict : 'E',
		templateUrl : 'circuit/primary-points/secondaryPointsTable.template.html',
		controller: 'secondaryPointsTableController',
		scope: {
      circuitId: "@circuitId",
			primaryPointId: "@primaryPointId"
    }
	};
});

'use strict';
var authorsBlocksController = function ($mdDialog, $log, $scope, authorsService, $q, $timeout) {
    var ctrl = this;
    ctrl.authorsVisible = false;
    $timeout(function(){
        var authors = angular.copy(ctrl.authors);        
        ctrl.authors = [];
        ctrl.authors = authors; 
    },500)
    $log.debug('authors ctrl ', ctrl.authors)
    ctrl.allAuthors = [];
    ctrl.authorSearchText = '';
    authorsService.onAllAuthorsLoaded($scope, function (event, authors) {
        ctrl.allAuthors = authors;
    });
    authorsService.loadAll();
    ctrl.removeAuthor = function(index) {
        ctrl.authors.splice(index, 1);
    }
    ctrl.addAuthor = function (author) {
        if (author === '' || author === undefined || author === null) {
            return;
        }
        for (var i = 0; i < ctrl.authors.length; i++) {
            if (ctrl.authors[i].id === author.id) {
                return;
            }
        }
        ctrl.authors.push(author);
        ctrl.authorSearchText = undefined;
        ctrl.selectedAuthor = undefined;
    }
    ctrl.authorsSearch = function (searchText) {
        var deferred = $q.defer();
        deferred.resolve(ctrl.filterAuthors(searchText));
        return deferred.promise
    }
    ctrl.filterAuthors = function (searchText) {        
        var filteredSet = [];
        if (searchText === '' || searchText === undefined || searchText === null) {
            console.log('found ' + ctrl.allAuthors.length + ' authors')
            for (var i = 0; i < ctrl.allAuthors.length; i++) {
                var found = false;
                for (var j = 0; j < ctrl.authors.length; j++) {
                    if (ctrl.authors[j].id === ctrl.allAuthors[i].id) {
                        found = true;
                    }
                }
                if (!found) {
                    filteredSet.push(ctrl.allAuthors[i])
                }
            }
            return filteredSet;
        }
        for (var i = 0; i < ctrl.allAuthors.length; i++) {
            if (ctrl.allAuthors[i].name.indexOf(searchText) > -1 ||
                ctrl.allAuthors[i].name.toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                var found = false;
                for (var j = 0; j < ctrl.authors.length; j++) {
                    if (ctrl.authors[j].id === ctrl.allAuthors[i].id) {
                        found = true;
                    }
                }
                if (!found) {
                    filteredSet.push(ctrl.allAuthors[i])
                }
            }
        }
        return filteredSet;
    }

    ctrl.createAndAddNewAuthor = function (authorName) {
        console.log('creating autor ' + authorName)
        if (authorName === '' || authorName === undefined || authorName === null) {
            return;
        }
        console.log('svac call')
        authorsService.create(authorName).then(function (data) {
            console.log('author created ', data.author)
            authorsService.loadAll(true)
            ctrl.addAuthor(data.author);
        });
    }
}
angular.module('adminApp').component('authorsBlock', {
    templateUrl: 'plants/authors/authorsBlock.template.html',
    controller: authorsBlocksController,
    bindings: {
        authors: '='
    }
});
angular.module('adminApp').controller('imagesBlockController', ['$scope', '$mdDialog', '$log', 'Upload', 'settingsService', '$timeout', '$mdMedia', '$cookies', 'webpService',
    function ($scope, $mdDialog, $log, Upload, settingsService, $timeout, $mdMedia, $cookies, webpService) {
        $scope.previewDescription = '';
        $scope.previewDescriptionEn = '';
        $scope.previewAuthors = '';
        $scope.previewAuthorsEn = '';
        $scope.webp = webpService.supported();
        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        })
        settingsService.load(function (data) {
            $scope.globalSettings = data
        })
        // $scope.dropImage = function (image) {
        //     $scope.images.push(image);
        //     return true;
        // }
        // $scope.isDragging = function (idx) {
        //     console.log('isDrag', idx, $scope.dragIndex, idx === $scope.dragIndex);
        //     return idx === $scope.dragIndex
        // }
        $scope.startDrag = function(index) {
            console.log('start', index);
            $scope.dragIndex = index;
        }
        $scope.deleteImage = function (index, showConfirmation) {
            $scope.dragIndex = null;
            if (!showConfirmation) {
                $scope.images.splice(index, 1);
                return;
            }
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this image?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function () {
                $scope.images.splice(index, 1);
            }, function () { });
        }

        console.log('imgsBlock', $scope.images);
        $scope.getCreatedDateFormatted = function(dateString) {
            var date = new Date();
            if (dateString) {
                date = new Date(Date.parse(dateString));
            }

            var monthNames = ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "List.", "Pros."];

            var d = new Date(date.getFullYear(), date.getMonth(), 0);
            return d.getDate() + '.' + monthNames[date.getMonth()] + ' ' + date.getFullYear();
        };
        $scope.getPreviewUrl = function(image) {
            let extension = image.name.substr(image.name.lastIndexOf('.'));
            return $scope.globalSettings.offlineImagesUrl.replace(/\/$/, '') + '/250/' + image.id + ($scope.webp?'.webp':extension);
        }
        $scope.previewImage = function (image, imageUrl, ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
            console.log('preview open',image)
            $mdDialog.show({
                controller: 'imagePreviewDailogController',
                templateUrl: 'plants/imageProviewDialog.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                locals: {
                    image: image,                    
                },
                clickOutsideToClose: true,
                fullscreen: useFullScreen
            })
                .then(function (response) {                    
                    image.description = response.description;
                    image.description_en = response.descriptionEn;
                    image.authors = response.authors;
                    image.authors_en = response.authorsEn;
                    image.display = response.display;
                    image.display_vertical_offset = response.displayVerticalOffset;
                    //console.log('preview closed',image)
                }, function () {
                    console.log('You cancelled the dialog.');
                });

            //         $scope.$watch(function() {
            //             return $mdMedia('xs') || $mdMedia('sm');
            //         }, function(wantsFullScreen) {
            //             $scope.customFullscreen = (wantsFullScreen === true);
            //         });
        }
        $scope.upload = function (files) { //TODO: to service??
            if (files && files.length) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    if (!file.$error) {
                        var newImg = {
                            id: undefined,
                            url: '//' + $scope.globalSettings.baseUrl + '/public/img/ic_cloud_upload_black_48px.svg',
                            status: 'uploading'
                        }
                        $scope.images.push(newImg);
                        Upload.upload({
                            //url: '/api/api/index.php/images', //TODO: load from settings
                            url: '/upload/image', //TODO: load from settings
                            headers: {
                                'X-XSRF-TOKEN': $cookies.get('_csrf')
                            },
                            data: {
                                file: file
                            },
                            params: {
                                webpPreview: webpService.supported()
                            }
                        }).then(function (resp) {
                            var fileData = resp.data[0];
                            $timeout(function () {
                                if (fileData.id !== undefined && parseInt(fileData.id) > 0) {
                                    newImg.preview_url = fileData.url;
                                    newImg.created_at = new Date();
                                    newImg.id = parseInt(fileData.id);
                                    newImg.status = 'done'
                                } else { //wrong data received from server
                                    alert('Unable to uplaod image: ' + fileData)
                                    var idx = $scope.images.indexOf(newImg)
                                    if (idx > -1) {
                                        $scope.images.splice(idx, 1)
                                    }
                                }
                            },100);
                        },
                            function (resp) {
                                console.error('Unable to uplaod image: ', resp.data);
                                if (resp.data.code === 'upload_unsupported_format') {
                                    alert('Unable to upload image: unsupported format.');
                                }
                                var idx = $scope.images.indexOf(newImg)
                                if (idx > -1) {
                                    $scope.images.splice(idx, 1)
                                }
                            },
                            function (evt) {
                                var progressPercentage = parseInt(100.0 *
                                    evt.loaded / evt.total);
                                newImg['progress'] = progressPercentage
                            });
                    }
                }
            }
        }
    }]);

'use strict'
angular.module('adminApp').directive('imagesBlock', function() {
	return {
		restrict : 'E',
		templateUrl : 'plants/content/imagesBlock.template.html',
		controller: 'imagesBlockController',
		scope: {
      		images: '='
    	},
	};
});

'use strict'
//controller
var plantBlocksEditController = function($mdDialog, $log, $scope, Upload, settingsService) {    
    $log.debug('blocks edit', this.blocks)
    this.addMode = false;
    this.addType = 0;
    this.newBlockContent = ''
    this.newBlockContentEn = ''
    this.newBlockImages = [];
    this.newBlockColor = ''
    this.addButtonsVisible = true;

    var ctrl = this;

    settingsService.load(function(data) {
        ctrl.globalSettings = data
    })

    $scope.$on('$destroy',$scope.$on('delete-item', function (event, blockIdx) {
        ctrl.deleteBlock(blockIdx);
        event.stopPropagation();
    }));

    $scope.$on('$destroy', $scope.$on('show-mode-change', function(event, mode) {
        if (mode==='show') {
            ctrl.addButtonsVisible = true;
        }
        if (mode==='edit') {
            ctrl.addButtonsVisible = false;
        }
    }));

    $scope.$on('$destroy',$scope.$on('move-item-up', function (event, fromIdx) {
        var toIdx = fromIdx - 1;
        var movedItem = ctrl.blocks[fromIdx];
        ctrl.blocks.splice(fromIdx, 1)
        ctrl.blocks.splice(toIdx, 0, movedItem)
        event.stopPropagation();
    }));

    $scope.$on('$destroy',$scope.$on('move-item-down', function (event, fromIdx) {
        var movedItem = ctrl.blocks[fromIdx];
        ctrl.blocks.splice(fromIdx, 1)
        ctrl.blocks.splice(fromIdx + 1, 0, movedItem)
        event.stopPropagation();
    }));

    ctrl.addMoreInfoBlock = function() {
        ctrl.blocks.push({type: 2})
        $log.debug(ctrl.blocks)
    };

    $scope.$on('edit_enabled', function(event, enabled) {
        $log.debug('Edit: ' + enabled);
        $scope.$parent.$emit('save_enabled', !enabled);
    });

    ctrl.addBlock = function() {
        this.addMode = true;
        this.addType = 0;
        $scope.$parent.$emit('save_enabled', false);
    }
    ctrl.addImageBlock = function() {
        this.addType = 1;
        this.addMode = true;
        $scope.$parent.$emit('save_enabled', false);
    }
    ctrl.cancelAdd = function(ev) {
        if (ctrl.newBlockContent.length > 0 || ctrl.newBlockImages.length > 0) {
                var confirm = $mdDialog.confirm()
                    .title('Cancel block creation')
                    .textContent('Are you sure to cancel this newly created block? All content will be lost.')
                    .ariaLabel('Warning')
                    .targetEvent(ev)
                    .ok('Ok')
                    .cancel('Cancel');
                $mdDialog.show(confirm).then(function() {
                    ctrl.addMode = false;
                    $scope.$parent.$emit('save_enabled', true);
                    ctrl.newBlockContent = '';
                    ctrl.newBlockContentEn = '';
                    ctrl.newBlockImages = [];
                }, function() {
                    //cancel
                });
        } else {
            ctrl.addMode = false;
            $scope.$parent.$emit('save_enabled', true);
            ctrl.newBlockContent = '';
            ctrl.newBlockContentEn = '';
        }  
    }
    ctrl.deleteBlock = function(blockIdx) {
        ctrl.deleteBlocks.push(ctrl.blocks[blockIdx])        
        ctrl.blocks.splice(blockIdx, 1);
    }    
    ctrl.saveBlock = function(index) {
        console.log('sve', ctrl.addType);
        if (ctrl.addType === 0) {
            if (this.newBlockContent.length === 0) {
                return ctrl.cancelAdd();
            }
            ctrl.blocks.push({content_en: ctrl.newBlockContentEn, content: ctrl.newBlockContent, type: 0, backgroud_color: ctrl.newBlockColor});
            ctrl.newBlockContent = '';
            ctrl.newBlockContentEn = '';
            ctrl.newBlockColor = '';
            ctrl.addMode = false;
            $scope.$parent.$emit('save_enabled', true);
            return;
        } else {
            var activeImages = [];
            console.log('imgs!', ctrl.newBlockImages);
            for (var i = 0; i<ctrl.newBlockImages.length; i++) {
                if (ctrl.newBlockImages[i].status !== undefined && ctrl.newBlockImages[i].status === 'deleted') {
                    continue;
                }
                activeImages.push(ctrl.newBlockImages[i]);
            }
            console.log('adding images block', ctrl.newBlockImages);
            ctrl.blocks.push({type: 1, previewImages: activeImages,images: angular.copy(ctrl.newBlockImages)})
            ctrl.newBlockImages = [];
            ctrl.addMode = false;
            $scope.$parent.$emit('save_enabled', true);
            return;
        }
        if (index !== undefined) {
            if (ctrl.blocks[index].type === 0) {
                ctrl.blocks[index].content = ctrl.blocks[index].edit.content
                ctrl.blocks[index].background_color = ctrl.blocks[index].edit.backgroundColor
                delete ctrl.blocks[index].edit
                return;
            } else {
                ctrl.blocks[index].content = angular.copy(ctrl.blocks[index].edit.images);
                delete ctrl.blocks[index].edit
                return;
            }
        }        
    }
    
}
//definition
angular.module('adminApp').component('plantBlocksEdit', {		
		templateUrl : 'plants/content/plantBlocksEdit.template.html',
		controller: plantBlocksEditController,
        bindings: {
            blocks: '=',
            showLanguage: '=',
            deleteBlocks: '=',
            plant: '='
        }
    });
'use strict'
//controller
var singleBlockPlantContent = function ($mdDialog, $log, $scope, plantsService) {
    $log.debug('blocks edit', this.block, this.blockIdx)
    this.editMode = false;
    this.editBlockContent = ''
    this.editBlockContentEnglish = ''
    this.editBlockColor = ''

    var ctrl = this;
    ctrl.families = [];
    ctrl.showMoreVisible = false;

    plantsService.getFamilies(function(families) {
        ctrl.families = families
    })

    ctrl.showMore = function() {
        $log.debug('more!');
        ctrl.showMoreVisible = !ctrl.showMoreVisible;
    }

    ctrl.moveUp = function() {
        $scope.$emit('move-item-up', ctrl.blockIdx)
    }
    ctrl.moveDown = function() {
        $scope.$emit('move-item-down', ctrl.blockIdx)
    }
    ctrl.deleteBlock = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Delete block confirmation')
            .textContent('Are you sure you want to delete this block?')
            .ariaLabel('Warning')
            .targetEvent(ev !== undefined ? ev : null)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            $scope.$emit('delete-item', ctrl.blockIdx)
        });
    }
    ctrl.getFamilyName = function() {
        for (var i = 0; i < ctrl.families.length; i++) {
            if (ctrl.families[i].id == ctrl.plant.family) {
                return ctrl.families[i].name
            }
        }
    }
    ctrl.getLatinFamilyName = function() {
        for (var i = 0; i < ctrl.families.length; i++) {
            if (ctrl.families[i].id == ctrl.plant.family) {
                return ctrl.families[i].name_lat
            }
        }
    }
    ctrl.getLatinName = function() {
        return ctrl.plant.name_lat;
    }
    ctrl.getGermanName = function() {
        return ctrl.plant.name_de;
    }
    ctrl.getSlovakName = function() {
        return ctrl.plant.name_sk;
    }
    ctrl.getEnglishName = function() {
        return ctrl.plant.name_en;
    }
    ctrl.getOccurence = function() {
        return ctrl.plant.occurrence;
    }
}
//definition single-block-plant-more-info
angular.module('adminApp').component('singleBlockMoreInfoContent', {
    templateUrl: 'plants/content/singleBlockMoreInfoContent.template.html',
    controller: singleBlockPlantContent,
    bindings: {
        block: '=',
        blockIdx: '=',
        isFirst: '=',
        isLast: '=',
        plant: '='
    }
});
'use strict'
//controller
var singleBlockPlantContent = function ($mdDialog, $log, $scope) {
    $log.debug('blocks edit', this.block, this.blockIdx)
    this.editMode = false;
    this.editBlockContent = ''
    this.editBlockContentEnglish = ''
    this.editBlockColor = ''

    var ctrl = this;

    ctrl.cancelEdit = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Edit cancel confirmation')
            .textContent('Are you sure you want to cancel editation? All changed content will be lost.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            ctrl._clearTempVariables()
            ctrl.editMode = false;
            $scope.$parent.$emit('edit_enabled', false);
            $scope.$emit('show-mode-change', 'show')
        });
    };
    ctrl._clearTempVariables = function () {
        ctrl.editBlockContent = '';
        ctrl.editBlockContentEnglish = '';
        ctrl.editBlockColor = '';
    }
    ctrl.editBlock = function () {
        ctrl._clearTempVariables();
        ctrl.editBlockContent = ctrl.block.content;
        ctrl.editBlockContentEnglish = ctrl.block.content_en
        ctrl.editBlockColor = ctrl.block.backgroud_color;
        ctrl.editMode = true
        $scope.$emit('show-mode-change', 'edit')
        $scope.$parent.$emit('edit_enabled', true);
    }
    ctrl.saveBlock = function () {
        ctrl.block.content = ctrl.editBlockContent
        ctrl.block.content_en = ctrl.editBlockContentEnglish
        ctrl.block.backgroud_color = ctrl.editBlockColor
        ctrl._clearTempVariables()
        ctrl.editMode = false;
        $scope.$parent.$emit('edit_enabled', false);
        $scope.$emit('show-mode-change', 'show')
    }
    ctrl.moveUp = function() {
        $scope.$emit('move-item-up', ctrl.blockIdx)
    }
    ctrl.moveDown = function() {
        $scope.$emit('move-item-down', ctrl.blockIdx)
    }
    ctrl.deleteBlock = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Delete block confirmation')
            .textContent('Are you sure you want to delete this block? All block content will be lost.')
            .ariaLabel('Warning')
            .targetEvent(ev !== undefined ? ev : null)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            $scope.$emit('delete-item', ctrl.blockIdx)
        });
    }
}
//definition
angular.module('adminApp').component('singleBlockPlantContent', {
    templateUrl: 'plants/content/singleBlockPlantContent.template.html',
    controller: singleBlockPlantContent,
    bindings: {
        block: '=',
        blockIdx: '=',
        isFirst: '=',
        isLast: '=',
        showLanguage: '=',
    }
});
'use strict'
//controller
var singleBlockPlantContent = function ($mdDialog, $log, $scope, settingsService) {
    var ctrl = this;
    ctrl.editMode = false;
    ctrl.editBlockImages = '';
    ctrl.visibleIndex = 0;
    ctrl.imgViewStyle = {
        'object-position': '50% 50%',
        'position': 'relative',
        'object-fit': 'cover',
        'height': '300px',
        'width': '100%'
    }
    ctrl.pictureViewStyle = {
        'position': 'relative',
        'width': '100%'
    }

    settingsService.load(function (data) {
        $scope.globalSettings = data
    })

    //ng-class="{'plant-edit-thumb-container-contain':$ctrl.block.display==='contain','plant-edit-thumb-container-cover':$ctrl.block.display==='cover'||$ctrl.block.display===undefined||$ctrl.block.display===''||$ctrl.block.display===null,'plant-edit-thumb-container-none':$ctrl.block.display==='none'}"
    console.log('imgs block ', this.block);

    ctrl.applyImgStyle = function () {
        console.log('DDDD', this.block);
        if (!this.block) {
            return;
        }
        if (this.block.display === 'cover' || this.block.display === 'none' || this.block.display === 'contain') {
            this.imgViewStyle['object-fit'] = this.block.display;
        }
        if (ctrl.block.images[ctrl.visibleIndex].display === 'cover' || ctrl.block.images[ctrl.visibleIndex].display === 'none' || ctrl.block.images[ctrl.visibleIndex].display === 'contain') {
            this.imgViewStyle['object-fit'] = ctrl.block.images[ctrl.visibleIndex].display;
        }

        if (this.block.display === 'contain' || ctrl.block.images[ctrl.visibleIndex].display === 'contain') {
            this.pictureViewStyle['margin-left'] = 'auto';
            this.pictureViewStyle['margin-right'] = 'auto';
        }
        if (ctrl.block.images[ctrl.visibleIndex].display !== this.block.display) {
            this.pictureViewStyle.width = ctrl.block.images[ctrl.visibleIndex].display;
        }
        if (this.block.display_vertical_offset >= 0 && this.block.display_vertical_offset <= 100) {
            this.imgViewStyle['object-position'] = '50% ' + this.block.display_vertical_offset + '%';
        }
        if ((ctrl.block.images[ctrl.visibleIndex].display === 'cover' || ctrl.block.images[ctrl.visibleIndex].display === 'none') &&
            ctrl.block.images[ctrl.visibleIndex].display_vertical_offset >= 0 && ctrl.block.images[ctrl.visibleIndex].display_vertical_offset <= 100) {
            this.imgViewStyle['object-position'] = '50% ' + ctrl.block.images[ctrl.visibleIndex].display_vertical_offset + '%';
        }
    }
    setTimeout(function() {
        ctrl.applyImgStyle();
    }, 10);

    ctrl.cancelEdit = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Edit cancel confirmation')
            .textContent('Are you sure you want to cancel editation? All changed content will be lost.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            ctrl._clearTempVariables()
            ctrl.editMode = false;
            $scope.$parent.$emit('edit_enabled', false);
            $scope.$emit('show-mode-change', 'show')
        });
    };
    ctrl._clearTempVariables = function () {
        ctrl.editBlockImages = [];
    }
    ctrl.editBlock = function () {
        ctrl._clearTempVariables();
        ctrl.editBlockImages = ctrl.block.images
        ctrl.editMode = true
        $scope.$parent.$emit('edit_enabled', true);
        $scope.$emit('show-mode-change', 'edit')
    }
    ctrl.saveBlock = function () {
        ctrl.block.images = angular.copy(ctrl.editBlockImages)
        ctrl._clearTempVariables();
        ctrl.applyImgStyle();
        ctrl.editMode = false;
        $scope.$parent.$emit('edit_enabled', false);
        $scope.$emit('show-mode-change', 'show')
    }
    ctrl.nextSlide = function () {
        ctrl.visibleIndex++;
        ctrl.applyImgStyle();
    }
    ctrl.prevSlide = function () {
        ctrl.visibleIndex--;
        ctrl.applyImgStyle();
    }
    ctrl.moveUp = function () {
        $scope.$emit('move-item-up', ctrl.blockIdx)
    }
    ctrl.moveDown = function () {
        $scope.$emit('move-item-down', ctrl.blockIdx)
    }
    ctrl.getPreviewUrl = function(url) {
        return url.replace(/\{\$BASE_URL\}/, $scope.globalSettings.baseUrl);
    }
    ctrl.deleteBlock = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Delete block confirmation')
            .textContent('Are you sure you want to delete this block? All block content will be lost.')
            .ariaLabel('Warning')
            .targetEvent(ev !== undefined ? ev : null)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            $scope.$emit('delete-item', ctrl.blockIdx)
        });
    }
}
//definition
angular.module('adminApp').component('singleBlockPlantImages', {
    templateUrl: 'plants/content/singleBlockPlantImages.template.html',
    controller: singleBlockPlantContent,
    bindings: {
        block: '=',
        blockIdx: '=',
        isFirst: '=',
        isLast: '=',
    }
});
'use strict'
//controller
//TODO: merge this and sourcesBlock somehow!
var textBlockController = function ($mdDialog, $log, $scope, settingsService) {
    var ctrl = this;
    ctrl.options = ctrl.options = settingsService.getFullCkEditorSettings();
    if (ctrl.showLanguage === 'cz') {
        ctrl.selectedTabIdx = 1;
    } else {
        ctrl.selectedTabIdx = 0;
    }
}
//definition
angular.module('adminApp').component('textBlock', {
    templateUrl: 'plants/content/textBlock.template.html',
    controller: textBlockController,
    bindings: {
        showLanguage: '@',
        backgroundColor: '=',
        content: '=',
        content_en: '='
    }
});
'use strict'
//controller
var sourcesBlockController = function ($log, settingsService) {
    var ctrl = this;
    ctrl.options = settingsService.getFullCkEditorSettings();
    if (ctrl.showLanguage === 'cz') {
        ctrl.selectedTabIdx = 1;
    } else {
        ctrl.selectedTabIdx = 0;
    }
}
//definition
angular.module('adminApp').component('sourcesBlock', {
    templateUrl: 'plants/sources/sourcesBlock.template.html',
    controller: sourcesBlockController,
    bindings: {
        showLanguage: '@',
        content: '=',
        contentEn: '='
    }
});
'use strict'
//controller
var sourcesEditorController = function($log, $mdDialog) {
    var ctrl = this;
    ctrl.editMode=false;
    ctrl.editContent = '';
    ctrl.editContentEn = ''; 
    if (ctrl.showLanguage === undefined) {
        ctrl.showLanguage = 'cz';
    }
        
    ctrl.editBlock = function() {
        ctrl.editContent = ctrl.content;
        ctrl.editContentEn = ctrl.contentEn;
        ctrl.editMode = true;
    }
    ctrl.saveBlock = function() {
        ctrl.content = ctrl.editContent;
        ctrl.contentEn = ctrl.editContentEn;
        $log.debug('saving' + ctrl.editContentEn)
        ctrl._clearTempVariables();    
        ctrl.editMode = false;
    }
    ctrl._clearTempVariables = function() {
        ctrl.editContent = '';
        ctrl.editContentEn = '';
    }
    ctrl.cancelEdit = function(ev) {
        var confirm = $mdDialog.confirm()
            .title('Edit cancel confirmation')
            .textContent('Are you sure you want to cancel editation? All changed content will be lost.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            ctrl._clearTempVariables()
            ctrl.editMode = false;            
        });
    }
}
//definition
angular.module('adminApp').component('sourcesEditor', {		
		templateUrl : 'plants/sources/sourcesEditor.template.html',
		controller: sourcesEditorController,
        bindings: {            
            showLanguage: '=',
            content: '=',
            contentEn: '='
        }
    });
'use strict'
//controller
var imageAlignmentcomponent = function ($mdDialog, $log, $scope, plantsService) {

}
//definition single-block-plant-more-info
angular.module('adminApp').component('imageAlignment', {
    templateUrl: 'plants/content/image/imageAlignment.template.html',
    controller: imageAlignmentcomponent,
    bindings: {
        display: '=',
        displayVerticalOffset: '=',
        parentType: '@'
    }
});