'use strict';
angular.module('adminApp').controller('menuEditController', [ '$location', '$scope', '$mdDialog', '$mdMedia', 'menusService', '$timeout', '$routeParams', function($location, $scope, $mdDialog, $mdMedia, menusService, $timeout, $routeParams) {
	$scope.model = {
        id: $routeParams.menuId
    }
    $scope.flashMessages = []

    $scope.save = function() {
        menusService.save($scope.model, function(updatedMenu) {
            var okMessage = {id: new Date().getTime(), status: 'ok', message: 'Menu was saved'};
            $scope.flashMessages.push(okMessage);
            $timeout(function() {
                for (var i = 0; i < $scope.flashMessages.length; i++) {
                    if ($scope.flashMessages[i].id == okMessage.id) {
                        $scope.flashMessages.splice(i, 1);
                        return;
                    }
                }
            }, 3000);
        }, function(err) {
            alert('Unable to seve menu. Try again later.')
        });

    }
		$scope.openMenusList = function() {
			$location.url('menus')
		}
    $scope.onItemSaved = function(item) {
        var okMessage = {id: new Date().getTime(), status: 'ok', message: 'Menu item was saved'};
        $scope.flashMessages.push(okMessage);
        $timeout(function() {
            for (var i = 0; i < $scope.flashMessages.length; i++) {
                if ($scope.flashMessages[i].id == okMessage.id) {
                    $scope.flashMessages.splice(i, 1);
                    return;
                }
            }
        }, 3000);
    }
    $scope.onItemsReordered = function() {
        var okMessage = {id: new Date().getTime(), status: 'ok', message: 'Menu items order saved'};
        $scope.flashMessages.push(okMessage);
        $timeout(function() {
            for (var i = 0; i < $scope.flashMessages.length; i++) {
                if ($scope.flashMessages[i].id == okMessage.id) {
                    $scope.flashMessages.splice(i, 1);
                    return;
                }
            }
        }, 3000);
    }

    $scope.onItemPublished = function(published) {
        var pMessage = published?"published":"unpublished"
        var okMessage = {id: new Date().getTime(), status: 'ok', message: 'Menu item was ' + pMessage};
        $scope.flashMessages.push(okMessage);
        $timeout(function() {
            for (var i = 0; i < $scope.flashMessages.length; i++) {
                if ($scope.flashMessages[i].id == okMessage.id) {
                    $scope.flashMessages.splice(i, 1);
                    return;
                }
            }
        }, 3000);
    }

    menusService.loadMenu($scope.model.id, function(menuModel) {
        $scope.model = menuModel;
    });
} ]);
