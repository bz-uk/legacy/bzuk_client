'use strict';
var editGalleryComponent = function ($log, $location, galleriesService, tagsService, categoriesService, $mdDialog, $scope, $routeParams, webpService, leftMenuService, $timeout) {
    var ctrl = this;
    ctrl.gallery = { //init/new value
        active: false,
        plants: {}
    };
    ctrl.tags = {};
    ctrl.categories = [];
    ctrl.plants = [];
    ctrl.stickyPlants = {};
    ctrl.previewPlants = {};
    ctrl.isNew = $routeParams.galleryId == '_';
    ctrl.modelChanged = false;
    ctrl.saving = false;
    ctrl.loading = false;

    ctrl.webp = webpService.supported() ? '/webp' : '';

    $scope.$on('$destroy', $scope.$on('$locationChangeStart', function (event, next, current) {
        if (ctrl.modelChanged === true) {
            var confirm = $mdDialog.confirm()
                .title('Opusti editaci/vytvoření galerie?')
                .textContent('Galerie obsahuje neuložené změny. Odejít?')
                .ariaLabel('Opusti editaci/vytvoření galerie?')
                .ok('Ano')
                .cancel('Ne');
            $mdDialog.show(confirm).then(function () {
                $scope.modelChanged = false;
                leftMenuService.clearOpenConfirmation();
                $location.url('/galleries');
            }, function () {
            });
            event.preventDefault();
            return false;
        }
    }));

    ctrl.onStickyChange = function () {
        _.forEach(ctrl.stickyPlants, function (sticky, plantId) {
            if (sticky) {
                if (!ctrl.gallery.plants[plantId]) {
                    ctrl.gallery.plants[plantId] = {};
                }
                ctrl.gallery.plants[plantId].sticky = sticky;
            } else {
                if (ctrl.gallery.plants[plantId]) {
                    delete ctrl.gallery.plants[plantId].sticky;
                }
            }
        })
        ctrl.refreshGallery();
    }

    ctrl.onFilterChange = function () {
        //nothing
    }

    ctrl.processTags = function (tags) {
        for (var i = 0; i < tags.length; i++) {
            ctrl.tags[parseInt(tags[i].id)] = tags[i].name;
        }
    };

    ctrl.getTagName = function (tagId) {
        var id = tagId;
        console.log(id, ctrl.tags[id]);
        return ctrl.tags[id]
    }

    ctrl.processCategories = function (categories) {
        for (var i = 0; i < categories.length; i++) {
            ctrl.categories.push({name: categories[i].name, id: parseInt(categories[i].id)});
        }
        console.log('CC', ctrl.categories);
    };

    ctrl.getCardStyle = function (plant) {
        var style = {
            'background-color': '#ffffff'
        }
        if (ctrl.gallery.plants && ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].backgroundColor) {
            style['background-color'] = ctrl.gallery.plants[plant.id].backgroundColor;
        }
        return style;
    }

    ctrl.openPlantQuickEdit = function(plant, ev) {
        $mdDialog
        .show({
            controller: 'plantQuickEditDialogController',
            templateUrl: 'gallery/plantQuickEditDialog.template.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            resolve: {
                plant: function () {
                    return plant
                },
            },
            clickOutsideToClose: true,
            fullscreen: true
        })
        .then(function () {
            ctrl.reload();
        }, function () {
            console.log('You cancelled the dialog.');
        });
    }

    ctrl.openPlantSettings = function (plant, ev) {
        $mdDialog
            .show({
                controller: 'plantSettingsDialogController',
                templateUrl: 'gallery/plantSettingsDialog.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                resolve: {
                    plant: function () {
                        return plant
                    },
                    plantSettings: function () {
                        return {
                            sticky: ctrl.stickyPlants[plant.id] || false,
                            overrideImage: ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].previewImages ? 'selected' : 'latest',
                            selectedImages: ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].previewImages ? ctrl.gallery.plants[plant.id].previewImages : [],
                            backgroundColor: ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].backgroundColor ? ctrl.gallery.plants[plant.id].backgroundColor : '#ffffff'
                        }
                    }
                },
                clickOutsideToClose: true,
                fullscreen: true
            })
            .then(function (plantSettings) {
                if (plantSettings.sticky && !ctrl.stickyPlants[plant.id]) { //for loacal dialog
                    ctrl.stickyPlants[plant.id] = plantSettings.sticky;
                } else if (!plantSettings.sticky && ctrl.stickyPlants[plant.id]) {
                    delete ctrl.stickyPlants[plant.id];
                }
                if (plantSettings.sticky && !ctrl.gallery.plants[plant.id]) { //for global model&changes detection
                    if (!ctrl.gallery.plants[plant.id]) {
                        ctrl.gallery.plants[plant.id] = {};
                    }
                    ctrl.gallery.plants[plant.id].sticky = plantSettings.sticky;
                } else if (!plantSettings.sticky && ctrl.gallery.plants[plant.id]) {
                    delete !ctrl.gallery.plants[plant.id].sticky;
                }

                if (plantSettings.overrideImage === 'latest' && ctrl.gallery.plants[plant.id]) {
                    delete ctrl.gallery.plants[plant.id].previewImages;
                }
                if (plantSettings.overrideImage === 'selected') {
                    if (!ctrl.gallery.plants[plant.id]) {
                        ctrl.gallery.plants[plant.id] = {};
                    }
                    ctrl.gallery.plants[plant.id].previewImages = plantSettings.selectedImages;
                }
                if (plantSettings.backgroundColor) {
                    if (!ctrl.gallery.plants[plant.id]) {
                        ctrl.gallery.plants[plant.id] = {};
                    }
                    ctrl.gallery.plants[plant.id].backgroundColor = plantSettings.backgroundColor;
                }
                ctrl.refreshGallery();
            }, function () {
                console.log('You cancelled the dialog.');
            });
    }

    ctrl.refreshGallery = function () {
        var output = _.cloneDeep(ctrl.plants);
        // for (var i = 0; i < output.length; i++) {
        _.forIn(output, function (plant, plantId) {
            if (ctrl.stickyPlants[plantId]) {
                plant.sticky = ctrl.stickyPlants[plantId]
            }
        })

        // }
        ctrl.previewPlants = output;
    }
    ctrl.getActiveLabel = function () {
        if (ctrl.gallery.active) {
            return "Aktivní"
        } else {
            return "Neaktivní"
        }
    }
    ctrl.getPlantName = function (plant) {
        if (plant.name_lat !== undefined && plant.name_lat.length > 0) {
            return plant.name_lat;
        }
        return '';
    }

    ctrl.imageIsLandscape = function (img) {
        return img.width && img.height && parseInt(img.width) > parseInt(img.height);
    }

    ctrl.getSaveLabel = function () {
        if ($scope.isNew) {
            return ctrl.saving ? 'Vytvářím...' : 'Vytvořit'
        } else {
            return ctrl.saving ? 'Ukládám...' : 'Uložit'
        }
    }

    ctrl.getSaveAndCloseLabel = function () {
        if (ctrl.isNew) {
            return ctrl.saving ? 'Vytvážím...' : 'Vytvořit a zavřít'
        } else {
            return ctrl.saving ? 'Ukládám...' : 'Uložit a zavřít'
        }
    }

    ctrl.getImagePreviewWidth = function (img) {
        if (!ctrl.imageIsLandscape(img)) {
            return 250;
        }
        return 512;
    }

    ctrl.getSubTitle = function (plant) {
        if (plant.name !== undefined && plant.name.length > 0) {
            return plant.name;
        }
        return plant.family_name;
    }

    ctrl.getPlantsCount = function() {
        if (ctrl.plants) {
            return _.keys(ctrl.plants).length;
        } else {
            return 0;
        }
    }

    ctrl.getImageOverride = function (plant) {
        if (ctrl.gallery.plants && ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].previewImages && ctrl.gallery.plants[plant.id].previewImages.length > 0) {
            return true;
        }
        return false;
    }
    ctrl.getImageUrl = function (plant) {
        var idx = null;
        if (ctrl.gallery.plants && ctrl.gallery.plants[plant.id] && ctrl.gallery.plants[plant.id].previewImages && ctrl.gallery.plants[plant.id].previewImages.length > 0) {
            idx = parseInt(ctrl.gallery.plants[plant.id].previewImages[0]);
        } else {
            idx = _.keys(plant.images)[0];
        }
        if (!plant.images[idx]) {
            return '';
        }
        var url = plant.images[idx].url;
        if (url.substr(0, 7) === 'http://') {
            url = '//' + url.substr(6);
        }
        return url + '/max-width/' + ctrl.getImagePreviewWidth(plant.images[idx]) + ctrl.webp;

    };

    ctrl.getImagesCount = function(plant) {
        return _.size(plant.images);
    }

    ctrl.hasMoreTahnOneImage = function(plant) {
        return _.size(plant.images) > 1;
    }

    ctrl.openGalleriesList = function () {
        if (ctrl.modelChanged === true) {
            var confirm = $mdDialog.confirm()
                .title('Přejít na seznam galerii')
                .textContent('Galerie obsahuje neuložené změny. Přejít na seznam galerii?')
                .ariaLabel('Přejít na seznam galerii')
                .ok('Ano')
                .cancel('Ne');
            $mdDialog.show(confirm).then(function () {
                leftMenuService.clearOpenConfirmation();
                $location.url('/galleries')
            }, function () {
            });
        } else {
            $location.url('/galleries')
        }
    }

    ctrl.load = function (id) {
        return galleriesService.loadGallery($routeParams.galleryId)
            .then(function (galleryData) {
                ctrl.gallery = galleryData;
                ctrl.stickyPlants = {};
                _.forEach(ctrl.gallery.plants, function (plantData, plantId) {
                    console.log(plantData);
                    if (plantData.sticky) {
                        ctrl.stickyPlants[plantId] = plantData.sticky;
                    }
                })
            })
    }

    ctrl.getTagsFromFilter = function () {
        return [185];
    }

    ctrl.save = function (okCallback) {
        $scope.saving = true
        if (ctrl.isNew) {
            //galleriesService.
        } else {
            galleriesService.update(ctrl.gallery)
                .then(function () {
                    if (okCallback) {
                        ctrl.modelChanged = false;
                        return okCallback()
                    }
                    ctrl.load($routeParams.galleryId)
                        .then(function () {
                            ctrl.saving = false
                            leftMenuService.clearOpenConfirmation();
                            ctrl.modelChanged = false;
                            ctrl.originalModel = _.cloneDeep(JSON.parse(angular.toJson(ctrl.gallery)));
                        })
                        .catch(function (err) {
                            ctrl.saving = false
                            $rootScope.$emit('error-message', 'Uložení galerie zlyhalo');
                        })
                })
        }
    }

    ctrl.saveAndClose = function () {
        ctrl.save(function () {
            ctrl.openGalleriesList();
        })
    }

    ctrl.cancel = function () {
        ctrl.openGalleriesList();
    }

    ctrl.reload = function() {
        ctrl.load($routeParams.galleryId)
        .then(function () {
            $scope.$watch(function () {
                return ctrl.gallery
            }, function (newVal, oldVal) {
                if (ctrl.originalModel === undefined) {
                    ctrl.originalModel = _.cloneDeep(newVal.toJSON());
                    return;
                }
                ctrl.modelChanged = !_.isEqual(JSON.parse(angular.toJson(newVal)), $scope.originalModel);
                if (ctrl.modelChanged) {
                    leftMenuService.setOpenConfirmation('Opravdu odejít? Všechny změny budou ztracenny.')
                } else {
                    leftMenuService.clearOpenConfirmation();
                }
            }, true);
        })
        .then(function () {
            return galleriesService.getImages(ctrl.gallery.filter).then(function (data) {
                ctrl.loading = false;
                ctrl.plants = JSON.parse(angular.toJson(data.plants));
                console.log('P', ctrl.plants);
                ctrl.onStickyChange();
                ctrl.loading = false;
                //ctrl.previewPlants = JSON.parse(angular.toJson(plants));
            });
        })
    }


    ctrl.loading = true;
    tagsService.getAllTagsAsync(null, null, function () {
    })
        .then(function (tags) {
            ctrl.processTags(tags);
            return categoriesService.getAllCategoriesAsync(null, null);
        })
        .then(function (categories) {
            ctrl.processCategories(categories);
            if (!ctrl.isNew) {
                ctrl.reload()
            }
        });
}

angular.module('adminApp').filter('plantSorter', function () {
    return function (items, field) {
        var getPlantName = function (plant) {
            if (plant.name_lat !== undefined && plant.name_lat.length > 0) {
                return plant.name_lat;
            }
            return '';
        }
        var filtered = [];
        angular.forEach(items, function (item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            if (a.sticky && !b.sticky) {
                return 1
            }
            if (!a.sticky && b.sticky) {
                return -1
            }
            var aName = getPlantName(a);
            var bName = getPlantName(b);
            //return a.localeCompare(b);
            if (aName < bName) {
                return 1;
            }
            if (aName > bName) {
                return -1
            }
            return 0;
        });
        return _.reverse(filtered);
    };
});

angular.module('adminApp').component('editGallery', {
    templateUrl: 'gallery/editGallery.template.html',
    controller: editGalleryComponent,
    bindings: {}
});