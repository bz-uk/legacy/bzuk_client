'use strict';
const debug = require('debug')('image-preview-max-width-webp');
const sharp = require('sharp');
const process = require('process');
const path = require('path');
const fs = require('fs');
const os = require('os');
const mkdirp = require('mkdirp');
const HttpStatusCodes = require('http-status-codes');

const configuration = require('./../../../common/config');
const knex = require('./../../../database/local').knex;
const BaseRoute = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const UnauthorizedError = require('./../../../error/unauthorized');
const Resizer = require('./../../../common/image/resizer');
const fsPromise = require('./../../../common/fs');
const routeUtil = require('./../../../util/route');

const UPLAOD_TARGET_PATH = 'data/images/';
const RESIZE_WIDTHS = [100, 150, 200, 250, 384, 512, 640, 800, 1024, 1280, 2048];

module.exports = class Route extends BaseRoute {
    /**
     * @override >max-width/:maxWidth
     */
    getConfig() {
        return {
            method: 'get',
            path: '/images-preview/:imageId/max-width/:maxwidth/webp',
            name: 'maxWidthWebpPreview'
        }
    }

    main(req, res) {
        debug('Params: ', req.params)
        if (!req.params.imageid || !req.params.maxwidth || RESIZE_WIDTHS.indexOf(parseInt(req.params.maxwidth)) === -1) {
            res.status(HttpStatusCodes.BAD_REQUEST).json({errorCode: 'invalid_value', message: 'invalid value', z: parseInt(req.params.maxwidth)});
            return Promise.resolve();
        }
        let fileName = req.params.imageid + '.webp';
        debug('Searching for file ', fileName);
        return fsPromise.fileExists(path.join(process.cwd(), UPLAOD_TARGET_PATH, req.params.maxwidth + '', fileName))
        .then((exists) => {
            if (!exists) {
                debug('file does not exists.')
                return sequelize.models.ImageCompat.findOne({where: {id: req.params.imageid}})
                .then((image) => {
                    debug('resizing...');
                    return Resizer.resize(path.join(process.cwd(), UPLAOD_TARGET_PATH, 'originals', image.name), 'webp', image.id, [req.params.maxwidth], UPLAOD_TARGET_PATH);
                })
            } else {
                debug('file already exists.')
            }
        })
        .then(() => {
            debug('returning image');
            routeUtil.serveFileInline(res, path.join(process.cwd(), UPLAOD_TARGET_PATH, req.params.maxwidth, req.params.imageid + '.webp'), 'webp');
        })
    }
}