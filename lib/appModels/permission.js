'use strict';
const _     = require('lodash');
const debug = require('debug')('AppPermission');

// const BaseModel  = require('./base');
// const bookshelf  = require('./../database/local').bookshelf;
const knex       = require('./../database/local').knex;
// const Collection = require('./../database/local').bookshelf.Collection;
const models = require('./../database/local').sequelize.models;
const sequelize = require('./../database/local').sequelize;

const DB_MODEL = 'Permission';

/**
 * @class Role
 */
module.exports = class Permission {

    // dbModel() {
    //     return bookshelf.model(DB_MODEL);
    // }

    /**
     *
     * @param permissionId
     * @return {Promise}
     */
    static loadById(permissionId) {
        return models[DB_MODEL].findOne({where: {id: permissionId}})
        .then((model) => {
            return model;
        })
        //return Permission.$loadBy({id: permissionId});
    }

    static loadByRoleId(roleId) {
        debug('Loading role permissions: ', roleId);
        let query = knex
        .from('ro2_role_permissions')
        .innerJoin('ro2_permissions', 'ro2_permissions.id', 'ro2_role_permissions.permission_id').where('ro2_role_permissions.role_id', roleId)
        return sequelize.query(query.toString(), { type: sequelize.QueryTypes.SELECT, model: models[DB_MODEL]})
        .then((rows) => {
            return rows;
            // let result = [];
            // for (let i = 0; i < rows.length; i++) {
            //     result.push(new Permission(rows[i]));
            // }
            // return result;
        })
    }
};