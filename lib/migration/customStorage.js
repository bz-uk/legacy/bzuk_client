const sequelize = require('./../database/local').sequelize;

module.exports = class CustomStorage {
    logMigration(migrationName) {
        return sequelize.models.Migration.create({name: migrationName, finished_at: new Date()})
    }

    unlogMigration(migrationName) {
        return sequelize.models.Migration.findOne({name: migrationName})
        .then((model) => {
            return model.destroy();
        })
    }

    executed() {
        return sequelize.models.Migration.findAll()
        .then((results) => {
            return results.map((item) => {
                return item.name;
            })
        })
    }
}