<?php
namespace botgarApi\plants;

use Slim\Slim;

class CreateTag {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message) {
      $response = ['status'=>'error', 'message'=> $message];
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run() {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);

      //validation
      if (!isset($data->name)) {
        return $this->returnInvalidResponse('Please enter tag name');
    	}

      //ok now create

      $conf = $this->app->container->get('configuration');
      $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

      $db->beginTransaction();

      //map point

      $sql = 'insert into tags(name, type, name_lowercase) values('.$db->quote($data->name).',1,'.$db->quote(strtolower($data->name)).')';
      $result = $db->query($sql);
      if (!$result) {
          $db->rollback();
          return $this->returnInvalidResponse('Unable to create new plant tag: ' . $sql);
      }
      $tagId = $db->lastInsertId();
      $db->commit();
      $response = ['status'=> 'ok', 'message' => 'Tag created', 'id' => $tagId];
      $this->app->response->setStatus(200);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
      return;
    }
}
