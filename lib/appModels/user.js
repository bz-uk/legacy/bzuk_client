'use strict';
const _     = require('lodash');
const debug = require('debug')('AppUser');

//const BaseModel      = require('./base');
const models = require('./../database/local').sequelize.models;
//const bookshelf      = require('./../database/local').bookshelf;

const DB_MODEL = 'User';

/**
 * @class User
 * @type {User}
 */
module.exports = class User {

    dbModel() {
        return models[DB_MODEL];
    }

    hasPermission(name, level, ownership) {
        return true;
    }

    /**
     *
     * @param userId
     * @return {Promise}
     */
    static loadById(userId) {
        return models[DB_MODEL].findOne({where: {id: userId}})
        .then((model) => {
            return model;
        })
        //return BaseModel.$loadOneBy({id: userId}, DB_MODEL, User);
    }

    static loadByUsername(username) {
        return models[DB_MODEL].findOne({where: {username: username}})
        .then((model) => {
            return model;
        })
        //return BaseModel.$loadOneBy({username: username}, DB_MODEL, User);
    }
};