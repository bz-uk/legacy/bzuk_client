<?php
namespace botgarApi\plants;

use Slim\Slim;

class DeleteCategory
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message)
    {
        $response = ['status' => 'error', 'message' => $message];
        $this->app->response->setStatus(400);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }

    public function run($categoryId)
    {
        if (!isset($categoryId) || !is_numeric($categoryId)) {
            return $this->returnInvalidResponse('Category id must be numeric value');
        }

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $db->beginTransaction();

        $sql = 'UPDATE plants SET category_id=NULL WHERE category_id=' . $categoryId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollBack();
            return $this->returnInvalidResponse('Unable to unassign category from plants: ' . $sql);
        }
        $sql = 'DELETE from plant_category WHERE id='.$categoryId;
        $db->query($sql);
        if (!$result) {
            $db->rollBack();
            return $this->returnInvalidResponse('Unable to delete category: ' . $sql);
        }
        $db->commit();
        $response = ['status' => 'ok', 'message' => 'category saved'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
