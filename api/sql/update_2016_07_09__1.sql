CREATE TABLE `authors` (
  `id` INT NOT NULL,
  `name` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `authors`
  ADD COLUMN `note` VARCHAR(255) NULL AFTER `name`;

CREATE TABLE `sources` (
    `id` INT NOT NULL,
    `name` VARCHAR(512) NULL,
    `url` VARCHAR(255) NULL,
    `note` VARCHAR(200) NULL,
    `type` INT NULL,
    PRIMARY KEY (`id`));

CREATE TABLE `plant_authors` (
      `plant_id` INT NOT NULL,
      `author_id` INT NOT NULL,
      PRIMARY KEY (`plant_id`, `author_id`));

CREATE TABLE `plant_sources` (
        `plant_id` INT NOT NULL,
        `source_id` INT NOT NULL,
        PRIMARY KEY (`plant_id`, `source_id`));

ALTER TABLE `botgar`.`plant_authors` 
        ADD COLUMN `note` VARCHAR(255) NULL AFTER `author_id`;
