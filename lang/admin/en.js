'use strict';
module.exports = {
    "Logout": "Logout", //top right user menu item
    "Dashboard": "Dashboard",
    "Plants": "Plants",
    "Loading...": "Loading...",
    "Total plants:": "Total plants:",
    "Published:": "Published:",
    "Articles": "Articles",
    "Articles:": "Articles:",
    "Create": "Create"
}