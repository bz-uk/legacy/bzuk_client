'use strict'
angular.module('adminApp').directive('articleEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'article/articleEdit.template.html',
		controller: 'articleEditController'
	};
});
