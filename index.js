'use strict';
const Application = require('./lib/application');
const configuration = require('./lib/common/config'); //TODO currently not editable

let app = new Application(configuration);
app.on('listen', (port) => {
    console.log('Listening on port ' + port)
});
app.on('error', (err) => {
    console.log('Application error', err, err.stack);
});
app.listen();