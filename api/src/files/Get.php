<?php

namespace botgarApi\files;

use Slim\Slim;

class Get
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($fileId)
    {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $sql = "select * from settings where name='baseUrl'";
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setBody(json_encode(['error' => 'unable to get baseurl', 'debug' => $sql]));
            $this->app->response->setStatus(400);
            return;
        }
        $row = $result->fetch();
        $baseUrl = $row['value'];

        $sql = 'SELECT * FROM files WHERE id=' . $fileId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setBody(json_encode(['error' => 'unable to get file', 'debug' => $sql]));
            $this->app->response->setStatus(404);
            return;
        }
        $fileInfo = $result->fetch();

        $response = file_get_contents($fileInfo['path']);
        $type = $fileInfo['type'];
        $this->app->response->header('Content-Type', $type);
        $this->app->response->header('Cache-Control', 'max-age=10');
        $size = strlen($response);
        $this->app->response->header('Content-Length', $size);
        echo $response;
        return;
    }
}
