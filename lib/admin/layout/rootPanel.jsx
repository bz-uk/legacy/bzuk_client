'use strict';
import React from 'react';
import AppBar from 'material-ui/AppBar';
import ActionPermIdentity from 'material-ui/svg-icons/action/perm-identity'
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import ActionExitToApp from 'material-ui/svg-icons/action/exit-to-app';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import muiThemeable from 'material-ui/styles/muiThemeable';
import IconButton from 'material-ui/IconButton';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';
import Popover from 'material-ui/Popover';
import FontIcon from 'material-ui/FontIcon';
import {hashHistory} from 'react-router'
const Translate = require('react-i18nify').Translate;

import eventBus from './../eventBus';
import i18n from './../i18n/i18n.js';

import LeftMenu from '../menu/leftMenu.jsx';

const PanelComponent = muiThemeable()(class RootPanelComponent extends React.Component {
    constructor(props) {
        super(props);

        let showLabels = true;
        let hiddenMenu = true;
        if (JSON.parse(localStorage.getItem('left-menu-hidden')) === false) {
            hiddenMenu = false;
        }
        if (JSON.parse(localStorage.getItem('left-menu-show-labels')) === false) {
            showLabels = false;
        }

        this.state = {
            drawerVisible: false || !hiddenMenu,
            hiddenMenu: hiddenMenu,
            profileMenuVisible: false,
            activeScreenName: this.props.controller.activeScreen.getCode(),
            expanded: showLabels //TODO merge this with left menu -> catch menu clicn of 'expand', allow to set menu width
        }
    }

    componentDidMount() {
        //this.props.leftMenu.on('menuItemSelected', this.onMenuItemSelected);
        eventBus.on('screen-activated', this.onScreenActivated);
    }

    componentWillUnmount() {
        //this.props.leftMenu.off('menuItemSelected', this.onMenuItemSelected);
        eventBus.off('screen-activated', this.onScreenActivated);
    }

    onMenuPinTouchTap = () => {
        this.setState({hiddenMenu: !this.state.hiddenMenu}, () => {
            localStorage.setItem('left-menu-hidden', JSON.stringify(this.state.hiddenMenu));
        });
    }

    render() {
        let styles = {
            drawerMenu: {
                top: 64,
                overflow: 'hidden',
                height: 'calc(100vh - 64px)',
                maxWidth: this.state.expanded ? '12rem' : '4rem',
                transition: 'max-width .3s ease'
            },
            profileMenuIcon: {
                position: 'relative'
            }
        }
        if (!this.state.hiddenMenu) {
            styles.drawerMenu.float = 'left';
            styles.drawerMenu.borderRight = '1px rgb(224, 224, 224) solid';
            styles.drawerMenu.marginRight = '0.5rem';
            styles.drawerMenu.boxShadow = 'none';
            styles.drawerMenu.top = 0;
            styles.drawerMenu.position = 'relative';
        }
        return (
            <div>
                <AppBar
                    title={this.getTitle()}
                    iconElementLeft={this.state.hiddenMenu?<IconButton onTouchTap={this.toggleDrawer}>{this.state.drawerVisible ?
                        <NavigationClose
                            color={this.props.muiTheme.palette.alternateTextColor}/>
                        :
                        <NavigationMenu
                            color={this.props.muiTheme.palette.alternateTextColor}/>
                    }</IconButton>:<span />}
                    iconElementRight={<div style={styles.profileMenuIcon}><IconButton
                        onTouchTap={this.toggleProfileMenu}><ActionPermIdentity
                        color={this.props.muiTheme.palette.alternateTextColor}/></IconButton>
                        <Popover
                            open={this.state.profileMenuVisible}
                            anchorEl={this.state.profileMenuanchorEl}
                            onRequestClose={this.handleProfileMenuClose}
                        >
                            <Menu>
                                <MenuItem leftIcon={<ActionExitToApp/>} primaryText={i18n.t('Logout')}
                                          onTouchTap={this.logout}/>
                            </Menu>
                        </Popover></div>}
                />
                <Drawer containerStyle={styles.drawerMenu} open={this.state.drawerVisible}>
                    <LeftMenu onPinTouchTap={this.onMenuPinTouchTap} hidable={this.state.hiddenMenu} showLabels={this.state.expanded}
                              onExpanded={this.onMenuExpanded} onMenuItemSelected={this.onMenuItemSelected}
                              screens={this.props.screens}/>
                </Drawer>
                {this.props.children}
            </div>
        )
    }

    onMenuExpanded = (expanded) => {
        this.setState({expanded: expanded}, () => {
            localStorage.setItem('left-menu-show-labels', JSON.stringify(this.state.expanded));
        })
    }

    toggleProfileMenu = (event) => {
        event.preventDefault();
        this.state.profileMenuanchorEl = event.currentTarget;
        this.state.profileMenuVisible = true;
        this.setState(this.state);
    }

    logout = (event) => {
        window.location.href = '/logout';
    }

    handleProfileMenuClose = () => {
        this.state.profileMenuVisible = false;
        this.setState(this.state);
    }

    onScreenActivated = (screen) => {
        this.state.activeScreenName = screen.getCode();
        this.setState(this.state);
        document.title = this.getTitle();
    }

    onMenuItemSelected = (mnuItemComponent, menuItemCode) => {
        if (menuItemCode.substr(0, 4) === 'old-') {
            window.location = menuItemCode.substr(4);
            return;
        }
        if (this.state.hiddenMenu === true) {
            this.state.drawerVisible = false;
        }
        this.setState(this.state);
        hashHistory.push(menuItemCode)
    }

    toggleDrawer = (evt) => {
        this.state.drawerVisible = !this.state.drawerVisible;
        this.setState(this.state);
    }

    getTitle() {
        let title = 'Admin'
        if (this.state.activeScreenName) {
            title = this.state.activeScreenName;
        }
        return title;
    }
});

export default class RootPanel {
    constructor(screens) {
        this.screens = screens;
        //this.menu       = new LeftMenu(screens);
    }

    getComponent(controller) {
        let self = this;
        return class extends React.Component {
            constructor(props) {
                super(props);
                console.log(props.children)
            }

            render() {
                let props = {screens: self.screens, children: this.props.children, controller: controller};
                return (
                    <PanelComponent {...props}></PanelComponent>
                );
            }
        }
    }
}