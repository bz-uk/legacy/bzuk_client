'use strict';
const Promise            = require('bluebird');
const AuthenticatedRoute = require('./../../common/authenticatedBackendRoute');
const RouteRedirect      = require('./../../error/routeRedirect');
const debug              = require('debug')('login-route');

module.exports = class Route extends AuthenticatedRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'post',
            path: '/logout',
            name: 'logout'
        }
    }

    main(req, res) {
        req.session.destroy(() => {
            debug('session destroyed!');
            return Promise.reject(new RouteRedirect('/login'));
        })
    }
}