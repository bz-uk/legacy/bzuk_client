'use strict';
class LoggedUser {
    constructor() {
        this.userPermissions = window['app_settings_2256da08-abd3-4ac9-bd97-eb780a5138f7'].permissions;
        this.userRole        = window['app_settings_2256da08-abd3-4ac9-bd97-eb780a5138f7'].role;
        this.userData        = window['user_2256da08-abd3-4ac9-bd97-eb780a5138f7'];
    }

    getRole() {
        return this.userRole;
    }

    hasPermission(permission) {
        if (!permission || !permission.name) {
            console.error('Invalid permission ', permission);
            throw Error('not valid permission ');
        }
        let name = permission.name;
        if (!this.userPermissions[permission.name]) {
            return false;
        }
        if (permission.level && !this.userPermissions[permission.name].level.toLowerCase() !== permission.level.toLowerCase()) {
            return false;
        }
        if (permission.ownership && !this.userPermissions[permission.name].ownership.toLowerCase() !== permission.ownership.toLowerCase()) {
            return false;
        }
        return true;
    }
}

export default new LoggedUser();