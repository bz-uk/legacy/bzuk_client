<?php
namespace botgarApi\plants;

use Slim\Slim;

class PlantModel
{
    private $data;
    private $db;

    private function isNew()
    {
        return $this->data == null || !isset($this->data['id']) || $this->data['id'] == null;
    }

    /**
     * @return PlantModel
     */
    public static function loadFromData($data, $db)
    {
        $instance = new self();
        $instance->data = $data;
        $instance->db = $db;
        return $instance;
    }

    /**
     * @return PlantModel
     */
    public static function loadFromDb($id, $db)
    {
        $sql = 'select * from plants where id=' . $id;
        $result = $db->query($sql);
        if (!$result) {
            throw new Exception('Unable to load plant ' . $id);
            //TODO: add err message
        }
        $instance = new self();
        $instance->data = $result->fetch(\PDO::FETCH_ASSOC);
        $instance->db = $db;
        return $instance;
    }

    private function getId()
    {
        return $this->data['id'];
    }

    private function quote($var)
    {
        return $this->db->quote($var);
    }

    public function updateAuthors($authorsData)
    {
        $sql = 'DELETE from plant_authors where plant_id=' . $this->getId();
        $result = $this->db->query($sql);
        if (!$result) {
            $error = $this->db->errorInfo();
            throw new \Exception('Unable to process author: ' . $sql . ', error: ' . $error[2]);
        }
        foreach ($authorsData as $author) {
            $sql = 'INSERT INTO plant_authors(author_id, plant_id) VALUES(' . $author->id . ', ' . $this->getId() . ')';
            $result = $this->db->query($sql);
            if (!$result) {
                $error = $this->db->errorInfo();
                throw new \Exception('Unable to inser author: ' . $sql . ', error: ' . $error[2]);
            }
        }
    }

    public function updatePlantImages($images)
    {
        $sql = 'delete from plant_image where plant_id=' . $this->getId();
        $result = $this->db->query($sql);
        if (!$result) {
            throw new \Exception('Unable remove old images');
        }
        $order = 0;
        foreach ($images as $image) {
            $sql = 'insert into plant_image(plant_id, image_id, `order`) values(' . $this->getId() . ', ' . $image->id . ', ' . $order . ')';
            $result = $this->db->query($sql);
            if (!$result) {
                throw new \Exception('Unable assign image');
            }
            $order++;
        }
    }

    public function upsertBlocks($blocksData, $deleteBlocks = [])
    {
        //delete old shit
        error_log('we have ' . count($deleteBlocks) . ' to delete');
        foreach ($deleteBlocks as $block) {
            if (isset($block->id) && is_numeric($block->id) && $block->id > 0) {
                error_log('deleting block ' . $block->id);
                $now = strftime('%Y-%m-%d %H:%M:%S');
                $sql = 'UPDATE plant_block SET deleted_at="' . $now . '" WHERE id=' . $block->id;
                $result = $this->db->query($sql);
                if (!$result) {
                    throw new \Exception('Unable to delete old block: ' . $sql);
                }
            }
            //todo: clean dependencies
        }
        if (!is_array($blocksData) || (is_array($blocksData) && count($blocksData) === 0)) {
            return;
        }
        $order = 0;
        $existingIds = [];
        $contentLength = 0;
        foreach ($blocksData as $block) {
            if ($block->type == 0) {
                //text block
                $content = $block->content;
                $content_plain = strip_tags($block->content);
                $contentEn = $block->content_en;
                $contentEn_plain = strip_tags($block->content_en);
                $contentLength += strlen($content_plain);
                $backgroundColor = '';
                if (isset($block->backgroud_color)) {
                    $backgroundColor = $block->backgroud_color;
                }
                if (isset($block->id) && is_numeric($block->id) && $block->id > 0) {
                    $sql = 'update plant_block SET backgroud_color=' . $this->quote($backgroundColor) . ',content=' . $this->quote($content) . ',content_en=' . $this->quote($contentEn) . ',content_plain=' . $this->quote($content_plain) . ',content_en_plain=' . $this->quote($contentEn_plain) . ', `order`=' . $order . ' WHERE id=' . $block->id;
                } else {
                    $sql = 'insert into plant_block(plant_id, backgroud_color, content, content_en, `order`, type, content_plain, content_en_plain) values(' . $this->getId() . ', ' . $this->quote($backgroundColor) . ',' . $this->quote($content) . ', ' . $this->quote($contentEn) . ',' . $order . ', 0, ' . $this->quote($content_plain) . ', ' . $this->quote($contentEn_plain) . ')';
                }
                $result = $this->db->query($sql);
                if (!$result) {
                    $error = $this->db->errorInfo();
                    throw new \Exception('Unable to process block: ' . $sql . ', error: ' . $error[2]);
                }
                $order++;
            } else if ($block->type == 1) {
                //images block
                if (!isset($block->images) || count($block->images) == 0) {
                    continue;
                }
                $display = 'cover';
                if (isset($block->display)) {
                    $display = $block->display;
                }
                $displayVerticalOffset = 50;
                if (isset($block->display_vertical_offset) && is_numeric($block->display_vertical_offset)) {
                    $displayVerticalOffset = $block->display_vertical_offset;
                }
                if (isset($block->id) && is_numeric($block->id) && $block->id > 0) {
                    $blockId = $block->id;

                    $sql = 'update plant_block SET `order`=' . $order . ', `display`=' . $this->quote($display) . ', display_vertical_offset=' . $displayVerticalOffset . ' WHERE id=' . $block->id;
                    $result = $this->db->query($sql);
                    if (!$result) {
                        $error = $this->db->errorInfo();
                        throw new \Exception('Unable to update image block: ' . $sql . ', error: ' . $error[2]);
                    }
                } else {
                    $sql = 'insert into plant_block(plant_id, `order`, type, display, display_vertical_offset) values(' . $this->getId() . ',' . $order . ', 1, '.$this->quote($display).', '.$displayVerticalOffset.')'; //insert if missing
                    $result = $this->db->query($sql);
                    if (!$result) {
                        $error = $this->db->errorInfo();
                        throw new \Exception('Unable to process image block: ' . $sql . ', error: ' . $error[2]);
                    }
                    $blockId = $this->db->lastInsertId();
                }
                $imgOrder = 0;
                //clear old images
                $sql = 'delete from plant_block_image where plant_block_id=' . $blockId;
                $result = $this->db->query($sql);
                if (!$result) {
                    $error = $this->db->errorInfo();
                    error_log('delete old images err: ' . $sql . var_export($error, true));
                    throw new \Exception('Unable to process image: ' . $sql);
                }
                foreach ($block->images as $image) {
//                    if (!isset($image->status) || $image->status == null) {
//                        $sql = 'UPDATE plant_block_image SET `order`=' . $imgOrder . ' where plant_block_id=' . $blockId . ' and image_id=' . $image->id;
//                        $result = $this->db->query($sql);
//                        if (!$result) {
//                            $error = $this->db->errorInfo();
//                            error_log('delete img err: ' . $sql . ';' . var_export($error, true));
//                            throw new \Exception('Unable to process image: ' . $sql);
//                        }
//                        $imgOrder++;
//                        continue;
//                    }
//                    if ($image->status === 'done') {
                    $sql = 'insert into plant_block_image(plant_block_id, image_id, `order`) values(' . $blockId . ', ' . $image->id . ',' . $imgOrder . ')';
                    $result = $this->db->query($sql);
                    if (!$result) {
                        $error = $this->db->errorInfo();
                        error_log('inserting img err: ' . $sql . var_export($error, true));
                        throw new \Exception('Unable to process image: ' . $sql);
                    }
                    $imgOrder++;
//                    } else if ($image->status === 'deleted') {
//                        $sql = 'delete from plant_block_image where plant_block_id=' . $blockId . ' and image_id=' . $image->id;
//                        $result = $this->db->query($sql);
//                        error_log('deleting img: ' . $sql);
//                        if (!$result) {
//                            $error = $this->db->errorInfo();
//                            error_log('delete img err: ' . $sql . var_export($error, true));
//                            throw new \Exception('Unable to process image: ' . $sql);
//                        }
//                    }
                }
                $order++;
            } else if ($block->type == 2) {
                if (isset($block->id) && is_numeric($block->id) && $block->id > 0) {
                    $sql = 'update plant_block SET `order`=' . $order . ' WHERE id=' . $block->id;
                } else {
                    $sql = 'insert into plant_block(plant_id, `order`, type) values(' . $this->getId() . ', ' . $order . ', 2)';
                }
                $result = $this->db->query($sql);
                if (!$result) {
                    $error = $this->db->errorInfo();
                    throw new \Exception('Unable to process block: ' . $sql . ', error: ' . $error[2]);
                }
                $order++;
            } else {
                error_log('Unknown block type. Skipping. ' . var_export($block, true));
            }
        }
        return ['contentLength' => $contentLength];
    }
}