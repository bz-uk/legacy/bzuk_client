<?php
namespace botgarApi\gallery;

use Slim\Slim;

class GetOne
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($galleryId)
    {
        $conf = $this->app->container->get('configuration');

        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        if (!is_numeric($galleryId)) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(["success"=>false, 'code'=>'wrongValue']));
            return;
        }

        $sql = 'SELECT dgw.* FROM drupal_gallery_widgets dgw WHERE dgw.id=' . $galleryId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(["success"=>false, 'code'=>'noResult']));
            return;
        }
        $row = $result->fetch(\PDO::FETCH_ASSOC);
        $gallery = [
            'id' => $row['id'],
            'filter' => json_decode($row['filter']),
            'code' => $row['code'],
            'active' => ($row['active'] + 0)==1?true:false,
            'show_plant_category' => ($row['show_plant_category'] + 0)==1?true:false,
            'sort_plants_order' => $row['sort_plants_order'],
            'sort_categories_order' => $row['sort_categories_order'],
            'show_category_headings' => ($row['show_category_headings'] + 0)==1?true:false,
            'sort_by_category' => ($row['sort_by_category'] + 0)==1?true:false,
            'name' => $row['name'],
            'plants' => json_decode($row['plants']),
        ];

        $response = json_encode($gallery);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
