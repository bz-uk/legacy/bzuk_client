'use strict';
const fs = require('fs');
const debug = require('debug')('fs');

module.exports.fileExists = function fileExists(fileName) {
    return new Promise(function(resolve, reject) {
        fs.stat(fileName, (err, stats) => {
            if (err) {
                if (err.code === 'ENOENT') {
                    debug('file ' + fileName + ' does not exists');
                    return resolve(false);
                }
                console.error('Err', e, e.stack);
                return reject(err);
            }
            debug('fileExists ok');
            return resolve(true);
        })
    })
};