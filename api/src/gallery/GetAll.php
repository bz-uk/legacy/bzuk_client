<?php
namespace botgarApi\gallery;

use Slim\Slim;

class GetAll {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run() {
        $conf = $this->app->container->get('configuration');

        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = 'SELECT dgw.id,dgw.code,dgw.active,dgw.name,dgw.filter,dgws.total_views FROM drupal_gallery_widgets dgw LEFT JOIN drupal_gallery_widgets_stats dgws on dgw.id=dgws.gallery_id';
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(["success"=>false, 'code'=>'noResult']));
            return;
        }
        $rows = [];
        //get plant images
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $rows[] = [
                'id' => $row['id'],
                'filter' => json_decode($row['filter']),
                'code' => $row['code'],
                'active' => ($row['active'] + 0)==1?true:false,
                'name' => $row['name'],
                'views' => $row['total_views']==null?0:$row['total_views']
                //'plants' => json_decode($row['plants']),
            ];
        }
//        $cluster = new \CouchbaseCluster($conf['cb:server']);
//        $bucket = $cluster->openBucket($conf['cb:bucket']);
//
//        $query = \CouchbaseN1qlQuery::fromString("select `default`.*, meta(`default`).id as id from `default` where META(`default`).id LIKE 'bzukpr_widgets_drgal_%' AND META(`default`).id NOT LIKE '%_stats' AND META(`default`).id NOT LIKE '%_cache' LIMIT 10 OFFSET 0");
//        $result = $bucket->query($query);
//
//        foreach ($result->rows as $row) {
//            $stats = new \stdClass();
//            try {
//                $statsResult = $bucket->get($row->id . '_stats');
//                $stats = $statsResult->value;
//            } catch (\Exception $e) {
//                $stats->views = 0;
//            }
//            $row->views = $stats->views;
//        }
        $response = json_encode($rows);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
