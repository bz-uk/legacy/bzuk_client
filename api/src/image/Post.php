<?php

namespace botgarApi\image;

use Slim\Slim;

class Post
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run()
    {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $sql = "select * from settings where name='baseUrl'";
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setBody(json_encode(['error' => 'unable to get baseurl', 'debug' => $sql]));
            $this->app->response->setStatus(400);
            return;
        }
        $row = $result->fetch();
        $baseUrl = $row['value'];
        /*
      $filename = $_FILES['file']['name'];
      $meta = $_POST;
      $destination = $meta['targetPath'] . $filename;
      move_uploaded_file( $_FILES['file']['tmp_name'] , $destination );
      */
      error_log(var_export($_FILES, true));
        if (!isset($_FILES['file'])) {
            $response = ['error' => 'No files found'];
            $this->app->response->setBody(json_encode($response));
            $this->app->response->setStatus(400);

            return;
        }
        $img = [];
        $file = $_FILES['file'];
        //
        error_log('processing file '.$file['tmp_name']);
        if ($file['error'] === 0) {
            $name = uniqid('img-'.date('Ymd').'-');
            if ($file['type'] === 'image/jpeg') {
                $name .= '.jpg';
            }
            if ($file['type'] === 'image/png') {
                $name .= '.png';
            }
            if ($file['type'] === 'image/gif') {
                $name .= '.gif';
            }
            if (move_uploaded_file($file['tmp_name'], '../data/img/'.$name) === true) {
                $info = getimagesize('../data/img/'.$name);
                $now = strftime('%Y-%m-%d %H:%M:%S');
                $sql = 'insert into image(bundle_id, name, width, height, type, size, path, created_date) values(0, '.$db->quote($name).', '.$info[0].', '.$info[1].', '.$db->quote($file['type']).', '.$file['size'].', '.$db->quote('../data/img/'.$name).','.$db->quote($now).')';
                $db->beginTransaction();
                $result = $db->query($sql);
                if (!$result) {
                    error_log('no img data saved');
                    $this->app->response->setBody(json_encode(['error' => 'unable to save img info to db', 'debug' => $sql]));
                    $this->app->response->setStatus(400);
                    $db->rollback();
                    return;
                }
                $id = $db->lastInsertId();
                $url = '//' . $baseUrl . 'api/api/images/' . $id;
                $sql = 'update image set url='.$db->quote($url).' WHERE id='. $id;
                $result = $db->query($sql);
                if (!$result) {
                    error_log('no img data saved');
                    $this->app->response->setBody(json_encode(['error' => 'unable to save img info to db', 'debug' => $sql]));
                    $this->app->response->setStatus(400);
                    $db->rollback();
                    return;
                }
                $db->commit();
                $img = ['url' => $url, 'name' => $file['name'], 'size' => $file['size'], 'id'=>$id];
            }
        }
        error_log(var_export($img, true));

        $this->app->response->setBody(json_encode($img));
        $this->app->response->setStatus(200);

        return;
    }
}
