'use strict';
import Logger from './../util/logger';
/**
 * @class Settings
 */
export default class Settings {
    /**
     * @param String varIdentifier - string identifier. A javascript object should be available on window as key with this name
     * @example
     * window['app_settings_randomKey123456'] = {name: 'testSetting'};
     * let settings = new Settings('app_settings_randomKey123456');
     * console.log(settings.get('name')) //will print 'testSetting' string to console
     */
    constructor(varIdentifier, options) {
        this.logger;
        if (options && options.logger) {
            this.logger = options.logger;
        } else {
            this.logger = new Logger(Logger.LEVEL_DEBUG);
        }
        this.settingsVarName = varIdentifier;
        if (!window[this.settingsVarName]) {
            this.logger.warn('Application settings not found!');
        }
    }
    
    get(settingName) {
        if (window[this.settingsVarName][settingName] === undefined) {
            return null;
        }
        return window[this.settingsVarName][settingName];
    }
}