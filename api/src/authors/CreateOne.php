<?php
namespace botgarApi\authors;

use Slim\Slim;

class CreateOne {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message, $debug = null) {
      $response = ['status'=>'error', 'message'=> $message];
      if ($debug !== null) {
        $response['debug'] = $debug;
      }
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run() {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);
        
        $json = $this->app->request->getBody();
    	$data = json_decode($json);
        if (!isset($data->name)) {
            return $this->returnInvalidResponse('name is missing');
        }
        $note = '';
        if (isset($data->note)) {
            $note = $data->note;
        }
        
        $now = strftime('%Y-%m-%d %H:%M:%S');
        $sql = 'INSERT INTO authors(name, note, created_at, updated_at) VALUES('.$db->quote($data->name).', '.$db->quote($data->note).', "'.$now.'", "'.$now.'")';
        $result = $db->query($sql);
        if (!$result) {
            return $this->returnInvalidResponse('Unable to add author');
        }
        $authorId = $db->lastInsertId();
        $sql = 'SELECT * from authors WHERE id=' . $authorId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            return;
        }
        $author = $result->fetch(\PDO::FETCH_ASSOC);

        $response = ['status'=> 'ok', 'message' => 'Author saved', 'author' => $author];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
