<?php
namespace botgarApi\mapOverlay;

use Slim\Slim;

/**
 * Class Get
 * @package botgarApi\newsfeed
 * @url http://192.168.33.99/botgar_api/api/news/[fromDate]
 * [fromDate] - YYYY-MM-DD HH:MM:SS (uri encoded)
 */
class Get {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {

        $this->app = $app;
    }

    public function run($code) {
        $db = new \PDO('mysql:host=localhost;port=3306;dbname=botgar;charset=UTF8;','botgar','ooIKRCsJBYVo3MSwG3KO9oLyn');
        $sql = "SELECT * FROM map_overlay where code='$code'";
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            return;
        }
        $row = $result->fetch(\PDO::FETCH_ASSOC);
        $output = ['id' => $row['id'], 'name' => $row['name'], 'code'=>$row['code'], 'markers' => []];
        $sql = "SELECT * FROM map_marker where map_overlay_id=" . $row['id'];
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            $response = json_encode($output);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $output['markers'][] = [
                'id' => $row['id'],
                'title' => $row['title'],
                'contents' => $row['contents'],
                'marker_visible' => $row['marker_visible'],
                'tag_id' => $row['tag_id'],
                'image' => $row['image'],
                'article_id' => $row['article_id'],
                'latitude' => $row['latitude'],
                'longitude' => $row['longitude']
            ];
        }
        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);

//        sleep(2);
//        $items = simplexml_load_file('../data/newsfeedItems.xml');
//        $output = array();
//        foreach ($items as $item) {
//            $apiItem = new ItemModel();
//            $apiItem->loadFromXml($item);
//            if ($fromDate != null && strtotime($apiItem->getDateCreated()) > strtotime($fromDate)) {
//                $output[] = $apiItem->asArray();
//            }
//        }
//        $response = json_encode($output);
//        $this->app->response->header('Content-Length', strlen($response));
//        $this->app->response->setBody($response);
    }
}
