<?php
namespace botgarApi\circuit;

use Slim\Slim;

class CreateOnePrimaryPoint {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($circuitId) {
        $json = $this->app->request->getBody();
        $data = json_decode($json);

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT * FROM circuit where id=" . $circuitId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            $this->app->response->setBody(['error' => 'unable to load circuit']);
            return;
        }
        $circuit = $result->fetch(\PDO::FETCH_ASSOC);
        //
        $db->beginTransaction();
        //
        // //image
        $image_id = 'null';
        if (isset($data->image) && $data->image != null && $data->image->id > 0) {
          $image_id = $data->image->id;
        }
        $mapMarkerId = 'null';
        if (isset($data->markers) && is_array($data->markers) && count($data->markers) > 0) {
            $sql = 'insert into map_marker (latitude, longitude, marker_visible, title,contents) values('.$data->markers[0]->latitude.', '.$data->markers[0]->longitude.', 1, " ", "")';
            $result = $db->query($sql);
            if (!$result) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to insert marker point: ' . $sql);
            }
            $mapMarkerId = $db->lastInsertId();
        }

        $sql = "INSERT INTO circuit_primary_points(name, name_en, active, circuit_id, image_id, map_marker_id) values(".$db->quote($data->name).", ".$db->quote($data->name_en).",1, ".$circuitId.", ".$image_id.", ".$mapMarkerId.")";

        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            //$err = var_export($db->errorInfo(), true);
            $this->app->response->setBody(json_encode(['error' => 'unable to create point', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }
        $pointId = $db->lastInsertId();

        //parts
        foreach ($data->parts as $part) {
          $sql = "INSERT INTO primary_point_parts(primary_point_id, content, content_en, `order`) values(".$pointId.",".$db->quote($part->content).", ".$db->quote($part->content_en).",0)";
          $result = $db->query($sql);
          if (!$result) {
              $this->app->response->setStatus(400);
              //$err = var_export($db->errorInfo(), true);
              $this->app->response->setBody(json_encode(['error' => 'unable to create point part content', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
              return;
          }
        }

        //primary points circuit count
        $sql = 'select count(*) as cnt from circuit_primary_points where circuit_id=' . $circuitId . ' and is_deleted = 0';
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(['error' => 'unable to count primary points', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }
        $row = $result->fetch(\PDO::FETCH_ASSOC);
        $primaryPointsCount = $row['cnt'];

        $sql = 'UPDATE circuit set primary_count=' . $primaryPointsCount . ' where id=' . $circuitId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(['error' => 'unable to update circuit', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }

        $db->commit();

        $response = json_encode(['status' => 'ok', 'message' => 'point created']);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
