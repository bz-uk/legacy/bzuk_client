'use strict';
import BaseRequest from './../../common/baseRequest';
import EventBus from './../eventBus';
import axios from 'axios';

export default class Request extends BaseRequest {
    constructor(options) {
        super();
        //
    }

    /**
     * @override
     * @param type
     * @param url
     * @param data
     * @return {Promise}
     */
    $request(type, url, data) {
        //TODO: fix post/get thing...
        return axios({method: type, url: url, data: data, xsrfCookieName: '_csrf'});
    }

    /**
     * @override
     * @param eventName
     * @param eventData
     */
    $emit(eventName, eventData) {
        EventBus.emit(eventName, eventData);
    }
}