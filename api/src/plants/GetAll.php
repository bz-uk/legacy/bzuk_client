<?php
namespace botgarApi\plants;

use Slim\Slim;

class GetAll
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run()
    {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $json = $this->app->request->getBody();
        $data = json_decode($json);
        $filter = $data->filter;


        //TODO: do not use join!
        $sql = "SELECT DISTINCT pl.*, plc.name as category_name, plf.name as family_name, plf.name_en as family_name_en, plf.name_lat as family_name_lat  FROM plants pl left join plant_category plc on pl.category_id=plc.id left join plant_family plf on pl.family_id=plf.id";
        $filterQuery = [];
        $filterQuery[] = 'pl.is_deleted=0';
        if (isset($filter->tags) && is_array($filter->tags) && count($filter->tags) > 0) {
            $tagIds = [];
            foreach ($filter->tags as $tag) {
                $tagIds[] = $tag->id;
            }
            $sql .= ' inner join plant_tags plt on plt.plant_id=pl.id AND plt.tag_id in (' . join(',', $tagIds) . ')';
        }
        if (isset($filter->category) && $filter->category != null) {
            $filterQuery[] = ' pl.category_id = ' . $filter->category;
        }
        if (isset($filter->family) && $filter->family != null) {
            $filterQuery[] = ' pl.family_id = ' . $filter->family;
        }
        if (isset($filter->author_filter) && $filter->author_filter != null && $filter->author_filter != 'any') {

            if ($filter->author_filter == 'selected' && count($filter->authors) > 0) {
                $authorsIds = [];
                foreach ($filter->authors as $author) {
                    $authorsIds[] = $author;
                }
                $sql .= ' inner join plant_authors pla on pla.plant_id=pl.id AND pla.author_id in (' . join(',', $authorsIds) . ')';
            }
            if ($filter->author_filter == 'none') {
                $sql .= ' left join plant_authors pla on pla.plant_id=pl.id';
                $filterQuery[] = 'pla.author_id is null';
            }
        }
        if (isset($filter->search) && $filter->search != null) {
            $srcColumns = [
                'pl.name like \'%' . str_replace("'", "\\'", $filter->search) . '%\'',
                'pl.name_en like \'%' . str_replace("'", "\\'", $filter->search) . '%\'',
                'pl.name_lat like \'%' . str_replace("'", "\\'", $filter->search) . '%\''
            ];
            if (is_numeric($filter->search)) {
                $srcColumns[] = 'pl.id=' . $filter->search;
            }
            $filterQuery[] = '(' . join(' OR ', $srcColumns) . ')';
        }
        if (count($filterQuery) > 0) {
            $sql .= ' WHERE ' . join($filterQuery, ' and ');
        }
        $rsql = $sql;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            return;
        }
        $rows = [];
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $row['category_id'] = $row['category_id'] + 0;
            $row['is_active'] = $row['is_active'] === '1' ? true : false;
            $row['id'] = $row['id'] + 0;
            try {
                if ($row['tags'] == null) {
                    $tagsSerialized = [];
                    $sql = 'select t.* from plant_tags pt inner join tags t where t.id=pt.tag_id AND pt.plant_id=' . $row['id'] . ' AND t.type=1';
                    $tagRes = $db->query($sql);
                    if ($tagRes) {
                        while ($t = $tagRes->fetch(\PDO::FETCH_ASSOC)) {
                            $tagsSerialized[] = $t;
                        }
                    }
                    $sql = 'update plants set tags=' . $db->quote(json_encode($tagsSerialized)) . ' where id=' . $row['id'];
                    $db->query($sql);
                }
                $row['tags'] = json_decode($row['tags']);
            } catch (Exception $e) {
                $row['tags'] = [];
            }
            try {
                if ($row['authors'] == null) {
                    $authorsSerialized = [];
                    $sql = 'select a.* from plant_authors pa inner join authors a where a.id=pa.author_id AND pa.plant_id=' . $row['id'] . ' AND a.is_deleted=0';
                    $authorsRes = $db->query($sql);
                    if ($authorsRes) {
                        while ($a = $authorsRes->fetch(\PDO::FETCH_ASSOC)) {
                            $authorsSerialized[] = $a;
                        }
                    }
                    $sql = 'update plants set authors=' . $db->quote(json_encode($authorsSerialized)) . ' where id=' . $row['id'];
                    $db->query($sql);
                    $row['authors'] = $authorsSerialized;
                } else {
                    $row['authors'] = json_decode($row['authors']);
                }
            } catch (Exception $e) {
                $row['authors'] = [];
                $row['authors_exception'] = $e->getMessage();
            }
            $rows[] = $row;
        }

        $markers = [];
        $sql = 'SELECT DISTINCT pl.id as plant_id,pl.is_blooming,m.* FROM plants pl left join map_marker m on pl.map_marker_id=m.id OR m.map_overlay_id=pl.map_overlay_id';
        $filterQuery = [];
        $filterQuery[] = 'pl.is_deleted=0';
        $filterQuery[] = 'm.latitude is not null';
        $filterQuery[] = 'm.longitude is not null';
        if (isset($filter->tags) && is_array($filter->tags) && count($filter->tags) > 0) {
            $tagIds = [];
            foreach ($filter->tags as $tag) {
                $tagIds[] = $tag->id;
            }
            $sql .= ' inner join plant_tags plt on plt.plant_id=pl.id AND plt.tag_id in (' . join(',', $tagIds) . ')';
        }
        if (isset($filter->category) && $filter->category != null) {
            $filterQuery[] = ' pl.category_id = ' . $filter->category;
        }
        if (isset($filter->family) && $filter->family != null) {
            $filterQuery[] = ' pl.family_id = ' . $filter->family;
        }
        if (isset($filter->author_filter) && $filter->author_filter != null && $filter->author_filter != 'any') {
            if ($filter->author_filter == 'selected' && count($filter->authors) > 0) {
                $authorsIds = [];
                foreach ($filter->authors as $author) {
                    $authorsIds[] = $author;
                }
                $sql .= ' inner join plant_authors pla on pla.plant_id=pl.id AND pla.author_id in (' . join(',', $authorsIds) . ')';
            }
            if ($filter->author_filter == 'none') {
                $sql .= ' left join plant_authors pla on pla.plant_id=pl.id';
                $filterQuery[] = 'pla.author_id is null';
            }
        }
        if (isset($filter->search) && $filter->search != null) {
            //$filterQuery[] = '(pl.name like \'%' . $filter->search . '%\' OR pl.name_en like \'%' . $filter->search . '%\' OR pl.name_lat like \'%' . $filter->search . '%\')';
            $srcColumns = [
                'pl.name like \'%' . str_replace("'", "\\'", $filter->search) . '%\'',
                'pl.name_en like \'%' . str_replace("'", "\\'", $filter->search) . '%\'',
                'pl.name_lat like \'%' . str_replace("'", "\\'", $filter->search) . '%\''
            ];
            if (is_numeric($filter->search)) {
                $srcColumns[] = 'pl.id=' . $filter->search;
            }
            $filterQuery[] = '(' . join(' OR ', $srcColumns) . ')';
        }
        if (count($filterQuery) > 0) {
            $sql .= ' WHERE ' . join($filterQuery, ' and ');
        }
        $result = $db->query($sql);
        if ($result) {
            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                $markers[] = $row;
            }
        }

        $output = ['rows' => $rows, 'markers' => $markers, 'sql' => $sql, 'rows_sql' => $rsql];
        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
