'use strict';
angular.module('adminApp').directive('mainContent', ['$routeParams', function($routeParams) {
    return {
        restrict : 'E',
        templateUrl : "content/mainContent.template.html",
        controller: ['$scope', '$location', function($scope, $location) {
            $scope.model = {
                location: $location.path()
            }
            $scope.isMenuEdit = function(location) {
                return location == '/menus/' + $routeParams.menuId
            }
            $scope.isArticleEdit = function(location) {
                return location == '/articles/' + $routeParams.articleId
            }
            $scope.isCircuitEdit = function(location) {
                return location == '/circuits/' + $routeParams.circuitId
            }
            $scope.isCircuitPrimaryPointEdit = function(location) {
                return location == '/circuits/' + $routeParams.circuitId + '/primary-points/' + $routeParams.pointId
            }
            $scope.isMenuItemEdit = function(location) {
              return location == '/menus/' + $routeParams.menuId + "/menu-items/" + $routeParams.menuItemId
            }
            $scope.isPlantEdit = function(location) {
                return location == '/plants/' + $routeParams.plantId
            }
            $scope.isGalleryEdit = function(location) {
                return location == '/galleries/' + $routeParams.galleryId
            }
        }],
        link: function() {
            //console.log('LINKING!');
        }
    };
}]);
