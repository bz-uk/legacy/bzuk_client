<?php
namespace botgarApi\plants;

use Slim\Slim;

class AddPlant
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message, $debug = null)
    {
        $response = ['status' => 'error', 'message' => $message];
        if ($debug !== null) {
            $response['debug'] = $debug;
        }
        $this->app->response->setStatus(400);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }

    public function run()
    {
        $json = $this->app->request->getBody();
        $data = json_decode($json);

        error_log(var_export($data, true));

        //validation
        if (isset($data->plant_id) && is_numeric($data->plant_id)) {
            return $this->returnInvalidResponse('Plant id should be empty when creating new one!');
        }
        if (!isset($data->name) &&
            !isset($data->name_en) &&
            !isset($data->name_lat)
        ) {
            return $this->returnInvalidResponse('Please enter at least one name in some language');
        }

        //ok now create

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $db->beginTransaction();

        //map point
        $markerId = [];
        $overlayId = null;
        if (isset($data->markers) && is_array($data->markers) && count($data->markers) > 0) {
            if (count($data->markers) > 1) {
                //create overlay
                if (isset($data->name)) {
                    $overlayName = $data->name;
                } else {
                    $overlayName = 'plant_overlay';
                }
                $overlaySql = 'insert into map_overlay(name, code) values(' . $db->quote($overlayName) . ', ' . $db->quote($overlayName) . ')';
                $result = $db->query($overlaySql);
                if (!$result) {
                    $db->rollback();
                    return $this->returnInvalidResponse('Unable to create markers overlay: ' . $overlaySql);
                }
                $overlayId = $db->lastInsertId();
            }
            foreach ($data->markers as $marker) {
                $plantName = '';
                if (isset($data->name)) {
                    $plantName = $data->name;
                }
                $now = strftime('%Y-%m-%d %H:%M:%S');
                if ($overlayId != null) {
                    $sql = 'insert into map_marker(title, contents, marker_visible, latitude, longitude, map_overlay_id, created_at, updated_at) values(' . $db->quote($plantName) . ', ' . $db->quote($plantName) . ', 1, ' . $marker->latitude . ', ' . $marker->longitude . ', ' . $overlayId . ', "'.$now.'", "'.$now.'")';
                } else {
                    $sql = 'insert into map_marker(title, contents, marker_visible, latitude, longitude, created_at, updated_at) values(' . $db->quote($plantName) . ', ' . $db->quote($plantName) . ', 1, ' . $marker->latitude . ', ' . $marker->longitude . ', "'.$now.'", "'.$now.'")';
                }
                $result = $db->query($sql);
                if (!$result) {
                    $db->rollback();
                    return $this->returnInvalidResponse('Unable to create new marker point: ' . $sql);
                }
                $markerId[] = $db->lastInsertId();
            }
        }

        //category
        if (isset($data->new_category) && $data->new_category === 'new') {
            $sql = 'insert into plant_category(name) values(' . $db->quote($data->category) . ')';
            $result = $db->query($sql);
            if (!$result) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to create new category: ' . $sql);
            }
            $categoryId = $db->lastInsertId();
        } else if ($data->new_category === 'chosen') {
            if (isset($data->category)) {
                $categoryId = $data->category;
            } else {
                $categoryId = 'NULL';
            }
        } else if ($data->new_category === 'none') {
            $categoryId = 'NULL';
        }
        //family
        if (isset($data->new_family) && $data->new_family === 'new') {
            $familyEn = isset($data->family_en) ? $data->family_en : '';
            $familyLat = isset($data->family_lat) ? $data->family_lat : '';
            $sql = 'insert into plant_family(name, name_en, name_lat) values(' . $db->quote($data->family) . ', ' . $db->quote($familyEn) . ', ' . $db->quote($familyLat) . ')';
            $result = $db->query($sql);
            if (!$result) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to create new family');
            }
            $familyId = $db->lastInsertId();
        } else if ($data->new_family === 'chosen') {
            if (isset($data->family)) {
                $familyId = $data->family;
            } else {
                $familyId = 'NULL';
            }
        } else if ($data->new_family === 'none') {
            $familyId = 'NULL';
        }

        $name = isset($data->name) ? $data->name : '';
        $nameEn = isset($data->name_en) ? $data->name_en : '';
        $sources = isset($data->sources) ? $data->sources : '';
        $sourcesEn = isset($data->sources_en) ? $data->sources_en : '';
        $nameLat = isset($data->name_lat) ? $data->name_lat : '';
        $nameSk = isset($data->name_sk) ? $data->name_sk : '';
        $nameDe = isset($data->name_de) ? $data->name_de : '';
        $audioCz = isset($data->audio_cz) ? $data->audio_cz : '';
        $isActive = 0;
        if (isset($data->is_active) && ($data->is_active === true || $data->is_active === '1')) {
            $isActive = 1;
        }
        $occurrence = isset($data->occurrence) ? $data->occurrence : '';
        $now = strftime('%Y-%m-%d %H:%M:%S');
        $columns = ['is_active', 'name', 'name_en', 'name_lat', 'name_sk', 'name_de', 'category_id', 'family_id', 'occurrence', 'sources', 'sources_en', 'created_date', 'modified_date'];
        $values = [$isActive, $db->quote($name), $db->quote($nameEn), $db->quote($nameLat), $db->quote($nameSk), $db->quote($nameDe), $categoryId, $familyId, $db->quote($occurrence), $db->quote($sources), $db->quote($sourcesEn), $db->quote($now), $db->quote($now)];
        if ($audioCz) {
            $columns[] = 'audio_cz';
            $values[] = $audioCz;
        }
        if (count($markerId) == 1) {
            $columns[] = 'map_marker_id';
            $values[] = $markerId[0];
        } else {
            $columns[] = 'map_overlay_id';
            $values[] = $overlayId;
            //use overlay
            //$sql = 'insert into plants(audio_cz, is_active, name, name_en, name_lat, name_sk, name_de, category_id, family_id, occurrence) values(' . $audioCz . ',' . $isActive . ',' . $db->quote($name) . ',' . $db->quote($nameEn) . ',' . $db->quote($nameLat) . ',' . $db->quote($nameSk) . ',' . $db->quote($nameDe) . ',' . $categoryId . ', ' . $familyId . ', ' .  . ', ' . $db->quote($occurrence) . ')';
        }
        $sql = 'insert into plants('.join(',', $columns).') values('.join(',', $values).')';
        $result = $db->query($sql);
        if (!$result) {
            $err = $db->errorInfo();
            $db->rollback();
            return $this->returnInvalidResponse('Unable to create new plant: ' . $sql . ', error: ', var_export($err, true) );
        }
        $plantId = $db->lastInsertId();
        $plantModel = PlantModel::loadFromDb($plantId, $db);
        //blocks
        $contentLength = 0;
        try {
            $blocks_data = $plantModel->upsertBlocks($data->blocks);
            $contentLength = is_numeric($blocks_data['contentLength'])?$blocks_data['contentLength']:0;
        } catch (Exception $e) {
            $db->rollback();
            return $this->returnInvalidResponse('Unable to update plant blocks: ' . $e->getMessage());
        }
        $sql = 'update plants set content_length=' . $contentLength . ' where id=' . $plantId;
        $result = $db->query($sql);
        if (!$result) {
            $err = $db->errorInfo();
            $db->rollback();
            return $this->returnInvalidResponse('Unable to update content length: ' . $sql, var_export($err, true));
        }

        //images
        //$plantModel->updatePlantImages($data->images);
        if (isset($data->images)) {
            try {
                $plantModel->updatePlantImages($data->images);
            } catch (Exception $e) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to update plant images: ' . $e->getMessage());
            }
        }
        //tags
        if (isset($data->tags) && count($data->tags) > 0) {
            foreach ($data->tags as $tag) {
                $sql = 'insert into plant_tags(plant_id, tag_id) values(' . $plantId . ', ' . $tag->id . ')';
                $result = $db->query($sql);
                if (!$result) {
                    $err = $db->errorInfo();
                    $db->rollback();
                    return $this->returnInvalidResponse('Unable to insert plant tag: ' . $sql . ', ' . var_export($err, true));
                }
            }
            $tagsSerialized = [];
            $sql = 'select t.* from plant_tags pt inner join tags t where t.id=pt.tag_id AND pt.plant_id=' . $plantId . ' AND t.type=1';
            $result = $db->query($sql);
            if ($result) {
                while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                    $row['stored'] = true;
                    $tagsSerialized[] = $row;
                }
            }
            $sql = 'update plants set tags=' . $db->quote(json_encode($tagsSerialized)) . ' where id=' . $plantId;
            $result = $db->query($sql);
            if (!$result) {
                $err = $db->errorInfo();
                $db->rollback();
                return $this->returnInvalidResponse('Unable to update serialized tags: ' . $sql, var_export($err, true));
            }
        }
        //authors
        if (isset($data->authors) && count($data->authors) > 0) {
            $plantModel->updateAuthors($data->authors);
        }

        $db->commit();
        $response = ['status' => 'ok', 'message' => 'Menu items order saved', 'id' => $plantId];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
