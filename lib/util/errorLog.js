'use strict';
const winston = require('winston');

let logger = module.exports = new winston.Logger({
    level:      'error',
    transports: [
        new (winston.transports.File)({
            handleExceptions: true,
            filename: 'logs/error.log',
            humanReadableUnhandledException: true,
            json: false
        })
    ]
});