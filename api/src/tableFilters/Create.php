<?php
namespace botgarApi\tableFilters;

use Slim\Slim;

class Create {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run() {
      error_log('sssss');
        $json = $this->app->request->getBody();
        $data = json_decode($json);

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        //TODO: do not use join!
        $code = md5(microtime() + $data->tableName + $json);
        $sql = 'INSERT into `table_filter` (`code`, `table_name`, `filter_json`) values("'.$code.'", '.$db->quote($data->tableName).', '.$db->quote(json_encode($data->filter)).')';
        $result = $db->query($sql);
        if (!$result) {
          $response = ['status'=>'error', 'message'=> var_export($db->errorInfo(), true)];
          $this->app->response->setStatus(400);
          $response = json_encode($response);
          $this->app->response->header('Content-Length', strlen($response));
          $this->app->response->setBody($response);
        }

        $output = [
          'filter_code' => $code
        ];

        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
