'use strict'
angular.module('adminApp').directive('primaryPointEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'circuit/primary-points/primaryPointEdit.template.html',
		controller: 'primaryPointEditController'
	};
});
