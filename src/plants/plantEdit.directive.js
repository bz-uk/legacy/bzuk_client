'use strict'
angular.module('adminApp').directive('plantEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'plants/plantEdit.template.html',
		controller: 'plantEditController'
	};
});
