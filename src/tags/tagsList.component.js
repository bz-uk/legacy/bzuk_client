'use strict';
var tagsListController = function ($log, tagsService, plantsService, $mdDialog, $scope) {
    $log.debug('tags!');
    var ctrl = this;
    ctrl.model = {
        tags: tagsService.tagsList,
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }

    function success(tags) {
        ctrl.model.tags = tags;
    }

    tagsService.onTagDeleted($scope, function(event, data) {
        ctrl.getTags(true);
    });

    plantsService.onPlantTagSaved($scope, function(event, data) {
        $log.debug('s', data);
        ctrl.getTags(true);
    });

    ctrl.getTags = function (force) {
        ctrl.model.promise = tagsService.getAllTagsAsync(ctrl.query, force, success);
    }

    ctrl.deleteTag = function (ev, tag) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
            .title('Potvrzeni')
            .textContent('Tag "' + tag.name + '" bude natrvalo odebrán ze všech roslitn a smazán. Pokračovat?')
            .ariaLabel('Potvrzeni')
            .targetEvent(ev)
            .ok('Ano')
            .cancel('Zrušit');
        $mdDialog.show(confirm).then(function () {
            tagsService.deleteTag(tag);
        }, function () {
            //
        });
    };

    ctrl.editTag = function (ev, tag) {
        $mdDialog.show({
            controller: 'editTagDialogController',
            templateUrl: 'plants/editTagDialog.template.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            resolve: {
                tag: function () {
                    return angular.copy(tag)
                },
            },
            clickOutsideToClose: true,
            fullscreen: true
        })
            .then(function () {
                console.log('tags saved!')
                ctrl.getTags(true);
            }, function () {
                console.log('You cancelled the dialog.');
            });
    }

    ctrl.selected = [];
    ctrl.limitOptions = [5, 10, 15];
    ctrl.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    ctrl.getTags(true);
}

angular.module('adminApp').component('tagsList', {
    templateUrl: 'tags/tagsList.template.html',
    controller: tagsListController,
    bindings: {}
});