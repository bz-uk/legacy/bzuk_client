ALTER TABLE `primary_point_parts`
ADD COLUMN `is_deleted` TINYINT(4) NOT NULL DEFAULT 0 AFTER `order`;
ALTER TABLE `primary_point_parts`
ADD COLUMN `active` TINYINT(4) NOT NULL DEFAULT 0 AFTER `is_deleted`;
