'use strict'
angular.module('adminApp').controller('menuItemEditController', [ '$location', '$scope', '$mdDialog', '$mdMedia', 'menusService', '$timeout', '$routeParams', function($location, $scope, $mdDialog, $mdMedia, menusService, $timeout, $routeParams) {
  $scope.saving = false
	$scope.model = {
        id: $routeParams.menuItemId
    }
    $scope.menuId = $routeParams.menuId
    $scope.flashMessages = []

    $scope.save = function() {
        $scope.saving = true
        menusService.saveMenuItem($scope.menuId, $scope.model, function(updatedMenu) {
            var okMessage = {id: new Date().getTime(), status: 'ok', message: 'Menu item was saved'};
            $scope.flashMessages.push(okMessage);
            $timeout(function() {
                $scope.openMenuEdit()
            }, 1000);
        }, function(err) {
            $scope.saving = false
            alert('Unable to save menu. Try again later.')
        });
    }
    $scope.openMenusList = function() {
      $location.url('/menus/')
    }
		$scope.openMenuEdit = function() {
			$location.url('/menus/' + $scope.menuId)
		}
    $scope.cancel = function() {
      $scope.openMenuEdit()
    }
    $scope.getSaveLabel = function() {
      return $scope.saving?'Saving...':'Save'
    }
    $scope.options = {
  		    language: 'en',
  	  		format_tags: 'h1;h2',
  		    allowedContent: true,
  		    entities: false,
  		    //width: '100%'
  		  };

    menusService.loadMenuItem($scope.menuId, $scope.model.id, function(menuItemModel) {
        if (menuItemModel.publishable === '1') {
          menuItemModel.publishable = true
        } else {
          menuItemModel.publishable = false
        }
        if (menuItemModel.show_subtitle === '1') {
          menuItemModel.show_subtitle = true
        } else {
          menuItemModel.show_subtitle = false
        }
        $scope.model = menuItemModel;
    });
} ]);
