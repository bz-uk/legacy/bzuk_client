<?php
namespace botgarApi\plants;

use Slim\Slim;

class AddTags {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message) {
      $response = ['status'=>'error', 'message'=> $message];
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run() {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);

      //validation - ??
      $conf = $this->app->container->get('configuration');
      $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

      $db->beginTransaction();

      $log = [];
      foreach ($data->plants as $plant) {
        //existing tags
        $sql = 'select t.id from plant_tags pt inner join tags t where t.id=pt.tag_id AND pt.plant_id=' . $plant->id . ' AND t.type=1';
        $result = $db->query($sql);
        $existingTags = [];
        if ($result) {
          while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
              $existingTags[] = $row['id'];
          }
        }
        //to be added
        $addTags = [];
        foreach ($data->tags as $tag) {
          $sql = 'insert ignore into plant_tags(plant_id, tag_id) values('.$plant->id.', '.$tag->id.')';
          $result = $db->query($sql);
          if (!$result) {
              $db->rollback();
              return $this->returnInvalidResponse('Unable to insert plant tag: ' . $sql);
          }
        }
        $tagsSerialized = [];
        $sql = 'select t.* from plant_tags pt inner join tags t where t.id=pt.tag_id AND pt.plant_id=' . $plant->id . ' AND t.type=1';
        $result = $db->query($sql);
        if ($result) {
          while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
              $tagsSerialized[] = $row;
          }
        }
        $sql = 'UPDATE plants SET tags=' .$db->quote(json_encode($tagsSerialized)) . ' WHERE id=' . $plant->id;
        $result = $db->query($sql);
        if (!$result) {
          $db->rollback();
          return $this->returnInvalidResponse('Unable to update plant tags list: ' . $sql);
        }
      }

      $db->commit();
      $response = ['status'=> 'ok', 'message' => 'tags updated', 'log' => $log];
      $this->app->response->setStatus(200);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
      return;
    }
}
