<?php
$raw = file_get_contents('data.json');
$data = json_decode($raw);

require('../config/config.php');
$db = new \PDO('mysql:host='.$config['db:host'].';port='.$config['db:port'].';dbname='.$config['db:dbname'].';charset=UTF8;',$config['db:user'],$config['db:pass']);

$cnt = 0;
foreach ($data as $plant) {
  $latinName = $plant->name;
  $name = '';
  echo $latinName . "\n";
  if (stripos($latinName, '(') !== false) {
    $name = substr($latinName, stripos($latinName, '('));
    $latinName = substr($latinName, 0, stripos($latinName, '('));
    $name = str_replace(['(',')'], '', $name);
    $latinName = str_replace(['(',')'], '', $latinName);
  }
  $imgUrl = parse_url($plant->image);
  $imgName = basename($imgUrl['path']);
  echo $cnt . ': ' . $name . ' | ' . $latinName . '|' . $imgName . "\n";
  //image
  $type = substr($imgName, strlen($imgName) - 3);
  $q = "insert into image (name, width, height, type, size, url) values(".$db->quote($imgName).", 0,0,'".$type."',0,".$db->quote($plant->image).")";
  $db->query($q);
  $imageId = $db->lastInsertId();
  // $imageId = 0;
  //plant
  $q = "insert into plants (name, name_lat, category_id, family_id, created_date, modified_date) values(".$db->quote($name).", ".$db->quote($latinName).", 9, 6, '".strftime('%F %T')."', '".strftime('%F %T')."')";
  //echo $q . "\n";
  $db->query($q);
  $plantId = $db->lastInsertId();
  //$plantId = 0;
  //pl->image
  $q = "insert into plant_image (plant_id, image_id) values(".$plantId.",".$imageId.")";
  $db->query($q);
  //plantBlock
  $q = "insert into plant_block (plant_id, content, `type`, `order`, content_plain) values(".$plantId.",".$db->quote($plant->text).", 0, 0, ".$db->quote(strip_tags($plant->text)).")";
  //echo $q . "\n";
  $r = $db->query($q);
  if (!$r) {
    var_export($db->errorInfo());
  }
  $cnt++;
}
