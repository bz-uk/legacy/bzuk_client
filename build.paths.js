'use strict';
module.exports = {
    buildDir: 'build',
    publicDir: 'public',
    admin:    {
        styles:    {
            sourceDir: 'styles/admin',
            targetDir: 'public/css'
        },
        sourceDir: 'lib/admin',
        targetDir: 'public/js'
    },
    login:    {
        styles:    {
            sourceDir: 'styles/login',
            targetDir: 'public/css'
        },
        sourceDir: 'lib/login',
        targetDir: 'public/js'
    }
}