'use strict';
var categoriesListController = function ($log, categoriesService, plantsService, $mdDialog, $scope) {
    $log.debug('tags!');
    var ctrl = this;
    ctrl.model = {
        categories: categoriesService.tagsList,
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }

    categoriesService.onCategoryDeleted($scope, function(event, data) {
        ctrl.getCategories(true);
    });

    plantsService.onPlantCategorySaved($scope, function(event, data) {
        ctrl.getCategories(true);
    });

    ctrl.getCategories = function (force) {
        ctrl.model.promise = categoriesService.getAllCategoriesAsync(ctrl.query, force).then(function(categories) {ctrl.model.categories = categories;})
    }

    ctrl.deleteCategory = function (ev, category) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
            .title('Potvrzeni')
            .textContent('Kategorie "' + category.name + '" bude natrvalo odebrána ze všech roslitn a smazána. Pokračovat?')
            .ariaLabel('Potvrzení')
            .targetEvent(ev)
            .ok('Ano')
            .cancel('Zrušit');
        $mdDialog.show(confirm).then(function () {
            categoriesService.deleteCategory(category);
        }, function () {
            //
        });
    };

    ctrl.editCategory = function (ev, category) {
        $mdDialog.show({
            controller: 'editCategoryDialogController',
            templateUrl: 'plants/editCategoryDialog.template.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            resolve: {
                category: function () {
                    return angular.copy(category)
                },
            },
            clickOutsideToClose: true,
            fullscreen: true
        })
            .then(function () {
                $log.debug('category saved!')
                ctrl.getCategories(true);
            }, function () {
                $log.debug('You cancelled the dialog.');
            });
    }

    ctrl.selected = [];
    ctrl.limitOptions = [5, 10, 15];
    ctrl.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    ctrl.getCategories(true);
}

angular.module('adminApp').component('categoriesList', {
    templateUrl: 'categories/categoriesList.template.html',
    controller: categoriesListController,
    bindings: {}
});