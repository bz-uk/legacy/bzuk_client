'use strict';
import ExtendableError from './../error/extendableError.js';

export default class UnsuccessfulRequestError extends ExtendableError {
}