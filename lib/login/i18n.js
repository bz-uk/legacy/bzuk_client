'use strict';
const I18n = require('react-i18nify').I18n;
import langDictionary from './langDictionary.js'

I18n.setTranslations({
    en: langDictionary.get('en'),
    cs: langDictionary.get('cs'),
});

I18n.setLocale('cs');
/**
 * @singleton
 */
export default I18n;