ALTER TABLE `circuit_primary_points`
ADD COLUMN `map_marker_id` INT(11) NULL DEFAULT NULL AFTER `circuit_id`;

ALTER TABLE `circuit_primary_points`
ADD COLUMN `is_deleted` INT(11) NOT NULL AFTER `map_marker_id`;
