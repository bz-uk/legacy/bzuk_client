<?php

namespace botgarApi\plants;

use Slim\Slim;

class RemoveTag
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message)
    {
        $response = ['status' => 'error', 'message' => $message];
        $this->app->response->setStatus(400);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }

    public function run($plantId, $tagId)
    {
        if (!is_numeric($plantId) || !is_numeric($tagId)) {
            return $this->returnInvalidResponse('Invalid data');
        }

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $db->beginTransaction();
        $log = [];

        $sql = 'DELETE from plant_tags where plant_id='.$plantId.' and tag_id='.$tagId;
        $log[] = $sql;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            return $this->returnInvalidResponse('Unable to remove plant tag: '.$sql);
        }

        $tagsSerialized = [];
        $sql = 'select t.* from plant_tags pt inner join tags t where t.id=pt.tag_id AND pt.plant_id='.$plantId.' AND t.type=1';
        $result = $db->query($sql);
        if ($result) {
            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                $log[] = var_export($row, true);
                $tagsSerialized[] = $row;
            }
        }
        $sql = 'UPDATE plants SET tags='.$db->quote(json_encode($tagsSerialized)).' WHERE id='.$plantId;
        $log[] = $sql;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            return $this->returnInvalidResponse('Unable to update plant tags list: '.$sql);
        }

        $db->commit();
        $response = ['status' => 'ok', 'message' => 'tags updated', 'log' => $log];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);

        return;
    }
}
