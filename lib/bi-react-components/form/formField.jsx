'use strict';
import React from "react";
import FieldValidator from './fieldValidator.js';

export default class FormField extends React.Component {
    constructor(props) {
        super(props);
        
        this.formController  = this.props.formController;
        this.name            = this.props.name;
        this.validators      = this.props.validators;
        this.validator       = new FieldValidator(this.props.validators, this.props.errorMessage);
        this.validationTimer = null;
        
        if (!this.formController.fields[this.name]) {
            this.formController.fields[this.name] = this;
        } else {
            //merge settings
        }
        
        this.counter = 0;
        
        this.state = {
            valid: null,
            touched: this.props.value ? true : false,
            error: null,
            value: this.props.value,
            validating: false
        }
    }
    
    componentWillReceiveProps(props) {
        this.setState({
            value: props.value
        });
    }
    
    onChange = (e) => {
        let value = e;
        if (e.target && e.target.value !== undefined) {
            value = e.target.value;
        }
        this.state.value   = value;
        this.state.touched = true;
        this.setState(this.state, () => {
            this.formController.emit('field-change', this.name, value);
            if (this.validationTimer) {
                clearTimeout(this.validationTimer);
            }
            this.validationTimer = setTimeout(() => {
                this.state.validating = true;
                this.setState(this.state, () => {
                    this.validate()
                    .then((result) => {
                        this.formController.emit('validation-done', this.name, result);
                    })
                });
            }, 500);
        });
    }
    
    validate = () => {
        this.counter++
        return this.validator.validate(this.state.value)
        .then((result) => {
            if (result === null) {
                this.counter--;
                return;
            }
            this.state.valid = result;
            if (!this.state.valid) {
                this.state.error          = this.validator.error;
                this.formController.valid = false;
            } else {
                this.state.error     = null;
                let otherFieldsValid = true;
                for (let i = 0; i < this.formController.fields.length; i++) {
                    if (this.formController.fields[i] === this) {
                        return;
                    }
                    otherFieldsValid = this.formController.fields[i].state.valid && otherFieldsValid;
                }
                this.formController.valid = otherFieldsValid;
            }
            this.counter--;
            if (this.counter === 0) {
                this.state.validating = false;
                this.setState(this.state);
            }
            return this.state.valid;
        })
    }
    
    getWrappedElement() {
        return React.cloneElement(this.props.children, {
            value: this.state.value,
            errorText: this.state.validating ? '...' : this.state.error ? this.state.error : '',
            onChange: this.onChange
        })
    }
    
    render() {
        return this.getWrappedElement();
    }
}