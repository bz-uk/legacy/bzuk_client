angular.module('adminApp').controller('articleEditorDailogController', ['$scope', '$mdDialog', 'articleModel', 'articlesService', function($scope, $mdDialog, articleModel, articlesService) {
	console.log(articleModel);
	
	$scope.model = articleModel;
	
    $scope.hide = function() {
      $mdDialog.hide();
    };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.save = function() {
	articlesService.save($scope.model, function() {
		$mdDialog.hide();
	},function(errMessage) {
		alert('There was some problem during article save: ' + errMessage);
	});
    
  };
  $scope.options = {
		    language: 'en',
	  		format_tags: 'h1;h2',
		    allowedContent: true,
		    entities: false,
		    width: 500
		  };
}]);