'use strict';
const passport = require('passport');
const debug    = require('debug')('login-route');

const configuration = require('./../../../common/config');
const knex              = require('./../../../database/local').knex;
const BaseRoute         = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const UnauthorizedError = require('./../../../error/unauthorized');

module.exports = class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/widget-data/articles-count',
            name: 'getArticlesCountWidgetData'
        }
    }

    main(req, res) {
        let query = knex.count('* as cnt')
        .from(configuration.storage.mysql.tablePrefix + 'articles')
        .whereNull('deleted_at');
        return sequelize.query(query.toString(), {type: sequelize.QueryTypes.SELECT})
        .then((rows) => {
            return res.status(200).json({articlesCount: rows[0].cnt});
        })
    }
}