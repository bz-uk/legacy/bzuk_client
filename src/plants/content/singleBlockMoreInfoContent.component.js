'use strict'
//controller
var singleBlockPlantContent = function ($mdDialog, $log, $scope, plantsService) {
    $log.debug('blocks edit', this.block, this.blockIdx)
    this.editMode = false;
    this.editBlockContent = ''
    this.editBlockContentEnglish = ''
    this.editBlockColor = ''

    var ctrl = this;
    ctrl.families = [];
    ctrl.showMoreVisible = false;

    plantsService.getFamilies(function(families) {
        ctrl.families = families
    })

    ctrl.showMore = function() {
        $log.debug('more!');
        ctrl.showMoreVisible = !ctrl.showMoreVisible;
    }

    ctrl.moveUp = function() {
        $scope.$emit('move-item-up', ctrl.blockIdx)
    }
    ctrl.moveDown = function() {
        $scope.$emit('move-item-down', ctrl.blockIdx)
    }
    ctrl.deleteBlock = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Delete block confirmation')
            .textContent('Are you sure you want to delete this block?')
            .ariaLabel('Warning')
            .targetEvent(ev !== undefined ? ev : null)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            $scope.$emit('delete-item', ctrl.blockIdx)
        });
    }
    ctrl.getFamilyName = function() {
        for (var i = 0; i < ctrl.families.length; i++) {
            if (ctrl.families[i].id == ctrl.plant.family) {
                return ctrl.families[i].name
            }
        }
    }
    ctrl.getLatinFamilyName = function() {
        for (var i = 0; i < ctrl.families.length; i++) {
            if (ctrl.families[i].id == ctrl.plant.family) {
                return ctrl.families[i].name_lat
            }
        }
    }
    ctrl.getLatinName = function() {
        return ctrl.plant.name_lat;
    }
    ctrl.getGermanName = function() {
        return ctrl.plant.name_de;
    }
    ctrl.getSlovakName = function() {
        return ctrl.plant.name_sk;
    }
    ctrl.getEnglishName = function() {
        return ctrl.plant.name_en;
    }
    ctrl.getOccurence = function() {
        return ctrl.plant.occurrence;
    }
}
//definition single-block-plant-more-info
angular.module('adminApp').component('singleBlockMoreInfoContent', {
    templateUrl: 'plants/content/singleBlockMoreInfoContent.template.html',
    controller: singleBlockPlantContent,
    bindings: {
        block: '=',
        blockIdx: '=',
        isFirst: '=',
        isLast: '=',
        plant: '='
    }
});