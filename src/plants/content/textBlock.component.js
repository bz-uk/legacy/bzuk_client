'use strict'
//controller
//TODO: merge this and sourcesBlock somehow!
var textBlockController = function ($mdDialog, $log, $scope, settingsService) {
    var ctrl = this;
    ctrl.options = ctrl.options = settingsService.getFullCkEditorSettings();
    if (ctrl.showLanguage === 'cz') {
        ctrl.selectedTabIdx = 1;
    } else {
        ctrl.selectedTabIdx = 0;
    }
}
//definition
angular.module('adminApp').component('textBlock', {
    templateUrl: 'plants/content/textBlock.template.html',
    controller: textBlockController,
    bindings: {
        showLanguage: '@',
        backgroundColor: '=',
        content: '=',
        content_en: '='
    }
});