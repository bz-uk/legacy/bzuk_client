angular.module('adminApp').controller('tableTagDialogController', ['$scope', '$mdDialog', 'plantsService', 'tags', 'allTags','$log', 'selectedPlants', 'isAdd', function($scope, $mdDialog, plantsService, tags, allTags, $log, selectedPlants, isAdd) {
    //start: model
    $scope.model = {
      tags: tags,
      newTags: [],
      isAdd: isAdd
    }
    //end: model

    //start: listeners
    plantsService.onTagsSaved($scope, function(event) {
       console.log('saved')
       $scope.hide({relaod: true})
    })
    //end: listeners

    //start:: helpers
    $scope.getTitle = function() {
      if (isAdd) {
        return "Add tags"
      }
      return "Remove tags"
    }
    $scope.filterSelected = true;
    $scope.tagQuerySearch = function(criteria) {
        return allTags.filter($scope.createFilterFor(criteria))
    }

    $scope.createFilterFor = function(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(tag) {
            // console.log(tag.name_lowercase + ', pos:' + tag.name_lowercase.indexOf(lowercaseQuery))
            return (tag.name_lowercase.indexOf(lowercaseQuery) != -1);;
        }
    }
    $scope.dummySearch = function() {
      return []
    }
    //end: helpers

    $scope.save = function() {
      if (isAdd) {
        plantsService.addTags(selectedPlants, $scope.model.newTags)
      } else {
        plantsService.deleteTags(selectedPlants, $scope.model.tags)
      }
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);
