'use strict';
const debug = require('debug')('image-upload');
const Busboy = require('busboy');
const sharp = require('sharp');
const process = require('process');
const path = require('path');
const fs = require('fs');
const os = require('os');
const mkdirp = require('mkdirp');
const crypto = require('crypto');
const readChunk = require('read-chunk');
const fileType = require('file-type');

const configuration = require('./../../../common/config');
const knex = require('./../../../database/local').knex;
const BaseRoute = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const UnauthorizedError = require('./../../../error/unauthorized');
const Resizer = require('./../../../common/image/resizer');

const UPLAOD_TARGET_PATH = 'data/audio/';

module.exports = class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'post',
            path: '/upload/audio',
            name: 'uploadAudio'
        }
    }

    main(req, res) {
        let incommingFiles = [];
        let uploadedFiles = [];
        let transaction;
        let queue = Promise.resolve()
        .then(() => {
            return new Promise((resolve, reject) => {
                mkdirp(path.join(process.cwd(), UPLAOD_TARGET_PATH), (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve();
                })
            })
        })
        .then(() => {
            let busboy = new Busboy({headers: req.headers});
            busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
                console.log('MIME is ', mimetype);
                let serverFileName = 'img_' + Math.ceil(Math.random() * 8999999 + 1000000) + '_' + new Date().getTime() + '_' + crypto.createHash('md5').update(filename).digest("hex");
                let fullServerFileName = path.join(path.join(UPLAOD_TARGET_PATH), serverFileName);
                let valid = true;
                if (mimetype === 'audio/mp3') {
                    file.pipe(fs.createWriteStream(fullServerFileName));
                } else {
                    debug('Unsupported mime type ', mimetype);
                    file.resume();
                    valid = false;
                }
                file.on('end', () => {
                    debug('Got audio in temp: ', fullServerFileName);
                    let info = {
                        name: serverFileName,
                        fullName: fullServerFileName,
                        originalName: filename,
                        extension: path.extname(filename)
                    }
                    queue = queue.then(() => {
                        if (!valid) {
                            throw new Error(ERROR_CODES.admin.upload.unsupportedFormat);
                        }
                        return new Promise((resolve, reject) => {
                            fs.stat(fullServerFileName, (err, stats) => {
                                if (err) {
                                    reject(err);
                                }
                                resolve(stats);
                            });
                        })
                    })
                    .then((fsFileInfo) => {
                        info.size = fsFileInfo.size;
                        incommingFiles.push(info);
                        debug('file info stored');
                    })
                })
            });
            busboy.on('finish', function () {
                debug('upload finished');
                queue = queue.then(() => {
                    let subQueue = Promise.resolve();
                    debug('post-processing files', incommingFiles);
                    incommingFiles.map((fileData) => {
                        debug('processing ', fileData);
                        let context = {};
                        context.fileData = fileData;
                        subQueue = subQueue.then(() => {
                            return sequelize.transaction()
                        })
                        .then((t) => {
                            context.fileData.t = t;
                            return sequelize.models.FileCompat.create({
                                name: context.fileData.name,
                                type: 'audio/mp3',
                                path: context.fileData.fullName,
                                original_name: context.fileData.originalName,
                                size: context.fileData.size,
                            }, {transaction: context.fileData.t})
                        })
                        .then((model) => {
                            debug('audio added to db');
                            context.fileData.dataModel = model;
                            context.fileData.id = model.id;
                            //TODO parse length of mp3
                            // return sharp(context.fileData.fullName)
                            // .metadata()
                        })
                        .then((metadata) => {
                            debug('updating db item');
                            context.fileData.url = '{$BASE_URL}/files/audio/' + context.fileData.name;
                            return context.fileData.dataModel.update({
                                meta: JSON.stringify({totalTime: 123456}),
                                url: context.fileData.url,
                            }, {transaction: context.fileData.t});
                        })
                        .then(() => {
                            debug('file item updated');
                            return context.fileData.t.commit();
                        })
                        .then(() => {
                            debug('sql data commited');
                            delete context.fileData.t;
                            uploadedFiles.push({url: context.fileData.url, id: context.fileData.id});
                        })
                        .catch((e) => {
                            debug('upload error', e);
                            console.error('File ' + fileData.fullName + ' upload failed ', e);
                            return context.fileData.t.rollback();
                        })
                    });
                    return subQueue;
                });
                queue = queue.then(() => {
                    debug('all done!', uploadedFiles);
                    res.writeHead(200, {'Connection': 'close'});
                    res.end(JSON.stringify(uploadedFiles));
                })
                .catch((err) => {
                    res.writeHead(400, {'Connection': 'close'});
                    res.end(JSON.stringify({code: err.message, message: err.message}));
                })
            });
            req.pipe(busboy);
        })
        return queue;
    }
}