angular.module('adminApp').factory('menusService', ['$log', '$resource',function ($log, $resource) {
	var menusResource = $resource('/api/api/index.php/menus', {}, {
		'get':  {method:'GET', isArray:true},
		'save': {method: 'PUT'}
	});
	var menuResource = $resource('/api/api/index.php/menu/:menuId', {}, {
		'save': {method: 'PUT'},
		'get': {method: 'GET'}
	});
	var menuItemResource = $resource('/api/api/index.php/menu/:menuId/item/:menuItemId', {}, {
		'save': {method: 'PUT'},
		'get': {method: 'GET'}
	});
	var menuItemsResource = $resource('/api/api/index.php/menu/:menuId/items', {}, {
		'get': {method: 'GET'}
	});
	var menuItemsOrderResource = $resource('/api/api/index.php/menu/:menuId/items/order', {}, {
		'save': {method: 'POST'}
	});
	return {
		load: function(query, successCallback) {
			//TODO: implement query
			return menusResource.get({}, function(menus) {
				var menusList = angular.copy(menus);
				delete menusList.$promise;
				delete menusList.resolved;
				return successCallback(menusList);
			}, function (err) {
				$log.error('Unable to load menus: ',err);
			} )
		},
		save: function(menu, successCallback, errCallback) {
			return menuResource.save({menuId: menu.id},menu, function(data) {
				successCallback(data);data
			}, function(err) {
				console.log(err);
				var message = '';
				if (err.status == 500) {
					message = err.data;
				} else {
					message = err.data.message;
				}
				$log.error('Unable to save menu', err);
				errCallback(message);
			});
		},
		loadMenuItems: function(menuId, query, successCallback) {
			//TODO: implement query
			return menuItemsResource.get({menuId: menuId}, function(menuItems) {
				var menuItems = angular.copy(menuItems);
				delete menuItems.$promise;
				delete menuItems.resolved;
				return successCallback(menuItems);
			}, function (err) {
				$log.error('Unable to load menu items: ',err);
			} )
		},
		loadMenu: function(menuId, successCallback) {
			return menuResource.get({menuId: menuId}, function(menuModel) {
				var menuModel = angular.copy(menuModel);
				delete menuModel.$promise;
				delete menuModel.resolved;
				return successCallback(menuModel);
			}, function (err) {
				$log.error('Unable to load menu: ',err);
			} )
		},
		saveItemsOrder: function(menuId, newOrder, tableQuery, okCallback) {
			var data = {
				order: newOrder,
				fromIndex: (tableQuery.page-1) * tableQuery.limit
			}
			return menuItemsOrderResource.save({menuId: menuId}, data, function() {
				return okCallback()
			})
		},
		loadMenuItem: function(menuId, menuItemId, successCallback) {
			return menuItemResource.get({menuId: menuId, menuItemId: menuItemId}, function(menuItemModel) {
				var menuItemModel = angular.copy(menuItemModel);
				delete menuItemModel.$promise;
				delete menuItemModel.resolved;
				if (menuItemModel.published === '1') {
					menuItemModel.published = true
				} else {
					menuItemModel.published = false
				}
				return successCallback(menuItemModel);
			}, function (err) {
				$log.error('Unable to load menu item: ',err);
			} )
		},
		saveMenuItem: function(menuId, menuItemModel, successCallback) {
			return menuItemResource.save({menuId: menuId, menuItemId: menuItemModel.id}, menuItemModel, function(data) {
				return successCallback(data);
			}, function (err) {
				$log.error('Unable to load menu: ',err);
			} )
		}
	}
}]);
