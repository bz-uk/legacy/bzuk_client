/**
 * @module
 */
var imagePreviewDailogController = function ($scope, $mdDialog, image, imagesService, $log) {
    $scope.imageUrl = image.preview_url;
    $scope.description = image.description;
    $scope.descriptionEn = image.description_en;
    $scope.authors = image.authors;
    $scope.authorsEn = image.authors_en;
    $scope.displayVerticalOffset = image.display_vertical_offset;
    $scope.display = image.display;

    $scope.save = function () {
        var data = { description: $scope.description, descriptionEn: $scope.descriptionEn, authors: $scope.authors, authorsEn: $scope.authorsEn, display: $scope.display, displayVerticalOffset: $scope.displayVerticalOffset };
        imagesService.save(image.id, data).then(function() {
            $mdDialog.hide(data);
        }).catch(function(err) {
            $log.error(err)
            alert('Unable to save image');
        })
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
};
imagePreviewDailogController.$inject = ['$scope', '$mdDialog', 'image', 'imagesService', '$log'];
angular.module('adminApp').controller('imagePreviewDailogController', imagePreviewDailogController);