'use strict'
angular.module('adminApp').directive('secondaryPointsTable', function() {
	return {
		restrict : 'E',
		templateUrl : 'circuit/primary-points/secondaryPointsTable.template.html',
		controller: 'secondaryPointsTableController',
		scope: {
      circuitId: "@circuitId",
			primaryPointId: "@primaryPointId"
    }
	};
});
