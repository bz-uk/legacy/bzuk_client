'use strict';
const CronJob = require('cron').CronJob;
const debug = require('debug')('runner');

const resizeTask = require('../lib/task/imageResizer')('5 * * * *');

let tasks = [];
tasks.push(resizeTask);

tasks.map((task) => {
    console.log('Starting task...');
    resizeTask.start();
})