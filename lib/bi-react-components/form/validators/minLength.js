'use strict';
import StringValidator from './stringValidator.js';
import _ from 'lodash';

export default class MinLengthValidator extends StringValidator {
    constructor(minLength, options) {
        let defaultOptions = {
            errorMessage: 'Value can not be empty'
        }
        
        super(_.assign(defaultOptions,options));
        this.minLength = minLength;
    }
    
    isValid(value) {
        return super.isValid(value) && value.length >= this.minLength;
    }
}