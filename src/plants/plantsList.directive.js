'use strict';
angular.module('adminApp').directive('plantsList', function() {
	return {
		restrict : 'E',
		templateUrl : "plants/plantsList.template.html",
    controller: "plantsListController"
	};
});
