'use strict';
/**
 * @desc english language file for login app
 *
 */
module.exports = {
    "User name": "User name",
    "Password":  "Password",
    "remember":  "remember",
    "login":     "login"
}
