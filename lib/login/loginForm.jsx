'use strict';
import React from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import FontIcon from 'material-ui/FontIcon';
import Popover from 'material-ui/Popover';
import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';

import FormController from './../common/components/form/fomController';
import FormField from './../common/components/form/formField';
import RequiredValidator from './../common/components/form/validator/requiredValidator';
import EmailValidator from './../common/components/form/validator/emailValidator';
import i18n from './i18n.js';

const Translate = require('react-i18nify').Translate;

export default class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {
                username: '',
                password: '',
                remember: true,
            },
            langOpen: false,
            language: 'cs'
        }
        this.formController = new FormController();
    }

    // watchForAutofill() { //TODO: fix autofill prolbem on login
    //     setTimeout(100, () => {
    //         document.querySelector('#login-password')
    //     })
    // }

    componentWillUnmount() {
        this.formController.off('field-change', this.onFormFieldChange);
    }

    componentDidMount() {
        this.formController.on('field-change', this.onFormFieldChange);

    }

    handleRememberChange = (evt, value) => {
        console.log('VVV', value);
        this.state.model.remember = value
        this.setState(this.state);
    }

    openLanguages = (evt) => {
        event.preventDefault();
        this.state.anchorEl = evt.currentTarget;
        this.state.langOpen = true;
        this.setState(this.state);
    }

    closeLanguage = (evt) => {
        this.state.langOpen = false;
        this.setState(this.state);
    }

    switchLang = (event, langCode) => {
        console.log('switching to ', langCode);
        i18n.setLocale(langCode);
        this.state.language = langCode;
        this.state.langOpen = false;
        this.setState(this.state);
    }

    render() {
        let styles = {
            cardStyle: {
                maxWidth: '20vw',
                minWidth: '300px'
            },
            logoImageStyle: {
                maxWidth: '100%'
            },
            textFiled: {
                width: '100%'
            },
            actionLeft: {
                textAlign: 'left'
            },
            actionRight: {
                textAlign: 'right'
            },
            topLangBar: {
                maxWidth: '20vw',
                minWidth: 300,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-end'
            },
            loginButton: {
                whiteSpace: 'nowrap'
            },
            bottomActions: {
                margin: 0
            }
        }
        return (
            <div>
                {/*todo move to component*/}
                <div style={styles.topLangBar}>
                    <FontIcon className="icon-language"
                              onTouchTap={this.openLanguages}></FontIcon>
                </div>
                <Card style={styles.cardStyle}>
                    <CardMedia>
                        <picture>
                            <source style={styles.logoImageStyle} srcSet="img/login/logo.webp" type="image/webp"/>
                            <source style={styles.logoImageStyle} srcSet="img/login/logo.png" type="image/png"/>
                            <img style={styles.logoImageStyle} src="img/login/logo.png"/>
                        </picture>
                    </CardMedia>
                    <CardText>
                        <FormField value={this.state.model.username} formController={this.formController}
                                   validators={[new RequiredValidator({errorMessage: 'is empty'}), new EmailValidator({errorMessage: 'is not email address'})]}
                                   errorMessage="Username {commaSeparatedErrors}" name="username">
                            <TextField id="login-username" style={styles.textFiled}
                                       hintText={this.props.usernameHint || "valid username"}
                                       floatingLabelText={this.props.usernameLabel || i18n.t('User name')}
                                       type={this.props.usernameType || ''}
                            />
                        </FormField>
                        <br/>
                        <FormField value={this.state.model.password} formController={this.formController}
                                   validators={[new RequiredValidator({errorMessage: 'is empty'})]}
                                   errorMessage="Password {commaSeparatedErrors}" name="password">
                            <TextField id="login-password" value={this.state.password}
                                       style={styles.textFiled}
                                       hintText="password"
                                       floatingLabelText={i18n.t('Password')}
                                       type="password"
                            />
                        </FormField>
                    </CardText>
                    <CardActions>
                        <div style={styles.bottomActions} className="row middle-xs">
                            <div style={styles.actionLeft} className="col-xs">
                                <Checkbox defaultChecked={this.state.model.remember}
                                          onCheck={this.handleRememberChange} label={i18n.t('remember')}/>
                            </div>
                            <div style={styles.actionRight} className="col-xs">
                                <RaisedButton style={styles.loginButton}
                                              disabled={!this.state.valid || this.props.isLoggingIn}
                                              onClick={this.onLoginButtonClick}
                                              label={this.props.isLoggingIn ?
                                                  '...'
                                                  :
                                                  i18n.t('login')}/>
                            </div>
                        </div>
                    </CardActions>
                </Card>
                <Popover
                    open={this.state.langOpen}
                    anchorEl={this.state.anchorEl}
                    onRequestClose={this.closeLanguage}
                >
                    <Menu onChange={this.switchLang}>
                        <MenuItem value="cs" primaryText="Česky"/>
                        <MenuItem value="en" primaryText="English"/>
                    </Menu>
                </Popover>
            </div>
        );
    }

    onLoginButtonClick = () => {
        this.props.onLogin(this.state.model);
    }

    onFormFieldChange = (fieldName, fieldValue) => {
        this.state.model[fieldName] = fieldValue;
        this.state.valid = this.formController.valid;
        this.setState(this.state);
    }
}