'use strict';
angular.module('adminApp').directive('menuItemsList', function() {
    return {
        restrict : 'E',
        templateUrl : 'menuEdit/menuItemsList.template.html',
        scope: {
            menuId: '=',
            items: '=',
            onItemsReordered: '&',
            onItemPublished: '&',
            onItemSaved: '&'
        },
        controller: 'menuItemsListController'
    };
});