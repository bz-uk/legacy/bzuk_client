'use strict';
const express = require('express');
const EventEmitter = require('events');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const hbs = require('hbs');
const path = require('path');
const debug = require('debug')(path.basename(__filename));
const hbsutils = require('hbs-utils')(hbs);
const csrf = require('csurf');
const passport = require('passport');
const process = require('process');

const configuration = require('./common/config');

const errorLog = require('./util/errorLog');

const Router = require('./router');

module.exports = class Application extends EventEmitter {
    constructor(configuration) {
        super();
        this.expressApp = null;
        this.configuration = configuration;
    }

    listen() {
        if (this.expressApp) {
            console.log('Application is already listening!')
            return;
        }
        this.expressApp = new express();
        //setup express
        this.expressApp.use(cookieParser());
        let sessionConfiguration = this.configuration.session;
        if (this.expressApp.get('env') === 'production') {
            this.expressApp.set('trust proxy', 1) // trust first proxy
            sessionConfiguration.cookie.secure = true // serve secure cookies
        }

        //this.expressApp.use(session(sessionConfiguration));
        var sessionStore = new MySQLStore({
            host: configuration.storage.mysql.host,
            user: configuration.storage.mysql.user,
            password: configuration.storage.mysql.password,
            database: configuration.storage.mysql.database,
            schema: {
                tableName: 'ro2_sessions', //todo load from models def
            }
        });
        this.expressApp.use(session({
            key: 'connect.sid',
            secret: 'yBGlXsWx6c3x1Dg4zAF8QHtlfrGG4slLGSr11GnWpbFftPT58LcoWuu8TqjHukDC',
            store: sessionStore,
            resave: true,
            saveUninitialized: false
        }));

        this.expressApp.use(bodyParser.urlencoded({extended: true}))
        this.expressApp.use(bodyParser.json())

        this.expressApp.use(passport.initialize());
        this.expressApp.use(passport.session());
        require('./auth/local');

        //this.expressApp.use(csrf({ cookie: true }));

        this.expressApp.set('views', path.join(__dirname, path.sep + 'templates'));
        hbsutils.registerPartials(path.join(__dirname, path.sep + 'templates/partials'));

        this.expressApp.set('view engine', 'hbs');
        this.expressApp.use('/css', express.static('public/css'));
        this.expressApp.use('/js', express.static('public/js'));
        this.expressApp.use('/admin/font', express.static('public/admin/font'));
        this.expressApp.use('/login/font', express.static('public/login/font'));
        this.expressApp.use('/img', express.static('public/img'));
        this.expressApp.use('/files/audio', express.static('data/audio'));
        //setup end
        let router = new Router(this.expressApp);
        try {
            router.initRoutes();
        } catch (err) {
            this.emit('error', err);
            errorLog.log('error', new Date() + 'App error: ' + err.message);
            errorLog.log(new Date() + JSON.stringify(err.stack));
            return;
        }

        let port = process.env.LISTEN_PORT || this.configuration.listen.port;
        this.expressApp.listen(port, (err) => {
            if (err) {
                console.error('Unable to listen on port ', port, err);
                this.emit('error', err);
                return;
            }
            debug('Listening on port ', port);
            //errorLog.log('error', 'Application started');
            this.emit('listen', port);
        })
    }
}