'use strict';
import axios from 'axios';
import UnsuccessfulRequestError from './unsuccessfulRequestError.js';

/**
 * @abstract
 * @class BaseService
 */
export default class BaseService {
    constructor(options) {
        let defaultOptions = {}
        this.options       = _.assign(defaultOptions, options);
        if (this.options.logger) {
            this.logger = this.options.logger;
        }
        if (this.options.eventBus) {
            this.eventBus = this.options.eventBus;
        }
    }
    
    debug() {
        if (this.logger) {
            this.logger.debug(arguments);
        }
    }
    
    emit(type, message) {
        if (this.eventBus) {
            this.eventBus.emit(type, message);
        }
    }
    
    baseCall(method, url, data) {
        return axios({
            method: method,
            url: url,
            data: method.toLowerCase() !== 'get' ? data : {},
            params: method.toLowerCase() === 'get' ? data : {},
            xsrfCookieName: 'xsrf-token'
        })
        .then((response) => {
            return response;
        }).catch((err) => {
            this.debug('Error during ' + method + ' request', err);
            if (err instanceof UnsuccessfulRequestError) {
                this.emit('ok-message', err.message);
            } else {
                this.emit('ok-message', 'Error during ' + method + ' request');
            }
            throw err;
        });
    }
}