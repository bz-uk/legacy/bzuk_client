<?php
namespace botgarApi\circuit;

use Slim\Slim;

class SetPrimaryPointsOrder {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($circuitId) {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);
    	if (!isset($data->fromIndex)) {
    		$response = ['status'=>'error', 'message'=> 'from index can not be empty'];
    		$this->app->response->setStatus(400);
    		$response = json_encode($response);
    		$this->app->response->header('Content-Length', strlen($response));
    		$this->app->response->setBody($response);
    		return;
    	}

    	$orderedItemIds = $data->order;
    	$fromIndex = $data->fromIndex;

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $db->beginTransaction();
        $count = 0;
        foreach ($orderedItemIds as $itemId) {
            $newOrder = $fromIndex + $count;
            $newOrder++;
            $sql = "UPDATE circuit_primary_points SET `order`=".$newOrder."  where id=" . $itemId;
            $result = $db->query($sql);
            if (!$result) {
                $db->rollback();
                $response = ['status'=>'error', 'message'=> 'Problem during saving primary points', 'debug'=>$sql];
                $this->app->response->setStatus(400);
                $response = json_encode($response);
                $this->app->response->header('Content-Length', strlen($response));
                $this->app->response->setBody($response);
                return;
            }
            $count++;
        }
        $db->commit();
        $response = ['status'=> 'ok', 'message' => 'Primary points order saved'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
