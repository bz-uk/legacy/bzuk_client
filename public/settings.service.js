angular.module('adminApp').factory('settingsService', ['$resource', '$log', '$rootScope',function ($resource, $log, $rootScope) {
	var settingsResource = $resource('/api/api/index.php/settings', {}, {
		'get':  {method:'GET'}
	});
  var settings = []

	var registerHandler = function(eventName, scope, callback) {
			var handler = $rootScope.$on(eventName, callback);
			scope.$on('$destroy', handler);
	}

	return {
		onError: function(scope, callback) {
				registerHandler('settings-service-error-event', scope, callback);
		},
		onSettingsLoaded: function(scope, callback) {
				registerHandler('settings-loaded-event', scope, callback);
		},
		loadAndEmitEvent: function() {
			if (settings.length > 0) {
				$rootScope.$emit('settings-loaded-event', settings);
        return;
      }
			return settingsResource.get({}, function(data) {
        settings = angular.copy(data)
        delete settings.$prmoise
        delete settings.resolved
				$rootScope.$emit('settings-loaded-event', settings);
			}, function(err) {
				var message = '';
				if (err.status == 500) {
					message = err.data;
				} else {
					message = err.data.message;
				}
				$log.error('Unable to load settings', err);
				$rootScope.$emit('settings-service-error-event', err);
			});
		},
		load: function(successCallback, errCallback) {
      if (settings.length > 0) {
        successCallback(settings)
				$rootScope.$emit('settings-loaded-event', settings);
        return;
      }
			return settingsResource.get({}, function(data) {
        settings = angular.copy(data)
        delete settings.$prmoise
        delete settings.resolved
				successCallback(settings)
				$rootScope.$emit('settings-loaded-event', settings);
			}, function(err) {
				console.log(err);
				var message = '';
				if (err.status == 500) {
					message = err.data;
				} else {
					message = err.data.message;
				}
				$log.error('Unable to load settings', err);
				errCallback(message);
				$rootScope.$emit('settings-service-error-event', err);
			});
		},
	}
}]);
