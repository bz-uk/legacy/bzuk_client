<?php
namespace botgarApi\circuit;

use botgarApi\image\ImageModel;
use Slim\Slim;

class Put {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($circuitId) {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);
      //validation
    	if (!isset($data->title) || $data->title == "") {
    		$response = ['status'=>'error', 'message'=> 'Title can not be empty'];
    		$this->app->response->setStatus(400);
    		$response = json_encode($response);
    		$this->app->response->header('Content-Length', strlen($response));
    		$this->app->response->setBody($response);
    		return;
    	}
    	if (!isset($data->content) || $data->content == "") {
    		$response = ['status'=>'error', 'message'=> 'Content can not be empty'];
    		$this->app->response->setStatus(400);
    		$response = json_encode($response);
    		$this->app->response->header('Content-Length', strlen($response));
    		$this->app->response->setBody($response);
    		return;
    	}

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $db->beginTransaction();

        $content = '';
        if (isset($data->content)) {
          $content = $data->content;
        }
        $content_en = '';
        if (isset($data->content_en)) {
          $content = $data->content;
        }
        //image
        $image_id = 'null';
        if (isset($data->image) && $data->image != null && $data->image->id > 0) {
          $image_id = $data->image->id;
        }
        //circuit
        $titleEn = '';
        if (isset($data->title_en)) {
            $titleEn = $data->title_en;
        }
        $sql = "UPDATE circuit SET color=".$db->quote($data->color).", title=".$db->quote($data->title).", title_en=".$db->quote($titleEn).", content=".$db->quote($data->content).", content_en=".$db->quote($data->content_en).", image_id=".$image_id." where id=" . $circuitId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            $response = ['status'=>'error', 'message'=> 'Problem during saving circuit'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }
        $db->commit();
        $response = ['status'=> 'ok', 'message' => 'Circuit saved'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
