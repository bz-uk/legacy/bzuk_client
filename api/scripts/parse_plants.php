<?php
$mainUrl = 'https://bz-uk.cz/cs/glossary/';
$letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'ú', 'v', 'w', 'x', 'y', 'z'];
$plantsList = [];
foreach ($letters as $letter) {
  echo 'Getting letter "'.$letter.'"' . "\n";
  for ($i = 0; $i<=4; $i++) {
    $pageUrl = $mainUrl . $letter . '?page=' . $i;
    //$page = file_get_contents($pageUrl);
    echo 'Getting page ' . $i . " - URL: " . $pageUrl . "\n";
    $page = file_get_contents($pageUrl);
    $dom = new domDocument;
    @$dom->loadHTML($page);
    $tables = $dom->getElementsByTagName('table');
    foreach ($tables as $table) {
        $class = $table->getAttribute('class');
        if ($class != 'views-table cols-2') {
          continue;
        }
        //
        echo 'We have content table' . "\n";
        $rows = $table->getElementsByTagName('tr');
        if ($rows->length == 0) {
          break;
        }
        echo 'Rows ' . $rows->length . "\n";
        foreach ($rows as $row) {
          $nameLinks = $row->getElementsByTagName('a');
          if ($nameLinks->length > 1) {
            continue;
          }
          echo 'We have ' . $nameLinks->length . ' links' . "\n";
          $href = $nameLinks->item(0)->getAttribute('href');
          $plantName = strip_tags($nameLinks->item(0)->ownerDocument->saveXML($nameLinks->item(0)));
          //echo 'Link ' . $href . " Text: " . $text . "\n";
          $detailPage = file_get_contents('https://bz-uk.cz' . $href);
          $dom2 = new domDocument;
          @$dom2->loadHTML($detailPage);
          $divs = $dom2->getElementsByTagName('div');
          foreach ($divs as $div) {
            $class = $div->getAttribute('class');
            if ($class=='b2-postcontent') {
              echo '  parsing main div...' . "\n";
              $contentDivs = $div->getElementsByTagName('div');
              $images = $div->getElementsByTagName('img');
              if ($images->length > 0) {
                $imgUrl = $images->item(0)->getAttribute('src');
                if (stripos($imgUrl, '/cs/image_captcha?') !== false) {
                  continue;
                }
                echo '  We have image url: ' . $images->item(0)->getAttribute('src') . "\n";
                $ps = $div->getElementsByTagName('p');
                if ($ps->length > 0) {
                    foreach($ps as $p) {
                        echo $p->ownerDocument->saveXML($p) . "\n";
                        $plantText = $p->ownerDocument->saveXML($p);
                    }
                } else {
                  $plantText = '';
                  echo "no text\n";
                }
                $plantsList[] = ['name' => $plantName, 'image' => $imgUrl, 'text' => $plantText];
                break;
              } //images loop
            } //contents on content page loop
          } //all divs on content page
        } //table row on register page
        break;
    } //all tables loop
  } //pages loop
} //letters loop

file_put_contents('data.json', json_encode($plantsList));
