const configuration = require('./../common/config');
module.exports.models = { //TODO: maybe split it into many files??!??
    Role: {
        fields: {
            id: {
                type: 'INTEGER',
                autoIncrement: true,
                primaryKey: true
            },
            name: {
                type: 'STRING',
                maxSize: 255
            },
            note: {
                type: 'TEXT',
            },
            locked: {
                type: 'BOOLEAN'
            }
        },
        onClient: true,
        tableName: configuration.storage.mysql.tablePrefix + 'roles',
        timestamps: true,
        paranoid: true,
    },
    Permission: {
        fields: {
            id: {type: 'INTEGER', autoIncrement: true, primaryKey: true},
            code: {type: 'STRING', maxSize: 255},
            name: {type: 'STRING', maxSize: 255},
            note: {type: 'TEXT'},
            locked: {type: 'BOOLEAN'},
            type: {type: 'INTEGER'},
        },
        onClient: true,
        tableName: configuration.storage.mysql.tablePrefix + 'permissions',
        timestamps: true,
        paranoid: true,
    },
    Settings: {
        fields: {
            name: {type: 'STRING', maxSize: 255, primaryKey: true},
            value: {type: 'TEXT'},
        },
        tableName: configuration.storage.mysql.tablePrefix + 'settings',
        timestamps: false,
        paranoid: false,
    },
    User: {
        fields: {
            id: {type: 'INTEGER', autoIncrement: true, primaryKey: true},
            username: {type: 'STRING', maxSize: 200},
            password: {type: 'STRING', maxSize: 255},
            role_id: {type: 'INTEGER'}
        },
        onClient: true,
        tableName: configuration.storage.mysql.tablePrefix + 'users',
        timestamps: true,
        paranoid: true,
    },
    RolePermission: {
        fields: {
            role_id: {type: 'INTEGER', primaryKey: true},
            permission_id: {type: 'INTEGER', primaryKey: true},
            level: {type: 'STRING', maxSize: 100},
            ownership: {type: 'STRING', maxSize: 100},
        },
        onClient: true,
        tableName: configuration.storage.mysql.tablePrefix + 'role_permissions',
        timestamps: false,
        paranoid: false,
    },
    FileCompat: {
        fields: {
            id: {type: 'INTEGER', primaryKey: true, autoIncrement: true},
            name: {type: 'STRING', maxSize: 255},
            meta: {type: 'TEXT'},
            type: {type: 'STRING', maxSize: 45},
            size: {type: 'INTEGER'},
            path: {type: 'TEXT'},
            url: {type: 'TEXT'},
            original_name: {type: 'STRING', maxSize: 2555},
        },
        onClient: true,
        tableName: 'files',
        timestamps: true,
        paranoid: true,
    },
    ImageCompat: {
        fields: {
            id: {type: 'INTEGER', primaryKey: true, autoIncrement: true},
            name: {type: 'STRING', maxSize: 256},
            width: {type: 'INTEGER'},
            height: {type: 'INTEGER'},
            type: {type: 'STRING', maxSize: 20},
            size: {type: 'INTEGER'},
            url: {type: 'STRING', maxSize: 512},
            preview_url: {type: 'STRING', maxSize: 255},
            path: {type: 'STRING', maxSize: 512},
            description: {type: 'TEXT'},
            description_en: {type: 'TEXT'},
            authors: {type: 'TEXT'},
            authors_en: {type: 'TEXT'},
            display: {type: 'STRING', maxSize: 45},
            display_vertical_offset: {type: 'INTEGER'},
            original_name: {type: 'STRING', maxSize: 256},
        },
        onClient: true,
        tableName: 'image',
        timestamps: true,
        paranoid: true,
    },
    Migration: {
        fields: {
            id: {type: 'INTEGER', autoIncrement: true, primaryKey: true},
            name: {type: 'STRING', maxSize:255},
            finished_at: {type: 'DATE'}
        },
        onClient: false,
        tableName: configuration.storage.mysql.tablePrefix + 'migrations',
        timestamps: false,
        paranoid: false,
    },
    Tag: {
        fields: {
            id: {type: 'INTEGER', autoIncrement: true, primaryKey: true},
            name: {type: 'STRING', maxSize:50},
            image_id: {type: 'INTEGER'},
            name_en: {type: 'STRING', maxSize:50},
            type: {type: 'INTEGER'},
            color: {type: 'STRING', maxSize:10},
            name_lowercase: {type: 'STRING', maxSize:50},
            name_en_lowercase: {type: 'STRING', maxSize:50},
            show_in_public_regioster: {type: 'BOOLEAN'},
            system: {type: 'BOOLEAN'},
            code: {type: 'STRING', maxSize:50},
        },
        onClient: false,
        tableName: 'tags',
        timestamps: false,
        paranoid: false,
    }
}
