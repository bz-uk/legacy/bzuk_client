<?php

namespace botgarApi\files;

use Slim\Slim;

class Post
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run()
    {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        error_log('uploading...');
        /*
      $filename = $_FILES['file']['name'];
      $meta = $_POST;
      $destination = $meta['targetPath'] . $filename;
      move_uploaded_file( $_FILES['file']['tmp_name'] , $destination );
      */
        if (!isset($_FILES['file'])) {
            $response = ['error' => 'No files found'];
            $this->app->response->setBody(json_encode($response));
            $this->app->response->setStatus(400);

            return;
        }
        $file = $_FILES['file'];
        //
        $data = [];
        if ($file['error'] === 0) {
            $name = uniqid('file_' . date('Ymd') . '-');
            if ($file['type'] === 'audio/mp3') {
                $name .= '.mp3';
            }
            if (move_uploaded_file($file['tmp_name'], '../data/files/' . $name) === true) {
                $now = strftime('%Y-%m-%d %H:%M:%S');
                $sql = 'insert into files(name, original_name, type, size, path, created_at) values(' . $db->quote($name) . ',' . $db->quote($file['name']) . ', ' . $db->quote($file['type']) . ', ' . $file['size'] . ', ' . $db->quote('../data/files/' . $name) . ',' . $db->quote($now) . ')';
                $db->beginTransaction();
                $result = $db->query($sql);
                if (!$result) {
                    error_log('no file saved');
                    $this->app->response->setBody(json_encode(['error' => 'unable to save file info to db', 'debug' => $sql]));
                    $this->app->response->setStatus(400);
                    $db->rollback();
                    return;
                }
                $id = $db->lastInsertId();
                $url = '{PROTO}{HOST_NAME}/api/api/files/' . $id;
                $sql = 'update files set url=' . $db->quote($url) . ' WHERE id=' . $id;
                $result = $db->query($sql);
                if (!$result) {
                    $this->app->response->setBody(json_encode(['error' => 'unable to save file info to db', 'debug' => $sql]));
                    $this->app->response->setStatus(400);
                    $db->rollback();
                    return;
                }
                $db->commit();
                $data = ['success' => true, 'url' => $url, 'name' => $file['name'], 'size' => $file['size'], 'id' => $id];
            }
        }
        $this->app->response->setBody(json_encode($data));
        $this->app->response->setStatus(200);

        return;
    }
}
