'use strict';
const passport = require('passport');
const debug = require('debug')('login-route');

const configuration = require('./../../../common/config');
const knex = require('./../../../database/local').knex;
const BaseRoute = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const UnauthorizedError = require('./../../../error/unauthorized');

module.exports = class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/widget-data/plants-count',
            name: 'getPlantsCountWidgetData'
        }
    }

    main(req, res) {
        let query = knex.count('* as cnt')
        .from('plants')
        .where('is_deleted', 0);
        let response = {}
        return sequelize.query(query.toString(), {type: sequelize.QueryTypes.SELECT})
        .then((rows) => {
            response.plantsCount = rows[0].cnt;
            return sequelize.query(knex.count('* as cnt')
            .from('plants')
            .where('is_deleted', 0).where('is_active', 1).toString(), {type: sequelize.QueryTypes.SELECT})
        })
        .then((rows) => {
            response.publishedCount = rows[0].cnt;
            return res.status(200).json(response);
        })
    }
}