'use strict';

/**
 * @class Settings
 */
export default class Settings {
    /**
     *
     * @param {String} varName
     */
    constructor(varName) {
        if (!window[varName]) {
            console.warn('Unable to load app settings from id ' + varName);
        }
        this.varName = varName;
    }

    /**
     *
     * @param settingName
     * @return {Object | Array | String | int | null }
     */
    get(settingName) {
        if (!window[this.varName] || !window[this.varName][settingName]) {
            return null;
        }
        //TODO: add type conversion here!
        return window[this.varName][settingName];
    }
}
