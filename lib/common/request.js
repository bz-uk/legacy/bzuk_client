'use strict';
import BaseRequest from './baseRequest';

export default class Request extends BaseRequest {
    constructor(options) {
        super();
        if (options.eventBus && options.eventBus instanceof EventBus) {
            this.eventBus = options.eventBus;
        }
    }

    /**
     * @override
     * @param eventName
     * @param eventData
     */
    $emit(eventName, eventData) {
        if (this.eventBus) {
            this.eventBus.emit(eventName, eventData);
        }
    }
}