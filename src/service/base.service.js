var baseService = function ($log, $rootScope) {
    var base = {};
    base.registerHandler =  function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }
    return base;
}
baseService.$inject = ['$log', '$rootScope'];
angular.module('adminApp').factory('baseService', baseService);