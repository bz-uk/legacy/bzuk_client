'use strict';
import axios from 'axios';

export default class BaseRequest {
    $emit(eventName, eventData) {
        throw new Error('not implemented');
    }

    $request(type, url, data) {
        throw new Error('not implemented');
    }

    onRequestComplete(type, url, status, response) {
        this.$emit('request-completed', {type: type, url: url, status: status, response: response})
    }

    onRequestFailed(type, url, status, response) {
        this.$emit('request-failed', {type: type, url: url, status: status, response: response})
    }

    baseCall(type, url, data) {
        //return axios({method: type, url: url, data: data, xsrfCookieName: '_csrf'})
        return this.$request(type, url, data)
        .then((response) => {
            this.onRequestComplete(type, url, response.status, response.data);
            return response.data;
        })
        .catch((err) => {
            if (err.response) {
                this.onRequestFailed(type, url, err.response.status, err.response.data);
            } else {
                this.onRequestFailed(type, url, null, err);
            }
            throw err;
        })
    }
}