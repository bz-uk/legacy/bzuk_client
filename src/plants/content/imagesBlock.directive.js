'use strict'
angular.module('adminApp').directive('imagesBlock', function() {
	return {
		restrict : 'E',
		templateUrl : 'plants/content/imagesBlock.template.html',
		controller: 'imagesBlockController',
		scope: {
      		images: '='
    	},
	};
});
