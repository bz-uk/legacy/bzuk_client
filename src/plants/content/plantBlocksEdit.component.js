'use strict'
//controller
var plantBlocksEditController = function($mdDialog, $log, $scope, Upload, settingsService) {    
    $log.debug('blocks edit', this.blocks)
    this.addMode = false;
    this.addType = 0;
    this.newBlockContent = ''
    this.newBlockContentEn = ''
    this.newBlockImages = [];
    this.newBlockColor = ''
    this.addButtonsVisible = true;

    var ctrl = this;

    settingsService.load(function(data) {
        ctrl.globalSettings = data
    })

    $scope.$on('$destroy',$scope.$on('delete-item', function (event, blockIdx) {
        ctrl.deleteBlock(blockIdx);
        event.stopPropagation();
    }));

    $scope.$on('$destroy', $scope.$on('show-mode-change', function(event, mode) {
        if (mode==='show') {
            ctrl.addButtonsVisible = true;
        }
        if (mode==='edit') {
            ctrl.addButtonsVisible = false;
        }
    }));

    $scope.$on('$destroy',$scope.$on('move-item-up', function (event, fromIdx) {
        var toIdx = fromIdx - 1;
        var movedItem = ctrl.blocks[fromIdx];
        ctrl.blocks.splice(fromIdx, 1)
        ctrl.blocks.splice(toIdx, 0, movedItem)
        event.stopPropagation();
    }));

    $scope.$on('$destroy',$scope.$on('move-item-down', function (event, fromIdx) {
        var movedItem = ctrl.blocks[fromIdx];
        ctrl.blocks.splice(fromIdx, 1)
        ctrl.blocks.splice(fromIdx + 1, 0, movedItem)
        event.stopPropagation();
    }));

    ctrl.addMoreInfoBlock = function() {
        ctrl.blocks.push({type: 2})
        $log.debug(ctrl.blocks)
    };

    $scope.$on('edit_enabled', function(event, enabled) {
        $log.debug('Edit: ' + enabled);
        $scope.$parent.$emit('save_enabled', !enabled);
    });

    ctrl.addBlock = function() {
        this.addMode = true;
        this.addType = 0;
        $scope.$parent.$emit('save_enabled', false);
    }
    ctrl.addImageBlock = function() {
        this.addType = 1;
        this.addMode = true;
        $scope.$parent.$emit('save_enabled', false);
    }
    ctrl.cancelAdd = function(ev) {
        if (ctrl.newBlockContent.length > 0 || ctrl.newBlockImages.length > 0) {
                var confirm = $mdDialog.confirm()
                    .title('Cancel block creation')
                    .textContent('Are you sure to cancel this newly created block? All content will be lost.')
                    .ariaLabel('Warning')
                    .targetEvent(ev)
                    .ok('Ok')
                    .cancel('Cancel');
                $mdDialog.show(confirm).then(function() {
                    ctrl.addMode = false;
                    $scope.$parent.$emit('save_enabled', true);
                    ctrl.newBlockContent = '';
                    ctrl.newBlockContentEn = '';
                    ctrl.newBlockImages = [];
                }, function() {
                    //cancel
                });
        } else {
            ctrl.addMode = false;
            $scope.$parent.$emit('save_enabled', true);
            ctrl.newBlockContent = '';
            ctrl.newBlockContentEn = '';
        }  
    }
    ctrl.deleteBlock = function(blockIdx) {
        ctrl.deleteBlocks.push(ctrl.blocks[blockIdx])        
        ctrl.blocks.splice(blockIdx, 1);
    }    
    ctrl.saveBlock = function(index) {
        console.log('sve', ctrl.addType);
        if (ctrl.addType === 0) {
            if (this.newBlockContent.length === 0) {
                return ctrl.cancelAdd();
            }
            ctrl.blocks.push({content_en: ctrl.newBlockContentEn, content: ctrl.newBlockContent, type: 0, backgroud_color: ctrl.newBlockColor});
            ctrl.newBlockContent = '';
            ctrl.newBlockContentEn = '';
            ctrl.newBlockColor = '';
            ctrl.addMode = false;
            $scope.$parent.$emit('save_enabled', true);
            return;
        } else {
            var activeImages = [];
            console.log('imgs!', ctrl.newBlockImages);
            for (var i = 0; i<ctrl.newBlockImages.length; i++) {
                if (ctrl.newBlockImages[i].status !== undefined && ctrl.newBlockImages[i].status === 'deleted') {
                    continue;
                }
                activeImages.push(ctrl.newBlockImages[i]);
            }
            console.log('adding images block', ctrl.newBlockImages);
            ctrl.blocks.push({type: 1, previewImages: activeImages,images: angular.copy(ctrl.newBlockImages)})
            ctrl.newBlockImages = [];
            ctrl.addMode = false;
            $scope.$parent.$emit('save_enabled', true);
            return;
        }
        if (index !== undefined) {
            if (ctrl.blocks[index].type === 0) {
                ctrl.blocks[index].content = ctrl.blocks[index].edit.content
                ctrl.blocks[index].background_color = ctrl.blocks[index].edit.backgroundColor
                delete ctrl.blocks[index].edit
                return;
            } else {
                ctrl.blocks[index].content = angular.copy(ctrl.blocks[index].edit.images);
                delete ctrl.blocks[index].edit
                return;
            }
        }        
    }
    
}
//definition
angular.module('adminApp').component('plantBlocksEdit', {		
		templateUrl : 'plants/content/plantBlocksEdit.template.html',
		controller: plantBlocksEditController,
        bindings: {
            blocks: '=',
            showLanguage: '=',
            deleteBlocks: '=',
            plant: '='
        }
    });