angular.module('adminApp').controller('editFamilyDialogController', ['$scope', '$mdDialog', 'plantsService', 'family', '$log', function($scope, $mdDialog, plantsService, family, $log) {
    $log.debug('D', family)
    $scope.model = family

    //start: listeners
    plantsService.onPlantFamilySaved($scope, function(event, family) {
      $scope.hide({relaod: true})
    })
    //end: listeners

    $scope.save = function() {
      if (($scope.model.name === undefined || $scope.model.name.length === 0) &&
          ($scope.model.name_lat === undefined || $scope.model.name_lat.length === 0) &&
          ($scope.model.name_en === undefined || $scope.model.name_en.length === 0)) {
            alert('Name can not be empty!')
            return
          }
      plantsService.saveFamily($scope.model.id, $scope.model)
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);
