<?php
namespace botgarApi\circuit;

use Slim\Slim;

class SaveOnePrimaryPointSlide {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message) {
      $response = ['status'=>'error', 'message'=> $message];
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run($circuitId, $pointId, $slideId) {
        $json = $this->app->request->getBody();
        $data = json_decode($json);

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        //
        $db->beginTransaction();
        //
        $title = '';
        if (isset($data->title)) {
          $title = $data->title;
        }
        $titleEn = '';
        if (isset($data->title_en)) {
          $titleEn = $data->title_en;
        }
        $content = '';
        if (isset($data->content)) {
          $content = $data->content;
        }
        $contentEn = '';
        if (isset($data->content_en)) {
          $contentEn = $data->content_en;
        }
        $isActive = '';
        if (isset($data->active)) {
          $isActive = $data->active === true?1:0;
        }

        $sql = "UPDATE primary_point_parts SET title=".$db->quote($title).", title_en=".$db->quote($titleEn).", content=".$db->quote($content).", content_en=".$db->quote($contentEn).",active=".$isActive." WHERE id=" . $slideId;

        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            //$err = var_export($db->errorInfo(), true);
            $this->app->response->setBody(json_encode(['error' => 'unable to update slide', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }

        $db->commit();

        $response = json_encode(['status' => 'ok', 'message' => 'point saved']);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
