'use strict';
var authorsBlocksController = function ($mdDialog, $log, $scope, authorsService, $q, $timeout) {
    var ctrl = this;
    ctrl.authorsVisible = false;
    $timeout(function(){
        var authors = angular.copy(ctrl.authors);        
        ctrl.authors = [];
        ctrl.authors = authors; 
    },500)
    $log.debug('authors ctrl ', ctrl.authors)
    ctrl.allAuthors = [];
    ctrl.authorSearchText = '';
    authorsService.onAllAuthorsLoaded($scope, function (event, authors) {
        ctrl.allAuthors = authors;
    });
    authorsService.loadAll();
    ctrl.removeAuthor = function(index) {
        ctrl.authors.splice(index, 1);
    }
    ctrl.addAuthor = function (author) {
        if (author === '' || author === undefined || author === null) {
            return;
        }
        for (var i = 0; i < ctrl.authors.length; i++) {
            if (ctrl.authors[i].id === author.id) {
                return;
            }
        }
        ctrl.authors.push(author);
        ctrl.authorSearchText = undefined;
        ctrl.selectedAuthor = undefined;
    }
    ctrl.authorsSearch = function (searchText) {
        var deferred = $q.defer();
        deferred.resolve(ctrl.filterAuthors(searchText));
        return deferred.promise
    }
    ctrl.filterAuthors = function (searchText) {        
        var filteredSet = [];
        if (searchText === '' || searchText === undefined || searchText === null) {
            console.log('found ' + ctrl.allAuthors.length + ' authors')
            for (var i = 0; i < ctrl.allAuthors.length; i++) {
                var found = false;
                for (var j = 0; j < ctrl.authors.length; j++) {
                    if (ctrl.authors[j].id === ctrl.allAuthors[i].id) {
                        found = true;
                    }
                }
                if (!found) {
                    filteredSet.push(ctrl.allAuthors[i])
                }
            }
            return filteredSet;
        }
        for (var i = 0; i < ctrl.allAuthors.length; i++) {
            if (ctrl.allAuthors[i].name.indexOf(searchText) > -1 ||
                ctrl.allAuthors[i].name.toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                var found = false;
                for (var j = 0; j < ctrl.authors.length; j++) {
                    if (ctrl.authors[j].id === ctrl.allAuthors[i].id) {
                        found = true;
                    }
                }
                if (!found) {
                    filteredSet.push(ctrl.allAuthors[i])
                }
            }
        }
        return filteredSet;
    }

    ctrl.createAndAddNewAuthor = function (authorName) {
        console.log('creating autor ' + authorName)
        if (authorName === '' || authorName === undefined || authorName === null) {
            return;
        }
        console.log('svac call')
        authorsService.create(authorName).then(function (data) {
            console.log('author created ', data.author)
            authorsService.loadAll(true)
            ctrl.addAuthor(data.author);
        });
    }
}
angular.module('adminApp').component('authorsBlock', {
    templateUrl: 'plants/authors/authorsBlock.template.html',
    controller: authorsBlocksController,
    bindings: {
        authors: '='
    }
});