'use strict';
import React from 'react';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import EventEmitter from 'wolfy87-eventemitter';
import FontIcon from 'material-ui/FontIcon';
import MapsLocalFlorist from 'material-ui/svg-icons/maps/local-florist';
import ActionBook from 'material-ui/svg-icons/action/book';
import FileFolderOpen from 'material-ui/svg-icons/file/folder-open';
import CommunicationCallSplit from 'material-ui/svg-icons/communication/call-split';
import SocialPeople from 'material-ui/svg-icons/social/people';
import ImageCollectionsBookmark from 'material-ui/svg-icons/image/collections-bookmark';
import HardwareKeyboardArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import HardwareKeyboardArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import ActionVisibility from 'material-ui/svg-icons/action/visibility';
import ActionVisibilityOff from 'material-ui/svg-icons/action/visibility-off';
import IconButton from 'material-ui/IconButton';

import LoggedUser  from './../services/loggedUser';
const Translate = require('react-i18nify').Translate;

let topMenu = [
    {screenCode: 'dashboard'},
    //{screenCode: 'registry'},
    //{screenCode: 'articles'}
];

let bottomMenu = [
    {screenCode: 'about'},
]

export default class LeftMenu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: this.props.showLabels,
            hidable: this.props.hidable
        }

        this.styles = {
            menuItem: {
                textTransform: 'capitalize',
                minWidth: '12rem'
            },
            topMenu: {
                width: '100%'
            },
            bottomMenu: {
                position: 'absolute',
                bottom: 0,
                width: '100%'
            },
            menu: {
                transition: 'max-width .5s ease',
            },
            menuIcon: {
                marginLeft: 16
            },
            hideMenuItem: {
                width: '100%',
                textAlign: 'right'
            }
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({expanded: newProps.showLabels, hidable: newProps.hidable})
    }

    getMenu(menuItems) {
        return menuItems.filter((menuItem) => {
            if (!this.props.screens[menuItem.screenCode]) {
                console.warn('Screen ' + menuItem.screenCode + ' not found!');
            }
            if (!this.props.screens[menuItem.screenCode].requirePermission()) {
                return true;
            }
            return LoggedUser.hasPermission(this.props.screens[menuItem.screenCode].requirePermission());
        })
        .map((menuItem) => {
            let screen = this.props.screens[menuItem.screenCode];
            return (
                <MenuItem style={this.styles.menuItem} key={screen.getCode()}
                          leftIcon={screen.getIcon()}
                          value={'/' + screen.getCode()}>{this.state.expanded && screen.getTitle()}</MenuItem>)
        });
    }

    getTopMenu(topMenu) {
        let expandIconStyle = {
            marginLeft: 16,
            transform: this.state.expanded ? 'rotate(0deg)' : 'rotate(180deg)'
        }
        let items = this.getMenu(topMenu);
        //temporary
        items.push(<MenuItem title="Rostliny" value="old-plants" style={this.styles.menuItem} key="plants"
                             leftIcon={<MapsLocalFlorist
                                 style={this.styles.menuIcon}/>}>{this.state.expanded && 'Rostliny'}</MenuItem>);
        items.push(<MenuItem title="Tagy" value="old-tags" style={this.styles.menuItem} key="tags"
                             leftIcon={<ActionBook
                                 style={this.styles.menuIcon}/>}>{this.state.expanded && 'Tagy'}</MenuItem>);
        items.push(<MenuItem title="Ketegorie" value="old-categories" style={this.styles.menuItem} key="categories"
                             leftIcon={<FileFolderOpen
                                 style={this.styles.menuIcon}/>}>{this.state.expanded && 'Kategorie'}</MenuItem>);
        items.push(<MenuItem title="Čeledi" value="old-families" style={this.styles.menuItem} key="families"
                             leftIcon={<CommunicationCallSplit
                                 style={this.styles.menuIcon}/>}>{this.state.expanded && 'Čeledi'}</MenuItem>);
        items.push(<MenuItem title="Autoři" value="old-authors" style={this.styles.menuItem} key="authors"
                             leftIcon={<SocialPeople
                                 style={this.styles.menuIcon}/>}>{this.state.expanded && 'Autoři'}</MenuItem>);
        items.push(<MenuItem title="Galerie" value="old-galleries" style={this.styles.menuItem} key="galleries"
                             leftIcon={<ImageCollectionsBookmark
                                 style={this.styles.menuIcon}/>}>{this.state.expanded && 'Galerie'}</MenuItem>);
        items.push(<Divider key="divider1"/>)
        items.push(<MenuItem title={this.state.expanded ? 'Skrýt' : 'Zobrazit'} value="expand"
                             style={this.styles.menuItem} key="expand"
                             leftIcon={<HardwareKeyboardArrowLeft
                                 style={expandIconStyle}/>}>{this.state.expanded && 'Skrýt'}</MenuItem>);
        return items;
    }

    onMenuItemSelected = (menuItemComponent, code) => {
        if (code === 'expand') {
            this.setState({expanded: !this.state.expanded}, () => {
                this.props.onExpanded(this.state.expanded)
            })
            return;
        }
        this.props.onMenuItemSelected(menuItemComponent, code);
    }

    render() {
        return (
            <div>
                <Menu autoWidth={true} onChange={this.onMenuItemSelected}>
                    {this.getTopMenu(topMenu)}
                </Menu>
                <div style={this.styles.hideMenuItem}>
                    <IconButton onTouchTap={this.props.onPinTouchTap} tooltip={this.state.hidable?'Pin':'Unpin'}>
                        {this.state.hidable ?
                            <ActionVisibilityOff />
                            :
                            <ActionVisibility />
                        }
                    </IconButton>
                </div>
                <Menu autoWidth={true} style={this.styles.bottomMenu} onChange={this.props.onMenuItemSelected}>
                    {this.getMenu(bottomMenu)}
                </Menu>
            </div>
        )
    }
}