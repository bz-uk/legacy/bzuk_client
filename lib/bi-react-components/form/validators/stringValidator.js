'use strict';
import _ from 'lodash';
import BaseValidator from './baseValidator';

export default class StringValidator extends BaseValidator {
    constructor(options) {
        let defaultOptions = {errorMessage: 'Value is not string'}
        super(_.assign(defaultOptions, options));
    }
    
    isValid(value) {
        return _.isString(value);
    }
}