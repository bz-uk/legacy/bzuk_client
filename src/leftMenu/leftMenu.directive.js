'use strict';
angular.module('adminApp').directive('leftMenu', function() {
	return {
		restrict : 'E',
		templateUrl : "leftMenu/leftMenu.template.html" 
	};
});