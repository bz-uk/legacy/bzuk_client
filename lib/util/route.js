'use strict';
const fs = require('fs');
const path = require('path');

module.exports.serveFileInline = function (res, filename, extension) {
    res.setHeader('Content-disposition', 'inline; filename="' + encodeURIComponent(path.basename(filename)) + '"');
    let mime = extension === 'jpg' ? 'jpeg' : extension;
    res.setHeader('Content-type', 'image/' + mime);
    let stream = fs.createReadStream(filename);
    stream.pipe(res);
}
