'use strict';
angular.module('adminApp').directive('downloadContent', function() {
    return {
        restrict : 'E',
        templateUrl : "content/downloadContent.template.html"
    };
});