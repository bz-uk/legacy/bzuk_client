//adminApp.js
angular.module('adminApp', ['ngMaterial', 'md.data.table', 'ngMdIcons', 'ckeditor', 'ngResource', 'ngRoute', 'ui.sortable', 'uiGmapgoogle-maps', 'ngFileUpload', 'ngSanitize', 'dndLists', 'monospaced.qrcode', 'ngAudio', 'ngCookies']);
//adminController.js
angular.module('adminApp').controller('adminController', ['$scope', '$mdDialog', '$mdMedia', '$location', '$rootScope', '$log', 'notificationsService', 'settingsService',
    function($scope, $mdDialog, $mdMedia, $location, $rootScope, $log, notificationsService, settingsService) {
        $scope.flashMessages = []
        notificationsService.onMessageRemoved($scope, function() {
          $scope.flashMessages = notificationsService.getmessages()
        })
        notificationsService.onMessageAdded($scope, function() {
          $scope.flashMessages = notificationsService.getmessages()
        })
        $scope.settings = {};
        $scope.title = '';
        $scope.subtitle = '';
        $scope.brandingLogoSrc = '';
        settingsService.load(function(data) {
            $scope.title = data.brandingTitle;
            $scope.subtitle = data.brandingSubtitle;
            $scope.brandingLogoSrc = data.brandingLogo;
            console.log('branding logo src', data);
        })

        //start: error events
        $rootScope.$on('circuits-service-error-event', function(event, err) {
            $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('settings-service-error-event', function(event, err) {
            $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('circuit-save-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('circuit-create-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-loaded-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-saved-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-created-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-points-order-save-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-deleted-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('secondary-point-deleted-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('secondary-point-saved-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('plants-loaded-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('secondary-point-added-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('plant-category-save-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('plant-family-save-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('plant-saved-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        //start slides
        $rootScope.$on('primary-point-slides-loaded-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-slide-deleted-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('primary-point-slide-saved-error-event', function(event, err) {
          $scope.onErrorEvent(event, err)
        })
        $rootScope.$on('error-message', function(event, message) {
          $scope.onErrorEventSimple(event, message)
        })
        //end slides
        //end: error events
        //start: ok events
        $rootScope.$on('plant-saved-event', function(event, plant, message) {
          $scope.onSuccessEvent(event, message)
        })
        $rootScope.$on('ok-message', function(event, message) {
          $scope.onSuccessEvent(event, message)
        })
        //end: ok events

        $scope.showToolbarMenu = function($mdMenu, ev) {
            originatorEv = ev;
            $mdMenu.open(ev);
        }

        $scope.logout = function() {
            window.location = "/logout";
        }

        $scope.onSuccessEvent = function(event, okMessage) {
          $log.log(okMessage)
          notificationsService.addSuccessMessage(okMessage)
        }

        $scope.onErrorEventSimple = function(event, message) {
            notificationsService.addError(message)
        }

        $scope.onErrorEvent = function(event, error) {
            $log.error(error)
            notificationsService.addError(error.statusText)
        }
    }
]);
angular.module('adminApp').config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyCmL3Ck_qfaDOTsZIHje779InGh7I8-BP0', //TODO: move to cfg file
        //v: '3.20', //defaults to latest 3.X anyhow
        libraries: 'geometry,visualization'
    });
});

