'use strict';
import React from 'react';
import {Table, Column, AutoSizer, InfiniteLoader} from 'react-virtualized';
import BaseScreenComponent from './baseScreenComponent.jsx';
import Screen from '../common/screen';
import Permissions from './../app/permissions';

class RegistryScreenComponent extends BaseScreenComponent {
    constructor(props) {
        super(props);

        this.data = []
        for (let i = 0; i <= 10000; i++) {
            this.data.push({
                family:                  'aaa' + i,
                genus:                   'bbb' + i,
                species:                 'ccc' + i,
                origin_location_verbose: 'ddd' + i
            },)
        }
        this.state = {
            sortBy:        'family',
            sortDirection: 'ASC',
            localData:     []
        }
    }

    noRowsRenderer() {
        return (
            <div>
                No rows
            </div>
        )
    }

    getRowData = ({index}) => {
        console.log('get data ', index, this.state.localData);
        return this.state.localData[index];
    }

    setSort({sortBy, sortDirection}) {
        //this.setState({ sortBy, sortDirection })
    }

    _headerRenderer({
        columnData,
        dataKey,
        disableSort,
        label,
        sortBy,
        sortDirection
    }) {
        return (
            <div>
                {dataKey}
            </div>
        )
    }
isRowLoaded = ({index}) => {
        return !!this.state.localData[index]
    }

    loadMoreRows = ({startIndex, stopIndex}) => {
        return Promise.resolve()
        .then(() => {
            for (let i = startIndex; i <= stopIndex; i++) {
                this.state.localData[i] = this.data[i];
            }
        })
        .then(() => {
            this.setState(this.state);
        })
    }

    render() {
        let styles = {
            simplePage:  {
                padding:       0,
                display:       'flex',
                alignItems:    'flex-start',
                flexDirection: 'column',
                width:         '100%'
            },
            pageContent: {
                padding: '0 10px 10px 10px',
                width:   'calc(100% - 10px)'
            },
            tableHeader: {
                display: 'flex'
            }
        }
        return (
            <div style={styles.simplePage}>
                <h1>Registry</h1>
                <div style={styles.pageContent}>
                    <InfiniteLoader
                        isRowLoaded={this.isRowLoaded}
                        loadMoreRows={this.loadMoreRows}
                        rowCount={this.data.length}
                    >
                        {({onRowsRendered, registerChild}) => (
                            <AutoSizer disableHeight>
                                {({width}) => (
                                    <Table
                                        ref={registerChild}
                                        headerHeight={30}
                                        noRowsRenderer={this.noRowsRenderer}
                                        onRowsRendered={onRowsRendered}
                                        overscanRowCount={10}
                                        rowHeight={30}
                                        headerStyle={styles.tableHeader}
                                        rowClassName="table-row"
                                        rowGetter={this.getRowData}
                                        rowCount={this.data.length}
                                        sort={this.setSort}
                                        sortBy={this.state.sortBy}
                                        sortDirection={this.state.sortDirection}
                                        height={400}
                                        width={width}
                                    >
                                        <Column
                                            dataKey='family'
                                            lebel="Family"
                                            cellDataGetter={
                                                ({ columnData, dataKey, rowData }) => {
                                                    return rowData ? rowData.family : '...'
                                                }
                                            }
                                            disableSort={false}
                                            headerRenderer={this._headerRenderer}
                                            width={90}
                                        />
                                        <Column
                                            dataKey='genus'
                                            lebel="Genus"
                                            disableSort={false}
                                            cellDataGetter={
                                                ({ columnData, dataKey, rowData }) => {
                                                    return rowData ? rowData.genus : '...'
                                                }
                                            }
                                            headerRenderer={this._headerRenderer}
                                            width={90}
                                        />
                                        <Column
                                            dataKey='species'
                                            lebel="Species"
                                            cellDataGetter={
                                                ({ columnData, dataKey, rowData }) => {
                                                    return rowData ? rowData.species : '...'
                                                }
                                            }
                                            disableSort={false}
                                            headerRenderer={this._headerRenderer}
                                            width={90}
                                        />
                                    </Table>
                                )}
                            </AutoSizer>
                        )}
                    </InfiniteLoader>
                </div>
            </div>
        )
    }
}

export default class RegistryScreen extends Screen {
    constructor() {
        super();
    }

    requirePermission() {
        return Permissions.REGISTRY;
    }

    getIconClass() {
        return 'icon-book';
    }

    getTitle() {
        return 'registry';
    }

    getComponent() {
        let self = this;
        return class extends React.Component {
            render() {
                let props = {widgets: self.widgets, screenCode: self.getCode()};
                return (
                    <RegistryScreenComponent {...props}></RegistryScreenComponent>
                );
            }
        }
    }
}