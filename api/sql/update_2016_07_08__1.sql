ALTER TABLE `plant_category`
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ;

ALTER TABLE `plant_category`
CHANGE COLUMN `name` `name` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL ;

ALTER TABLE `plant_family`
CHARACTER SET = utf8 , COLLATE = utf8_general_ci ,
CHANGE COLUMN `name` `name` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL ,
CHANGE COLUMN `name_en` `name_en` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL ,
CHANGE COLUMN `name_lat` `name_lat` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL ;

ALTER TABLE `image`
ADD COLUMN `created_date` DATETIME NULL AFTER `path`;

ALTER TABLE `botgar`.`plant_block`
CHANGE COLUMN `content` `content` TEXT CHARACTER SET 'utf8' NOT NULL ,
CHANGE COLUMN `content_en` `content_en` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL ,
CHANGE COLUMN `content_plain` `content_plain` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL ,
CHANGE COLUMN `content_en_plain` `content_en_plain` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL ;
