var leftMenuService = function ($log, $rootScope, baseService) {
    var confirmationMessage;
    var svc = {
        setOpenConfirmation: function (message) {
            confirmationMessage = message;
        },
        clearOpenConfirmation: function () {
            confirmationMessage = undefined;
        },
        getOpenConfirmation: function () {
            return confirmationMessage;
        }
    }
    return angular.extend({}, baseService, svc)
}
leftMenuService.$inject = ['$log', '$rootScope', 'baseService'];
angular.module('adminApp').factory('leftMenuService', leftMenuService);