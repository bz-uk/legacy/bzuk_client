'use strict';
const webpack = require('webpack');
const path    = require('path');
const BabiliPlugin = require("babili-webpack-plugin");

const paths = require('./build.paths');

const LOGIN_APP_DIR = path.resolve(__dirname, paths.login.sourceDir);
const ADMIN_APP_DIR = path.resolve(__dirname, paths.admin.sourceDir);
let config     = {
    entry: {
        login: ['core-js/fn/promise', LOGIN_APP_DIR + '/entryPoint.jsx'],
        admin: ['core-js/fn/promise', ADMIN_APP_DIR +  '/entryPoint.jsx']
    },
    /**
     * output tells webpack where to dump the files it has processed.
     * [name].[hash].js will output something like app.3531f6aad069a0e8dc0e.js
     */
    output: {
        filename: '[name].min.js',
        path: path.join(__dirname, paths.login.targetDir),
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        loaders: [ // Loaders allow you to preprocess files!
            {
                test: /\.(js|jsx)$/, // look for .js files
                exclude: /node_modules/,

                loader: 'babel-loader',
                query: {
                    cacheDirectory: true,
                    presets: ["react", "es2015", "stage-0"],
                },
            },
        ],
    },
    devServer: {
        hot: true,
    },
    plugins: [
    ],
};
module.exports = config;
if (process.env.NODE_ENV === 'production') {
    module.exports.devtool = '#source-map'
    // http://vue-loader.vuejs.org/en/workflow/production.html
    module.exports.plugins = (module.exports.plugins || []).concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new BabiliPlugin({}, {comments: false, sourceMap: false})
    ]);
}