angular.module('adminApp').controller('plantQuickEditDialogController', ['$scope', '$mdDialog', '$log', 'plant', 'webpService', 'plantsService', function ($scope, $mdDialog, $log, plant, webpService, plantsService) {
    $scope.model = {}
    $scope.allTags = [];

    $scope.save = function () {
        plantsService.updateTags($scope.model.id, $scope.model.tags)
        .then(function() {
            $mdDialog.hide();
        })
        .catch(function(err) {
            console.log('Unable to save tags: ', err);
        })
    }

    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    plantsService.getPlant(plant.id, function (plant) {
        $scope.model = plant;
    }, function (err) {
        console.log('Unable to load plant ', err);
    })

    $scope.createFilterFor = function (query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(tag) {
            return (tag.name_lowercase.indexOf(lowercaseQuery) != -1);
        }
    }

    $scope.filterSelected = true;
    $scope.tagQuerySearch = function (criteria) {
        console.log($scope.allTags)
        return $scope.allTags.filter($scope.createFilterFor(criteria))
    }

    plantsService.getAllTags(function (allTags) {
        $scope.allTags = allTags;
    }, function (err) {
        $log.error(err)
    })
}]);
