<?php
namespace botgarApi\article;

use botgarApi\image\ImageModel;
use Slim\Slim;

class Post
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run()
    {
        $json = $this->app->request->getBody();
        $data = json_decode($json);
        if (!isset($data->title) || $data->title == "") {
            $response = ['status' => 'error', 'message' => 'Title can not be empty'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $db->beginTransaction();
        //images
        $image_id = 'null';
        if (isset($data->image) && $data->image != null && $data->image->id > 0) {
            $image_id = $data->image->id;
        }
        $sql = "INSERT INTO article(`title`,`image_id`,`title_en`) VALUES(" . $db->quote($data->title) . "," . $image_id . "," . $db->quote($data->title_en) . ")";
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            $response = ['status' => 'error', 'message' => 'Problem during creating article'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }
        $articleId = $db->lastInsertId();

        $sql = "INSERT INTO article_block (`article_id`,`content`,`content_en`,`content_plain`,`content_en_plain`,`order`,`type`) VALUES(" . $articleId . "," . $db->quote($data->blocks->{0}->content) . "," . $db->quote($data->blocks->{0}->content_en) . "," . $db->quote(strip_tags($data->blocks->{0}->content)) . "," . $db->quote(strip_tags($data->blocks->{0}->content_en)) . ",0,0)";
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            $response = ['status' => 'error', 'message' => 'Problem during add article block: ' . var_export($db->errorInfo(), true)];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }

        $db->commit();
        $response = ['status' => 'ok', 'message' => 'Article created'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
