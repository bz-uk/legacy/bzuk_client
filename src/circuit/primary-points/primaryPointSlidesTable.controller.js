'use strict';
angular.module('adminApp').controller('primaryPointSlidesTableController', ['$scope', '$rootScope', '$log', '$mdDialog', '$mdMedia', 'circuitsService', '$timeout', '$routeParams', 'uiGmapIsReady', 'settingsService', 'plantsService', 'Upload', '$location',
    function($scope, $rootScope, $log, $mdDialog, $mdMedia, circuitsService, $timeout, $routeParams, uiGmapIsReady, settingsService, plantsService, Upload, $location) {
        $log.debug('loading slides for ' + $scope.circuitId + '/' + $scope.primaryPointId)
            //start: models
        $scope.slidesTable = {
                slides: [],
                query: {
                    order: 'id',
                    limit: 10,
                    page: 1
                },
                selected: [],
            }
            //end: models
            //start listeners
        circuitsService.onPrimaryPointSlidesLoaded($scope, function(event, data) {
            $scope.slidesTable.slides = data
        })
        circuitsService.onPrimaryPointSlideDeleted($scope, function(event, data) {
            $scope.getSlides()
        })
        circuitsService.onPrimaryPointSlideEdited($scope, function(event, data) {
                $scope.getSlides()
            })
            //end listeners
            //start: helpers
        $scope.getSlides = function() {
            $scope.slidesTable.promise = circuitsService.loadPrimaryPointSlides($scope.circuitId, $scope.primaryPointId, $scope.slidesTable.query).$promise
        }
        $scope.deleteSlide = function(slide) {
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this slide?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {
                circuitsService.deletePrimaryPointSlide($scope.circuitId, $scope.primaryPointId, slide.id)
            }, function() {});
        }
        $scope.publish = function(slide) {
                slide.active = !slide.active
                circuitsService.updatePrimaryPointSlide($scope.circuitId, $scope.primaryPointId, slide.id, slide)
            }
            //end: helpers
            //start: loading data
        $scope.getSlides()
            //end: loading data
    }
]);
