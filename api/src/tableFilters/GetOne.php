<?php
namespace botgarApi\tableFilters;

use Slim\Slim;

class GetOne {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($filterCode) {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT * FROM table_filter WHERE code=" . $db->quote($filterCode);
        $result = $db->query($sql);
        if (!$result) {
            $response = ['error' => 'No filter found', 'debug' => $sql];
            $this->app->response->setBody(json_encode($response));
            $this->app->response->setStatus(404);
            return;
        }
        $output = $result->fetch(\PDO::FETCH_ASSOC);
        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
