'use strict';
angular.module('adminApp').controller('articlesList', ['$scope', '$location', '$mdDialog', '$mdMedia', 'articlesService', '$timeout', function ($scope, $location, $mdDialog, $mdMedia, articlesService, $timeout) {
    $scope.model = {
        articles: articlesService.articlesList,
        query: {
            order: 'title',
            limit: 10,
            page: 1
        },
        selected: [],
        flashMessages: []
    }
    $scope.selected = [];
    $scope.limitOptions = [5, 10, 15];
    $scope.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    articlesService.onArticleDeleted($scope, function (event, article) {
        $scope.getArticles();
    });

    $scope.addArticle = function () {
        $location.url('/articles/_')
    }
    $scope.editArticle = function (article) {
        $location.url('articles/' + article.id)
    };
    $scope.deleteArticle = function (article, ev) {
        var confirm = $mdDialog.confirm()
            .title('Delete')
            .textContent('Do you really want to delete this article?')
            .ariaLabel('Delete')
            .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            articlesService.deleteArticleAndEmitEvent(article);
        }, function () {
            //on cancel
        });
    }
    $scope.publishArticle = function (article) {
        //article.published = !article.published
        articlesService.publishOneArticle(article.id, article.published);
    }

    $scope.getArticles = function () {
        console.log('getting...');
        $scope.model.promise = articlesService.load($scope.query, function (articles) {
            $scope.model.articles = articles
        }).$promise;
    }
    $scope.getArticles();
}]);
