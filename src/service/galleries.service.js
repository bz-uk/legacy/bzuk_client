angular.module('adminApp').factory('galleriesService', ['$log', '$resource', '$rootScope', '$q', function ($log, $resource, $rootScope, $q) {
    var galleriesResource = $resource('/api/api/index.php/widgets/dr-galleries', {}, {
        'getAll': {method: 'GET', isArray: true}
    });

    var galleryStatusResource = $resource('/api/api/index.php/widgets/dr-galleries/:galleryId/active', {}, {
        'setActive': {method: 'PUT'}
    });

    var galleryResource = $resource('/api/api/index.php/widgets/dr-galleries/:galleryId', {}, {
        'get': {method: 'GET'},
        'put': {method: 'PUT'}
    });

    var galleryImagesResource = $resource('/api/api/index.php/widgets/dr-galleries/images', {}, {
        'getAll': {method: 'POST'},
    });

    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }

    return {
        //start: hanlders
        onGalleriesLoaded: function (scope, callback) {
            registerHandler('galleries-loaded-event', scope, callback)
        },
        //end: hanlders
        getImages: function (filter) {
            return galleryImagesResource.getAll({}, {filter: filter}, function (images) {
            }, function (err) {
                $rootScope.$emit('error-message', 'Nepodařilo se načíst obráazky galerie');
            }).$promise;
        },
        update: function(galleryData) {
            return galleryResource.put({galleryId: galleryData.id}, galleryData, function (result) {
            }, function (err) {
                $log.error(err);
                $rootScope.$emit('error-message', 'Nepodařilo se uložit galerii');
            }).$promise;
        },
        setActive: function (galleryId, active) {
            return galleryStatusResource.setActive({galleryId: galleryId}, {active: active}, function (response) {
                var activeVerbose = active ? 'aktivována' : "deaktivována";
                $rootScope.$emit('ok-message', 'Galerie ' + activeVerbose);
            }, function (err) {
                $rootScope.$emit('error-message', 'Zmena stavu galerie zlyhala');
                $log.error(err);
            }).$promise;
        },
        loadGallery: function (galleryId) {
            return galleryResource.get({galleryId: galleryId}, function (galleryData) {
            }, function (err) {
                $log.error(err);
                $rootScope.$emit('error-message', 'Nepodařilo se načíst galerii');
            }).$promise;
        },
        getAllGalleries: function (query) {
            return galleriesResource.getAll({}, {query: query}, function (galleries) {
                delete galleries.$promise;
                delete galleries.resolved;
                $rootScope.$emit('galleries-loaded-event', galleries);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status === 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to get galleries', err);
            }).$promise;
        }
    }
}]);
