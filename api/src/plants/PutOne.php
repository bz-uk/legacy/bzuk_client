<?php
namespace botgarApi\plants;

use Slim\Slim;

class PutOne
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message)
    {
        $response = ['status' => 'error', 'message' => $message];
        $this->app->response->setStatus(400);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }

    public function run($plantId)
    {
        $json = $this->app->request->getBody();
        $data = json_decode($json);

        //validation
        if (!isset($data->name) &&
            !isset($data->name_en) &&
            !isset($data->name_lat)
        ) {
            return $this->returnInvalidResponse('Please enter at least one name in some language');
        }
        // if (!isset($data->category) || $data->category == null) {
        //   return $this->returnInvalidResponse('Category can not be empty. pick or create one.');
        // }
        // if (!isset($data->family) || $data->family == null) {
        //   return $this->returnInvalidResponse('Family can not be empty. pick or create one.');
        // }

        //ok now update

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $sql = 'select * from plants where id=' . $plantId;
        $result = $db->query($sql);
        if (!$result) {
            return $this->returnInvalidResponse('Unable to find plant');
        }
        $plant = $result->fetch(\PDO::FETCH_ASSOC);
        $plantModel = PlantModel::loadFromData($plant, $db);

        $db->beginTransaction();

        //map point
        if ($plant['map_marker_id'] != null) {
            $sql = 'delete from map_marker where id=' . $plant['map_marker_id'];
            $result = $db->query($sql);
            if (!$result) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to clear old marker point: ' . $sql);
            }
        }
        if ($plant['map_overlay_id'] != null) {
            $sql = 'delete from map_marker where map_overlay_id=' . $plant['map_overlay_id'];
            $result = $db->query($sql);
            if (!$result) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to clear old marker points: ' . $sql);
            }
            $sql = 'delete from map_overlay where id=' . $plant['map_overlay_id'];
            $result = $db->query($sql);
            if (!$result) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to clear old markers overlay: ' . $sql);
            }
        }
        $markerId = [];
        $overlayId = null;
        if (isset($data->markers) && is_array($data->markers) && count($data->markers) > 0) {
            if (count($data->markers) > 1) {
                //create overlay
                if (isset($data->name)) {
                    $overlayName = $data->name;
                } else {
                    $overlayName = 'plant_overlay';
                }
                $overlaySql = 'insert into map_overlay(name, code) values(' . $db->quote($overlayName) . ', ' . $db->quote($overlayName) . ')';
                $result = $db->query($overlaySql);
                if (!$result) {
                    $db->rollback();
                    return $this->returnInvalidResponse('Unable to create markers overlay: ' . $overlaySql);
                }
                $overlayId = $db->lastInsertId();
            }
            foreach ($data->markers as $marker) {
                if (!isset($marker->latitude) && isset($marker->longitude)) {
                    continue;
                }
                if (isset($data->name)) {
                    $overlayName = $data->name;
                } else {
                    $overlayName = 'plant_overlay';
                }
                $overlaySql = 'insert into map_overlay(name, code) values(' . $db->quote($overlayName) . ', ' . $db->quote($overlayName) . ')';
                $now = strftime('%Y-%m-%d %H:%M:%S');
                if ($overlayId != null) {
                    $sql = 'insert into map_marker(title, contents, marker_visible, latitude, longitude, map_overlay_id, created_at, updated_at) values(' . $db->quote($overlayName) . ', ' . $db->quote($overlayName) . ', 1, ' . $marker->latitude . ', ' . $marker->longitude . ', ' . $overlayId . ', "'.$now.'", "'.$now.'")';
                } else {
                    $sql = 'insert into map_marker(title, contents, marker_visible, latitude, longitude, created_at, updated_at) values(' . $db->quote($overlayName) . ', ' . $db->quote($overlayName) . ', 1, ' . $marker->latitude . ', ' . $marker->longitude . ', "'.$now.'", "'.$now.'")';
                }
                $result = $db->query($sql);
                if (!$result) {
                    $db->rollback();
                    return $this->returnInvalidResponse('Unable to create new marker point: ' . $sql);
                }
                $markerId[] = $db->lastInsertId();
            }
        }
        //category
        if ($data->new_category === 'new') {
            $sql = 'insert into plant_category(name) values(' . $db->quote($data->category) . ')';
            $result = $db->query($sql);
            if (!$result) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to create new category: ' . $sql);
            }
            $categoryId = $db->lastInsertId();
        } else if ($data->new_category === 'chosen') {
            if (isset($data->category)) {
                $categoryId = $data->category + 0;
            } else {
                $categoryId = 'NULL';
            }
        } else if ($data->new_category === 'none') {
            $categoryId = 'NULL';
        }
        //family
        if ($data->new_family === 'new') {
            $familyEn = isset($data->family_en) ? $data->family_en : '';
            $familyLat = isset($data->family_lat) ? $data->family_lat : '';
            $sql = 'insert into plant_family(name, name_en, name_lat) values(' . $db->quote($data->family) . ', ' . $db->quote($familyEn) . ', ' . $db->quote($familyLat) . ')';
            $result = $db->query($sql);
            if (!$result) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to create new family');
            }
            $familyId = $db->lastInsertId();
        } else if ($data->new_family === 'chosen') {
            if (isset($data->family)) {
                $familyId = $data->family + 0;
            } else {
                $familyId = 'NULL';
            }
        }else if ($data->new_family === 'none') {
            $familyId = 'NULL';
        }
        //blocks
        $contentLength = 0;
        if (isset($data->blocks)) {
            try {
                $deletedBlocks = [];
                if (isset($data->delete_blocks) && count($data->delete_blocks) > 0) {
                    $deletedBlocks = $data->delete_blocks;
                }
                $blocksInfo = $plantModel->upsertBlocks($data->blocks, $deletedBlocks);
                $contentLength = is_numeric($blocksInfo['contentLength'])?$blocksInfo['contentLength']:0;
            } catch (Exception $e) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to update plant blocks: ' . $e->getMessage());
            }
        }

        //images
        if (isset($data->images)) {
            try {
                $plantModel->updatePlantImages($data->images);
            } catch (Exception $e) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to update plant images: ' . $e->getMessage());
            }
        }
        //tags
        $sql = 'delete from plant_tags where plant_id=' . $plantId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            return $this->returnInvalidResponse('Unable to clear plant tags before save them: ' . $sql);
        }
        $tagsSerialized = [];
        if (isset($data->tags) && is_array($data->tags) && count($data->tags) > 0) {
            foreach ($data->tags as $tag) {
                $sql = 'insert into plant_tags(plant_id, tag_id) values(' . $plantId . ', ' . $tag->id . ')';
                $result = $db->query($sql);
                if (!$result) {
                    $db->rollback();
                    return $this->returnInvalidResponse('Unable to insert plant tag: ' . $sql);
                }
            }
            $sql = 'select t.* from plant_tags pt inner join tags t where t.id=pt.tag_id AND pt.plant_id=' . $plantId . ' AND t.type=1';
            $result = $db->query($sql);
            if ($result) {
                while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                    $tagsSerialized[] = $row;
                }
            }
        }
        //

        $name = isset($data->name) ? $data->name : '';
        $nameEn = isset($data->name_en) ? $data->name_en : '';
        $nameDe = isset($data->name_de) ? $data->name_de : '';
        $nameSk = isset($data->name_sk) ? $data->name_sk : '';
        $nameLat = isset($data->name_lat) ? $data->name_lat : '';
        $sources = isset($data->sources) ? $data->sources : '';
        $sourcesEn = isset($data->sources_en) ? $data->sources_en : '';
        $occurrence = isset($data->occurrence) ? $data->occurrence : '';
        $audioCz = (isset($data->audio_cz)&&is_numeric($data->audio_cz)) ? 'audio_cz='.$data->audio_cz.', ' : '';
        $mapMarkerSql = '';
        if (count($markerId) == 1) {
            $mapMarkerSql = 'map_marker_id=' . $markerId[0] . ',map_overlay_id=NULL, ';
        } else if (count($markerId) > 1) {
            $mapMarkerSql = 'map_overlay_id=' . $overlayId . ',map_marker_id=NULL, ';
        }
        $categorySql = '';
        if ($categoryId !== null) {
            $categorySql = 'category_id='.$categoryId.',';
        }
        $familySql = '';
        if ($familyId !== null) {
            $familySql = 'family_id='.$familyId.',';
        }
        $isActive = 0;
        if (isset($data->is_active) && ($data->is_active === true || $data->is_active === '1')) {
            $isActive = 1;
        }
        $now = strftime('%Y-%m-%d %H:%M:%S');
        $sql = 'update plants set '.$audioCz.' content_length='.$contentLength.', name=' . $db->quote($name) . ',name_sk=' . $db->quote($nameSk) . ',name_de=' . $db->quote($nameDe) . ', name_en=' . $db->quote($nameEn) . ', sources=' . $db->quote($sources) . ', sources_en=' . $db->quote($sourcesEn) . ', name_lat=' . $db->quote($nameLat) . ', occurrence=' . $db->quote($occurrence) . ', ' . $categorySql . $familySql . ' ' . $mapMarkerSql . ' is_active=' . $isActive . ', tags=' . $db->quote(json_encode($tagsSerialized)) . ', modified_date=\''.$now.'\' where id=' . $plantId;
        $result = $db->query($sql);
        if (!$result) {
            $err = $db->errorInfo();
            $db->rollback();
            return $this->returnInvalidResponse('Unable to update plant: ' . $sql . ', err: ' . var_export($err, true));
        }
        if (isset($data->authors)) {
            $plantModel->updateAuthors($data->authors);
        }
        $db->commit();
        $response = ['status' => 'ok', 'message' => 'plant saved'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
