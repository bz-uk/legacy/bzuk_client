'use strict';
import React from 'react';
import eventbus from './../eventBus';

export default class BaseScreenComponent extends React.Component {
    render() {
        let styles = {
            content: {
                width:  '100%',
                height: 'calc(100vh - 64px)',
                margin: 0,
                padding: 0,
            }
        }
        return (
            <div style={styles.content}>
                {this.getContent()}
            </div>
        )
    }

    componentWillMount() {
        //
    }

    getContent() {
        throw new Error('not implemented');
    }
}