angular.module('adminApp').factory('notificationsService', ['$resource', '$log', '$rootScope', '$timeout', function($resource, $log, $rootScope, $timeout) {
    var messages = []

    var _putMessage = function(msgObject) {
      messages.push(msgObject)      
      $rootScope.$emit('notifcation-message-added-event')
      $timeout(function() {
          for (var i = 0; i < messages.length; i++) {
              if (messages[i].id == msgObject.id) {
                  messages.splice(i, 1);
                  $rootScope.$emit('notifcation-message-deleted-event')
                  return;
              }
          }
      }, 3000)
    }

    return {
        addError: function(errorMessage) {
					var msgObject = {
	            id: new Date().getTime(),
	            status: 'error',
	            message: errorMessage
	        }
          _putMessage(msgObject)
        },
        addSuccessMessage: function(message) {
          var msgObject = {
	            id: new Date().getTime(),
	            status: 'success',
	            message: message
	        }
          _putMessage(msgObject)
        },
				getmessages: function() {
					return messages
				},
				onMessageRemoved: function(scope, callback) {
					var handler = $rootScope.$on('notifcation-message-deleted-event', callback);
					scope.$on('$destroy', handler);
				},
				onMessageAdded: function(scope, callback) {
					var handler = $rootScope.$on('notifcation-message-added-event', callback);
					scope.$on('$destroy', handler);
				}
    }
}]);
