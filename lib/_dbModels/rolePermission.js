const connection = require('./../database/local');
const configuration = require('./../common/config');

var RolePermission = connection.bookshelf.Model.extend({
    tableName: configuration.storage.mysql.tablePrefix + 'permissions',
    hasTimestamps: true,
});

module.exports = connection.bookshelf.model('Permission', RolePermission);