const sequelize = require('./../lib/database/local').sequelize;
const Promise = require('bluebird');
const package = require('./../package.json');
const umzug = require('./../lib/migration/umzug');

let promise = Promise.resolve();
console.log(sequelize.models);
console.log('Initialising ' + Object.keys(sequelize.models).length + ' models');
Object.keys(sequelize.models).map((modelName) => {
    promise = promise.then(() => {
        console.log('Creating table ' + modelName);
        return sequelize.models[modelName].sync();
    })
});
promise.then(() => {
    console.log('Initialising db...');
    return sequelize.models.Setting.create({name: 'app_version', value: package.version});
})
.then(() => {
    return umzug.pending().each((migration) => {
        return umzug.storage.logMigration(migration.file);
    })
})
.then(() => {
    console.log('App installed');
})