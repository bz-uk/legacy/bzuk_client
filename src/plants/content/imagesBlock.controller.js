angular.module('adminApp').controller('imagesBlockController', ['$scope', '$mdDialog', '$log', 'Upload', 'settingsService', '$timeout', '$mdMedia', '$cookies', 'webpService',
    function ($scope, $mdDialog, $log, Upload, settingsService, $timeout, $mdMedia, $cookies, webpService) {
        $scope.previewDescription = '';
        $scope.previewDescriptionEn = '';
        $scope.previewAuthors = '';
        $scope.previewAuthorsEn = '';
        $scope.webp = webpService.supported();
        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        })
        settingsService.load(function (data) {
            $scope.globalSettings = data
        })
        // $scope.dropImage = function (image) {
        //     $scope.images.push(image);
        //     return true;
        // }
        // $scope.isDragging = function (idx) {
        //     console.log('isDrag', idx, $scope.dragIndex, idx === $scope.dragIndex);
        //     return idx === $scope.dragIndex
        // }
        $scope.startDrag = function(index) {
            console.log('start', index);
            $scope.dragIndex = index;
        }
        $scope.deleteImage = function (index, showConfirmation) {
            $scope.dragIndex = null;
            if (!showConfirmation) {
                $scope.images.splice(index, 1);
                return;
            }
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this image?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function () {
                $scope.images.splice(index, 1);
            }, function () { });
        }

        console.log('imgsBlock', $scope.images);
        $scope.getCreatedDateFormatted = function(dateString) {
            var date = new Date();
            if (dateString) {
                date = new Date(Date.parse(dateString));
            }

            var monthNames = ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "List.", "Pros."];

            var d = new Date(date.getFullYear(), date.getMonth(), 0);
            return d.getDate() + '.' + monthNames[date.getMonth()] + ' ' + date.getFullYear();
        };
        $scope.getPreviewUrl = function(image) {
            let extension = image.name.substr(image.name.lastIndexOf('.'));
            return $scope.globalSettings.offlineImagesUrl.replace(/\/$/, '') + '/250/' + image.id + ($scope.webp?'.webp':extension);
        }
        $scope.previewImage = function (image, imageUrl, ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
            console.log('preview open',image)
            $mdDialog.show({
                controller: 'imagePreviewDailogController',
                templateUrl: 'plants/imageProviewDialog.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                locals: {
                    image: image,                    
                },
                clickOutsideToClose: true,
                fullscreen: useFullScreen
            })
                .then(function (response) {                    
                    image.description = response.description;
                    image.description_en = response.descriptionEn;
                    image.authors = response.authors;
                    image.authors_en = response.authorsEn;
                    image.display = response.display;
                    image.display_vertical_offset = response.displayVerticalOffset;
                    //console.log('preview closed',image)
                }, function () {
                    console.log('You cancelled the dialog.');
                });

            //         $scope.$watch(function() {
            //             return $mdMedia('xs') || $mdMedia('sm');
            //         }, function(wantsFullScreen) {
            //             $scope.customFullscreen = (wantsFullScreen === true);
            //         });
        }
        $scope.upload = function (files) { //TODO: to service??
            if (files && files.length) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    if (!file.$error) {
                        var newImg = {
                            id: undefined,
                            url: '//' + $scope.globalSettings.baseUrl + '/public/img/ic_cloud_upload_black_48px.svg',
                            status: 'uploading'
                        }
                        $scope.images.push(newImg);
                        Upload.upload({
                            //url: '/api/api/index.php/images', //TODO: load from settings
                            url: '/upload/image', //TODO: load from settings
                            headers: {
                                'X-XSRF-TOKEN': $cookies.get('_csrf')
                            },
                            data: {
                                file: file
                            },
                            params: {
                                webpPreview: webpService.supported()
                            }
                        }).then(function (resp) {
                            var fileData = resp.data[0];
                            $timeout(function () {
                                if (fileData.id !== undefined && parseInt(fileData.id) > 0) {
                                    newImg.preview_url = fileData.url;
                                    newImg.created_at = new Date();
                                    newImg.id = parseInt(fileData.id);
                                    newImg.status = 'done'
                                } else { //wrong data received from server
                                    alert('Unable to uplaod image: ' + fileData)
                                    var idx = $scope.images.indexOf(newImg)
                                    if (idx > -1) {
                                        $scope.images.splice(idx, 1)
                                    }
                                }
                            },100);
                        },
                            function (resp) {
                                console.error('Unable to uplaod image: ', resp.data);
                                if (resp.data.code === 'upload_unsupported_format') {
                                    alert('Unable to upload image: unsupported format.');
                                }
                                var idx = $scope.images.indexOf(newImg)
                                if (idx > -1) {
                                    $scope.images.splice(idx, 1)
                                }
                            },
                            function (evt) {
                                var progressPercentage = parseInt(100.0 *
                                    evt.loaded / evt.total);
                                newImg['progress'] = progressPercentage
                            });
                    }
                }
            }
        }
    }]);
