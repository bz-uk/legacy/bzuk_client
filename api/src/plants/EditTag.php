<?php
namespace botgarApi\plants;

use Slim\Slim;

class EditTag {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message) {
      $response = ['status'=>'error', 'message'=> $message];
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run($tagId) {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);

      //validation
      if (!isset($data->name)) {
        return $this->returnInvalidResponse('Tag name can not be empty');
    	}
      //ok now update

      $conf = $this->app->container->get('configuration');
      $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

      $db->beginTransaction();

      //tag
        $sql = 'UPDATE tags set name='.$db->quote($data->name).',name_lowercase='.$db->quote(strtolower($data->name)).' where id='.$tagId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            return $this->returnInvalidResponse('Unable to update tag: ' . $sql);
        }

        $sql = 'SELECT DISTINCT pl.id from plant_tags pt inner join plants pl ON pt.plant_id=pl.id WHERE pt.tag_id=' . $tagId;
        $result = $db->query($sql);
        if ($result) {
          while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $tagsSerialized = [];
            $sql = 'select t.* from plant_tags pt inner join tags t where t.id=pt.tag_id AND pt.plant_id=' . $row['id'] . ' AND t.type=1';
            $tagRes = $db->query($sql);
            if ($tagRes) {
              while ($t = $tagRes->fetch(\PDO::FETCH_ASSOC)) {
                  $tagsSerialized[] = $t;
              }
            }
            $sql = 'update plants set tags='.$db->quote(json_encode($tagsSerialized)).' where id=' . $row['id'];
            $db->query($sql);
          }
        }
      $db->commit();
      $response = ['status'=> 'ok', 'message' => 'tag saved'];
      $this->app->response->setStatus(200);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
      return;
    }
}
