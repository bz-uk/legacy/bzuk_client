'use strict';
angular.module('adminApp').directive('articlesList', function() {
	return {
		restrict : 'E',
		templateUrl : "article/articlesList.template.html" 
	};
});