<?php

namespace botgarApi\circuit;

use Slim\Slim;

class DeleteOneSecondaryPoint
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($circuitId, $pointId, $secondaryPointId)
    {
        if (!is_numeric($pointId) && !is_numeric($secondaryPointId)) {
            $this->app->response->setStatus(400);

            return;
        }
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $db->beginTransaction();

        $sql = 'DELETE FROM circuit_secondary_points WHERE circuit_id='.$circuitId.' AND primary_point_id='.$pointId.' AND plant_id='.$secondaryPointId;
        $result = $db->query($sql);
        if (!$result) {
            $response = ['error' => 'Unable to delete secondary point', 'debug' => $sql];
            $this->app->response->setBody(json_encode($response));
            $this->app->response->setStatus(404);

            return;
        }

        //secondary points count
        $sql = 'select count(*) as cnt from circuit_secondary_points where primary_point_id=' . $pointId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(['error' => 'unable to count secondary points', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }
        $row = $result->fetch(\PDO::FETCH_ASSOC);
        $secondaryPointsCount = $row['cnt'];

        $sql = 'UPDATE circuit set secondary_count=' . $secondaryPointsCount . ' where id=' . $circuitId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(['error' => 'unable to update circuit', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }

        $db->commit();
        $response = json_encode(['status' => 'ok']);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
