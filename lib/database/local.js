'use strict'
const Sequelize = require('sequelize');
const process = require('process');
const path = require('path');
const glob = require('glob');
const debug = require('debug')('localDatabase');

const configuration = require('./../common/config');
const modelDefinitions = require('./../models/modelsDefinitions');
const SequelizeBuilder = require('./../models/sequelizeBuilder');

const knex = require('knex')({
    client: 'mysql',
    debug: true,
});

let sequelize = new Sequelize(configuration.storage.mysql.database, configuration.storage.mysql.user, configuration.storage.mysql.password, {
    host: configuration.storage.mysql.host,
    dialect: 'mysql',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

Object.keys(modelDefinitions.models).map((modelName) => {
    SequelizeBuilder.build(sequelize, modelName, modelDefinitions.models[modelName]);
    debug(modelName + ' DB model registered (sequelize)');
});

module.exports.sequelize = sequelize;
module.exports.knex = knex;
