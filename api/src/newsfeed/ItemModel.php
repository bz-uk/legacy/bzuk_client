<?php
namespace botgarApi\newsfeed;

/**
 * Created by PhpStorm.
 * User: juraj
 * Date: 21.11.15
 * Time: 22:28
 */

class ItemModel
{
    private $id;
    private $dateCreated;
    private $title;
    private $description;
    private $imageId;
    private $tag;
    private $articleId;
    private $externalUrl;
    private $type;
    private $tagId;
    private $itemId;
    private $intent;
    private $dateModified;

//    public function loadFromXml(\SimpleXMLElement $xml)
//    {
//        $this->id = (String)$xml->id;
//        $this->dateCreated = (String)$xml->date_created;
//        $this->title = (String)$xml->title;
//        $this->description = (String)$xml->description;
//        $this->imageId = (String)$xml->image_id;
//        $this->tag = (String)$xml->tag;
//        $this->articleId = (String)$xml->article_id;
//        $this->externalUrl = (String)$xml->external_url;
//        $this->type = (String)$xml->type;
//    }

    /**
     * @return array
     */
    public function asArray()
    {
        return array(
            'id' => $this->id,
            'title' => $this->title,
            'dateCreated' => $this->dateCreated,
            'dateModified' => $this->dateModified,
            'tagId' => $this->tagId,
            'type' => $this->type,
            'imageId' => $this->imageId,
            'description' => $this->description,
            'externalUrl' => $this->externalUrl,
            'articleId' => $this->articleId,
            'itemId' => $this->itemId,
            'intent' => $this->intent
        );
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function loadFromDbRow(array $row)
    {
        $this->id = $row['id'];
        $this->title = $row['title'];
        $this->dateCreated = $row['date_created'];
        $this->dateModified = $row['date_modified'];
        $this->tagId = $row['tag_id'];
        $this->type = $row['type'];
        $this->imageId = $row['image_id'];
        $this->description = $row['description'];
        $this->externalUrl = $row['external_url'];
        $this->articleId = $row['article_id'];
        $this->itemId = $row['item_id'];
        $this->intent = $row['intent'];
    }
}