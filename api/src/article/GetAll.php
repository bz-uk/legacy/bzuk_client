<?php
namespace botgarApi\article;

use Slim\Slim;

class GetAll {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run() {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);
        $sql = "SELECT * FROM article WHERE deleted_at is null";
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            return;
        }
        $output = [];
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
        	//add article
        	$article = [
        			'id' => $row['id'] + 0,
        			'title' => $row['title'],
        			'title_en' => $row['title_en'],
        			'code' => $row['code'],
        			'type' => $row['type'],
        			'author' => $row['author'],
        			'date_created' => $row['date_created'],
        			'date_changed' => $row['date_changed'],
        			'image_id' => $row['image_id'],
        			'tag_id' => $row['tag_id'],
        			'map_marker_id' => $row['map_marker_id'],
        			'map_overlay_id' => $row['map_overlay_id'],
                    'published' => $row['published']==1?true:false,
        			'blocks' => []
        	];        
        

        $blocksSql = "SELECT * FROM article_block WHERE article_id=".$article['id']." ORDER BY `order`";
        $blocksResult = $db->query($blocksSql);
        if (!$blocksResult) {
            continue;
            return;
        }
        while ($block = $blocksResult->fetch(\PDO::FETCH_ASSOC)) {            
            $article['blocks'][] = [
                'id' => $block['id'],
                'content' => $block['content'],
				'content_en' => $block['content_en'],
                'backgroud_color' => $block['backgroud_color']
            ];
        }

//marker
// 	if ($article['map_marker_id'] != null && $article['map_marker_id'] > 0) {
//             $markerSql = "SELECT * FROM map_marker where id=" . $article['map_marker_id'];
//             $result = $db->query($sql);
//             $output['map_marker'] = [];
//             while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
//                 $output[$row['article_id']]['map_marker'] = [
//                     'id' => $row['id'],
//                     'title' => $row['title'],
//                     'contents' => $row['contents'],
//                     'marker_visible' => $row['marker_visible'],
//                     'tag_id' => $row['tag_id'],
//                     'image' => $row['image'],
//                     'article_id' => $row['article_id'],
//                     'latitude' => $row['latitude'],
//                     'longitude' => $row['longitude']
//                 ];
//             }
//         }
			$output[] = $article;
        }
        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
