'use strict';
import React from 'react';
import BaseScreenComponent from './baseScreenComponent.jsx';
import FontIcon from 'material-ui/FontIcon';
import Screen from '../common/screen';

class AboutScreenComponent extends BaseScreenComponent {
    getContent() {
        let styles = {
            centeredPage: {
                display:       'flex',
                flexDirection: 'column',
                alignItems:    'center'
            },
            pageContent:  {
                maxWidth: 800
            },
            owlIcon: {
                fontSize: '5em'
            },
            owlBlock: {
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center'
            }
        };
        return (
            <div style={styles.centeredPage}>
                <div style={styles.pageContent}>
                    <h1>About</h1>
                    <p>Administrative panel ver.{this.getVersion()}</p>
                    {/*our goal: complete, free & opensource administration of botanical garden or park*/}
                    {/*add an owl icon here*/}
                    <div style={styles.owlBlock}>
                        <FontIcon style={styles.owlIcon} className="icon-animals-owl"></FontIcon>
                        <div>Free and opensource solution for gardens and parks.</div>
                    </div>
                    <p>Developed by <a href="https://www.roamingowl.com">roamingowl.com</a></p>
                </div>
            </div>
        )
    }

    getVersion() {
        return window['app_settings_2256da08-abd3-4ac9-bd97-eb780a5138f7'].version;
    }
}

export default class AboutScreen extends Screen{
    constructor(options) {
        super(options);
    }

    getTitle() {
        return 'About';
    }

    getIconClass() {
        return 'icon-animals-owl';
    }

    getComponent() {
        let self = this;
        return class extends React.Component {
            render() {
                let props = {screenCode: self.getCode()};
                return (
                    <AboutScreenComponent {...props}></AboutScreenComponent>
                );
            }
        }
    }
}