'use strict';
import assign from 'lodash.assign';
/**
 * @class baseValidator
 */
export default class baseValidator {

    constructor(options) {
        let defaultOptions = {
            errorMessage: 'Value "{value}" is not valid'
        }

        this.options = assign(defaultOptions, options);
    }

    /**
     * @abstract
     */
    isValid(value) {
        throw new Error('abstract')
    }

    validate(value) {
        let result = {
            valid: this.isValid(value)
        }
        if (!result.valid) {
            result.errorMessage = this.options.errorMessage.replace(/\{value\}/i, value);
        }
        return result;
    }
}