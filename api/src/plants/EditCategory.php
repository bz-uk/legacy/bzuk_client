<?php
namespace botgarApi\plants;

use Slim\Slim;

class EditCategory {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message) {
      $response = ['status'=>'error', 'message'=> $message];
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run($categoryId) {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);

      error_log('data: ' . var_export($json, true));
      //validation
      if (!isset($data->name)) {
        return $this->returnInvalidResponse('Category name can not be empty');
    	}
      //ok now update

      $conf = $this->app->container->get('configuration');
      $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

      $db->beginTransaction();

      //category
        $sql = 'UPDATE plant_category set name='.$db->quote($data->name).' where id='.$categoryId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            return $this->returnInvalidResponse('Unable to update category: ' . $sql);
        }
      $db->commit();
      $response = ['status'=> 'ok', 'message' => 'category saved'];
      $this->app->response->setStatus(200);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
      return;
    }
}
