<?php

namespace botgarApi\circuit;

use Slim\Slim;

class DeleteOnePrimaryPoint
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($circuitId, $pointId)
    {
        if (!is_numeric($pointId)) {
            $this->app->response->setStatus(400);

            return;
        }
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $db->beginTransaction();

        $sql = 'UPDATE circuit_primary_points SET is_deleted=1 WHERE id='.$pointId;
        $result = $db->query($sql);
        if (!$result) {
            $response = ['error' => 'No point found', 'debug' => $sql];
            $this->app->response->setBody(json_encode($response));
            $this->app->response->setStatus(404);

            return;
        }

        //primary points count
        $sql = 'select count(*) as cnt from circuit_primary_points where circuit_id=' . $circuitId . ' and is_deleted = 0';
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(['error' => 'unable to count primary points', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }
        $row = $result->fetch(\PDO::FETCH_ASSOC);
        $primaryPointsCount = $row['cnt'];

        $sql = 'UPDATE circuit set primary_count=' . $primaryPointsCount . ' where id=' . $circuitId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(['error' => 'unable to update circuit', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }

        $db->commit();
        $response = json_encode(['status' => 'ok']);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
