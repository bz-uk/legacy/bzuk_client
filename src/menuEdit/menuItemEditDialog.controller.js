angular.module('adminApp').controller('menuItemEditorDailogController', ['$scope', '$mdDialog', 'menuItemModel', function($scope, $mdDialog, menuItemModel) {
	$scope.model = menuItemModel;
	
    $scope.hide = function() {
      $mdDialog.hide();
    };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.save = function() {
	  $mdDialog.hide($scope.model);
  };
  $scope.options = {
		    language: 'en',
		    allowedContent: true,
		    entities: false,
		    width: 500
		  };
}]);