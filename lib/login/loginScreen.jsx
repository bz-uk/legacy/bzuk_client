'use strict';
import React from 'react';
import LoginDialog from './../bi-react-components/component/loginDialog';
import HeaderImage from './LoginScreen/headerImage';
import settings from './settings';
import {toast} from 'react-toastify';

export default class LoginScreen extends React.Component {
    onSuccessfulLogin() {
        window.location = '/';
    }

    onFailedLogin(err) {
        toast(<span>{err.message}</span>, {type: 'error'});
    }

    tryToLogin(loginData) {
        loginData['_csrf'] = settings.get('csrf');
        return fetch('/login', {
            method: 'POST',
            body: JSON.stringify(loginData),
            credentials: 'include',
            headers: {
                'X-XSRF-TOKEN': settings.get('csrf'),
                'Content-Type': 'application/json',
                'Accept': 'application/json, text/plain, */*'
            }
        })
        .then((response) => {
            return response.json()
            .then((jsonResponse) => {
                if (response.status !== 200) {
                    console.log(jsonResponse);
                    throw new Error(jsonResponse.message);
                }
                return jsonResponse;
            })
        })
    }

    render() {
        let styles = {
            cardStyle: {
                maxWidth: '20vw',
                minWidth: '300px'
            }
        }
        return (<div style={styles.cardStyle}>
            <LoginDialog title={<HeaderImage/>} onLoginCallback={this.tryToLogin}
                         onSuccessfulLogin={this.onSuccessfulLogin} onFailedLogin={this.onFailedLogin}/>
        </div>);
    }
}
