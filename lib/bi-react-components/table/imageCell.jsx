'use strict';
import React from 'react';
import UploadedFile from './../../util/uploadedFile.js';

export default class ImageCell extends React.Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        var styles = {
            avatarImage: {
                width: 64,
                height: 64,
                backgroundImage: `url("${UploadedFile.getFullUrl(this.props.row[this.props.columnName])}")`,
                backgroundSize: 'contain'
            }
        }
        return (
            <div style={styles.avatarImage}></div>
        );
    }
}