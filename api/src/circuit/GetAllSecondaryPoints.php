<?php
namespace botgarApi\circuit;

use Slim\Slim;

class GetAllSecondaryPoints {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($circuitId, $primaryPointId) {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT pl.*,sp.active FROM circuit_secondary_points sp
          INNER JOIN plants pl on pl.id=sp.plant_id
          WHERE sp.circuit_id=" . $circuitId . ' AND sp.primary_point_id=' . $primaryPointId;
        // $sql = "SELECT pl.*,mm.* FROM circuit_secondary_points sp
        //   INNER JOIN plants pl on pl.id=sp.plant_id
        //   LEFT JOIN map_marker mm ON mm.id=pl.map_marker_id OR mm.map_overlay_id=pl.map_overlay_id
        //   WHERE sp.circuit_id=" . $circuitId . ' AND sp.primary_point_id=' . $primaryPointId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setBody(json_encode(['query' => $sql]));
            $this->app->response->setStatus(404);
            return;
        }
        $output = ['tableData' => []];
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $row['id'] += 0;
            $row['active'] = $row['active']==='1'?true:false;
		        $output['tableData'][] = $row;
        }
        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
