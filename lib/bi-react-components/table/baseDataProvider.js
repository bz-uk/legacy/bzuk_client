'use strict';
import EventEmitter from 'wolfy87-eventemitter';
import _ from 'lodash';

import TableDataProvider from './../service/tableDataProvider';

export default class BaseDataProvider extends EventEmitter {
    constructor(options) {
        super();
        let defaultOptions = {};
        this.options       = _.assign(defaultOptions, options);
    }
    
    removeAllListeners() {
        //TODO
    }
    
    destruct() {
        this.removeAllListeners();
    }
    
    requestData(columns, filter, rowsPerPage, page) {
        throw new Error('not implemented');
    }
}