'use strict';
export default {
    getDefinition(name, level, ownership) {
        return {
            name: name,
            level: level,
            ownership: ownership
        }
    },
    level: {
        READ: 'r',
        READWRITE: 'rw'
    },
    ownership: {
        OWN: 'own',
        ALL: 'all'
    },
    permissions: {
        REGISTRY: 'registry',
        ARTICLES: 'articles',
    }
}