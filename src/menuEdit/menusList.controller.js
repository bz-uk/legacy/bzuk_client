'use strict';
angular.module('adminApp').controller('menusListController', [ '$scope', '$mdDialog', '$mdMedia', 'menusService', '$timeout', '$location', function($scope, $mdDialog, $mdMedia, menusService, $timeout, $location) {
			$scope.model = {
				circuits : menusService.menusList,
				query: {
					    order: 'name',
					    limit: 10,
					    page: 1
					  },
				selected: [],
			    flashMessages: []
			}

			$scope.editMenu = function(menu, ev) {
				$location.url('/menus/' + menu.id)
				return;

			};

			function success(articles) {
				$scope.model.menus = articles;
			}

			$scope.getMenus = function () {
				$scope.model.promise = menusService.load($scope.query, success).$promise;
			}
			$scope.selected = [];
			$scope.limitOptions = [5,10,15];
			$scope.options = {
				    rowSelection: false,
				    multiSelect: false,
				    autoSelect: false,
				    decapitate: false,
				    largeEditDialog: true,
				    boundaryLinks: false,
				    limitSelect: true,
				    pageSelect: true
				  };
			$scope.getMenus();
} ]);
