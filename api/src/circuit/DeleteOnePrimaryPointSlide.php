<?php

namespace botgarApi\circuit;

use Slim\Slim;

class DeleteOnePrimaryPointSlide
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($circuitId, $pointId, $slideId)
    {
        if (!is_numeric($slideId)) {
            $this->app->response->setStatus(400);

            return;
        }
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $db->beginTransaction();

        $sql = 'UPDATE primary_point_parts SET is_deleted=1 WHERE id='.$slideId;
        $result = $db->query($sql);
        if (!$result) {
            $response = ['error' => 'No slide found', 'debug' => $sql];
            $this->app->response->setBody(json_encode($response));
            $this->app->response->setStatus(404);

            return;
        }        

        $db->commit();
        $response = json_encode(['status' => 'ok']);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
