-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: botgar
-- ------------------------------------------------------
-- Server version	5.1.69

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL,
  `code` varchar(10) NOT NULL,
  `author` varchar(1024) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_changed` datetime NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  `map_lat` double NOT NULL,
  `map_lon` double NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'Vystava orchideji','','Botanicka zahrada','2015-12-27 18:03:01','2015-12-27 18:03:01',2,1,0,0,'Lorem ipsum'),(2,'Vanocni vystava','','Botanicka zahrada','2015-12-27 18:03:06','2015-12-27 18:03:06',2,1,0,0,'lorem ipsum');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_gallery_image`
--

DROP TABLE IF EXISTS `article_gallery_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_gallery_image` (
  `article_id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_gallery_image`
--

LOCK TABLES `article_gallery_image` WRITE;
/*!40000 ALTER TABLE `article_gallery_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_gallery_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_source`
--

DROP TABLE IF EXISTS `article_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_source` (
  `article_id` bigint(20) NOT NULL,
  `source_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_source`
--

LOCK TABLES `article_source` WRITE;
/*!40000 ALTER TABLE `article_source` DISABLE KEYS */;
INSERT INTO `article_source` VALUES (1,1),(1,2),(1,3);
/*!40000 ALTER TABLE `article_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bundle_id` bigint(20) DEFAULT NULL COMMENT 'conects multiple images together',
  `name` varchar(256) NOT NULL,
  `width` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `type` varchar(4) NOT NULL,
  `size` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (1,0,'orchudea.png',900,600,'png',55368),(2,0,'flowers.png',709,454,'png',764004),(3,0,'prague1.png',699,364,'png',584594),(4,NULL,'tag_video.png',64,64,'png',123456),(5,NULL,'tag_exhibition.png',64,64,'png',12345),(6,NULL,'tag_tree.png',64,64,'png',123456),(7,NULL,'quercus_robur.png',1024,1024,'png',234567);
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `information_source`
--

DROP TABLE IF EXISTS `information_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL,
  `name` varchar(1024) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information_source`
--

LOCK TABLES `information_source` WRITE;
/*!40000 ALTER TABLE `information_source` DISABLE KEYS */;
INSERT INTO `information_source` VALUES (1,1,'wiki','https://www.google.sk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjVmt_S6vfJAhWLjiwKHfN_CeoQFggdMAA&url=https%3A%2F%2Fsk.wikipedia.org%2Fwiki%2FDub_letn%25C3%25BD&usg=AFQjCNHo1Ti_FIS7kJW-TI_-693wTCIS6Q&sig2=hBUu7eCoSG9t9VqKBVJjvA'),(2,2,'nahuby.sk','http://www.nahuby.sk/atlas-rastlin/Quercus-robur/dub-letny/dub-letni/ID7415'),(3,3,'darius.cz','http://www.darius.cz/ekolog/mater_drevo.html');
/*!40000 ALTER TABLE `information_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_changed` datetime NOT NULL,
  `author` varchar(512) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `map_lat` double NOT NULL,
  `map_lon` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,'000xls3433','2015-12-27 18:12:02','2015-12-27 18:12:02','Botanicka zahrada',3,7,0,0);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_map_marker`
--

DROP TABLE IF EXISTS `item_map_marker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_map_marker` (
  `item_id` bigint(20) NOT NULL,
  `map_marker_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_map_marker`
--

LOCK TABLES `item_map_marker` WRITE;
/*!40000 ALTER TABLE `item_map_marker` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_map_marker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_names`
--

DROP TABLE IF EXISTS `item_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_names` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `language_code` varchar(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `item_names_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_names`
--

LOCK TABLES `item_names` WRITE;
/*!40000 ALTER TABLE `item_names` DISABLE KEYS */;
INSERT INTO `item_names` VALUES (1,1,'Dub letny','svk'),(2,1,'Dub letni','cze'),(3,1,'Quercus robur','lat'),(4,1,'English oak','eng');
/*!40000 ALTER TABLE `item_names` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_text_part`
--

DROP TABLE IF EXISTS `item_text_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_text_part` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `order` int(11) NOT NULL,
  `background_color` varchar(9) NOT NULL,
  `title` varchar(256) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_text_part`
--

LOCK TABLES `item_text_part` WRITE;
/*!40000 ALTER TABLE `item_text_part` DISABLE KEYS */;
INSERT INTO `item_text_part` VALUES (1,1,0,'#ffffff','Fakta','blooming: apr-may\r\nliving for: 80-120yr\r\nleafs:  Listy obvejčité, nepravidelně peřenolaločnaté, na bázi srdčitě ouškaté.\r\nfruits: Plodem je žalud (jednosemenná nažka) sedící v číšce. Stopka je 3–7 cm dlouhá.\r\nflowers:  Květy jsou jednopohlavné, samčí květenství má charakter jehněd na loňských větévkách, samičí jehnědy rostou na letorostech.\r\nheight: 45m\r\noccurence/origin: Přirozeně se vyskytuje v Evropě a Malé Asii, na Kavkaze a v některých lokalitách severní Afriky.'),(2,1,1,'#ffff99','Vedeli ste ze...','Kôra sa používa v humánnom i veterinárnom lekárstve ako prudko zvieravý a krvácanie utišujúci prostriedok, aj na omrzliny a proti poteniu nôh.\r\nDubienky - Sú to guľaté nádory, čiže patologické novotvary spôsobené hmyzom. Vyvolávajú ich hrčiarky.\r\n'),(3,1,2,'#ff99ff','Dalsie nazvy','lorem ipsum'),(4,1,3,'#99ffff','Podobne/Pribuzne druhy','poddruhy: Dub letný slavónsky (Quercus robur ssp. slavonica)  Vyskytuje sa v Chorvátsko – Srbskej oblasti Slavónsko.\r\ndub šedozelený (Quercus pedunculiflora)'),(5,1,3,'#ffffff','Autori a ddroje','Autori: bla bla\r\n\r\nZdroje:\r\nlorem ipsum');
/*!40000 ALTER TABLE `item_text_part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `map_marker`
--

DROP TABLE IF EXISTS `map_marker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map_marker` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `map_overlay_id` bigint(20) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `contents` varchar(1024) NOT NULL,
  `marker_visible` tinyint(4) NOT NULL DEFAULT '1',
  `tag_id` bigint(20) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `article_id` bigint(20) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `map_marker`
--

LOCK TABLES `map_marker` WRITE;
/*!40000 ALTER TABLE `map_marker` DISABLE KEYS */;
INSERT INTO `map_marker` VALUES (1,1,'Central European flora (Grove)','',1,NULL,'map_marker_leaf.png',NULL,50.0709886574673,14.420518912375),(2,1,'Maidenhair tree ( Ginkgo biloba \'Praga\')','',1,NULL,'map_marker_tree.png',NULL,50.0710545064673,14.4201363623142),(3,1,'Central European flora (Calcareous vegetation)','',1,NULL,'map_marker_leaf.png',NULL,50.0711455328772,14.4208216667175),(4,1,'Central European flora (Peat bog)','50.07161228260516',1,NULL,'map_marker_leaf.png',NULL,50.0716122826052,14.4215559214354),(5,1,'Central European flora (Sandbank)','',1,NULL,'map_marker_leaf.png',NULL,50.0717304218574,14.4215904548764),(6,1,'Aquatic and wetland plants','',1,NULL,'map_marker_wetland.png',NULL,50.0707758311594,14.4216913729906),(7,1,'Useful plants','',1,NULL,'map_marker_leaf.png',NULL,50.071052784926,14.4215988367796),(8,1,'System of plant taxonomy','50.070540623635516',1,NULL,'map_marker_leaf.png',0,50.0705406236355,14.4210918992758),(9,1,'Ericaceous plants','',1,NULL,'map_marker_leaf.png',NULL,50.0712968127933,14.4218043610454),(10,1,'Collection of conifers','',1,NULL,'map_marker_leaf.png',NULL,50.0708767569985,14.4208672642708),(11,1,'Subtropical plants (in summer) and Mediterranean rock garden','',1,NULL,'map_marker_leaf.png',NULL,50.0707900339556,14.4204293936491),(12,1,'Collection of oaks','',1,NULL,'map_marker_leaf.png',NULL,50.0708888078308,14.4220293313265),(13,1,'Geological park','',1,NULL,'map_marker_fossils.png',NULL,50.0715660166985,14.4222603365779),(14,1,'Tropical greenhouse','',1,NULL,'map_marker_leaf.png',NULL,50.0711642545997,14.4202379509807),(15,1,'Subtropical greenhouse (exhibition greenhouse in summer)','',1,NULL,'map_marker_leaf.png',NULL,50.0710239490994,14.4196528941393),(16,1,'Cacti and other succulents','',1,NULL,'map_marker_leaf.png',NULL,50.0709527202298,14.4198882579803),(17,1,'Stock greenhouses (not accessible to the public)','',1,NULL,'map_marker_not_accessible.png',NULL,50.0712053562865,14.4204941019416),(18,1,'Upper exhibition greenhouse','',1,NULL,'map_marker_leaf.png',NULL,50.0712961672186,14.4203861430287),(19,1,'Stock sector (not accessible to the public)','',1,NULL,'map_marker_not_accessible.png',NULL,50.0719901550369,14.4223579019308),(20,1,'Department of Botany and Institute for Environmental Studies','',1,NULL,'map_marker_office_building.png',NULL,50.0712354831551,14.4211703538895),(21,1,'Central office of the Botanical Garden and Office for Studies of the Faculty of Science','',1,NULL,'map_marker_office_building.png',NULL,50.0705427755851,14.419763199985),(22,1,'Toilets','',1,NULL,'map_marker_toilets.png',NULL,50.0704145192181,14.4197625294328),(23,1,'Entrance 1','',1,NULL,'map_marker_entrance.png',NULL,50.0705862449472,14.4195127487183),(24,1,'Entrance 2','',1,NULL,'map_marker_entrance.png',NULL,50.070738602598,14.4194182008505),(25,1,'Entrance 3','',1,NULL,'map_marker_entrance.png',NULL,50.0715234089868,14.4207150489092),(26,1,'Entrance 4','',1,NULL,'map_marker_entrance.png',NULL,14.4209007918835,50.0715853838277),(27,1,'Entrance 5','',1,NULL,'map_marker_entrance.png',NULL,50.0723559740306,14.4229067489505),(28,1,'Entrance 6','',1,NULL,'map_marker_entrance.png',NULL,50.0713904210372,14.422828964889),(29,1,'Stock greenhouses (not accessible to the public)','',1,NULL,'map_marker_not_accessible.png',NULL,50.070884288769,14.4197471067309),(30,1,'Aquatic and wetland plants','',1,NULL,'map_marker_wetland.png',NULL,50.0705580544248,14.4213892892003);
/*!40000 ALTER TABLE `map_marker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `map_overlay`
--

DROP TABLE IF EXISTS `map_overlay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map_overlay` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `map_overlay`
--

LOCK TABLES `map_overlay` WRITE;
/*!40000 ALTER TABLE `map_overlay` DISABLE KEYS */;
INSERT INTO `map_overlay` VALUES (1,'General map','general_map');
/*!40000 ALTER TABLE `map_overlay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_item`
--

DROP TABLE IF EXISTS `news_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `tag_id` bigint(20) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'big image, small image, no image',
  `image_id` bigint(20) DEFAULT NULL,
  `description` varchar(1024) NOT NULL,
  `external_url` varchar(1024) DEFAULT NULL,
  `article_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `intent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_item`
--

LOCK TABLES `news_item` WRITE;
/*!40000 ALTER TABLE `news_item` DISABLE KEYS */;
INSERT INTO `news_item` VALUES (1,'Vystava orchideji','2016-01-03 14:55:19','2016-01-03 14:55:19',1,1,1,'Pridite sa pozriet na vystavu orchideji',NULL,1,NULL,NULL),(2,'Vánoční výstava','2016-01-07 04:50:39','2016-01-07 04:50:39',1,2,2,'Přijďte do Botanické zahrady navštivit vánoční ráj pohádek',NULL,NULL,NULL,NULL),(3,'Zo vzduchu!','2016-01-03 14:38:14','2016-01-03 14:38:14',2,1,3,'Proleťte se s námi nad stromy!','https://www.facebook.com/photo.php?v=10152596395509809',NULL,NULL,NULL),(4,'Seznam vystav na rok 2016','2016-01-07 05:03:55','2016-01-07 05:03:55',1,0,NULL,'Aktualizovali jsme seznam vystav v rokce 2016',NULL,NULL,NULL,'exhibitions');
/*!40000 ALTER TABLE `news_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_index`
--

DROP TABLE IF EXISTS `search_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_index` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) NOT NULL COMMENT 'copied from item/article',
  `code` varchar(10) NOT NULL COMMENT 'copied from item/article',
  `article_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `tag_id` bigint(20) NOT NULL COMMENT 'copied from item/article',
  `date_created` datetime NOT NULL COMMENT 'copied from item/article',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_index`
--

LOCK TABLES `search_index` WRITE;
/*!40000 ALTER TABLE `search_index` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'vystava',5),(2,'video',4),(3,'tree',6);
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-26  8:03:35
