'use strict';
import React from 'react';
import axios from 'axios';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import {hashHistory} from 'react-router'
import {List, ListItem, makeSelectable} from 'material-ui/List';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import {white} from 'material-ui/styles/colors';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import ActionVisibility from 'material-ui/svg-icons/action/visibility';
import ActionVisibilityOff from 'material-ui/svg-icons/action/visibility-off';

import Logger from './../util/logger';

var SelectableList = makeSelectable(List);

/**
 * @class SinglePageAppPanel
 */
export default class SinglePageAppPanel extends React.Component {
    
    constructor(props) {
        super(props);
        
        this.screens           = this.props.screens || [];
        this.defaultScreenCode = this.props.default;
        this.eventBus          = this.props.eventBus;
        this.logger            = this.props.logger || new Logger(Logger.LEVEL_DEBUG);
        this.appSettings       = this.props.appSettings;
        this.drawerContent     = this.props.drawerContent;
        
        let menuAutoHideEnabled = true;
        try {
            menuAutoHideEnabled = JSON.parse(localStorage.getItem('left-menu-autohide-enabled'));
        } catch (e) {
            //ignore
        }
        if (menuAutoHideEnabled === null || menuAutoHideEnabled === undefined) {
            menuAutoHideEnabled = true;
            localStorage.setItem('left-menu-autohide-enabled', menuAutoHideEnabled);
        }
        
        this.state = {
            autoHideDuration: 4000,
            snackbar: false,
            message: '',
            drawerOpen: false,
            title: this.props.title || 'Application',
            autoHideMenu: menuAutoHideEnabled
        }
        
        this.styles = {
            staticMenu: {
                float: 'left',
                height: 'calc(100vh - 64px)',
                top: 64,
                position: 'relative',
                width: 256,
                borderRight: '1px solid #e7e7e7'
            },
            drawerMenu: {},
            topToolbar: {
                position: 'fixed'
            },
            divider: {
                margin: '-1px 0px 0px',
                height: 1,
                border: 'none',
                backgroundColor: 'rgb(224, 224, 224)'
            },
            menuBottomLeftIcons: {
                textAlign: 'right',
            },
        }
    }
    
    onDrawerButtonClicked = (evt) => {
        this.state.drawerOpen = !this.state.drawerOpen;
        this.setState(this.state);
    }
    
    onLogoutHandler = (evt) => {
        let data = {}
        if (this.appSettings) {
            data['_csrf'] = this.appSettings.get('csrf');
        }
        axios.post('/logout', data).then((response) => {
            location.href = '/login';
        }).catch((err) => {
            console.error('Unable to logout', err);
            this.eventBus.emit('ok-message', 'Unable to logout!');
        })
    }
    
    openScreen = (screenInstance) => {
        this.onMenuSelected();
        if (screenInstance.getCode() === this.defaultScreenCode) {
            hashHistory.push('/');
            return;
        }
        hashHistory.push('/' + screenInstance.getCode());
    }
    
    getMenuItems(screens) {
        if (!screens) {
            return;
        }
        return screens.map((screenInstance, index) => {
            return (
                <ListItem key={index} primaryText={screenInstance.getName()}
                          value={'/' + screenInstance.getCode()}
                          onTouchTap={() => this.openScreen(screenInstance)}/>
            )
        })
    }
    
    onMenuSelected = () => {
        this.state.drawerOpen = false;
        this.setState(this.state);
    }
    
    componentWillReceiveProps = (nextProps) => {
        if (nextProps.title !== this.state.title) {
            this.state.title = nextProps.title;
            this.setState(this.state);
        }
    }
    
    toggleMenuAutoHide = () => {
        localStorage.setItem('left-menu-autohide-enabled', !this.state.autoHideMenu);
        this.setState({autoHideMenu: !this.state.autoHideMenu})
    }
    
    getMenu() {
        return (
            <div style={this.state.autoHideMenu ? this.styles.drawerMenu : this.styles.staticMenu}>
                {this.drawerContent ?
                    React.cloneElement(this.drawerContent, {onMenuSelected: this.onMenuSelected})
                    :
                    <SelectableList value={location.pathname}>
                        {this.getMenuItems(this.screens)}
                    </SelectableList>
                }
                <hr style={this.styles.divider}/>
                <div style={this.styles.menuBottomLeftIcons}>
                    <IconButton title={this.state.autoHideMenu ? 'Disable menu autohiding' : 'Enable menu autohiding'}
                                onTouchTap={this.toggleMenuAutoHide}>
                        {this.state.autoHideMenu ?
                            <ActionVisibility />
                            :
                            <ActionVisibilityOff/>
                        }
                    </IconButton>
                </div>
            </div>)
    }
    
    render() {
        var styles = {}
        
        var drawerStyle      = {
            position: 'fixed',
            float: 'left',
            backgroundColor: '#ffffff',
            top: 64,
            height: 'calc(100vh - 64px)',
            overflowX: 'hidden'
        }
        var contentPageStyle = {
            paddingTop: 64, //toolbatr height
            overflowX: 'hidden',
        }
        return (
            <div>
                <AppBar id="top-menu"
                        title={this.state.title}
                        style={this.styles.topToolbar}
                        iconElementLeft={!this.state.autoHideMenu ?
                            <span></span>
                            :
                            this.state.drawerOpen ?
                                <IconButton onTouchTap={this.onDrawerButtonClicked}><NavigationClose/></IconButton>
                                :
                                <IconButton onTouchTap={this.onDrawerButtonClicked}><NavigationMenu/></IconButton>
                        }
                        iconElementRight={<AppBarLoggedIcon onLogout={this.onLogoutHandler}/>}
                />
                <div id="app-content">
                    {this.state.autoHideMenu ?
                        <Drawer containerStyle={drawerStyle} open={this.state.drawerOpen}>
                            {this.getMenu()}
                        </Drawer>
                        :
                        this.getMenu()
                    }
                    <div style={contentPageStyle}>
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

/**
 * @private
 */
class AppBarLoggedIcon extends React.Component {
    
    render() {
        var dropdownMenuStyle = {
            'margin': '0'
        }
        return (
            <IconMenu
                iconButtonElement={
                    <IconButton><FontIcon color={white} className="icon-user"/></IconButton>
                }
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
            >
                <MenuItem onTouchTap={this.props.onLogout}
                          leftIcon={<FontIcon style={dropdownMenuStyle} className="icon-logout"/>}
                          primaryText="Logout"/>
            </IconMenu>
        )
    }
}