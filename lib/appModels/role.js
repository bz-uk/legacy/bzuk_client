'use strict';
const _     = require('lodash');
const debug = require('debug')('AppRole');

// const BaseModel = require('./base');
// const bookshelf = require('./../database/local').bookshelf;
const models = require('./../database/local').sequelize.models;

const DB_MODEL = 'Role';

/**
 * @class Role
 */
module.exports = class Role {

    dbModel() {
        return models[DB_MODEL];//bookshelf.model(DB_MODEL);
    }

    /**
     *
     * @param roleId
     * @return {Promise}
     */
    static loadById(roleId) {
        return models[DB_MODEL].findOne({where: {id: roleId}})
        .then((model) => {
            return model;
        })
        //return BaseModel.$loadOneBy({id: roleId}, DB_MODEL, Role);
    }
};