const Promise = require('bluebird');
const rawQuery = require('./../lib/util/query');

module.exports = {
    up: function (query, DataTypes) {
        return rawQuery('CREATE TABLE `test01` (\
            `id` INT NOT NULL,\
            PRIMARY KEY (`id`))', {type: 'RAW'});
    },

    down: function (query, DataTypes) {
        return rawQuery('DROP TABLE `test01`', {type: 'RAW'});
    }
};