'use strict';
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const debug = require('debug')('local-auth');
const bcrypt = require('bcrypt');

const User = require('./../appModels/user');
const PasswordNotMatch = require('./../error/passwordNotMatch');
const WrongUsernameOrPassError = require('../error/WrongUsernameOrPassError')
const NotFoundError = require('./../error/notFound');
const errorLog = require('./../util/errorLog');

passport.serializeUser(function (user, done) {
    debug('Passport serialize user : ', user.id);
    done(null, user.id);
});
passport.deserializeUser(function (id, done) {
    debug('Passport deserialize user by id: ', id);
    User.loadById(id)
    .then(function (userInstance) {
        var info = {scope: '*'};
        return done(null, userInstance, info);
    })
    .catch(function (err) {
        return done(err, false);
    });
});

passport.use('local', new LocalStrategy(
    function (username, password, done) {
        debug('logging in ', username, password);
        let context = {};
        User.loadByUsername(username)
        .then((user) => {
            if (!user) {
                throw new NotFoundError('User');
            }
            debug('user found');
            context.user = user;
            return bcrypt.compare(password, user.password)
        })
        .then((result) => {
            if (!result) {
                throw new PasswordNotMatch();
            }
            debug('password match!');
            return done(null, context.user);
        })
        .catch(function (err) {
            if (!(err instanceof PasswordNotMatch) && !(err instanceof NotFoundError)) {
                errorLog.log('error', err.message, {error: err});
            }
            debug('serious err', err);
            return done(new WrongUsernameOrPassError());
        });
    }
));