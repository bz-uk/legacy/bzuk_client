'use strict';
import BaseService from './baseService.js';

/**
 * @abstract
 * @class TableDataProvider
 * @extends BaseService
 */
export default class TableDataProvider extends BaseService {
    constructor(backendEndPointUrl, options) {
        super(options);
        this.backendEndPointUrl = backendEndPointUrl;
    }
    
    prepareRequestData(columnsList, filter, rowsPerPage, page) {
        let requestData = {columns: columnsList, filter: filter};
        if (rowsPerPage && page) {
            requestData.rowsPerPage = rowsPerPage;
            requestData.page        = page;
        }
        if (filter && filter.sortBy) {
            requestData.sortBy = filter.sortBy;
        }
        if (filter && filter.sortOrder) {
            requestData.sortOrder = filter.sortOrder;
        }
        return requestData;
    }
    
    getRowsForTable(columnsList, filter, rowsPerPage, page) {
        return this.baseCall('get', this.backendEndPointUrl, this.prepareRequestData(columnsList, filter, rowsPerPage, page))
        .then((response) => {
            if (!response.data.rows) {
                throw Error('Wrong table request response format. Field named "rows" is missing');
            }
            this.debug(response.data.rows.length + ' rows received');
            return response.data;
        })
    }
}