'use strict';
const gulp        = require('gulp');
var browserSync   = require('browser-sync').create();
var path          = require('path');
var gutil         = require('gulp-util');
const replace     = require('gulp-replace');
const concat      = require('gulp-concat');
var postcss       = require('gulp-postcss');
var sourcemaps    = require('gulp-sourcemaps');
var autoprefixer  = require('autoprefixer');
var nestedcss     = require('postcss-nested');
var postCssImport = require('postcss-import');
var cssNano       = require('cssnano');
var rename        = require('gulp-rename');
var webpackStream = require('webpack-stream');
const webpack     = require('webpack');
const webp        = require('gulp-webp');
const plumber = require('gulp-plumber');
const paths = require('./build.paths');
var url = require('url');
var proxy = require('proxy-middleware');

gulp.task('post-css', ['admin-css', 'login-css'], () => {
    return gulp.src(paths.buildDir + '/*.{css,css.map}')
    .pipe(gulp.dest(paths.admin.styles.targetDir));
});

gulp.task('webserver', ['post-css', 'js', 'resources'], () => {
    // var root = url.parse('http://localhost:9001/');
    // root.route = '/';
    var js = url.parse('http://localhost:9001/js');
    js.route = '/js';
    var index = url.parse('http://localhost:9001/');
    index.route = '/';

    browserSync.init({
        proxy:          "http://localhost:8222",
        //server: {
        //     baseDir: "./",
        middleware: [proxy(js, index)],
        // },
        port:           7070,
        files:          ['./public/**/*'],
        https:          false,
        online:         true,
        open:           false,
        reloadDebounce: 1000,
        reloadDelay:    150,
    });
    gulp.watch('/styles/**/*.css', ['post-css']);
});

gulp.task('js', function() {
        let webpackConfig = require('./webpack2.config.js');
        webpackConfig.watch = true;
        gulp.src(paths.login.sourceDir)
        .pipe(plumber())
        .pipe(webpackStream(webpackConfig, webpack))
        .pipe(gulp.dest(paths.login.targetDir));
});

gulp.task('resources', ['resources-login', 'resources-admin']);

gulp.task('resources-login', () => {
    gulp.src(['resources/login/fonts/fontello/font/*.*'])
    .pipe(gulp.dest(paths.publicDir + '/login/font'));

    gulp.src(['resources/login/img/*.*']) //webps
    .pipe(webp()) //webpsf
    .pipe(gulp.dest(paths.publicDir + '/img/login'));

    return gulp.src(['resources/login/img/*.*']) //originals
    .pipe(gulp.dest(paths.publicDir + '/img/login'));
})

gulp.task('resources-admin', () => {
    gulp.src(['resources/login/fonts/fontello/font/*.*'])
    .pipe(gulp.dest(paths.publicDir + '/admin/font'));

    gulp.src(['resources/admin/img/*.*']) //webps
    .pipe(webp()) //webpsf
    .pipe(gulp.dest(paths.publicDir + '/img/admin'));

    return gulp.src(['resources/admin/img/*.*']) //originals
    .pipe(gulp.dest(paths.publicDir + '/img/admin'));
})

let buildCss = (sourceDir, targetDir, stylesheetName) => {
    return function () {
        var processors = [
            postCssImport,
            nestedcss,
            autoprefixer({browsers: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']}),
            cssNano,
        ];
        return gulp.src(sourceDir + '/index.css')
        .pipe(sourcemaps.init())
        .pipe(postcss(processors))
        .pipe(replace(/\.\.\/font\/fontello/g, '../'+stylesheetName+'/font/fontello'))
        .pipe(rename(stylesheetName + '.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(targetDir));
    }
}

gulp.task('admin-css', buildCss(paths.admin.styles.sourceDir, paths.buildDir, 'admin'));
gulp.task('login-css', buildCss(paths.login.styles.sourceDir, paths.buildDir, 'login'));

gulp.task('default', ['webserver']);
gulp.task('build', ['post-css', 'js', 'resources']);