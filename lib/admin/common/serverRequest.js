'use strict';
import axios from 'axios';

export default class ServerRequest {
    constructor(path) {
        this.path = path;
    }

    baseCall(method, data, params) {
        console.log('Requesting data from ', method, this.path);
        return axios({
            method:         method,
            url:            this.path,
            data:           data,
            params:         params,
            xsrfCookieName: 'xsrf-token'
        })
        .then(function (response) {
            return response.data;
        }).catch((err) => {
            this.logger.debug('Error during ' + method + ' request', err);
            if (this.eventBus) {
                this.eventBus.emit('ok-message', 'Error during ' + method + ' request');
            }
            throw err;
        });
    }

    get(params) {
        return this.baseCall('get', undefined, params);
    }

    post(data, params) {
        return this.baseCall('get', data, params);
    }
}