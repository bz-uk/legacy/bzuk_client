-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: botgar
-- ------------------------------------------------------
-- Server version	5.1.69

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL,
  `title` varchar(1024) NOT NULL,
  `title_en` varchar(1024) NOT NULL,
  `code` varchar(10) NOT NULL,
  `author` varchar(1024) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_changed` datetime NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  `content` text NOT NULL,
  `map_marker_id` bigint(20) NOT NULL,
  `map_overlay_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,0,'Vystava orchideji','','','Botanicka zahrada','2015-12-27 18:03:01','2015-12-27 18:03:01',2,1,'Lorem ipsum',0,0),(2,0,'Vanocni vystava','','','Botanicka zahrada','2015-12-27 18:03:06','2015-12-27 18:03:06',2,1,'lorem ipsum',0,0),(3,0,'Oteviraci doba','Opening hours','','Botanicka zahrada','2016-02-04 06:22:01','2016-02-04 06:22:01',0,0,'',0,0),(4,0,'Historie zahrady','','','Botanicka zahrada','2016-02-04 06:22:01','2016-02-04 06:22:01',0,0,'',0,0),(5,0,'Jak se k nám dostanete','','','Botanicka zahrada','2016-02-04 06:22:01','2016-02-04 06:22:01',0,0,'',31,0),(6,0,'Komentované prohlídky','','','Botanicka zahrada','2016-02-04 06:22:01','2016-02-04 06:22:01',0,0,'',0,0),(7,0,'Kalendar akcii','','','Botanicka zahrada','2016-02-05 06:52:17','2016-02-05 06:52:17',0,0,'',0,0),(8,0,'Kaktusy a ostatní sukulenty','Cacti and other succulents','','Botanicka zahrada','2016-02-05 20:27:13','2016-02-05 20:27:13',0,0,'',16,0),(9,0,'Subtropický skleník','Subtropical greenhouse','','Botanicka zahrada','2016-02-06 18:06:58','2016-02-06 18:06:58',0,0,'',15,0),(10,0,'Tropický skleník','Tropical greenhouse','','Botanicka zahrada','2016-02-06 18:15:25','2016-02-06 18:15:25',0,0,'',14,0),(11,0,'Sředoevropská květena (haj)','Central European flora (Grove)','','Botanicka zahrada','2016-03-29 01:36:26','2016-03-29 01:36:26',0,0,'In this exposition most of the plants are native to deciduous forests from riparian to beech forests. Along the stream and pond, besides the sciophilous species of forest streames, we find other marsh and aquatic plants.<br>\r\n<br>\r\nThe dominant tree in the grove is the monumental Caucasian walnut (Pterocarya fraxinifolia), a tree native to the Caucasus which is relative to walnuts.<br>\r\n<br>\r\nDuring the year a significant change in spring and summer aspect occurs. The spring aspect consists mainly of geophytes, i. e. plants that have underground perennating organs – bulbs, rhizomes or tubers (e.g. common snowdrop –  Galanthus nivalis).<br>\r\n<br>\r\nThe summer aspect is poorer in flowering plants, similarly to natural forests. Therefore, plants between the stream and the greenhouse will probably catch our attention. They are represented especially by wetland species as, for example, one of the most massive sedges – great fen-sedge (Cladium mariscus).',1,0),(12,0,'Středoevropská květena (Vápnomilná vegetace)','Central European flora (Calcareous vegetation)','','Botanicka zahrada','2016-03-29 08:48:33','2016-03-29 08:48:33',0,0,'',3,0),(13,0,'Středoevropská květena (Rašeliniště)','Central European flora (Peat bog)','','Botanicka zahrada','2016-03-29 08:51:24','2016-03-29 08:51:24',0,0,'',4,0),(14,0,'Středoevropská květena (Písčina)','Central European flora (Sandbank)','','Botanicka zahrada','2016-04-05 00:00:23','2016-04-05 00:00:23',0,0,'',5,0),(15,0,'Vodní a bahenní rostliny','Aquatic and wetland plants','','Botanicka zahrada','2016-04-05 00:07:26','2016-04-05 00:07:26',0,0,'',6,0),(16,0,'Užitkové rostliny','Useful plants','','Botanicka zahrada','2016-04-05 00:08:26','2016-04-05 00:08:26',0,0,'',7,0),(17,0,'Systém rostlin','System of plant taxonomy','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',8,0),(18,0,'Vřesovcovité rostliny','Ericaceous plants','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',9,0),(19,0,'Sbírka jehličnanů','Collection of conifers','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',10,0),(20,0,'Subtropické rostliny (v létě)','Subtropical plants (in summer) ','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',11,0),(21,0,'Geologický park','Geological park','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',13,0),(22,0,'Odpočinkové refugium','','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',0,0),(23,0,'Sbírka dubů','Collection of oaks','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',12,0),(24,0,'Sbírka vilínů','','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',0,0),(25,0,'Sbírka růží','','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',0,0),(26,0,'Asijská a americká květena','','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',0,0),(27,0,'Hadcová skalka','','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',0,0),(28,0,'Středomořská skalka','Mediterranean rock garden','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',0,0),(29,0,'Velké alpinum','','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',0,0),(30,0,'Horní výstavní skleník','Upper exhibition greenhouse','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',0,0),(31,0,'Cenník','Pricing','','Botanicka zahrada','2016-02-04 06:22:01','2016-02-04 06:22:01',0,0,'',0,0),(32,0,'O aplikaci','About','','Botanicka zahrada','2016-04-05 00:09:02','2016-04-05 00:09:02',0,0,'',0,0);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_block`
--

DROP TABLE IF EXISTS `article_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_block` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) NOT NULL,
  `content` text NOT NULL,
  `content_en` text NOT NULL,
  `backgroud_color` varchar(10) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_block`
--

LOCK TABLES `article_block` WRITE;
/*!40000 ALTER TABLE `article_block` DISABLE KEYS */;
INSERT INTO `article_block` VALUES (1,1,'lorem ipsum 1','','FFFFFF',0),(2,1,'and here is second one...','','88FFFF',1),(3,2,'vanoce lorem 1','','FFFFFF',0),(4,3,'<b>Unor-březen</b><br>\r\nSkleníky: 10-16 h<br>\r\nExteriéry: 10-17 h<br>\r\n','','',10),(5,3,'<b>Duben-srpen</b><br>\r\nSkleníky: 0-17 h<br>\r\nExteriéry: 10-19,30 h<br>\r\n','','',20),(6,3,'<b>Září-říjen</b> (do změny času)<br>\r\nSkleníky: 10-17 h<br>\r\nExteriéry: 10-18 h<br>','','',30),(7,3,'<b>Listopad-leden</b><br>\r\nSkleníky: 10-15,30 h<br>\r\nExteriéry: 10-16 h<br>','','',40),(8,3,'Otevírací doba skleníků se může během výstav měnit.<br>\r\n<br>\r\nPokladna se uzavírá 30 minut před uzavřením skleníku.<br>\r\n<br>\r\nSkleníky i exteriéry jsou otevřené denně po celý rok.<br>\r\n<br>','','',50),(9,4,'Univerzitní zahrada byla založena před 230 lety, r. 1775 v Praze na Smíchově. Kolem r. 1840 se v ní pěstovalo téměř 13 000 druhů a odrůd domácích i cizokrajných rostlin.\r\n<br><br>\r\nPro časté záplavy byla zahrada přemístěna na pozemek odkoupený od „Společnosti pro zvelebování zahrad“, podporované mj. knížetem Rohanem. Na současném místě byla otevřena spolu s novými botanickými ústavy české i německé univerzity v roce 1898 (budovy Benátská 2 a Viničná 5).\r\n<br><br>\r\nSbírky rostlin ze Smíchova byly rozděleny mezi obě univerzity rovným dílem. Převzaty byly rovněž sbírky ze skleníků „Společnosti pro zvelebování zahad“; část z nich se dochovala dodnes, proto je stáří některých palem, cykasů a dalších rostlin odhadováno na více než 130 let.\r\n<br><br>\r\nPři náletu 14. února. 1945 byly silně poškozeny a později strženy skleníky německé univerzity. Po osvobození převzala obě části zahrady jako celek Univerzita Karlova. Do osmdesátých let minulého století byla zahrada formálně propojena s Katedrou botaniky, od té doby je samostatnou správní jednotkou.\r\n<br><br>','','FFFFFF',0),(10,5,'<b>Adresa</b><br>\r\n<br>\r\nBotanická zahrada Přírodovědecké fakulty UK v Praze<br>\r\n<br>\r\nNa Slupi 16<br>\r\n<br>\r\n128 01  Praha 2<br>\r\n<br>\r\nGPS: 50°4’15.328”N, 14°25’12.432”E<br>\r\n<br>\r\n<b>Dopravní spojení</b><br>\r\n<br>\r\nTramvaj: Linky 6, 18 a 24, zastávka “Botanická zahrada”<br>\r\n<br>\r\nMetro: Linka B, zastávka “Karlovo náměstí”<br>','','FFFFFF',0),(11,6,'<b>EXterieary</b><br>\r\n<br>\r\nV exteriérech botanické zahrady naleznete v každém ročním období mnoho pozoruhodných rostlin. Naši průvodci pro vás vyberou nejzajímavější rostliny, které lze v daném ročním období spatřit nebo přizpůsobí prohlídku vašim individuálním požadavkům. Prohlídka trvá přibližně hodinu.\r\n','','',0),(12,6,'<b>Skleníky</b><br>\r\n</br>\r\nPokud máte zájem zapojit při návštěvě skleníků i jiné smysly než zrak, objednejte si komentovanou prohlídku skleníků. Prohlídka trvá přibližně hodinu a seznámí vás s nejpoutavějšími rostlinami našich skleníků. Pro mateřské školy máme připraven speciální program přizpůsobený potřebám dětí, které ještě neumějí číst.\r\n','','',1),(13,6,'<b>Ceny komentovaných prohlídek</b><br>\r\n<br>\r\n<b>Exteriéry - školy</b>\r\n<ul>\r\n<li>35 Kč na žáka (zahrnuje vstupné do skleníků), pedagogický doprovod zdarma</li>\r\n<li>20 Kč na žáka (nezahrnuje vstupné do skleníků), pedagogický doprovod zdarma</li>\r\n</ul><br>\r\n<b>Exteriéry - ostatní</b><br>\r\n200 Kč na skupinu (max 25 osob)<br>\r\n<br>\r\n<b>Skleníky - školy<b><br>\r\n35 Kč na žáka (zahrnuje vstupné do skleníků), pedagogický doprovod zdarma<br>\r\n<br>\r\n<b>Skleníky - ostatní</b><br>\r\n200 Kč na skupinu (max 15 osob) + vstupné do skleníků','','',2),(14,6,'<b>Jak se objednat?</b><br>\r\n<br>\r\nProhlídky objednávejte na botazah@natur.cuni.cz (link sends e-mail). Do předmětu emailu napište “Komentovaná prohlídka - Exteriéry/Skleníky” a do textu uveďte (i) datum a čas prohlídky, (ii) počet osob a orientační věkové složení a (iii) kontaktní osobu s telefonním číslem.<br>\r\n<br>\r\nŠkoly mohou prohlídku objednat i přes projekt Přírodovědci.cz<br>\r\n<br>','','',3),(15,31,'Vstup do exteriérů je zdarma.<br>\r\n<br>\r\nVstupné do skleníků:<br>\r\n<ul>\r\n<li>Dospělí: 55 Kč</li>\r\n<li>Děti, studenti (do 26 let), senioři:	30 Kč</li>\r\n<li>Rodinné vstupné (2 dospělí, maximálně 3 děti): 130 Kč</li>\r\n<li>ZTP, děti do 5 let, pedagogický doprovod, studenti a zaměstnanci PřF UK: zdarma</li>\r\n</ul>\r\n<br>\r\nPozn: Na výstavy se vybírá zvláštní vstupné.','','',0),(16,7,'<b>Výstavy v roce 2016</b><br>\r\n<b>11. 1. – 14. 2.</b> Malířská inspirace botani(c)kou<br>\r\n<b>30. 4. – 22. 5.</b> Pohádkové krajinky se sukulenty<br>\r\n<b>18. 5.</b> Den fascinace rostlinami – objevy ze světa rostlin (10:00–17:00)<br>\r\n<b>28. 5. – 15. 6.</b> Kaktusy a sukulenty – jarní etapa<br>\r\n<b>11. 6.</b> Pražská muzejní noc<br>\r\n<b>10. 6. – 19. 6.</b> Masožravé rostliny','','',0),(17,8,'Sukulentní skleník je věnován velké trvalé expozici sukulentních rostlin, která je navíc neustále doplňována.  Botanická zahrada má v současnosti největší sbírku kaktusů ze všech botanických  zahrad v České republice.<br>\r\n<br>\r\nSbírka kaktusů (čeleď Cactaceae, od vchodu napravo) je ve výstavních vitrínách rozdělena podle kontinentů, kterým odpovídá i zvolená hornina, ve které jsou kaktusy umístěny. Ve volné expozici jsou nepřehlédnutelné například letité exempláře rodu Echinocactus, které jsou lidově snad téměř v každém jazyce nazývány “sedátko pro tchýni”.<br>\r\n<br>\r\nV levé části skleníku nalezneme ostatní sukulenty z čeledí aloovitých, agávovitých a mnoha dalších. Příkladem atraktivních a na podzim bohatě kvetoucích sukulentů mohou být zástupci rodu Lithops, tzv. živé kameny z čeledi kosmatcovitých. Zajímavou skupinou jsou také převislé kaktusy rostoucí v zadní vitríně – zástupci rodu Rhipsalis, kteří se přirozeně vyskytují v tropických lesích. Mají sice sukulentní stonky, ale nepotřebují trny bránící se okusu, neboť rostou jako epifity na dřevinách.<br>','','',0),(18,9,'Subtropický skleník slouží k přezimování rostlin umístěných v létě ve venkovní expozici naproti skleníkům. Přenosné rostliny jsou zde uspořádány geograficky. Největší kolekce představují rostliny Středozemí a rostliny australské oblasti. Kolekce byla nově obohacena o jehličnatou wolémii vznešenou (Wollemia nobilis) z čeledi blahočetovitých. Jehličnan známý z fosilního záznamu byl objeven teprve v roce 1994 pouhých 100 km od Sydney.<br>\r\n<br>\r\nV zimě zaujme svými růžovými květy kamélie (Camellia japonica) či bíle kvetoucí lípěnka africká (Sparmannia africana), která paří do stejné čeledi jako náš národní strom lípa (tj. Tiliaceae). Na parapetu do Benátské ulice kvetou většinu zimní sezóny rovněž středozemské hluchavkovité, zejména rozmarýn lékařský (Rosmarinus officinalis).','','FFFFFF',0),(20,10,'Vstupní část tropického skleníku je věnována zejména cykasům a částečně i palmám. Mezi cykasy dominují mohutné exempláře jihoasijského druhu Cycas circinalis a Cycas edentata, které svým stářím (více než 150 let) řadí mezi unikáty Botanické zahrady. Z palem dominuje Washingtonia filifera s mohutnými, dlanitě se trhajícími čepelemi listů.<br>\r\n<br>\r\nPři vstupu do hlavního prostoru je na pravé straně umístěna ilustrativní sbírka tropických kapradin. Pozoruhodné jsou například zavěšené parožnatky, které v přírodě rostou v korunách stromů, aby získaly dostatek světla.<br>\r\nNejvětší prostor je v další části skleníku věnován rostlinám tropického lesa, zejména Starého světa. Dominantami jsou mohutná palma uprostřed expozice s velkými květenstvími (Sabal sp.) a za ní exempláře pandánů (Pandanus sp.) s chůdovitými kořeny a vějířem ostnitých listů vyrůstajících na vrcholu kmene.\r\nV další části tropického skleníku se nachází jezírko s každoročně vysazovanou viktorií Cruzovou (Victoria cruziana) náležející k leknínovitým, která je proslulá svou schopností unést na svých obrosvských listech malé dítě. Návštěvníky jistě zaujmou také vzrostlé exempláře banánovníků – největších bylin na světě. ','','FFFFFF',0),(21,11,'Většina zde rostoucích rostlin je domácí v listnatých lesích od lužních lesů po bučiny. Podél potůčků a v rybníčku pak najdeme vedle stínomilných druhů lesních potůčků i další bahenní a vodní rostliny. <br>\r\n<br>\r\nDominantou Háje je mohutná stoletá lapina jasanolistá (Pterocarya fraxinifolia), strom původem z Kavkazu příbuzný ořešákům. <br>\r\n<br>\r\nBěhem roku dochází v expozici k výrazné obměně jarního a letního aspektu. Jarní aspekt je tvořen hlavně geofyty, tj. rostlinami, které mají podzemní přezimující orgány – cibule, oddenky nebo hlízy (např. sněženka podsněžník – Galanthus nivalis). <br>\r\n<br>\r\nLetní aspekt je podobně jako v lesích na kvetoucí rostliny chudší. Proto nás zaujmou spíše rostliny mezi potůčkem a skleníkem, které většinou reprezentují druhy mokřadních stanovišť  jako např. jedna z nejmohutnějších šáchorovitých rostlin mařice pilovitá (Cladium mariscus).\r\n','In this exposition most of the plants are native to deciduous forests from riparian to beech forests. Along the stream and pond, besides the sciophilous species of forest streames, we find other marsh and aquatic plants.<br>\r\n<br>\r\nThe dominant tree in the grove is the monumental Caucasian walnut (Pterocarya fraxinifolia), a tree native to the Caucasus which is relative to walnuts.<br>\r\n<br>\r\nDuring the year a significant change in spring and summer aspect occurs. The spring aspect consists mainly of geophytes, i. e. plants that have underground perennating organs – bulbs, rhizomes or tubers (e.g. common snowdrop –  Galanthus nivalis).<br>\r\n<br>\r\nThe summer aspect is poorer in flowering plants, similarly to natural forests. Therefore, plants between the stream and the greenhouse will probably catch our attention. They are represented especially by wetland species as, for example, one of the most massive sedges – great fen-sedge (Cladium mariscus).','FFFFFF',0),(22,12,'Mohutná skalka (tzv. Karlštejn), vytvořená při založení zahrady z 30 vagonů hluočepského vápence, je druhově nejbohatší expozicí vůbec. Hostí zejména druhy stepních a skalních stanovišť a na vyvýšenině pod nárožní bustou prof. Krajiny i druhy vysokohorské. Pouze podél potůčku najdeme ve střední a dolní části rostliny vlhkého bezlesí.<br>\r\n<br>\r\nNávštěvník se zde může seznámit i s druhy kriticky ohroženými (např. včelník rakouský – Drachocephalum austriacum) a s některými českými endemity (např. hvozdík moravský – Dianthus moravicus).<br>\r\n<br>\r\nVůdčími rostlinami jara jsou fialově kvetoucí koniklece. V létě převládá na mnoha místech žlutá barva bohatých populací rostlin, jako jsou hvězdnicovité omany (Inula) či len žlutý (Linum flavum). Na podzim můžeme např. na skalce vidět žlutě kvetoucí hvězdnici zlatovlásku (Aster linosyris).','','#FFFFFF',0),(23,13,'Rašeliniště jsou místa s nerozpustným podkladem a vodou, kde nedochází k dostatečnému rozkladu organické hmoty. Rašeliniště vrchovištní vzniká v prostředí chudém na živiny a je syceno zejména srážkovou vodou. Rašeliniště slatinné vzniká naopak v prostředí na živiny bohatém, kde se hrormadí voda s vysokým obsahem minerálů.<br>\r\n<br>\r\nZ vrchovištních druhů zde rostou vlochyně bahenní (Vaccinium uliginosum) nebo vzácná šicha černá (Empetrum nigrum). Slatiny reprezentuje např. popelivka sibiřská (Ligularia sibirica), kosatec sibiřský (Iris sibirica) nebo v Čechách téměř vyhynulá přeslička různobarvá (Equisetum variegatum). ','This exhibition area has plants mainly from peat bogs but also from fens. Examples from the fens are the leopard plant Ligularia sibirica, Iris sibirica or, virtually extinct in the Czech Republic, the horsetail Equisetum variegatum. Peat bog species grown here are the bilberry Vaccinium uliginosum, marsh cinquefoil Potentilla palustris and the rare crowberry Empetrum nigrum.','FFFFFF',0),(24,14,'Ekologicky vymezená expozice rostlin typických pro lehké písčité půdy obsahuje jak rostliny se širokým areálem tyto biotopy charakterizující (hasivka orličí – Pteridium aquilinum, vřes obecný – Calluna vulgaris), tak vzácné rostliny zejména panonských písčin: chvojník dvouklasý (Ephedra distachya), dále ostřici leskloplodou (Carex liparicarpos), zlatovous jižní (Chrysopogon gryllus) a řebříček bledožlutý (Achillea ochroleuca). <br>\r\n<br>\r\nSpíše atlantské druhy písčin pak reprezentují např. ostřice vřesovištní (Carex ericetorum) či různé druhy bělolistů (Filago).','This environmentally defined exhibition of plants typical of light sandy soil contains both plants widely seen in these habitats (bracken – Pteridium aquilinum, heather – Calluna vulgaris) and rare plants especially from the Pannonian sands: Ephedra distachya, also Carex liparicarpos, scented grass Chrysopogon gryllus, yarrow Achillea pectinata; Atlantic sandbar species are represented by, for example, the sedge Carex ericetorum or species of cottonweed from the Filago genus.','FFFFFF',0),(25,15,'Důraz je v této sbírce kladen na domácí květenu. Rostliny vesměs pocházejí z přírodních lokalit, jen výjimečně jsou zastoupeny druhy severoamerické nebo středozemské. Ve vaničkách na začátku expozice můžete vidět vodokapradiny (např. nepukalka vzplývavá – Salvinia natans) a okřehkovité rostliny.Kořenující vodní rostliny jsou v expozici reprezentovány zejména rostlinami leknínovitými. Druhou skupinou těchto vodních rostlin jsou rdesty. <br>\r\n<br>\r\nNejbohatším rodem převážně bahenních rostlin jsou ostřice (Carex) z čeledi šáchorovitých. Proto je jim věnováno několik kruhových nádrží, v nichž najdeme zejména zástupce vysokých ostřic. Další specifickou skupinou jsou rostliny obnažených den, které dokážou přežít v semenné bance na dně mnoho let, aby při poklesu vody a obnažení dna masově vyklíčily. <br>\r\n<br>\r\nK expozici je přidružena i mohutná jihoamerická rostlina barota rukávovitá (Gunnera manicata) ze zvláštní čeledi barotovitých (Gunneraceae), která nemá v botanickém systému blízké příbuzné. ','The display is based on a combination of ecological and taxonomical criteria. Primarily the plants are in separate pools (water plants) and bowls (swamp plants) arranged environmentally – eg. littoral plants, plants from exposed pond beds, moorland plants, etc. When appropriate they are collected into a single exhibition of taxonomically related species of sedges (Carex), rushes (Juncus) and cattails (Typha). \r\nThe most interesting domestic species include marsh angelica (Ostericum palustre), a small water lily (Nuphar pumila), the fringed Nymphoides peltata or the cattail Typha minima, and of species not normally growing in this country, the prominently flowering primrose willow Ludwigia peploides is of interest.','FFFFFF',0),(26,16,'Mezi přibližně stem druhů užitkových rostlin najdeme zejména zástupce  (i) běžných i exotičtějších zelenin (rajče, celer, artyčok atd.), (ii) aromatických bylinek (meduňka – Melissa officinalis, yzop – Hyssopus officinalis) a také (iii) léčivých rostlin z naší domácí květeny (vlaštovičník větší – Chelidonium majus). <br>\r\n<br>\r\nKromě toho jsou zde též zastoupeny některé domácí jedovaté rostliny, např. blín černý (Hyoscyamus niger). Většina rostlin kvete až v létě; první kvetoucí rostlinou je rovněž jedovatá předjarní horská čemeřice černá (Helleborus niger), která kvete bíle, ale jméno má po černavém oddenku. V pozdně letním aspektu je nápadná sbírka kvetoucích tabáků \r\n(Nicotiana). Expozice je každoročně zčásti obnovována výsadbami krátkověkých druhů.','These are located on the first terrace. Approximately 100 perennial, annual and biennial species are under cultivation. Emphasis is placed on three groups of plants: (a) examples of common and exotic vegetables (tomatoes, celery, artichoke), (b) aromatic plants, especially Mediterranean plants of the Lamiaceae family (e.g. balm, hyssop, peppermint, lavender) and Asteraceae (Cnicus benedictus, Silybum marianum), (c) examples of medicinal plants from our domestic flora (celandine, strawberries, agrimony, soapwort).<br>\r\n<br>\r\nMany of the aromatic plants are listed in the second category, but are also used medicinally. Some poisonous plants (henbane, Solomon’s seal) are also represented. The exhibition is partially renewed annually by the planting of short-lived species.','FFFFFF',0),(27,17,'Snahou nově vytvářené expozice je demonstrovat zařazení rostlin do systému rostlin podle výsledků nejnovějších výzkumů. Jako zástupci čeledí jsou vybírány druhy středozemské, asijské nebo americké, ale třeba i evropské vysokohorské s důrazem na to, aby byly zastoupeny druhy jiné než ty, které jsou pěstovány v ostatních, často ekologicky orientovaných expozicích. <br>\r\n<br>\r\nVětšina zařazených čeledí se vyskytuje i u nás, ale někdy  i  člověka  botanicky  vzdělaného  překvapí  různorodost v rámci čeledí. Příkladem může být nádherně kvetoucí žluťucha Thalictrum dipterocarpum s fialovým okvětím, které naše druhy postrádají nebo třezalka bobulovitá (Hypericum androsaemum) mající oproti našim třezalkám plody typu bobule místo tobolky.','In this exposition we aim to demonstrate the system of plant taxonomy according to the latest research. As representatives of individual families, Mediterranean, Asian,  American and alpine Europaean species have been chosen with focus on the species that are not included in other expositions. <br>\r\n<br>\r\nThe majority of families on display can be found in the Czech Republic as well, but sometimes even a botanist can be surprised by the variety of plants within a family. For example, our species of St. John\'s wort (Hypericum) have capsules, but fruit of Eurasian tutsan (H. androsaemum) resembles rather berries for which tutsans are used to variegate bunches of flowers.','FFFFFF',0),(28,18,'Soubor  vřesovcovitých  rostlin  vyžadujících  kyselou  rašelinnou půdu je doplněn dalšími rostlinami s podobnými ekologickými nároky. Z čeledi vřesovcovitých zde nalezneme i u nás se vyskytující druhy, jako rojovník bahenní (Ledum palustre) a kyhanku sivolistou (Andromeda polifolia) nebo exotičtější zástupce – severoamerický druh libavku poléhavou (Gaultheria procumbens) či klikvu velkoplodou (Oxycoccus macrocarpus). <br>\r\n<br>\r\nDruhou skupinou jsou naše běžné kapradiny – kapraď samec (Dryopteris filix-mas), papratka samice (Athyrium filix-femina) a hned na okraji vzácný vápnomilný jazyk celolistý (Phyllitis scolopendrium). Novým přírůstkem je několik trsů podezřeně královské (Osmunda regalis). Dřevinnou dominantou expozice je borovice kleč (Pinus mugo). ','The ericaceous plant group requiring acidic peaty soil is complemented by other plants with similar ecological requirements. From the Ericaceae family we can find species seen in the Czech Republic such as Ledum palustre, Andromeda polifolia, Oxycoccus palustris, or more exotic representatives – the North American species of wintergreen Gaultheria procumbens or the Atlantic species Daboecia cantabrica. The second group is our common fern – male and female ferns, common ferns. The dominant tree in the exhibition is the dwarf pine (Pinus mugo).','FFFFFF',0),(29,19,'Dominantou expozice je mohutný exemplář tisovce dvouřadého (Taxodium distichum), v popředí u cestičky pak keřovitý kulotis peckovitý (Cephalotaxus harringtonii). Druhové bohatství tvoří zejména jedle, cypřišky a zeravy. \r\n \r\nV expozici je též venku pěstovatelný chilský blahočet (Araucaria araucana) a zatím menší stromky druhů zpravidla dorůstajících velkých výšek, zejména sekvojovec obrovský (Sequoiadendron giganteum). V létě na tuto sbírku organicky navazuje kolekce subtropických jehličnanů, přezimovaná ve studeném subtropickém skleníku. ','The dominant feature is a huge specimen of a bald cyprus (Taxodium distichum), in the foreground by the path is the shrubby cow’s tail pine (Cephalotaxus harringtonii). There are many species of fir (e.g. Abies concolor, A. koreana), false cypresses (Chamaecyparis nootkatensis, Ch. pisifera) and thuja (Thuja occidentalis, T. plicata). The monkey puzzle tree (Araucaria) is grown outdoors in this area, acting as winter cover, and the Himalayan cedar (Cedrus deodara) has been recently planted. In summer, this collection is augmented by the subtropical coniferous collection, which overwinters in the subtropical cold greenhouse.','FFFFFF',0),(30,20,'V květnu se ze subtropického skleníku většina rostlin přesunuje do této venkovní expozice. Rostliny jsou uspořádány geograficky podle kontinentů. Napravo od vstupních schůdků jsou umístěny rostliny Nového Zélandu a Austrálie, jako například jehličnan damaroň jižní (Agathis australis) s netypicky širokými listy. Za nimi následují rostliny Madeiry a Kanárských ostrovů a Evropy se vzrostlým rohovníkem obecným (Ceratonia siliqua), jehož plody jsou známé pod označením svatojánský chléb nebo karob. V zadní části jsou shromážděny asijské rostliny (např. kamélie – Camellia).\r\n\r\nStrana nalevo od schůdků je věnována nejprve americkým (např. sekvoj vždyzelená – Sequoia sempervirens) a africkým rostlinám. Nejdále je pak umístěna druhá část asijských rostlin s některými subtropickými borovicemi (Pinus) či vzácným druhem ostrolistcem kopinatým (Cunninghamia lanceolata). ','','FFFFFF',0),(31,21,'Výstava je tvořena celkem 54 vzorky magmatických (vyvřelých), sedimentárních (usazených) a metamorfovaných (přeměněných) hornin. Expozici otvírá vlevo před výukovými tabulemi bezmála dvoutunový monolit, který symbolizuje nejvýznamnější \r\ngeologickou událost na našem území – tzv. variskou orogenezi. Tato událost proběhla v časovém intervalu před 380–300 miliony let a po jejím konci se na našem území tyčilo horstvo o velikosti Alp či Himálají. <br>\r\n<br>\r\nExponáty jsou tematicky rozděleny do pěti sekcí podle nejvýznamnějších geologických jednotek na území ČR – saxothuringika, bohemika, moldanubika, moravosilezika a soubor sedimentárních a vulkanických (sopečných) hornin, které byly vytvořeny v mladé geologické minulosti po skončení variské orogeneze v období 300 milionů let po současnost.','','FFFFFF',0),(32,22,'Prostor získala zahrada symbolickým pronájmem od městské části Praha 2 jako dlouhodobě opuštěnou a zarostlou zahradu. Byl upraven jako izolovaná a tichá část zahrady, kde je možno posedět a kde se též pořádají různé kulturní akce. Proto je úprava parková a pěstované dřeviny i byliny nejsou označeny. Sochy, které tvoří výzdobu, i kašna v dolní části, byly objeveny při odklízení skládky. ','','FFFFFF',0),(33,23,'Dubů je v zahradě větší počet druhů od obyčejných po vzácné. Na nejhořejší terase zahrady jsou soustředěny převážně severoamerické druhy. Za zmínku stojí dub velkoplodý (Quercus macrocarpa), který má nejen ohromné žaludy s brvitými číškami, ale proti našim druhům i dvojnásobné listy. Z dalších význačných severoamerických druhů můžeme jmenovat například dub sametový (Q. velutina) s plstnatými mladými listy a dub celokrajný (Q. imbricaria), jehož listy mají celistvé okraje. \r\n\r\nZ původních evropských druhů na terase vyniká ztepilý dub pýřitý neboli šípák (Q. pubescens), rostoucí u schodů do fakultní budovy Viničná 5 (vedle skleníku). Šípák je naše xerotermní dřevina, zasahující k nám ze Středozemí, ale v přírodě roste obvykle jako nízký křivolaký strom. ','','FFFFFF',0),(34,24,'Pravděpodobně  nejčasnějšími  kvetoucími  krytosemennými dřevinami jsou zástupci čeledi vilínovitých (Hamamelidaceae), zejména rodu vilín, Vilíny kvetou neolistěné, v předjaří, v mírných zimách někdy již v prosinci. U altánku najdeme severoamerické druhy vilín viržinský (Hamamelis virginiana), kvetoucí často ještě v prosinci, v. prostřední (H. × intermedia) a v. jarní (H. vernalis). Z čeledi vilínovitých kvete ještě v předjaří žlutými květenstvími lískovníček klasnatý (Corylopsis spicata) z Japonska a Tchaj-wanu, později parocie perská (Parrotia persica) s nenápadnými temně červenými květy, pocházející ze Zakavkazska a Íránu.\r\n','','FFFFFF',0),(35,25,'Záhonová expozice v trávníku podél budovy Benátská 2 soustřeďuje velkokvěté růže tzv. „čajohybridy“, větvené „polyantky“, u nichž je výchozím druhem asijský druh Rosa multiflora a tzv. „floribundy“, pěstované růže vzniklé hybridizaci čajových růží a „polyantek“, v různých barvách. Drobný soubor je ale vlastně největší expozicí pěstovaných rostlin v zahradě.','','FFFFFF',0),(36,26,'Zadní část zahrady s jezírkem v centru přiléhá k budově Benátská 4. Vstup „střeží“ při cestičce vřesovcovitý keř pieris japonská (Pieris japonica), kvetoucí na jaře množstvím drobných baňkovitých květů. <br>\r\n<br>\r\nTěžištěm je volná výsadba jehličnanů; ze vzácnějších druhů jinde v zahradě nepěstovaných např. jedle korejská (Abies koreana) a  borovice kurilská (Pinus koraiensis). Podél cesty jsou pěstovány mohutnější stínomilné byliny, ze vzácnějších východoasijská Rodgersia aesculifolia  z čeledi lomikamenovitých nebo vlčímu bobu podobná severoamerická Baptisia australis. <br>\r\n<br>\r\nU jezírka roste nápadný žláznatý žlutě kvetoucí krtičník zlatožlutý (Scrophularia chrysantha), pocházející z Kavkazu a často pěstovaný jako medonosná rostlina. ','','FFFFFF',0),(37,27,'Hadcová skalka byla původně zamýšlena jako expozice rostlin, které se vyskytují na hadci (serpentinitu), v podstatě toxické hornině, na níž mnohé druhy rostlin nemohou růst a ty, které mohou, tvoří často nízké formy, tzv. nanismy. <br>\r\n<br>\r\nOpravdovým hadcovým specialistou je zde kapradina sleziník hadcový (Asplenium cuneifolium), která je na malých hadcových ostrůvcích často jediným indikátorem této horniny. Rostou zde i druhy, které hadec oblibují, ale mohou růst i na jiných výhřevných horninách – vřesovec pleťový (Erica carnea), trávnička obecná (Armeria vulgaris) či penízek horský (Thlaspi montanum).','','FFFFFF',0),(38,28,'Na rozdíl od středoevropských expozic není tato expozice vedena zcela systematicky, neboť přežívání středozemských druhů je značně závislé na výkyvech počasí. Přesto se zde po celý rok setkáváme s nápadně kvetoucími a typickými středozemskými druhy levandulí lékařskou (Lavandula angustifolia), paznehtníkem ostnitým (Acanthus spinosus) nebo zástupcem typicky mediteránní  čeledi  cistovitých  cistem  vavřínolistým  (Cistus  laurifolius). Zajímavý je též bohatý porost moltkie modré (Moltkia caerulea). <br>\r\n<br>\r\nNa horním konci je skalka ohraničena pěkným keřem ruje vlasaté (Cotinus coggygria). V jejím podrostu najdeme typickou středozemskou plazivou liánu brčál větší (Vinca major), kvetoucí po celé léto.','All year round the visitor can come across striking flowering and typical Euro-Mediterranean species, such as rue (Ruta graveolens), lavender thyme (Lavandula angustifolia), spiny bear’s breeches (Acanthus spinosus), a prominent representative of the typical Mediterranean cistaceae family, poplar-leaved cistus (Cistus populifolius) and monocots such as the bulbous asphodel Asphodeline lutea. \r\n','FFFFFF',0),(39,29,'Alpinum bylo v sedmdesátých letech 20. století opuštěno a zarůstalo dřevinami. Teprve počátkem devadesátých let byl proveden zásah a většina náletových dřevin byla pokácena. Úmysl vybudovat zde opravdu alpinum se zaměřením na alpské a karpatské alpinské rostliny byl ale pro nedostatek finančních prostředků odsunut. Do budoucna se nicméně plánuje alpinum profilovat jako kolekci asijských horských rostlin.<br>\r\n<br>\r\nV současnosti je zde vysazeno větší množství druhů a kultivarů okrasných trav a běžně pěstované (komerčně prodávané) skalničkové trvalky (např. hledíky – Antirrhinum, hvozdíky – Dianthus apod.). <br>\r\n<br>\r\nPlocha je zčásti posázena jehličnatými keři, zejména borovicí klečí (Pinus mugo) a jalovci –  jednak ze skupiny chvojky klášterské (Juniperus sabina) se šupinovitými listy,  jednak netypickými rozprostřenými kultivary jalovce obecného (Juniperus communis) s jehlicemi v trojčetných přeslenech.','','FFFFFF',0),(40,30,'Horní výstavní skleník je určen pro konání výstav v průběhu téměř celého roku. Mezi již tradiční výstavy, které se ve skleníku pořádají, patří výstava masožravých rostlin, sukulentů, andulek, citrusů, orchidejí a vánoční výstava. Na jaře pak skleníky slouží k výsevu a nakličování semen pro obnovování a doplňování sbírek Botanické zahrady. ','','FFFFFF',0),(41,32,'OBR_LOGO_HERE_LATER<br>\r\n<br>\r\nAplikace <b>Botanicka zahrada UK Praha</b>\r\n<br><br>\r\nVerze: 0.9\r\n<br>\r\n<br>\r\nAutori textov:<br>\r\nMgr. Magdalena Hrdinová<br>\r\n...<br>\r\n<br>\r\n<br>\r\nAutori fotografii:<br>\r\nBill Clinton<br>\r\nFredy Kruger<br>\r\n....<br>\r\n<br>\r\n<br>\r\nAutori aplikacie:<br>\r\nRoaming Owl Studio\r\n...\r\n','fcksdhfkjsahf akhfas','',0);
/*!40000 ALTER TABLE `article_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_gallery_image`
--

DROP TABLE IF EXISTS `article_gallery_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_gallery_image` (
  `article_id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_gallery_image`
--

LOCK TABLES `article_gallery_image` WRITE;
/*!40000 ALTER TABLE `article_gallery_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_gallery_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_source`
--

DROP TABLE IF EXISTS `article_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_source` (
  `article_id` bigint(20) NOT NULL,
  `source_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_source`
--

LOCK TABLES `article_source` WRITE;
/*!40000 ALTER TABLE `article_source` DISABLE KEYS */;
INSERT INTO `article_source` VALUES (1,1),(1,2),(1,3);
/*!40000 ALTER TABLE `article_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bundle_id` bigint(20) DEFAULT NULL COMMENT 'conects multiple images together',
  `name` varchar(256) NOT NULL,
  `width` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `type` varchar(4) NOT NULL,
  `size` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (1,0,'orchudea.png',900,600,'png',55368),(2,0,'flowers.png',709,454,'png',764004),(3,0,'prague1.png',699,364,'png',584594),(4,NULL,'tag_video.png',64,64,'png',123456),(5,NULL,'tag_exhibition.png',64,64,'png',12345),(6,NULL,'tag_tree.png',64,64,'png',123456),(7,NULL,'quercus_robur.png',1024,1024,'png',234567);
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `information_source`
--

DROP TABLE IF EXISTS `information_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL,
  `name` varchar(1024) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information_source`
--

LOCK TABLES `information_source` WRITE;
/*!40000 ALTER TABLE `information_source` DISABLE KEYS */;
INSERT INTO `information_source` VALUES (1,1,'wiki','https://www.google.sk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjVmt_S6vfJAhWLjiwKHfN_CeoQFggdMAA&url=https%3A%2F%2Fsk.wikipedia.org%2Fwiki%2FDub_letn%25C3%25BD&usg=AFQjCNHo1Ti_FIS7kJW-TI_-693wTCIS6Q&sig2=hBUu7eCoSG9t9VqKBVJjvA'),(2,2,'nahuby.sk','http://www.nahuby.sk/atlas-rastlin/Quercus-robur/dub-letny/dub-letni/ID7415'),(3,3,'darius.cz','http://www.darius.cz/ekolog/mater_drevo.html');
/*!40000 ALTER TABLE `information_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_changed` datetime NOT NULL,
  `author` varchar(512) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `map_lat` double NOT NULL,
  `map_lon` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,'000xls3433','2015-12-27 18:12:02','2015-12-27 18:12:02','Botanicka zahrada',3,7,0,0);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_map_marker`
--

DROP TABLE IF EXISTS `item_map_marker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_map_marker` (
  `item_id` bigint(20) NOT NULL,
  `map_marker_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_map_marker`
--

LOCK TABLES `item_map_marker` WRITE;
/*!40000 ALTER TABLE `item_map_marker` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_map_marker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_names`
--

DROP TABLE IF EXISTS `item_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_names` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `language_code` varchar(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `item_names_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_names`
--

LOCK TABLES `item_names` WRITE;
/*!40000 ALTER TABLE `item_names` DISABLE KEYS */;
INSERT INTO `item_names` VALUES (1,1,'Dub letny','svk'),(2,1,'Dub letni','cze'),(3,1,'Quercus robur','lat'),(4,1,'English oak','eng');
/*!40000 ALTER TABLE `item_names` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_text_part`
--

DROP TABLE IF EXISTS `item_text_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_text_part` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `order` int(11) NOT NULL,
  `background_color` varchar(9) NOT NULL,
  `title` varchar(256) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_text_part`
--

LOCK TABLES `item_text_part` WRITE;
/*!40000 ALTER TABLE `item_text_part` DISABLE KEYS */;
INSERT INTO `item_text_part` VALUES (1,1,0,'#ffffff','Fakta','blooming: apr-may\r\nliving for: 80-120yr\r\nleafs:  Listy obvejčité, nepravidelně peřenolaločnaté, na bázi srdčitě ouškaté.\r\nfruits: Plodem je žalud (jednosemenná nažka) sedící v číšce. Stopka je 3–7 cm dlouhá.\r\nflowers:  Květy jsou jednopohlavné, samčí květenství má charakter jehněd na loňských větévkách, samičí jehnědy rostou na letorostech.\r\nheight: 45m\r\noccurence/origin: Přirozeně se vyskytuje v Evropě a Malé Asii, na Kavkaze a v některých lokalitách severní Afriky.'),(2,1,1,'#ffff99','Vedeli ste ze...','Kôra sa používa v humánnom i veterinárnom lekárstve ako prudko zvieravý a krvácanie utišujúci prostriedok, aj na omrzliny a proti poteniu nôh.\r\nDubienky - Sú to guľaté nádory, čiže patologické novotvary spôsobené hmyzom. Vyvolávajú ich hrčiarky.\r\n'),(3,1,2,'#ff99ff','Dalsie nazvy','lorem ipsum'),(4,1,3,'#99ffff','Podobne/Pribuzne druhy','poddruhy: Dub letný slavónsky (Quercus robur ssp. slavonica)  Vyskytuje sa v Chorvátsko – Srbskej oblasti Slavónsko.\r\ndub šedozelený (Quercus pedunculiflora)'),(5,1,3,'#ffffff','Autori a ddroje','Autori: bla bla\r\n\r\nZdroje:\r\nlorem ipsum');
/*!40000 ALTER TABLE `item_text_part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `map_marker`
--

DROP TABLE IF EXISTS `map_marker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map_marker` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `map_overlay_id` bigint(20) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `contents` varchar(1024) NOT NULL,
  `marker_visible` tinyint(4) NOT NULL DEFAULT '1',
  `tag_id` bigint(20) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `article_id` bigint(20) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `map_marker`
--

LOCK TABLES `map_marker` WRITE;
/*!40000 ALTER TABLE `map_marker` DISABLE KEYS */;
INSERT INTO `map_marker` VALUES (1,1,'Central European flora (Grove)','',1,NULL,'map_marker_leaf.png',NULL,50.0709886574673,14.420518912375),(2,1,'Maidenhair tree ( Ginkgo biloba \'Praga\')','',1,NULL,'map_marker_tree.png',NULL,50.0710545064673,14.4201363623142),(3,1,'Central European flora (Calcareous vegetation)','',1,NULL,'map_marker_leaf.png',NULL,50.0711455328772,14.4208216667175),(4,1,'Central European flora (Peat bog)','50.07161228260516',1,NULL,'map_marker_leaf.png',NULL,50.0716122826052,14.4215559214354),(5,1,'Central European flora (Sandbank)','',1,NULL,'map_marker_leaf.png',NULL,50.0717304218574,14.4215904548764),(6,1,'Aquatic and wetland plants','',1,NULL,'map_marker_wetland.png',NULL,50.0707758311594,14.4216913729906),(7,1,'Useful plants','',1,NULL,'map_marker_leaf.png',NULL,50.071052784926,14.4215988367796),(8,1,'System of plant taxonomy','50.070540623635516',1,NULL,'map_marker_leaf.png',0,50.0705406236355,14.4210918992758),(9,1,'Ericaceous plants','',1,NULL,'map_marker_leaf.png',NULL,50.0712968127933,14.4218043610454),(10,1,'Collection of conifers','',1,NULL,'map_marker_leaf.png',NULL,50.0708767569985,14.4208672642708),(11,1,'Subtropical plants (in summer) and Mediterranean rock garden','',1,NULL,'map_marker_leaf.png',NULL,50.0707900339556,14.4204293936491),(12,1,'Collection of oaks','',1,NULL,'map_marker_leaf.png',NULL,50.0708888078308,14.4220293313265),(13,1,'Geological park','',1,NULL,'map_marker_fossils.png',NULL,50.0715660166985,14.4222603365779),(14,1,'Tropical greenhouse','',1,NULL,'map_marker_leaf.png',10,50.071194166302,14.4202161580324),(15,1,'Subtropical greenhouse (exhibition greenhouse in summer)','',1,NULL,'map_marker_leaf.png',9,50.071007379251,14.4196368008852),(16,1,'Cacti and other succulents','',1,NULL,'map_marker_leaf.png',10,50.070938732675,14.4199020043015),(17,1,'Stock greenhouses (not accessible to the public)','',1,NULL,'map_marker_not_accessible.png',NULL,50.0712053562865,14.4204941019416),(18,1,'Upper exhibition greenhouse','',1,NULL,'map_marker_leaf.png',NULL,50.0712961672186,14.4203861430287),(19,1,'Stock sector (not accessible to the public)','',1,NULL,'map_marker_not_accessible.png',NULL,50.0719901550369,14.4223579019308),(20,1,'Department of Botany and Institute for Environmental Studies','',1,NULL,'map_marker_office_building.png',NULL,50.0712354831551,14.4211703538895),(21,1,'Central office of the Botanical Garden and Office for Studies of the Faculty of Science','',1,NULL,'map_marker_office_building.png',NULL,50.0705427755851,14.419763199985),(22,1,'Toilets','',1,NULL,'map_marker_toilets.png',NULL,50.0704145192181,14.4197625294328),(23,1,'Entrance 1','',1,NULL,'map_marker_entrance.png',NULL,50.0705862449472,14.4195127487183),(24,1,'Entrance 2','',1,NULL,'map_marker_entrance.png',NULL,50.070738602598,14.4194182008505),(25,1,'Entrance 3','',1,NULL,'map_marker_entrance.png',NULL,50.0715234089868,14.4207150489092),(26,1,'Entrance 4','',1,NULL,'map_marker_entrance.png',NULL,14.4209007918835,50.0715853838277),(27,1,'Entrance 5','',1,NULL,'map_marker_entrance.png',NULL,50.0723559740306,14.4229067489505),(28,1,'Entrance 6','',1,NULL,'map_marker_entrance.png',NULL,50.0713904210372,14.422828964889),(29,1,'Stock greenhouses (not accessible to the public)','',1,NULL,'map_marker_not_accessible.png',NULL,50.070884288769,14.4197471067309),(30,1,'Aquatic and wetland plants','',1,NULL,'map_marker_wetland.png',NULL,50.0705580544248,14.4213892892003),(31,NULL,'Botanical garden','',1,NULL,'map_marker_leaf.png',NULL,50.0708767569985,14.4208672642708);
/*!40000 ALTER TABLE `map_marker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `map_overlay`
--

DROP TABLE IF EXISTS `map_overlay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map_overlay` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `map_overlay`
--

LOCK TABLES `map_overlay` WRITE;
/*!40000 ALTER TABLE `map_overlay` DISABLE KEYS */;
INSERT INTO `map_overlay` VALUES (1,'General map','general_map');
/*!40000 ALTER TABLE `map_overlay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_item`
--

DROP TABLE IF EXISTS `news_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `tag_id` bigint(20) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'big image, small image, no image',
  `image_id` bigint(20) DEFAULT NULL,
  `description` varchar(1024) NOT NULL,
  `external_url` varchar(1024) DEFAULT NULL,
  `article_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `intent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_item`
--

LOCK TABLES `news_item` WRITE;
/*!40000 ALTER TABLE `news_item` DISABLE KEYS */;
INSERT INTO `news_item` VALUES (1,'Vystava orchideji','2016-01-03 14:55:19','2016-01-03 14:55:19',1,1,1,'Pridite sa pozriet na vystavu orchideji',NULL,1,NULL,NULL),(2,'Vánoční výstava','2016-01-07 04:50:39','2016-01-07 04:50:39',1,2,2,'Přijďte do Botanické zahrady navštivit vánoční ráj pohádek',NULL,NULL,NULL,NULL),(3,'Zo vzduchu!','2016-01-03 14:38:14','2016-01-03 14:38:14',2,1,3,'Proleťte se s námi nad stromy!','https://www.facebook.com/photo.php?v=10152596395509809',NULL,NULL,NULL),(4,'Seznam vystav na rok 2016','2016-01-07 05:03:55','2016-01-07 05:03:55',1,0,NULL,'Aktualizovali jsme seznam vystav v rokce 2016',NULL,3,NULL,'');
/*!40000 ALTER TABLE `news_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_index`
--

DROP TABLE IF EXISTS `search_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_index` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) NOT NULL COMMENT 'copied from item/article',
  `code` varchar(10) NOT NULL COMMENT 'copied from item/article',
  `article_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `tag_id` bigint(20) NOT NULL COMMENT 'copied from item/article',
  `date_created` datetime NOT NULL COMMENT 'copied from item/article',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_index`
--

LOCK TABLES `search_index` WRITE;
/*!40000 ALTER TABLE `search_index` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'vystava',5),(2,'video',4),(3,'tree',6);
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-08 15:48:22
