'use strict';
angular.module('adminApp').directive('menuEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'menuEdit/menuEdit.template.html',
		controller: 'menuEditController',
		scope: {
			menu: '@'
		}
	};
});