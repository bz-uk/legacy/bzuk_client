'use strict';
const DataTypes = require('sequelize/lib/data-types');
module.exports = class SequelizeBuilder {
    static build(sequelize, definitionName, definition) {
        let fieldsDef = {};
        Object.keys(definition.fields).map((fieldName) => {
            let fieldDefinition = definition.fields[fieldName];
            fieldsDef[fieldName] = fieldDefinition;
            if (fieldsDef[fieldName].type === 'INTEGER') {
                fieldsDef[fieldName].type = DataTypes.INTEGER;
            } else if (fieldsDef[fieldName].type === 'TEXT') {
                fieldsDef[fieldName].type = DataTypes.TEXT;
            } else if (fieldsDef[fieldName].type === 'BOOLEAN') {
                fieldsDef[fieldName].type = DataTypes.BOOLEAN;
            } else if (fieldsDef[fieldName].type === 'DATE') {
                fieldsDef[fieldName].type = DataTypes.DATE;
            } else if (fieldsDef[fieldName].type === 'STRING' && !fieldsDef[fieldName].maxSize) {
                fieldsDef[fieldName].type = DataTypes.STRING;
            } else if (fieldsDef[fieldName].type === 'STRING' && fieldsDef[fieldName].maxSize > 0) {
                fieldsDef[fieldName].type = DataTypes.STRING(fieldsDef[fieldName].maxSize);
            } else {
                throw new Error(`Unknown field type ${fieldsDef[fieldName].type} in model definition ${definitionName}`);
            }
        })
        return sequelize.define(definitionName, fieldsDef, {
            tableName: definition.tableName,
            timestamps: definition.timestamps,
            paranoid: definition.paranoid,
            underscored: true
        });
    }
}