'use strict';
const Promise = require('bluebird');
const BaseRoute = require('./../../common/route');
const passport = require('passport');
const debug = require('debug')('login-route');
const User = require('./../../appModels/user');
const UserRolePermission = require('./../../appModels/compound/userRolePermission');
const Permissions = require('./../../models/app/permissions');
const UnauthorizedError = require('./../../error/unauthorized');

module.exports = class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'post',
            path: '/login',
            name: 'login'
        }
    }

    main(req, res) {
        debug('login ', req.body);
        return new Promise((resolve, reject) => {
            debug('Authenticating');
            if (req.session.user) {
                User.loadById(req.session.user.id)
                .then((user) => {
                    this.context.user = user;
                    return resolve();
                }).catch(function (err) {
                    return reject(err);
                })
            } else {
                passport.authenticate('local', (err, userObject, info) => {
                    if (err) {
                        debug('user search error!', err);
                        return reject(err);
                    }
                    if (!userObject) {
                        debug('no user found');
                        return reject(new Error('no user'));
                    }
                    debug('User instance found');
                    req.logIn(userObject, (err) => {
                        if (err) {
                            debug('Login error', err);
                            return reject(Error('no user'));
                        }
                        //let Compound = new UserRolePermission(userObject);
                        UserRolePermission.fetchFor(userObject)
                        .then((compound) => {
                            if (!Permissions.has(compound.permissions, 'can_login')) { //TODO - ask user this.
                                return reject(new UnauthorizedError());
                            }
                            this.context.user = compound.user;
                            req.session.user = compound.user;
                            this.context.role = compound.role;
                            req.session.role = compound.role;
                            this.context.permissions = compound.permissions;
                            req.session.permissions = compound.permissions;
                            res.json({success: true});
                            return resolve();
                        })
                    });
                })(req, res);
            }
        });
    }
}