<?php
namespace botgarApi\circuit;

use Slim\Slim;

class SaveOneSecondaryPoint {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message) {
      $response = ['status'=>'error', 'message'=> $message];
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run($circuitId, $pointId, $secondaryPointId) {
        $json = $this->app->request->getBody();
        $data = json_decode($json);

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT * FROM circuit where id=" . $circuitId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            $this->app->response->setBody(['error' => 'unable to load circuit']);
            return;
        }
        $circuit = $result->fetch(\PDO::FETCH_ASSOC);

        $db->beginTransaction();
        //
        $active = 0;
        if ($data->active === true) {
          $active = 1;
        }
        $sql = "UPDATE circuit_secondary_points SET active=".$active.' WHERE circuit_id='.$circuitId.' AND primary_point_id='.$pointId.' AND plant_id='.$secondaryPointId;

        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            //$err = var_export($db->errorInfo(), true);
            $this->app->response->setBody(json_encode(['error' => 'unable to update secondary point', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }

        $db->commit();

        $response = json_encode(['status' => 'ok', 'message' => 'point saved']);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
