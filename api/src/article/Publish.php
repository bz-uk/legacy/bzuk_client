<?php
namespace botgarApi\article;

use Slim\Slim;

class Publish
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message)
    {
        $response = ['status' => 'error', 'message' => $message];
        $this->app->response->setStatus(400);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }

    public function run()
    {
        $json = $this->app->request->getBody();
        $data = json_decode($json);

        //validation
        if (!isset($data->ids)) {
            return $this->returnInvalidResponse('No articles specified');
        }

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $db->beginTransaction();

        foreach ($data->ids as $articleId => $published) {
            if ($published === true) {
                $published = 1;
            } else {
                $published = 0;
            }
            $sql = 'UPDATE article SET published='.$published.' where id=' . $articleId;
            $result = $db->query($sql);
            if (!$result) {
                return $this->returnInvalidResponse('Unable to find article: ' . $sql);
            }
        }
        $db->commit();
        $response = ['status' => 'ok', 'message' => 'article published'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
