<?php
namespace botgarApi\article;

use botgarApi\image\ImageModel;
use Slim\Slim;

class Put {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($articleId) {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);
    	if (!isset($data->title) || $data->title == "") {
    		$response = ['status'=>'error', 'message'=> 'Title can not be empty'];
    		$this->app->response->setStatus(400);
    		$response = json_encode($response);
    		$this->app->response->header('Content-Length', strlen($response));
    		$this->app->response->setBody($response);
    		return;
    	}
    	if (!isset($data->blocks[0]->content) || $data->blocks[0]->content == "") {
    		$response = ['status'=>'error', 'message'=> 'Content can not be empty'];
    		$this->app->response->setStatus(400);
    		$response = json_encode($response);
    		$this->app->response->header('Content-Length', strlen($response));
    		$this->app->response->setBody($response);
    		return;
    	}

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $db->beginTransaction();
        //images
        $image_id = 'null';
        if (isset($data->image) && $data->image != null && $data->image->id > 0) {
          $image_id = $data->image->id;
        }
        $sql = "UPDATE article SET title=".$db->quote($data->title).", image_id=".$image_id.", title_en=".$db->quote($data->title_en)." where id=" . $articleId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            $response = ['status'=>'error', 'message'=> 'Problem during saving article'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }

        $sql = "UPDATE article_block SET content=".$db->quote($data->blocks[0]->content).", content_en=".$db->quote($data->blocks[0]->content_en).", content_plain=".$db->quote(strip_tags($data->blocks[0]->content)).", content_en_plain=".$db->quote(strip_tags($data->blocks[0]->content_en))." where article_id=" . $articleId . ' AND type=0 AND `order`=0';
        error_log(var_export($sql, true));
        $result = $db->query($sql);
        if (!$result) {
        	$db->rollback();
        	$response = ['status'=>'error', 'message'=> 'Problem during saving article: ' . var_export($db->errorInfo(), true)];
        	$this->app->response->setStatus(400);
        	$response = json_encode($response);
        	$this->app->response->header('Content-Length', strlen($response));
        	$this->app->response->setBody($response);
        	return;
        }
        $db->commit();
        $response = ['status'=> 'ok', 'message' => 'Article saved'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
