<?php
namespace botgarApi\menu;

use Slim\Slim;

class PutItem {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($menuId, $menuItemId) {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);
    	if (!isset($data->title) || $data->title == "") {
    		$response = ['status'=>'error', 'message'=> 'Title can not be empty'];
    		$this->app->response->setStatus(400);
    		$response = json_encode($response);
    		$this->app->response->header('Content-Length', strlen($response));
    		$this->app->response->setBody($response);
    		return;
    	}

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        if (isset($data->published) && ($data->published === true || $data->published === false)) {
            $data->published = $data->published?1:0;
        }
        if (isset($data->show_subtitle) && ($data->show_subtitle === true || $data->show_subtitle === false)) {
            $data->show_subtitle = $data->show_subtitle?1:0;
        }

        error_log(var_export($data, true));
        $db->beginTransaction();
        $sql = "UPDATE menu_item SET title=".$db->quote($data->title).",title_en=".$db->quote($data->title_en).", subtitle=".$db->quote($data->subtitle).",subtitle_en=".$db->quote($data->subtitle_en).", published=".$data->published.", show_subtitle=".$data->show_subtitle."  where id=" . $menuItemId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            $response = ['status'=>'error', 'message'=> 'Problem during saving menu'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }
        $db->commit();
        $response = ['status'=> 'ok', 'message' => 'Menu item saved'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
