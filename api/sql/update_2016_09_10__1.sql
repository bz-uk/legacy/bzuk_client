ALTER TABLE `image` 
ADD COLUMN `description` TEXT NULL AFTER `created_date`,
ADD COLUMN `description_en` TEXT NULL AFTER `description`;
CREATE TABLE `image_author` (
  `image_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  PRIMARY KEY (`image_id`, `author_id`));

ALTER TABLE `plants` 
ADD COLUMN `sources` TEXT NULL AFTER `occurrence`,
ADD COLUMN `sources_en` VARCHAR(45) NULL AFTER `sources`;

ALTER TABLE `plant_image` ADD COLUMN `order` INT NULL AFTER `image_id`;
