import React, {PropTypes} from 'react';
import _ from 'lodash';
import {
    Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn
}
    from 'material-ui/Table';
import LinearProgress from 'material-ui/LinearProgress';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import HardwareKeyboardArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left'
import HardwareKeyboardArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right'
import {black} from 'material-ui/styles/colors';
import FontIcon from 'material-ui/FontIcon';
import DateTimeReadonly from './../widgets/dateTimeReadonly';
import StrippedHtmlCell from './strippedHtmlCell';
import PositionAndOrderingCell from './positionAndOrderingCell';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Subheader from 'material-ui/Subheader';
import Logger from './../util/logger';

var defaultTableProperties = {
    fixedHeader: false,
    fixedFooter: false,
    stripedRows: false,
    showRowHover: false,
    selectable: true,
    multiSelectable: true,
    displaySelectAll: false,
    enableSelectAll: true,
    deselectOnClickaway: false,
    showCheckboxes: true,
    height: '100%'
}

export default class SimpleTable extends React.Component {
    
    constructor(props) {
        super(props);
        
        if (props.draggable) {
            this.createDragDropPlaceholder();
        }
        this.logger = this.props.logger || new Logger(Logger.LEVEL_DEBUG);
        
        let tableProps = defaultTableProperties;
        if (props.height) {
            tableProps.height = props.height;
        }
        if (props.fixedFooter) {
            tableProps.fixedFooter = props.fixedFooter;
        }
        if (props.fixedHeader) {
            tableProps.fixedHeader = props.fixedHeader;
        }
        
        this.state = {
            tableProperties: tableProps,
            tableData: null,
            draggable: props.draggable,
            actions: props.tableController.getRowActions(),
            columns: props.tableController.getColumns(),
            tableController: props.tableController,
            sortBy: props.tableController.getDefaultSorting().sortBy || 'id',
            sortOrder: props.tableController.getDefaultSorting().sortOrder || 'asc',
            pagination: props.tableController.getPagination(),
            tmpPageNumber: props.tableController.getPagination().page,
            totalCount: 0,
            pagesCount: 0,
            loading: false,
            reloadInProgress: false,
            configuration: props.tableController.configuration
        };
    }
    
    columnIsVisible = (columnId) => {
        if (this.state.configuration.columns && this.state.configuration.columns[columnId] && this.state.configuration.columns[columnId].visible === false) {
            return false;
        }
        return true;
    }
    
    getOptionalColumnsCount = () => {
        let count = 0;
        _.forIn(this.state.columns, (column) => {
            if (column.optional) {
                count++;
            }
        })
        return count;
    }
    
    getVisibleColumnsCount = () => {
        let hiddenColumnsCount = 0;
        if (this.state.configuration.columns) {
            _.forIn(this.state.configuration.columns, (column) => {
                if (column.visible === false) {
                    hiddenColumnsCount++;
                }
            })
        }
        return _.keys(this.state.columns).length - hiddenColumnsCount;
    }
    
    createDragDropPlaceholder() {
        this.placeholder                           = document.createElement('tr');
        this.placeholder.style['border-bottom']    = '1px solid rgb(224, 224, 224)';
        this.placeholder.style.color               = 'rgba(0, 0, 0, 0.870588)';
        this.placeholder.style.height              = '48px';
        this.placeholder.style['background-color'] = 'yellow';
        var td                                     = document.createElement('td');
        td.style.overflow                          = 'hidden';
        td.style['padding-left']                   = '24px';
        td.style.height                            = '48px';
        td.style.width                             = '100%';
        td.style['text-align']                     = 'left';
        td.style['font-size']                      = '13px';
        td.style['padding-right']                  = '24px';
        td.style['white-space']                    = 'nowrap';
        td.style['text-overflow']                  = 'ellipsis';
        td.style['background-color']               = 'inherit';
        td.style.cursor                            = 'inherit';
        td.colSpan                                 = _.keys(this.props.tableController.getColumns()).length + 2; //+id + actions
        td.innerHTML                               = 'Place here'
        this.placeholder.appendChild(td);
    }
    
    /**
     * @private
     * @return Promise
     */
    reload = () => {
        this.state.loading = true;
        this.setState(this.state);
        return this.state.tableController.dataProvider.requestData(_.keys(this.state.columns), {
            filter: this.state.tableController.getFilter(),
            sortBy: this.state.sortBy,
            sortOrder: this.state.sortOrder
        }, this.state.pagination.rowsPerPage, this.state.pagination.page); //TODO: filter
    }
    
    onDataLoadError = (err) => {
        this.state.loading = false;
        console.error('Unable to load table data');
        this.setState(this.state);
    }
    
    onDataLoaded = (data) => {
        this.state.tableData  = data.rows;
        this.state.totalCount = data.count;
        this.state.pagesCount = data.pages;
        this.state.loading = false;
        this.setState(this.state);
    }
    
    componentDidMount() { //TODO: maybe consolidate this somehow???
        this.state.tableController.on('reload', this.reload);
        this.state.tableController.dataProvider.on('data-loaded', this.onDataLoaded);
        this.state.tableController.dataProvider.on('data-load-error', this.onDataLoadError);
        this.reload();
    };
    
    componentWillUnmount() {
        this.state.tableController.off('reload', this.reload);
        this.state.tableController.dataProvider.off('data-loaded', this.onDataLoaded);
        this.state.tableController.dataProvider.off('data-load-error', this.onDataLoadError);
    }
    
    getActionsHeader() {
        if (this.state.actions && _.keys(this.state.actions).length > 0) {
            return (
                <TableHeaderColumn tooltip="Actions">Actions</TableHeaderColumn>
            );
        }
        return '';
    }
    
    onSortChange = (columnId) => {
        if (!this.state.sortBy || (this.state.sortBy && this.state.sortBy !== columnId)) {
            this.state.sortBy    = columnId;
            this.state.sortOrder = 'asc'
        } else {
            this.state.sortOrder = this.state.sortOrder === 'asc' ? 'desc' : 'asc';
        }
        this.setState(this.state);
        this.reload();
    }
    
    getDragIcon = (columnId, columnDefinition) => {
        let styles = {
            draggableIcon: {
                fontSize: 12,
                marginRight: 5,
                opacity: 0.3
            }
        }
        if (columnDefinition.draggable) {
            return (<FontIcon style={styles.draggableIcon} color={black} className="icon-up-hand"/>);
        }
    }
    
    getSortIcon = (columnId, columnDefinition) => {
        var styles = {
            sortIcon: {
                fontSize: 12,
                marginRight: 5
            },
            unsortedButsortableIcon: {
                fontSize: 12,
                marginRight: 5,
                opacity: 0.3
            }
        }
        // if (!this.state.sortBy || (this.state.sortBy && this.state.sortBy !== columnId)) {
        //     return '';
        // }
        if (!columnDefinition.sortable) {
            return '';
        }
        if (this.state.sortBy !== columnId) {
            return (<FontIcon style={styles.unsortedButsortableIcon} color={black} className="icon-sort"/>);
        }
        if (this.state.sortOrder === 'asc') {
            return (
                <FontIcon style={styles.sortIcon} color={black} className="icon-sort-alt-up"/>
            )
        }
        return (
            <FontIcon style={styles.sortIcon} color={black} className="icon-sort-alt-down"/>
        )
    }
    
    getHeaderCell(columnId, definition) {
        var headerCellStyle = {}
        if (this.state.columns[columnId].width) {
            headerCellStyle['width'] = this.state.columns[columnId].width
        }
        if (!this.state.columns[columnId].sortable) {
            return (<TableHeaderColumn
                key={columnId}
                style={headerCellStyle}
                tooltip={this.state.columns[columnId].tooltip}>
                {this.state.columns[columnId].label}
            </TableHeaderColumn>);
        }
        return (
            <TableHeaderColumn
                key={columnId}
                onTouchTap={() => {
                    this.onSortChange(columnId)
                }} style={headerCellStyle}
                tooltip={this.state.columns[columnId].tooltip}>
                {this.getSortIcon(columnId, definition)}
                {this.getDragIcon(columnId, definition)}
                {this.state.columns[columnId].label}
            </TableHeaderColumn>
        );
    }
    
    getHeaderColumns() {
        return (
            <TableRow>
                {_.keys(this.state.columns).map((columnId) => {
                    if (this.columnIsVisible(columnId)) {
                        return this.getHeaderCell(columnId, this.state.columns[columnId]);
                    }
                })}
                {this.getActionsHeader()}
            </TableRow>
        );
    }
    
    getActionIcon(index) {
        if (index === 'edit') {
            return 'action-icon icon-pencil-neg'
        }
        if (index === 'delete') {
            return 'action-icon icon-trash-empty'
        }
    }
    
    getActionsCell(rowData) {
        var self = this;
        if (this.state.actions && _.keys(this.state.actions).length > 0) {
            return (
                <TableRowColumn>
                    {_.values(_.mapValues(this.state.actions, (action, idx) => {
                        let iconName = this.getActionIcon(idx);
                        if (iconName) {
                            return (
                                <FontIcon key={idx} title={action.title}
                                          onClick={() => action.callback.call(null, rowData)}
                                          color={black} className={iconName}/>)
                        } else {
                            let ActionWidget = action.widget;
                            return (
                                <ActionWidget key={idx} definition={action} row={rowData}></ActionWidget>
                            )
                        }
                    }))}
                </TableRowColumn>
            )
        }
        return '';
    }
    
    getCellWidget(cellValue, row, columnId, columnDefinition) {
        if (!columnDefinition.type && !columnDefinition.widget) {
            return cellValue;
        }
        if (columnDefinition.widget) {
            const Widget = columnDefinition.widget;
            let props  = {
                cellValue: cellValue,
                row: row,
                columnId: columnId,
                definition: columnDefinition
            };
            return <Widget {...props}></Widget>
        }
        switch (columnDefinition.type) {
            case 'date':
                var parsedDate = Date.parse(cellValue);
                if (parsedDate) {
                    parsedDate = new Date(parsedDate);
                }
                return <DateTimeReadonly date={parsedDate}/>
            case 'position':
                return <PositionAndOrderingCell value={cellValue}/>
            case 'stripped_html':
                return <StrippedHtmlCell row={row} value={cellValue} columnName={columnId}
                                         definition={columnDefinition}/>
            default:
                return cellValue;
        }
    }
    
    //for D&D thx to http://jsfiddle.net/kb3gN/14594/?utm_source=website&utm_medium=embed&utm_campaign=kb3gN
    onRowDragEnd = (event) => {
        this.dragged.style.display = "";
        this.dragged.parentNode.removeChild(this.placeholder);
        //update data
        var data = this.state.tableData;
        var from = Number(this.dragged.dataset.id);
        var to   = Number(this.over.dataset.id);
        if (from < to) {
            to--;
        }
        if (this.nodePlacement == "after") {
            to++
        }
        data.splice(to, 0, data.splice(from, 1)[0]);
        //this.setState(this.state);
        var counter = 1;
        this.state.tableData.forEach((item, idx) => {
            item.position = counter;
            counter++;
        });
        this.setState(this.state);
        this.state.tableController.sendChangeOrderRequest(this.state.tableData.map((item) => {
            return item.id
        }))
    }
    
    onRowDragStart = (event) => {
        this.dragged                     = event.currentTarget;
        event.dataTransfer.effectAllowed = 'move';
        
        // Firefox requires calling dataTransfer.setData
        // for the drag to properly work
        event.dataTransfer.setData("text/html", event.currentTarget);
    }
    
    onDragOver = (e) => {
        e.preventDefault();
        this.dragged.style.display = "none";
        if (e.target.closest('tr').className == "placeholder") {
            return
        }
        this.over = e.target.closest('tr')
        
        var relY   = e.clientY - this.over.offsetTop;
        var height = this.over.offsetHeight / 2;
        var parent = e.target.closest('tr').parentNode;
        
        if (relY > height) {
            this.nodePlacement = "after";
            parent.insertBefore(this.placeholder, e.target.closest('tr').nextElementSibling);
        }
        else if (relY < height) {
            this.nodePlacement = "before"
            parent.insertBefore(this.placeholder, e.target.closest('tr'));
        }
    }
    
    getRowStyle = (idx) => {
        return {
            backgroundColor: idx % 2 === 0 ? 'rgb(245, 245, 245)' : 'rgb(255, 255, 255)'
        }
    }
    
    getTableBody() {
        if (!this.state.tableData) {
            return (
                <TableBody
                    displayRowCheckbox={false}
                    deselectOnClickaway={false}
                    showRowHover={false}
                    stripedRows={false}
                >
                    <TableRow>
                        <TableRowColumn>no data</TableRowColumn>
                    </TableRow>
                </TableBody>
            );
        }
        return (<TableBody displayRowCheckbox={this.state.tableProperties.showCheckboxes}
                           deselectOnClickaway={this.state.tableProperties.deselectOnClickaway}
                           showRowHover={this.state.tableProperties.showRowHover}
                           stripedRows={this.state.tableProperties.stripedRows}
        >
            {this.state.tableData.map((row, index) => (
                <TableRow style={this.getRowStyle(index)} data-id={index}
                          draggable={this.state.draggable ? 'true' : 'false'} onDragEnd={this.onRowDragEnd}
                          onDragStart={this.onRowDragStart} onDragOver={this.onDragOver} key={row.id}>
                    {_.map(this.state.columns, (columnDefinition, columnId) => {
                        if (this.columnIsVisible(columnId)) {
                            return (
                                <TableRowColumn
                                    key={columnId}>{this.getCellWidget(row[columnId], row, columnId, columnDefinition)}</TableRowColumn>)
                        }
                    })}
                    {this.getActionsCell(row)}
                </TableRow>
            ))}
        </TableBody>);
    }
    
    
    render() {
        var styles = {
            tableProgressBar: {
                display: this.state.loading ? 'block' : 'none'
            },
            tableWrapperStyle: {}
        }
        return (
            <div className="simple-table">
                {this.state.loading && <LinearProgress style={styles.tableProgressBar} mode="indeterminate"/>}
                <Table
                    height={this.state.tableProperties.height}
                    wrapperStyle={styles.tableWrapperStyle}
                    fixedHeader={this.state.tableProperties.fixedHeader}
                    fixedFooter={this.state.tableProperties.fixedFooter}
                    selectable={this.state.tableData && this.state.tableProperties.selectable}
                    multiSelectable={this.state.tableData && this.state.tableProperties.multiSelectable}
                >
                    <TableHeader
                        displaySelectAll={this.state.tableData && this.state.tableProperties.displaySelectAll}
                        adjustForCheckbox={this.state.tableData && this.state.tableProperties.showCheckboxes}
                        enableSelectAll={this.state.tableData && this.state.tableProperties.enableSelectAll}
                    >
                        <TableRow>
                            {this.getTableSuperHeader({showPager: false})}
                        </TableRow>
                        {this.getHeaderColumns()}
                    </TableHeader>
                    {this.getTableBody()}
                    {this.state.tableData && this.state.tableData.length > 0 ?
                        <TableFooter>
                            <TableRow>
                                {this.getTableSuperHeader({showConfig: false})}
                            </TableRow>
                        </TableFooter>
                        :
                        ''
                    }
                </Table>
            </div>
        )
    }
    
    onTableOptionsToggle = (ev) => {
        event.preventDefault();
        this.state.tableOptionsAnchorEl = ev.currentTarget;
        this.state.tableOptionsOpen     = !this.state.tableOptionsOpen;
        this.setState(this.state);
    }
    
    toggleColumnVisibility = (columnId) => {
        if (!this.state.configuration.columns) {
            this.state.configuration.columns = {};
        }
        if (!this.state.configuration.columns[columnId]) {
            this.state.configuration.columns[columnId] = {};
        }
        if (this.state.configuration.columns[columnId].visible === null ||
            this.state.configuration.columns[columnId].visible === undefined ||
            this.state.configuration.columns[columnId].visible === '') {
            this.state.configuration.columns[columnId].visible = true;
        }
        this.state.configuration.columns[columnId].visible = !this.state.configuration.columns[columnId].visible;
        this.setState(this.state);
        this.state.tableController.saveConfiguration(this.state.configuration);
    }
    
    showPreviousPage = () => {
        if (this.state.pagination.page <= 1) {
            return;
        }
        --this.state.pagination.page;
        this.state.tmpPageNumber = this.state.pagination.page;
        this.state.tableController.setPagination(this.state.pagination.page, this.state.pagination.rowsPerPage);
        this.setState(this.state);
        this.state.tableController.reload();
    }
    
    showNextPage = () => {
        if (this.state.pagination.page >= this.state.pagesCount) {
            return;
        }
        ++this.state.pagination.page;
        this.state.tmpPageNumber = this.state.pagination.page;
        this.state.tableController.setPagination(this.state.pagination.page, this.state.pagination.rowsPerPage);
        this.setState(this.state);
        this.state.tableController.reload();
    }
    
    onReloadButtonTouchTap = () => {
        if (this.state.loading) {
            return;
        }
        this.setState({reloadInProgress: true}, () => {
            this.reload()
            .then(() =>{
                this.setState({reloadInProgress: false})
            })
            .catch(() => {
                this.setState({reloadInProgress: false});
            });
        })
    }
    
    onPageChange = (evt, value) => {
        this.state.tmpPageNumber = value;
        this.setState(this.state);
    }
    
    onPageTextBoxKeyPress = (event) => {
        if (event.key == 'Enter') {
            this.showPage();
        }
    }
    
    showPage = () => {
        if (!parseInt(this.state.tmpPageNumber)) {
            this.state.tmpPageNumber = 1
            this.setState(this.state);
        }
        if (this.state.tmpPageNumber > this.state.pagesCount) {
            this.state.tmpPageNumber = this.state.pagesCount;
            this.setState(this.state);
        }
        if (this.state.tmpPageNumber < 1) {
            this.state.tmpPageNumber = 1;
            this.setState(this.state);
        }
        this.state.tableController.setPagination(this.state.tmpPageNumber, this.state.pagination.rowsPerPage);
        this.state.pagination.page = this.state.tmpPageNumber;
        this.state.tableController.reload();
    }
    
    getTableSuperHeader(options) {
        let defaultOptions = {
            showPager: true,
            showConfig: true
        };
        options            = _.assign(defaultOptions, options);
        let styles         = {
            recordsCountHeader: {
                alignItems: 'center',
                justifyContent: 'center',
                display: 'flex'
            },
            miniIcon: {
                fontSize: 12,
                paddingLeft: 10
            },
            pageNumberTextBox: {
                width: 60
            },
            pager: {
                display: 'flex',
                alignItems: 'center'
            },
            pageNumberTextBoxInput: {
                textAlign: 'center'
            },
            optionsGear: {
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-end'
            }
        }
        
        let rowsFrom = (this.state.pagination.page - 1) * this.state.pagination.rowsPerPage + 1;
        let rowsTo   = rowsFrom + this.state.pagination.rowsPerPage - 1;
        if (rowsTo > this.state.totalCount) {
            rowsTo = this.state.totalCount;
        }
        let headerColspanCount = this.getVisibleColumnsCount();
        if (this.state.actions && Object.keys(this.state.actions).length > 0) {
            headerColspanCount++;
        }
        return (
            <TableHeaderColumn colSpan={headerColspanCount}>
                <div className="grid-noBottom">
                    <div style={styles.recordsCountHeader} className="col">
                        {this.state.tableData && this.state.tableData.length > 0 ?
                            <span>Showing {rowsFrom} - {rowsTo} of {this.state.totalCount}</span>
                            :
                            <span>no records found</span>}
                    </div>
                    {options.showPager ?
                        <div style={styles.pager} className="col">
                            Page <IconButton disabled={this.state.pagination.page <= 1 || this.state.loading}
                                             onTouchTap={this.showPreviousPage}><HardwareKeyboardArrowLeft /></IconButton>
                            <TextField id="table-page" inputStyle={styles.pageNumberTextBoxInput}
                                       style={styles.pageNumberTextBox}
                                       disabled={this.state.loading}
                                       value={this.state.tmpPageNumber} onBlur={this.showPage}
                                       onKeyPress={this.onPageTextBoxKeyPress}
                                       onChange={this.onPageChange}/>
                            <IconButton
                                disabled={this.state.pagination.page >= this.state.pagesCount || this.state.loading}
                                onTouchTap={this.showNextPage}><HardwareKeyboardArrowRight /></IconButton>
                            from {this.state.pagesCount}
                        </div>
                        :
                        ''
                    }
                    {(options.showConfig && this.getOptionalColumnsCount() > 0) ?
                        <div style={styles.optionsGear} className="col-1">
                            <FontIcon title={this.state.reloadInProgress?'Reloading...':'Click to reload'} className={'action-icon icon-arrows-ccw ' + (this.state.reloadInProgress?'rotate':'')} onTouchTap={this.onReloadButtonTouchTap}/>
                            <FontIcon className="action-icon icon-cog" onTouchTap={this.onTableOptionsToggle}/>
                            <Popover
                                open={this.state.tableOptionsOpen}
                                anchorEl={this.state.tableOptionsAnchorEl}
                                anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                                targetOrigin={{horizontal: 'left', vertical: 'top'}}
                                onRequestClose={this.onTableOptionsToggle}
                            >
                                <Menu>
                                    <Subheader>Visible columns</Subheader>
                                    {_.keys(this.state.columns).map((column, idx) => {
                                        if (this.state.columns[column].optional === true) {
                                            return <MenuItem
                                                key={idx}
                                                checked={this.columnIsVisible(column)}
                                                onTouchTap={() => this.toggleColumnVisibility(column)}
                                                primaryText={this.state.columns[column].label}/>
                                        }
                                    })}
                                </Menu>
                            </Popover>
                        </div> : ''}
                </div>
            </TableHeaderColumn>
        );
    }
}