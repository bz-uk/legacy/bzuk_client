'use strict';
import React from 'react';
import {Card, CardActions, CardTitle, CardText, CardMedia} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import CircularProgress from 'material-ui/CircularProgress';
import Checkbox from 'material-ui/Checkbox';
import FormField from './../form/formField';
import FormController from './../form/formController.js';
import RequiredValidator from './../form/validators/requiredValidator.js';

//TODO: get rid of lodash - DONE!
//TODO: autocomplete hack
//TODO: password on enter launch auth... - DONE!
//TODO: use new form fields
export default class LoginDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {
                username: '',
                password: '',
                remembers: false,
            },
            inProgress: false
        }
        this.formController = new FormController();
    }

    componentWillUnmount() {
        this.formController.off('field-change', this.onFormFieldChange);
        this.formController.off('validation-done', this.onValidationDone);
    }

    componentDidMount() {
        this.formController.on('field-change', this.onFormFieldChange);
        this.formController.on('validation-done', this.onValidationDone);
    }

    onValidationDone = () => {
        this.setState({valid: this.formController.valid});
    }

    onFormFieldChange = (fieldName, fieldValue) => {
        this.state.model[fieldName] = fieldValue;
        //this.state.valid = this.formController.valid;
        this.setState(this.state);
    }

    onLoginButtonClick = () => {
        this.handleCallback();
    }

    getUsernameValidators() {
        if (this.props.usernameValidators && Array.isArray(this.props.usernameValidators)) {
            return this.props.usernameValidators;
        }
        return [new RequiredValidator({errorMessage: 'is empty'})];
    }

    handleRememberChange = (event, checked) => {
        this.state.model.remembers = checked;
        this.setState(this.state);
    }

    handleCallback = () => {
        this.setState({inProgress: true}, () => {
            this.formController.validate()
            .then((result) => {
                if (!result) {
                    throw Error('invalid');
                }
                return this.props.onLoginCallback(this.state.model);
            })
            .then((result) => {
                return this.props.onSuccessfulLogin && this.props.onSuccessfulLogin(result);
            })
            .then(() => {
                this.setState({inProgress: false});
            })
            .catch((err) => {
                if (err.message === 'invalid') {
                    return this.setState({inProgress: false});
                }
                console.error('Unable to login ', err);
                this.setState({inProgress: false}, () => {
                    return this.props.onFailedLogin(err);
                });
            });
        });
    }

    onPasswordKeyPress = (event) => {
        if (event.nativeEvent.keyCode === 13 && this.state.model.username && this.state.model.password) {
            this.handleCallback();
            event.preventDefault();
            return;
        }
    }

    render() {
        let styles = {
            cardStyle: Object.assign({
                backgroundColor: '#f7f7f7'
            }, this.props.cardStyle),
            buttonWithProcessing: {
                display: 'flex',
                alignItems: 'center',
            },
            progressBar: {
                marginRight: '1rem'
            },
            actionsRow: {
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                flexWrap: 'wrap'
            },
            remember: {
                maxWidth: '60%'
            },
            containerStyle: this.props.containerStyle || {}
        };
        return (
            <div style={styles.containerStyle}>
                <Card style={styles.cardStyle}>
                    {React.isValidElement(this.props.title) ?
                        <CardMedia>
                            {this.props.title}
                        </CardMedia>
                        :
                        <CardTitle title={this.props.title || 'Login'}/>
                    }
                    <CardText>
                        <form>
                            <FormField value={this.state.model.username} formController={this.formController}
                                       validators={this.getUsernameValidators()}
                                       errorMessage="Username {commaSeparatedErrors}" name="username">
                                <TextField hintText={this.props.usernameHint || "valid username"}
                                           floatingLabelText={this.props.usernameLabel || "User name"}
                                           fullWidth={true}
                                           type={this.props.usernameType || ''}
                                />
                            </FormField>
                            <br/>
                            <FormField value={this.state.model.password} formController={this.formController}
                                       validators={[new RequiredValidator({errorMessage: 'is empty'})]}
                                       errorMessage="Password {commaSeparatedErrors}" name="password">
                                <TextField value={this.state.password} onChange={this.handlePasswordChange}
                                           fullWidth={true}
                                           onKeyPress={this.onPasswordKeyPress}
                                           floatingLabelText="Password"
                                           type="password"
                                />
                            </FormField>
                        </form>
                        <br/>
                    </CardText>
                    <CardActions style={styles.actionsRow}>
                        <div style={styles.remember}>
                            <Checkbox defaultChecked={this.state.model.remembers}
                                      onCheck={this.handleRememberChange}
                                      label={this.props.rememberCheckboxLabel ? this.props.rememberCheckboxLabel : 'remember'}
                            />
                        </div>
                        <div style={styles.buttonWithProcessing}>
                            {this.state.inProgress && <CircularProgress size={30} style={styles.progressBar}/>}
                            <RaisedButton disabled={!this.state.valid || this.state.inProgress}
                                          onClick={this.onLoginButtonClick}
                                          label={this.props.loginButtonLabel || 'Login'}/>
                        </div>
                    </CardActions>
                </Card>
            </div>
        );
    }
}