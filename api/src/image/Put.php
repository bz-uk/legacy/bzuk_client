<?php

namespace botgarApi\image;

use Slim\Slim;

class Put
{
    /**
     *
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($imageId)
    {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO ('mysql:host=' . $conf ['db:host'] . ';port=' . $conf ['db:port'] . ';dbname=' . $conf ['db:dbname'] . ';charset=UTF8;', $conf ['db:user'], $conf ['db:pass']);

        $json = $this->app->request->getBody();
        $data = json_decode($json);
        $authors = '';
        if (isset($data->authors) && $data->authors != null) {
            $authors = $data->authors;
        }
        $authorsEn = '';
        if (isset($data->authors_en) && $data->authors_en != null) {
            $authorsEn = $data->authors_en;
        }
        $description = '';
        if (isset($data->description) && $data->description != null) {
            $description = $data->description;
        }
        $descriptionEn = '';
        if (isset($data->description_en) && $data->description_en != null) {
            $descriptionEn = $data->description_en;
        }
        $display = '';
        if (isset($data->display) && $data->display != null) {
            $display = $data->display;
        }
        $displayVerticalOffset = 0;
        if (isset($data->displayVerticalOffset) && $data->displayVerticalOffset != null && $data->displayVerticalOffset >= 0 && $data->displayVerticalOffset <= 100) {
            $displayVerticalOffset = $data->displayVerticalOffset;
        }
        $db->beginTransaction();
        $sql = "UPDATE image SET authors=" . $db->quote($authors) . ",authors_en=" . $db->quote($authorsEn) . ",description=" . $db->quote($description) . ",description_en=" . $db->quote($descriptionEn) . ", display=".$db->quote($display).", display_vertical_offset=".$displayVerticalOffset." where id=" . $imageId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            $response = ['success' => false, 'status' => 'error', 'message' => 'Problem during saving image'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }
//        if (isset($data->authors)) {
//            $sql = 'DELETE from image_author where image_id=' . $imageId;
//            $result = $db->query($sql);
//            if (!$result) {
//                $db->rollback();
//                $response = ['success' => false, 'status' => 'error', 'message' => 'Unable to clear image authors before save'];
//                $this->app->response->setStatus(400);
//                $response = json_encode($response);
//                $this->app->response->header('Content-Length', strlen($response));
//                $this->app->response->setBody($response);
//                return;
//            }
//            foreach ($data->authors as $author) {
//                $sql = 'INSERT INTO image_author(author_id, image_id) VALUES(' . $author->id . ', ' . $imageId . ')';
//                $result = $db->query($sql);
//                if (!$result) {
//                    $db->rollback();
//                    $response = ['success' => false, 'status' => 'error', 'message' => 'Unable to save author'];
//                    $this->app->response->setStatus(400);
//                    $response = json_encode($response);
//                    $this->app->response->header('Content-Length', strlen($response));
//                    $this->app->response->setBody($response);
//                    return;
//                }
//            }
//        }
        $db->commit();

        $this->app->response->setBody(json_encode(['success' => true, 'message' => 'image saved']));
        $this->app->response->setStatus(200);

        return;
    }
}
