'use strict';
import React from 'react';
import forIn from 'lodash.forin';

import FieldValidator from './fieldValidator.js';

export default class FormField extends React.Component {
    constructor(props) {
        super(props);

        this.formController = this.props.formController;
        this.name           = this.props.name;
        this.validators     = this.props.validators;
        this.validator      = new FieldValidator(this.props.validators, this.props.errorMessage);

        if (!this.formController.fields[this.name]) {
            this.formController.fields[this.name] = this;
        } else {
            //merge settings
        }

        this.state = {
            valid: null,
            touched: this.props.value ? true : false,
            error: null,
            value: this.props.value,
            validating: false
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            value: props.value
        });
    }

    onChange = (e) => {
        let value;
        if (e.target) {
            value = e.target.value;
        } else {
            value = e;
        }
        this.state.value      = value;
        this.state.touched    = true;
        this.state.validating = true;
        this.setState(this.state);
        this.validate(); //TODO will be promise!
        this.formController.emit('field-change', this.name, value);
    }

    validate = () => {
        console.log('validate ', this.name);
        this.state.valid = this.validator.validate(this.state.value);
        if (!this.state.valid) {
            this.state.error          = this.validator.error;
            this.formController.valid = false;
            console.log('n: ', this.name, 'v: ', this.state.valid);
        } else {
            this.state.error     = null;
            let otherFieldsValid = true;
            forIn(this.formController.fields, (field) => {
                if (field === this) {
                    return;
                }
                console.log('ccc: ', field.name, field.state.valid);
                otherFieldsValid = field.state.valid && otherFieldsValid;
            })
            console.log('n: ', this.name, 'v: ', this.state.valid, ',o: ', otherFieldsValid);
            this.formController.valid = otherFieldsValid;
        }
        this.state.validating = false;
        this.setState(this.state);
    }

    getWrappedElement() {
        //check for non standard components like ckeditor
        return React.cloneElement(this.props.children, {
            value: this.state.value,
            errorText: this.state.error,
            onChange: this.onChange
        })
    }

    render() {
        return this.getWrappedElement();
    }
}