'use strict';
angular.module('adminApp').controller('primaryPointEditController', ['$scope', '$rootScope', '$log', '$mdDialog', '$mdMedia', 'circuitsService', '$timeout', '$routeParams', 'uiGmapIsReady', 'settingsService', 'Upload', '$location',
    function($scope, $rootScope, $log, $mdDialog, $mdMedia, circuitsService, $timeout, $routeParams, uiGmapIsReady, settingsService, Upload, $location) {
        //start:models
        $scope.markersIdCounter = 0
        $scope.point = {}

        $scope.settings = []

        $scope.files //for uploads

        $scope.saving = false
        $scope.isNew = $routeParams.pointId == '_'
        $scope.circuitId = $routeParams.circuitId
        $scope.primaryPointId = $routeParams.pointId

        $scope.options = { //for mce
            language: 'cz',
            format_tags: 'h1;h2',
            allowedContent: true,
            entities: false,
            removePlugins: 'templates'
            //width: '100%'
        };
        $scope.map = {
            infoWindowTemplate: 'plants/plantEditMapMarkerInfoWindow.template.html',
            markerOptions: {
                draggable: true,
                animation: 'DROP'
            },
            markerEvents: {
                dragend: function(markerModel, eventName, markerObject) {
                  //
                }
            },
            center: {
                latitude: 50.0708767569985,
                longitude: 14.4208672642708
            },
            zoom: 18,
            bounds: {
                northeast: {
                    latitude: 50.0689551446,
                    longitude: 14.4179141521
                },
                southwest: {
                    latitude: 50.0733622808,
                    longitude: 14.4247806072
                }
            },
            events: {
                click: function(mapModel, eventName, originalEventArgs) {
                    $scope.$apply(function() {
                        //console.log('click!')

                        var e = originalEventArgs[0];
                        //$scope.model.markers = []
                        $scope.markersIdCounter++
                        $scope.point.markers = []
                        $scope.point.markers.push({
                            latitude: e.latLng.lat(),
                            longitude: e.latLng.lng(),
                            id: $scope.markersIdCounter,
                            title: $scope.point.name
                        })
                    });
                }
            }
        }
        //end:models
        //start: upload
        $scope.upload = function(file) { //TODO: to service??
          if (file === undefined) {
            return;
          }
            if (!file.$error) {
                var newImg = {
                    id: undefined,
                    url: 'https://bz-uk.cz/sites/default/files/styles/medium/public/images/abutilon_megapotamicum2.jpg',
                    status: 'uploading'
                }
                $scope.point.image = newImg
                Upload.upload({
                    url: '/api/api/index.php/images', //TODO: load from settings
                    data: {
                        file: file
                    }
                }).then(function(resp) {
                        console.log('upload complete')
                        console.log(resp)
                        $timeout(function() {
                            console.log('complete timeout')
                            if (resp.data.id !== undefined && parseInt(resp.data.id) > 0) {
                                console.log('done!')
                                newImg.url = resp.data.url
                                newImg.id = parseInt(resp.data.id)
                                newImg.status = 'done'
                            }
                            $scope.log = 'file: ' +
                                resp.config.data.file.name +
                                ', Response: ' + JSON.stringify(resp.data) +
                                '\n' + $scope.log;
                        });
                    },
                    function(resp) {
                        alert('Unable to uplaod image: ' + resp.data)
                        $scope.point.image = undefined
                    },
                    function(evt) {
                        var progressPercentage = parseInt(100.0 *
                            evt.loaded / evt.total);
                        newImg['progress'] = progressPercentage
                    });
            }

        }
        $scope.$watch('files', function() {
            $scope.upload($scope.files);
        })
        $scope.deleteImage = function(image) {
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this image?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                $scope.point.image = undefined
            }, function() {});
        }
        //end: upload
        //start: listeners
        settingsService.onSettingsLoaded($scope, function(event, settings){
          $scope.settings = settings
        })
        //??
        // circuitsService.onCircuitDataLoaded($scope, function(event, circuitData) {
        //         $scope.circuit = circuitData
        //         event.stopPropagation()
        //     })
        circuitsService.onPrimaryPointLoaded($scope, function(event, pointData) {
          $scope.point = pointData
          var maxId = 0
          for (var i=0;i<$scope.point.markers.length;i++) {
            if ($scope.point.markers[i].id > maxId) {
              maxId = $scope.point.markers[i].id
            }
          }
          $scope.markersIdCounter = maxId
          console.log('Point loaded')
          console.log($scope.point)
          event.stopPropagation()
        })
        circuitsService.onPrimaryPointSaved($scope, function(event, pointData) {
          $scope.openCircuit();
        })
        circuitsService.onPrimaryPointCreated($scope, function(event, pointData) {
          $scope.openCircuit();
        })
        //end: listeners
        //start: map logic
        $scope.getNormalizedCoord = function(coord, zoom) {
            var y = coord.y;
            var x = coord.x;

            // tile range in one direction range is dependent on zoom level
            // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
            var tileRange = 1 << zoom;

            // don't repeat across y-axis (vertically)
            if (y < 0 || y >= tileRange) {
                return null;
            }

            // repeat across x-axis
            if (x < 0 || x >= tileRange) {
                x = (x % tileRange + tileRange) % tileRange;
            }

            return {
                x: x,
                y: y
            };
        }
        $scope.initMap = function() {
            $scope.moonMapType = new google.maps.ImageMapType({
                getTileUrl: function(coord, zoom) {
                    // console.log(getProjection())
                    var normalizedCoord = $scope.getNormalizedCoord(coord, zoom);
                    if (!normalizedCoord) {
                        return null;
                    }

                    var bound = Math.pow(2, zoom);
                    var url = '//' + $scope.settings.baseUrl + 'api/map2/' + zoom + '/' + normalizedCoord.x + '/' + normalizedCoord.y + '.png'
                    return url;
                },
                tileSize: new google.maps.Size(256, 256),
                maxZoom: 18,
                minZoom: 17,
                radius: 1738000,
                name: 'Moon'
            });
        }
        uiGmapIsReady.promise(1).then(function(instances) {
            $log.log('map is ready')
            instances.forEach(function(inst) {
                var map = inst.map
                var uuid = map.uiGmap_id
                var mapInstanceNumber = inst.instance // Starts at 1.
                $scope.initMap()
                map.overlayMapTypes.push($scope.moonMapType);
            })
        })

        //end: map logic
        //start:helpers
        $scope.openCircuit = function() {
          $location.url('/circuits/' + $routeParams.circuitId)
        }
        $scope.openCircuits = function() {
            //TODO: save check?!?
            $location.url('/circuits')
        }
        $scope.save = function() {
          if ($scope.isNew) {
            circuitsService.createPrimaryPoint($routeParams.circuitId, $scope.point)
          } else {
            circuitsService.savePrimaryPoint($routeParams.circuitId, $scope.point)
          }
        }
        $scope.getSaveLabel = function() {
            if ($scope.isNew) {
                return $scope.saving ? 'Adding...' : 'Add'
            } else {
                return $scope.saving ? 'Saving...' : 'Save'
            }
        }
        //end: helpers
        //starts:load data
        if (!$scope.isNew) {
            circuitsService.loadOnePrimaryPoint($routeParams.circuitId, $routeParams.pointId)
        }
        settingsService.loadAndEmitEvent()
        //end:load data
    }
]);
