//routes.js
angular.module('adminApp').config(function ($routeProvider, $locationProvider, $mdThemingProvider, $mdIconProvider) {
    $mdThemingProvider.theme('default').primaryPalette('blue')
        .accentPalette('red');
    $routeProvider
    // .when('/', {
    //     templateUrl : 'main.template.html',
    //     controller : 'adminController'
    // })
    //start: articles
        .when('/articles', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/articles/:articleId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        //end: articles
        .when('/menus', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/plants/', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/galleries/', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/galleries/:galleryId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/tags/', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/categories/', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/families/', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/authors', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/plants/print/:filterCode', {
            templateUrl: 'plants/printPlantsList.template.html',
            controller: 'printPlantsListController'
        })
        .when('/plants/:plantId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/menus/:menuId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/menus/:menuId/menu-items/:menuItemId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        //start: circuits
        .when('/circuits', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/circuits/:circuitId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .when('/circuits/:circuitId/primary-points/:pointId', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        //end: circuits
        .when('/downloads', {
            templateUrl: 'main.template.html',
            controller: 'adminController'
        })
        .otherwise({redirectTo: '/plants'});

    $locationProvider.html5Mode(true);
});
