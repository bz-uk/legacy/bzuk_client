// fucking not working
// class NewPlantsService extends baseService {
// 	constructor($log, $resource, $rootScope) {
// 		super($rootScope)
// 		this.$resource = $resource
// 		this.$log = $log
// 		this.allPlantsTags = []
// 		this.initResurces()
// 	}
//
// 	initResurces() {
// 		this.plantResource = this.$resource('/api/api/index.php/plants/:plantId', {}, {
// 			'save': {method: 'PUT'},
// 			'get': {method: 'GET'},
// 			'delete': {method: 'DELETE'}
// 		})
// 	}
//
// 	onPlantSaved(scope, callback) {
// 		this.registerHandler('plant-saved-event', scope, callback)
// 	}
//
// 	updateSinglePlant(plantModel){
// 		return this.plantResource.save({plantId: plantModel.id},plantModel, function(data) {
// 			this.emit('plant-saved-event', plantModel, 'Plant "'+plantModel.name+'" saved successfully')
// 		}, function(err) {
// 			console.log(err);
// 			var message = '';
// 			if (err.status === 500) {
// 				message = err.data;
// 			} else {
// 				message = err.data.message;
// 			}
// 			this.emit('plant-saved-error-event', plantModel);
// 		});
// 	}
// }
// NewPlantsService.$inject = ['$log', '$resource', '$rootScope'];

//angular.module('adminApp').factory('NewPlantsService', NewPlantsService)

angular.module('adminApp').factory('plantsService', ['$log', '$resource', '$rootScope',function ($log, $resource, $rootScope) {
	var plantsResource = $resource('/api/api/index.php/plants', {}, {
    'insert': {method: 'POST'},
	});
	var plantsTableDataResource = $resource('/api/api/index.php/plants/table-data', {}, {
    'getAll': {method: 'POST'},
	});
	var plantResource = $resource('/api/api/index.php/plants/:plantId', {}, {
		'save': {method: 'PUT'},
		'get': {method: 'GET'},
		'delete': {method: 'DELETE'}
	});
    var allPlantTagsResource = $resource('/api/api/index.php/plants/:plantId/tags', {}, {
        'update': {method: 'PUT'}
    });
	var tagOnPlantResource = $resource('/api/api/index.php/plants/:plantId/tags/:tagId', {}, {
		'delete': {method: 'DELETE'}
	});
  var categoriesResource = $resource('/api/api/index.php/plants-categories', {}, {
		'get': {method: 'GET', isArray:true}
	});
	var tagsOnPlantsResource = $resource('/api/api/index.php/plants/tags', {}, {
		'add': {method: 'POST'},
		'put': {method: 'PUT'}
	});
	var categoryResource = $resource('/api/api/index.php/plants-categories/:categoryId', {}, {
		'save': {method: 'PUT'}
	});
  var familiesResource = $resource('/api/api/index.php/plants-families', {}, {
		'get': {method: 'GET', isArray:true}
	});
	var familyResource = $resource('/api/api/index.php/plants-families/:familyId', {}, {
		'save': {method: 'PUT'}
	});
	var plantsTagsResource = $resource('/api/api/index.php/plants-tags', {}, {
    'getAll': {method: 'GET', isArray:true},
		'insert': {method: 'POST'}
	});
	var plantTagResource = $resource('/api/api/index.php/plants-tags/:tagId', {}, {
		'save': {method: 'PUT'}
	})
	var plantsPublishResource = $resource('/api/api/index.php/plants/publish', {}, {
		'publish': {method: 'POST'}
	})

	var registerHandler = function(eventName, scope, callback) {
			var handler = $rootScope.$on(eventName, callback);
			scope.$on('$destroy', handler);
	}

	var allPlantsTags = []
	return {
		//start: hanlders
		onPlantsLoaded: function(scope, callback) {
			 registerHandler('plants-loaded-event', scope, callback)
		},
		onPlantCategorySaved: function(scope, callback) {
			registerHandler('plant-category-saved-event', scope, callback)
		},
		onPlantFamilySaved: function(scope, callback) {
			registerHandler('plant-family-saved-event', scope, callback)
		},
		onPlantSaved: function(scope, callback) {
			registerHandler('plant-saved-event', scope, callback)
		},
		onTagsSaved: function(scope, callback) {
			registerHandler('plants-tags-saved-event', scope, callback)
		},
		onPlantTagSaved: function(scope, callback) {
			registerHandler('plant-tag-saved-event', scope, callback)
		},
		onPlantTagRemoved: function(scope, callback) {
			registerHandler('plant-tag-removed-event', scope, callback)
		},
		//end: hanlders
		publishOnePlant: function(plantId, publish) {
			var data = {};
			data[plantId] = publish;
			return plantsPublishResource.publish({}, {ids: data}).$promise;
		},
		getAllTags: function(successCallback, errCallback) {
			if (allPlantsTags.length > 0) {
				return successCallback(allPlantsTags)
			}
			return plantsTagsResource.getAll({}, function(tags) {
				allPlantsTags = angular.copy(tags)
				delete allPlantsTags.$promise;
				delete allPlantsTags.resolved;
				successCallback(allPlantsTags);
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plant tags', err);
        errCallback(message);
			})
		},
		createTag: function(model, successCallback, errCallback) {
			return plantsTagsResource.insert(model, function(data) {
				console.log(data)
				var newTag = {
					id: data.id,
					name: model.name,
					name_lowercase: model.name.toLowerCase()
				}
				allPlantsTags.push(newTag)
				successCallback(newTag);
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to add new plant tag', err);
        errCallback(message);
			})
		},
		saveCategory: function (categoryId, category) {
			return categoryResource.save({categoryId: categoryId}, category, function(data) {
				$rootScope.$emit('plant-category-saved-event', data)
				$rootScope.$emit('ok-message', 'Kategorie uložena');
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to save category', err);
				$rootScope.$emit('plant-category-save-error-event', err)
			})
		},
		saveFamily: function (familyId, family) {
			return familyResource.save({familyId: familyId}, family, function(data) {
				$rootScope.$emit('plant-family-saved-event', data)
				$rootScope.$emit('ok-message', 'Čeleď uložena');
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to save family', err);
				$rootScope.$emit('plant-family-save-error-event', err)
			})
		},
		getPlant: function(plantId, successCallback, errCallback) {
			return plantResource.get({plantId: plantId}, function(data) {
				successCallback(data);
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plants', err);
        errCallback(message);
			}).$promise;
		},
		getAllPlantsAndSendEvent: function(query, filter) {
			var postData = {
				query: query,
				filter: filter
			}
			return plantsTableDataResource.getAll({}, postData, function(data) {
				var dataList = angular.copy(data);
				delete dataList.$promise;
				delete dataList.resolved;
				$rootScope.$emit('plants-loaded-event', dataList.markers, dataList.rows)
			}, function(err) {
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plants', err);
        $rootScope.$emit('plants-loaded-error-event', err)
      })
		},
		getAllPlants: function(query, filter, successCallback, errCallback) {
			var postData = {
				query: query,
				filter: filter
			}
			console.log(postData)
			return plantsTableDataResource.getAll({}, postData, function(data) {
				var dataList = angular.copy(data);
				delete dataList.$promise;
				delete dataList.resolved;
				// for (var i=0;i<data.rows.length;i++){
				// 	data.rows[i]['is_blooming'] = data.rows[i]['is_blooming']==='1'?true:false
				// }
        successCallback(dataList.markers, dataList.rows);
      },
			function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plants', err);
        errCallback(message);
      })
		},
		deletePlant: function(id, successCallback, errCallback) {
			return plantResource.delete({plantId: id}, function(data) {
				successCallback(data)
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to delete plant', err);
        errCallback(message);
			})
		},
    getFamilies: function(successCallback, errCallback) {
      return familiesResource.get({}, function(data) {
        successCallback(data);
      },function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plant families', err);
        errCallback(message);
      }
    )
    },
    getCategories: function(successCallback, errCallback) {
      return categoriesResource.get({}, function(data) {
        successCallback(data);
      },function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to get plant categories', err);
        errCallback(message);
      }
    )
    },
    insertOne: function(plantModel,successCallback, errCallback){
      return plantsResource.insert(plantModel, function(data) {
        successCallback(data);
      }, function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to insert plant', err);
        errCallback(message);
      });
    },
		updateOneAndSendEvent: function(plantModel){
      return plantResource.save({plantId: plantModel.id},plantModel, function(data) {
				$rootScope.$emit('plant-saved-event', plantModel, 'Plant "'+plantModel.name+'" saved successfully')
      }, function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $rootScope.$emit('plant-saved-error-event', plantModel);
      });
    },
        updateTags: function(plantId, tags) {
            return allPlantTagsResource.update({plantId: plantId}, {tags: tags}).$promise;
        },
		addTags: function(selectedPlants, tags) {
			return tagsOnPlantsResource.add({}, {plants: selectedPlants, tags: tags}, function(data) {
				$rootScope.$emit('plants-tags-saved-event', {}, 'Tags added successfully')
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $rootScope.$emit('plants-tags-saved-error-event');
			})
		},
		removeTag: function(plantId, tagId) {
			return tagOnPlantResource.delete({plantId: plantId, tagId: tagId}, function(data){
				$rootScope.$emit('ok-message', 'Plant tag removed successfully')
				$rootScope.$emit('plant-tag-removed-event')
			}, function(err) {
				$rootScope.$emit('error-message', 'Plant tag was NOT removed')
				$rootScope.$emit('plant-tag-removed-error-event')
			})
		},
		deleteTags: function(selectedPlants, tags) {
			return tagsOnPlantsResource.put({}, {plants: selectedPlants, tags: tags}, function(data) {
				$rootScope.$emit('plants-tags-saved-event', {}, 'Tags removed successfully')
			}, function(err) {
				console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $rootScope.$emit('plants-tags-saved-error-event');
			})
		},
		savePlantTag: function(tagId, tag) {
			return plantTagResource.save({tagId: tagId}, tag, function(data) {
				$rootScope.$emit('ok-message', 'Plant tag saved successfully')
				$rootScope.$emit('plant-tag-saved-event', tag)
			}, function(err){
				$rootScope.$emit('error-message', 'Plant tag save failed')
				$rootScope.$emit('plant-tag-saved-error-event', tag)
			})
		},
		updateOne: function(plantModel,successCallback, errCallback){
      return plantResource.save({plantId: plantModel.id},plantModel, function(data) {
        successCallback(data);
      }, function(err) {
        console.log(err);
        var message = '';
        if (err.status === 500) {
          message = err.data;
        } else {
          message = err.data.message;
        }
        $log.error('Unable to insert plant', err);
        errCallback(message);
      });
    }
	}
}]);
