'use strict'
//controller
var sourcesEditorController = function($log, $mdDialog) {
    var ctrl = this;
    ctrl.editMode=false;
    ctrl.editContent = '';
    ctrl.editContentEn = ''; 
    if (ctrl.showLanguage === undefined) {
        ctrl.showLanguage = 'cz';
    }
        
    ctrl.editBlock = function() {
        ctrl.editContent = ctrl.content;
        ctrl.editContentEn = ctrl.contentEn;
        ctrl.editMode = true;
    }
    ctrl.saveBlock = function() {
        ctrl.content = ctrl.editContent;
        ctrl.contentEn = ctrl.editContentEn;
        $log.debug('saving' + ctrl.editContentEn)
        ctrl._clearTempVariables();    
        ctrl.editMode = false;
    }
    ctrl._clearTempVariables = function() {
        ctrl.editContent = '';
        ctrl.editContentEn = '';
    }
    ctrl.cancelEdit = function(ev) {
        var confirm = $mdDialog.confirm()
            .title('Edit cancel confirmation')
            .textContent('Are you sure you want to cancel editation? All changed content will be lost.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            ctrl._clearTempVariables()
            ctrl.editMode = false;            
        });
    }
}
//definition
angular.module('adminApp').component('sourcesEditor', {		
		templateUrl : 'plants/sources/sourcesEditor.template.html',
		controller: sourcesEditorController,
        bindings: {            
            showLanguage: '=',
            content: '=',
            contentEn: '='
        }
    });