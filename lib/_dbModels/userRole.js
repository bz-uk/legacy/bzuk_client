'use strict';
const connection = require('./../database/local');
const configuration = require('./../common/config');

var UserRole = connection.bookshelf.Model.extend({
    tableName: configuration.storage.mysql.tablePrefix + 'roles',
    hasTimestamps: true,
});

module.exports = connection.bookshelf.model('UserRole', UserRole);