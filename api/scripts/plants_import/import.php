<?php
/*
SELECT n.nid, n.title as node_title,ti.tid,ttd.name as cat_name,frb.body_value, fdni.node_image_fid,fm.filename,fm.uri FROM node as n left join taxonomy_index ti on ti.nid=n.nid left join taxonomy_term_data ttd on ttd.tid=ti.tid
left join field_revision_body frb on frb.revision_id=n.vid
left join field_data_node_image fdni on fdni.entity_id=n.nid
left join file_managed fm on fm.fid=fdni.node_image_fid
where n.type='image'

categories:
20,21,140,22,14,19,15,17,18,13,206,12,43

tags:
168,23,11,167,37,170,198,24,143,185,150,199,226,16

skip:
59,195,196,
*/
$raw = file_get_contents('data_2016_10_05.json');
$data = json_decode($raw);

require '../../config/config.php';
$db = new \PDO('mysql:host='.$config['db:host'].';port='.$config['db:port'].';dbname='.$config['db:dbname'].';charset=UTF8;', $config['db:user'], $config['db:pass']);

$ignoreCategories = [59, 195, 196];

$tags = [168, 23, 11, 167, 37, 170, 198, 24, 143, 185, 150, 199, 226, 16, 169, 140, 17];
$categories = [20, 21, 140, 22, 14, 19, 15, 17, 18, 13, 206, 12, 43];

//nid 136, 782 !?!??
/*
error: multiple categories on plant 136, already have Asijská a americká květena and setting new: Květena střední Evropy
error: multiple categories on plant 782, already have Květena střední Evropy and setting new: Vodní a bahenní rostliny
*/
$plants = [];
$skipped = [];

$knownTags = [];
$knownCategories = [];
$families = [];
foreach ($data as $plant) {
    if (in_array($plant->tid, $ignoreCategories)) {
        $skipped[] = $plant;
        continue;
    }
    $isNew = true;
    if (array_key_exists($plant->nid, $plants)) {
        $isNew = false;
    } else {
        $plants[$plant->nid] = $plant;
        if (strpos($plant->node_title, '(') !== false) {
            $plant->cz_name = trim(str_replace(['(',')'],'',substr($plant->node_title, strpos($plant->node_title, '('))));
            $plant->lat_name = trim(str_replace(['(',')'],'',substr($plant->node_title, 0, strpos($plant->node_title, '('))));
            $plant->node_title = $plant->cz_name;
        }
    }

    $existingPlant = $plants[$plant->nid];

    //check new image
    if (!isset($existingPlant->images)) {
        $existingPlant->images = [];
    }
    $existingPlant->images[$plant->node_image_fid] = ['uri' => $plant->uri, 'name' => $plant->filename];

    if (in_array($plant->tid, $tags)) { //its tag
        if (!isset($existingPlant->tags)) {
            $existingPlant->tags = [];
        }
        $knownTags[$plant->tid] = $plant->cat_name;
        $existingPlant->tags[$plant->tid] = $plant->cat_name;
    } elseif (in_array($plant->tid, $categories)) {
        //$knownCategories[$plant->tid] = $plant->cat_name;
        if (!isset($existingPlant->category)) {
          $existingPlant->category = ['cid' => $plant->tid, 'name' => $plant->cat_name];
          $knownCategories[] = ['cid' => $plant->tid, 'name' => $plant->cat_name];
        } else if ($existingPlant->category['cid'] != $plant->tid) {
          // //check new image
          // if (!isset($existingPlant->images)) {
          //     $existingPlant->images = [];
          // }
          // $existingPlant->images[$plant->node_image_fid] = ['uri' => $plant->uri, 'name' => $plant->filename];
          // echo "new img\n";
          echo ">>>>>>>>>>>>>>>>double cat def\n";
        } else {
          echo ">>>>>>>>>>>>>>>>wot????\n";
        }
    } else {
      $existingPlant->family = ['fid' => $plant->tid, 'name' => $plant->cat_name];
      $families[$plant->tid] = ['fid' => $plant->tid, 'name' => $plant->cat_name];
      if (strpos($existingPlant->family['name'], '(') !== false) {
        $existingPlant->family['cz_name'] = trim(str_replace(['(',')'],'',substr($existingPlant->family['name'], strpos($existingPlant->family['name'], '('))));
        $existingPlant->family['lat_name'] = trim(str_replace(['(',')'],'',substr($existingPlant->family['name'], 0, strpos($existingPlant->family['name'], '('))));
        $existingPlant->family['name'] = $existingPlant->family['cz_name'];
        $families[$plant->tid] = ['cid' => $plant->tid, 'name' =>$existingPlant->family['name'], 'name_lat' => $existingPlant->family['lat_name']];
      } else {
        $families[$plant->tid] = ['cid' => $plant->tid, 'name' =>$existingPlant->family['name'], 'name_lat' => $existingPlant->family['name']];
      }
    }
}
echo 'total '.count($plants).' plants'."\n";
echo 'total '.count($skipped).' skipped'."\n";
echo 'total '.count($knownTags).' tags created'."\n";
echo 'total '.count($knownCategories).' categories created'."\n";
echo 'total '.count($families).' families created'."\n";
echo '===============================' . "\n";
//tags
foreach ($knownTags as $tagId => $tag) {
  $q = "insert into tags (id, name, name_lowercase, type) values(".$tagId.",".$db->quote($tag).", ".$db->quote(strtolower($tag)).", 1)";
  $res = $db->query($q);
  if (!$res) {
    echo 'err: ' . $q . ': ' . var_export($db->errorInfo(), true) . "\n";
  }
}
//cats
$knownCategories[] = ['cid'=>4567, 'name' => 'Ostatni'];
foreach ($knownCategories as $cat) {
  $q = "insert into plant_category (id, name) values(".$cat['cid'].",".$db->quote($cat['name']).")";
  $res = $db->query($q);
  if (!$res) {
    echo 'err: ' . $q . ': ' . var_export($db->errorInfo(), true) . "\n";
  }
}
//families
$families['4577'] = ['name' => 'Neznama'];
foreach ($families as $fid => $fam) {
  if ($fid == null || $fam['name'] == null) {
    continue;
  }
  $name = ucfirst($fam['name']);
  $latName = '';
  if (isset($fam['name_lat'])) {
    $latName = ucfirst($fam['name_lat']);
  }
  $q = "insert into plant_family (id, name, name_lat) values(".$fid.",".$db->quote($name).", ".$db->quote($latName).")";
  $res = $db->query($q);
  if (!$res) {
    echo 'err: ' . $q . ': ' . var_export($db->errorInfo(), true) . "\n";
  }
}
//plants
foreach ($plants as $p) {
  $name = ucfirst($p->node_title);
  $nameLat = '';
  if (isset($p->lat_name)) {
    $nameLat = ucfirst($p->lat_name);
  }
  $categoryId = 4567;
  if (isset($p->category)) {
    $categoryId = $p->category['cid'];
  }
  $familyId = 4577;
  if (isset($p->family) && $p->family['fid'] != null) {
    $familyId = $p->family['fid'];
  }
  $now = strftime('%Y-%m-%d %H:%M:%S');
  $q = "insert into plants (id, name, name_lat, category_id, family_id, created_date, modified_date, is_deleted) values(".$p->nid.",".$db->quote($name).", ".$db->quote($nameLat).", ".$categoryId.", ".$familyId.", '".$now."', '".$now."', 0)";
  $res = $db->query($q);
  if (!$res) {
    echo 'err: ' . $q . ': ' . var_export($db->errorInfo(), true) . "\n";
  }

  if (isset($p->tags) && is_array($p->tags)) {
    foreach ($p->tags as $id => $name) {
      $q = "insert into plant_tags(plant_id, tag_id) values(".$p->nid.",".$id.")";
      $res = $db->query($q);
      if (!$res) {
        echo 'err: ' . $q . ': ' . var_export($db->errorInfo(), true) . "\n";
      }
    }
  }

  if (isset($p->images) && is_array($p->images)) {
    foreach ($p->images as $image) {
      $now = strftime('%Y-%m-%d %H:%M:%S');
      $imageUrl = str_replace('public://', 'https://bz-uk.cz/sites/default/files/styles/medium/public/',$image['uri']);
      $q = "insert into image(name, url, created_date) values(".$db->quote($image['name']).",".$db->quote($imageUrl).", '".$now."')";
      $res = $db->query($q);
      if (!$res) {
        echo 'err: ' . $q . ': ' . var_export($db->errorInfo(), true) . "\n";
      }
      $imageId = $db->lastInsertId();
      $q = "insert into plant_image(plant_id, image_id) values(".$p->nid.", ".$imageId.")";
      $res = $db->query($q);
      if (!$res) {
        echo 'err: ' . $q . ': ' . var_export($db->errorInfo(), true) . "\n";
      }
    }
  }

  $q = "insert into plant_block(plant_id, content, `order`, type, content_plain) values(".$p->nid.",".$db->quote($p->body_value).",0, 0,".$db->quote(strip_tags($p->body_value)).")";
  $res = $db->query($q);
  if (!$res) {
    echo 'err: ' . $q . ': ' . var_export($db->errorInfo(), true) . "\n";
  }

}
die();
foreach ($plants as $p) {
  $imgs = [];
  if (isset($p->images)) {
    foreach ($p->images as $img) {
      $imgs[] = str_replace('public://', 'https://bz-uk.cz/sites/default/files/styles/medium/public/images/',$img['uri']);
    }
  }
  $tags = 0;
  if (isset($p->tags) && is_array($p->tags)) {
    $tags += count($p->tags);
  }
  echo "==================================================================\n";
  echo "ID: " .$p->nid . "\n";
  echo "Cz: " . $p->node_title . "\n";
  if (isset($p->lat_name)) {
    echo "Latin: " . $p->lat_name . "\n";
  }
  if (count($imgs)>0) {
    echo "images:" . join("\n" , $imgs) . "\n";
  }
  if (isset($p->tags) && is_array($p->tags)) {
    echo "tags:" . join("\n" , $p->tags) . "\n";
  }
  if (isset($p->category)) {
    echo "cat: " . $p->category['name'] . "\n";
    if (isset($p->category['lat_name'])) {
      echo "cat_lat: " . $p->category['lat_name'] . "\n";
    }
  }
}
