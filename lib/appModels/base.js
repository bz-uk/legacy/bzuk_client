'use strict';
const _ = require('lodash');

const NotFoundError = require('./../error/notFound');
const bookshelf     = require('./../database/local').bookshelf;
const Model         = require('./../database/local').bookshelf.Model
const Collection    = require('./../database/local').bookshelf.Collection;
const models = require('./../database/local').sequelize.models

module.exports = class BaseModel {
    constructor(data) {
        if (data instanceof Model) {
            this.dbModelInstance = data
        } else {
            let Model            = this.dbModel();
            this.dbModelInstance = new Model(data);
        }
        this.initModel();
    }

    dbModel() {
        throw new Error('not implemented');
    }

    initModel() {
        _.forEach(this.dbModelInstance.attributes, (value, name) => {
            Object.defineProperty(this, name, {
                get: function () {
                    return value
                }
            });
        });
    }



    static $loadOneBy(param, dbModelName, AppModelClass) {
        models[dbModelName].findOne(param)
        // let Model    = bookshelf.model(dbModelName);
        // let instance = new Model(param)
        // return instance.fetch()
        .then((model) => {
            return model;
        })
    }

    static $loadCollectionBy(param, dbModelName, AppModelClass) {
        let Model    = bookshelf.model(dbModelName);
        let instance = new Model(param)
        return instance.fetch()
        .then((model) => {
            if (model instanceof Collection) {
                let result = [];
                for (let i = 0; i < model.count; i++) {
                    result.push(new AppModelClass(model.get(i)));
                }
                return result;
            }
            return [new AppModelClass(model)];
        })
    }

    save() {
        if (this.dbModelInstance.isNew()) {
            return this.dbModelInstance.save();
        }
        if (this.dbModelInstance.hasChanged()) {
            return this.dbModelInstance.save(this.dbModelInstance.changed, {patch: true});
        }
    }

    refresh() {
        return this.dbModelInstance.refresh();
    }

    toJSON() {
        return this.dbModelInstance.toJSON();
    }
}