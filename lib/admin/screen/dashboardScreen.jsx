'use strict';
import React from 'react';
import ArticlesCountWidget from './home/articlesCountWidget';
import PlantsCountWidget from './home/plantsCountWidget';
import BaseScreenComponent from './baseScreenComponent';
import Screen from '../common/screen';
import I18n from './../i18n/i18n'

// let getScreenComponents = function (widgets) {

class HomeScreenComponent extends BaseScreenComponent {
    constructor(props) {
        super(props);
    }

    render() {
        let styles = {
            simplePage:      {
                padding:       0,
                margin:        '.5rem',
                display:       'flex',
                alignItems:    'center',
                flexDirection: 'column'
            },
            pageHeader:      {
                margin:          0,
                padding:         10,
                backgroundColor: 'rgba(0,0,0,0.3)',
                width:           '100%'
            },
            pageHeaderLabel: {
                textTransform: 'capitalize'
            },
            pageContent:     {
                padding: '0 10px 10px 10px'
            },
            grid: {
                width: '100%'
            }
        }
        return (
            <div style={styles.simplePage}>
                <div className="grid-4_xs-2_noBottom" style={styles.grid}>
                    {this.getWidgets()}
                </div>
            </div>
        )
    }

    getWidgets() {
        return this.props.widgets.map((Widget, idx) => {
            return (
                <div key={'widget' + idx} className="col">
                    <Widget key={idx}/>
                </div>
            )
        })
    }
}
// }

export default class DashboardScreen extends Screen {
    constructor(options) {
        super(options);
        this.widgets = [
            ArticlesCountWidget,
            PlantsCountWidget
        ];
    }

    getIconClass() {
        return 'icon-home';
    }

    getCode() {
        return 'dashboard'
    }

    getTitle() {
        return I18n.t('Dashboard');
    }

    getComponent() {
        let self = this;
        return class extends React.Component {
            render() {
                let props = {name: self.getTitle(), widgets: self.widgets, screenCode: self.getCode()};
                return (
                    <HomeScreenComponent {...props}></HomeScreenComponent>
                );
            }
        }
    }
}