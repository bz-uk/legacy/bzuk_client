ALTER TABLE `circuit`
ADD COLUMN `primary_count` INT NOT NULL DEFAULT 0 AFTER `map_overlay_id`,
ADD COLUMN `secondary_count` INT NOT NULL DEFAULT 0 AFTER `primary_count`;

ALTER TABLE `circuit`
ADD COLUMN `active` TINYINT NOT NULL DEFAULT 0 AFTER `secondary_count`;

ALTER TABLE `circuit`
ADD COLUMN `color` VARCHAR(10) NULL AFTER `active`;

CREATE TABLE `circuit_primary_points` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `name_en` VARCHAR(45) NULL,
  `active` TINYINT NOT NULL DEFAULT 0,
  `content` TEXT NULL,
  `content_en` TEXT NULL,
  `order` INT NULL,
  `plant_id` INT NULL,
  `image_id` INT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `circuit_primary_points`
  ADD COLUMN `circuit_id` INT NULL AFTER `image_id`;

CREATE TABLE `map_path` (
    `id` INT NOT NULL,
    `points_json` TEXT NOT NULL,
    `map_overlay_id` INT NULL,
    PRIMARY KEY (`id`));

ALTER TABLE `circuit`
      ADD COLUMN `image_id` INT NULL AFTER `color`;

CREATE TABLE `circuit_secondary_points` (
      `primary_point_id` VARCHAR(45) NOT NULL,
      `plant_id` VARCHAR(45) NOT NULL,
      PRIMARY KEY (`primary_point_id`, `plant_id`));

ALTER TABLE `circuit`
      ADD COLUMN `content` TEXT NULL AFTER `image_id`,
      ADD COLUMN `content_en` TEXT NULL AFTER `content`;

ALTER TABLE `circuit_primary_points`
      ADD COLUMN `plant_id` INT NULL AFTER `content_en`;

CREATE TABLE `primary_point_parts` (
        `id` INT NOT NULL,
        `primary_point_id` INT NOT NULL,
        `title` VARCHAR(255) NULL,
        `title_en` VARCHAR(255) NULL,
        `content` TEXT NULL,
        `content_en` TEXT NULL,
        `image_id` INT NULL,
        `order` INT NULL,
        PRIMARY KEY (`id`));

ALTER TABLE `circuit_primary_points`
        ADD COLUMN `order` INT NULL AFTER `plant_id`;

ALTER TABLE `circuit`
        DROP COLUMN `author`;

ALTER TABLE `circuit_primary_points`
        CHANGE COLUMN `order` `order` INT(11) NULL DEFAULT NULL AFTER `content_en`,
        ADD COLUMN `image_id` INT NULL AFTER `plant_id`;

ALTER TABLE `circuit_secondary_points`
        CHANGE COLUMN `primary_point_id` `primary_point_id` INT NOT NULL ,
        CHANGE COLUMN `plant_id` `plant_id` INT NOT NULL ,
        ADD COLUMN `circuit_id` INT NULL AFTER `plant_id`;

ALTER TABLE `circuit_secondary_points` 
        ADD COLUMN `active` TINYINT NOT NULL AFTER `circuit_id`;
