<?php
namespace botgarApi\circuit;

use Slim\Slim;

class GetOne {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($circuitId) {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT * FROM circuit where id=" . $circuitId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            return;
        }
        $output = [];
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
        	//add circuit
		      $output = $row;
          //image
          if ($row['image_id'] != null) {
            $sql = "SELECT * FROM image where id=" . $row['image_id'];
            $result = $db->query($sql);
            $imgRow = $result->fetch(\PDO::FETCH_ASSOC);
            if ($result) {
                $output['image'] = $imgRow;
            }
          }
          //markers
          $output['markers'] = [];
          $sql = 'select cpp.order as id,cpp.name,mm.latitude, mm.longitude from circuit_primary_points cpp inner join map_marker mm on mm.id=cpp.map_marker_id order by cpp.`order`';
          $result = $db->query($sql);
          if ($result) {
            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
              $output['markers'][] = $row;
            }
          }
        }
        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
