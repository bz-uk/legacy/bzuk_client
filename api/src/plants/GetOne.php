<?php
namespace botgarApi\plants;

use Slim\Slim;

class GetOne {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($plantId) {
        if (!is_numeric($plantId)) {
            $this->app->response->setStatus(400);
            return;
        }
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = 'SELECT `value` FROM settings WHERE name="baseUrl"';
        $result = $db->query($sql);
        $baseUrl = '';
        if ($result) {
            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                $baseUrl = $row['value'];
            }
        }

        $sql = "SELECT id, audio_cz, name, name_en,  name_de, name_sk, name_lat, sources, sources_en, occurrence, category_id as category, family_id as family, map_marker_id, map_overlay_id, is_active FROM plants WHERE id=" . $plantId;
        $result = $db->query($sql);
        if (!$result) {
            $response = ['error' => 'No plant found', 'debug' => $sql];
            $this->app->response->setBody(json_encode($response));
            $this->app->response->setStatus(404);
            return;
        }
        $output = $result->fetch(\PDO::FETCH_ASSOC);
        $output['is_active'] = $output['is_active']==1?true:false;

        $output['new_family'] = 'none';
        if (is_numeric($output['family']) && $output['family'] > 0) {
            $output['new_family'] = 'chosen';
        }
        $output['new_category'] = 'none';
        if (is_numeric($output['category']) && $output['category'] > 0) {
            $output['new_category'] = 'chosen';
        }
        $output['markers'] = [];

        //audio files
        $sql = "SELECT * FROM files WHERE id=" . $output['audio_cz'];
        $result = $db->query($sql);
        if ($result) {
            $audio = $result->fetch(\PDO::FETCH_ASSOC);
            $output['audio_cz_url'] = $audio['url'];
        }

        //image
        $output['images'] = [];
        $sql = "SELECT * FROM plant_image pli inner join image im on im.id=pli.image_id  WHERE pli.plant_id=" . $plantId . ' order by pli.`order` ASC';
        $result = $db->query($sql);
        if ($result) {
          while ($image = $result->fetch(\PDO::FETCH_ASSOC)) {
//            $image['authors'] = [];
//            $authorsSql = "SELECT a.* FROM image_author ia inner join authors a on a.id=ia.author_id  WHERE ia.image_id=" . $image['id'];
//            $authorsResult = $db->query($authorsSql);
//            if ($authorsResult) {
//                while ($author = $authorsResult->fetch(\PDO::FETCH_ASSOC)) {
//                    $image['authors'][] = $author;
//                }
//            }
              $image['preview_url'] = '//' . str_replace('{$BASE_URL}', rtrim($baseUrl, '/'), $image['preview_url']);
            $output['images'][] = $image;
          }
        }

        //marker
        if ($output['map_marker_id'] != null) {
          $sql = "SELECT * FROM map_marker WHERE id=" . $output['map_marker_id'];
        } else if ($output['map_overlay_id'] != null) {
          $sql = "SELECT * FROM map_marker WHERE map_overlay_id=" . $output['map_overlay_id'];
        }
        if ($output['map_marker_id'] != null || $output['map_overlay_id'] != null) {
          $result = $db->query($sql);
          if ($result) {
              while ($marker = $result->fetch(\PDO::FETCH_ASSOC)) {
                $marker['id'] = $marker['id'] + 0;
                $output['markers'][] = $marker;
              }
          } else {
              $output['error'] = $sql;
          }
        }

        //blocks
        $blocks = [];
        $sql = "SELECT * FROM plant_block WHERE plant_id=" . $plantId . ' AND deleted_at is null ORDER BY `order`';
        $result = $db->query($sql);
        if ($result) {
          while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
              $row['type'] += 0;
              if ($row['type'] == 1) {
                  $row['images'] = [];
                  $sql = "SELECT i.* FROM plant_block_image pbi INNER JOIN image i ON i.id=pbi.image_id WHERE plant_block_id=" . $row['id'] . ' ORDER BY pbi.`order` ASC';
                  $subResult = $db->query($sql);
                  if ($subResult) {
                      while ($imageRow = $subResult->fetch(\PDO::FETCH_ASSOC)) {
//                        $imageRow['authors'] = [];
//                        $authorsSql = "SELECT a.* FROM image_author ia inner join authors a on a.id=ia.author_id  WHERE ia.image_id=" . $imageRow['id'];
//                        $authorsResult = $db->query($authorsSql);
//                        if ($authorsResult) {
//                            while ($author = $authorsResult->fetch(\PDO::FETCH_ASSOC)) {
//                                $imageRow['authors'][] = $author;
//                            }
//                        }
                          $imageRow['preview_url'] = '//' . str_replace('{$BASE_URL}', rtrim($baseUrl, '/'), $imageRow['preview_url']);
                        $row['images'][] = $imageRow; 
                      }
                      $row['previewImages'] = $row['images']; 
                  }                  
              }
              $blocks[] = $row;
          }
        }
        $output['blocks'] = $blocks;
        $output['authors'] = [];
        $authorsSql = "SELECT a.* FROM plant_authors pa inner join authors a on a.id=pa.author_id  WHERE pa.plant_id=" . $output['id'] . ' AND a.is_deleted=0';
        $authorsResult = $db->query($authorsSql);
        if ($authorsResult) {
            while ($author = $authorsResult->fetch(\PDO::FETCH_ASSOC)) {
                $output['authors'][] = $author;
            }
        }
        //tags
        $tags = [];
        $sql = 'select t.* from plant_tags pt inner join tags t where t.id=pt.tag_id AND pt.plant_id=' . $plantId . ' AND t.type=1';
        $result = $db->query($sql);
        if ($result) {
          while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
              $tags[] = $row;
          }
        }
        $output['tags'] = $tags;

        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
