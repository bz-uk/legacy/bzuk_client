'use strict';
const AuthenticatedRoute = require('./authenticatedBackendRoute');
const Promise            = require('bluebird');
const debug              = require('debug')('route-auth');

const RouteRedirect = require('./../error/routeRedirect');

module.exports = class Route extends AuthenticatedRoute {
    constructor(options) {
        super(options);
        this.options.unauthorizedRedirectRoute = '/login'
    }
}