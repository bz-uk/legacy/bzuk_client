'use strict';
angular.module('adminApp').controller('circuitsListController', ['$scope', '$mdDialog', '$mdMedia', 'circuitsService', '$timeout', '$location', '$log',
    function($scope, $mdDialog, $mdMedia, circuitsService, $timeout, $location, $log) {
        //start models
        $scope.model = {
            circuits: circuitsService.circuitsList,
            query: {
                order: 'title',
                limit: 10,
                page: 1
            },
            selected: [],
            flashMessages: []
        }
        $scope.selected = [];
        $scope.limitOptions = [5, 10, 15];
        $scope.options = {
          rowSelection: false,
          multiSelect: false,
          autoSelect: false,
          decapitate: false,
          largeEditDialog: true,
          boundaryLinks: false,
          limitSelect: true,
          pageSelect: true
        };
        //end: models

        $scope.editCircuit = function(circuit, ev) {
					$location.url('circuits/' + circuit.id)
        };

        $scope.addCircuit = function() {
          $location.url('circuits/_')
        }

        //start: listeners
				circuitsService.onError($scope, function(event, err) {
					console.log('cntrl err - do nothing')
				})
				circuitsService.onAllCircuitsLoaded($scope, function(event, allcircuits) {
					$scope.model.circuits = allcircuits;
				})
        //end listeners
        // function success(articles) {
        //     $scope.model.circuits = articles;
        // }
        $scope.getCircuits = function() {
            $scope.model.promise = circuitsService.loadAllCircuits($scope.query).$promise;
        }
				$scope.getCircuits()

    }
]);
