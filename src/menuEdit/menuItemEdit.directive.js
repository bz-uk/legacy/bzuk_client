'use strict'
angular.module('adminApp').directive('menuItemEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'menuEdit/menuItemEdit.template.html',
		controller: 'menuItemEditController'
	};
});
