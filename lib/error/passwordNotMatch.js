'use strict';
module.exports = class PasswordNotMatch extends Error {
    constructor() {
        super('password not match');
    }
}