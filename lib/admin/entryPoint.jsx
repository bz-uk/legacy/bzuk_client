'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import {cyan500, cyan700, gray400, blue700, blue500} from 'material-ui/styles/colors';

import Webp from './../common/components/webp';
import RootPanel from './layout/rootPanel.jsx';
import application from './application';

injectTapEventPlugin();

const appTheme = getMuiTheme(Object.assign({}, darkBaseTheme, {
    palette: {
        primary1Color: blue500,
        primary2Color: blue700,
        primary3Color: gray400,
    }
}));

export default class Application extends React.Component {

    constructor(props) {
        super(props);

        this.state = {}
    }

    componentWillUnmount() {
        //
    }

    componentDidMount() {
        let loadingPlaceholder = document.querySelector('#loading-placeholder');
        loadingPlaceholder.classList.add('loading-invisible');
        setTimeout(() => {
            let appElement = document.querySelector('#entry-point');
            appElement.classList.remove('admin-invisible');
            loadingPlaceholder.parentNode.removeChild(loadingPlaceholder);
        }, 500);
        Webp.available()
        .then((available) => {
            this.state.webp = available;
            this.setState(this.state);
        })
    }


    render() {
        if (this.state.webp !== undefined) {
            return (
                <MuiThemeProvider muiTheme={appTheme}>
                    {application.getRouterComponent()}
                </MuiThemeProvider>
            );
        } else {
            return <div></div>;
        }
    }
}

ReactDOM.render(<Application/>, document.getElementById('entry-point'));