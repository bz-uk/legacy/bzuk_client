'use strict';
var authorsListController = function ($log, authorsService, plantsService, $mdDialog, $scope) {
    var ctrl = this;
    ctrl.model = {
        authors: [],
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }

    authorsService.onAuthorDeleted($scope, function(event, data) {
        ctrl.getAuthors(true);
    });

    // authorsService.onAuthorSaved($scope, function(event, data) {
    //     ctrl.getAuthors(true);
    // });

    ctrl.getAuthors = function (force) {
        ctrl.model.promise = authorsService.loadAllAsync(ctrl.query, force)
            .then(function(authors) {
                $log.debug(authors)
                ctrl.model.authors = authors;
            })
        //, function(authors) {$log.debug(authors); ctrl.model.authors = authors;});
    }

    ctrl.deleteAuthor = function (ev, author) {
        var confirm = $mdDialog.confirm()
            .title('Potvrzení')
            .textContent('Autor "' + author.name + '" bude natrvalo odebrán ze všech roslitn a smazán. Pokračovat?')
            .ariaLabel('Potvrzení')
            .targetEvent(ev)
            .ok('Ano')
            .cancel('Zrušit');
        $mdDialog.show(confirm).then(function () {
            authorsService.delete(author);
        }, function () {
            //
        });
    };

    ctrl.editAuthor = function (ev, author) {
        $mdDialog.show({
            controller: 'editAuthorDialogController',
            templateUrl: 'authors/editAuthorDialog.template.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            resolve: {
                author: function () {
                    return angular.copy(author)
                },
            },
            clickOutsideToClose: true,
            fullscreen: true
        })
            .then(function () {
                ctrl.getAuthors(true);
            }, function () {
                console.log('You cancelled the dialog.');
            });
    }

    ctrl.selected = [];
    ctrl.limitOptions = [5, 10, 15];
    ctrl.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    ctrl.getAuthors(true);
}

angular.module('adminApp').component('authorsList', {
    templateUrl: 'authors/authorsList.template.html',
    controller: authorsListController,
    bindings: {}
});