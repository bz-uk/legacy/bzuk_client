'use strict';
angular.module('adminApp').directive('circuitsList', function() {
	return {
		restrict : 'E',
		templateUrl : 'circuit/circuitsList.template.html',
		controller: 'circuitsListController'
	};
});