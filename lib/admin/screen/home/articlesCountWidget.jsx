'use strict';
import React from 'react';
import axios from 'axios';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import ServerRequest from '../../common/serverRequest';
import I18n from './../../i18n/i18n';

export default class ArticlesCountWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            articlesCount: null
        };
    }

    getWidgetContent() {
        return (
            <div>
                {I18n.t('Articles:')} {this.state.articlesCount}
            </div>
        )
    }

    render() {
        let styles = {
            widgetTitle: {
                marginLeft: '.5rem'
            },
            card:        {
                opacity: this.state.articlesCount!==null ? '1' : '.1'
            }
        }
        return (
            <Card style={styles.card}>
                <CardTitle
                    title={<div><span className="icon-doc"></span><span style={styles.widgetTitle}>{I18n.t('Articles')}</span>
                    </div>}/>
                <CardText>
                    {this.state.articlesCount === null ?
                        <div>{I18n.t('Loading...')}</div>
                        :
                        this.getWidgetContent()
                    }
                </CardText>
            </Card>

        );
    }

    componentDidMount() {
        let req = new ServerRequest('widget-data/articles-count');
        req.get()
        .then((response) => {
            this.state.articlesCount = response.articlesCount;
            this.setState(this.state);
        });
    }
}