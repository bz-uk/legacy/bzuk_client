angular.module('adminApp').controller('leftMenuController', ['$scope', '$mdMedia', '$location', '$window', 'leftMenuService', '$mdDialog', 'settingsService',
    function ($scope, $mdMedia, $location, $window, leftMenuService, $mdDialog, settingsService) {
        settingsService.onSettingsLoaded($scope, function (event, settings) {
            $scope.settings = settings
        })
        $scope.MENU_ARTICLES = "articles";
        $scope.MENU_CIRCUITS = "circuits";
        $scope.MENU_DOWNLOADS = "downloads";
        $scope.MENU_MENUS = "menus";
        $scope.MENU_PLANTS = "plants";
        $scope.MENU_GALLERIES = "galleries";
        $scope.MENU_TAGS = "tags";
        $scope.MENU_CATEGORIES = "categories";
        $scope.MENU_AUTHORS = "authors";
        $scope.MENU_FAMILIES = "families";
        $scope.showLabels = true;
        if ($window.localStorage.getItem('left-menu-show-labels') !== null && $window.localStorage.getItem('left-menu-show-labels') !== undefined && $window.localStorage.getItem('left-menu-show-labels') !== '') {
            try {
                $scope.showLabels = angular.fromJson($window.localStorage.getItem('left-menu-show-labels'))
            } catch (e) {
                $log.debug('Unable to get left-menu-show-labels from local storage');
            }
        }

        $scope.toggleMenuSize = function () {
            $scope.showLabels = !$scope.showLabels;
            $window.localStorage.setItem('left-menu-show-labels', angular.toJson($scope.showLabels));
        }

        $scope.open = function (path, isFullUrl) {
            if (leftMenuService.getOpenConfirmation() === undefined) {
                if (isFullUrl) {
                    $window.open(path, '_self');
                } else {
                    $location.path(path);
                }
            } else {
                var confirm = $mdDialog.confirm()
                .title('Navigace')
                .textContent(leftMenuService.getOpenConfirmation())
                .ariaLabel('Navigace')
                .ok('Ano')
                .cancel('Ne');
                $mdDialog.show(confirm).then(function () {
                    leftMenuService.clearOpenConfirmation();
                    if (isFullUrl) {
                        $window.open(path, '_self');
                    } else {
                        $location.path(path);
                    }
                }, function () {
                });
            }
        }

        $scope.toggleSideMenu = function (menuItemCode) {
            if (menuItemCode === 'dashboard') {
                $scope.open('/', true);
                return;
            }
            if (menuItemCode == $scope.MENU_ARTICLES) {
                $scope.open('/articles');
                return;
            }
            if (menuItemCode == $scope.MENU_PLANTS) {
                $scope.open('/plants');
                return;
            }
            if (menuItemCode == $scope.MENU_TAGS) {
                $scope.open('/tags');
                return;
            }
            if (menuItemCode == $scope.MENU_CATEGORIES) {
                $scope.open('/categories');
                return;
            }
            if (menuItemCode == $scope.MENU_FAMILIES) {
                $scope.open('/families');
                return;
            }
            if (menuItemCode == $scope.MENU_FAMILIES) {
                $scope.open('/families');
                return;
            }
            if (menuItemCode == $scope.MENU_AUTHORS) {
                $scope.open('/authors');
                return;
            }
            if (menuItemCode == $scope.MENU_GALLERIES) {
                $scope.open('/galleries');
                return;
            }
            if (menuItemCode == $scope.MENU_CIRCUITS) {
                $scope.open('/circuits');
                return;
            }
            if (menuItemCode == $scope.MENU_MENUS) {
                $scope.open('/menus');
                return;
            }
            if (menuItemCode == $scope.MENU_DOWNLOADS) {
                $scope.open('/downloads');
                return;
            }
        };
    }]);
