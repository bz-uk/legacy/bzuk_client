<?php

namespace botgarApi\plants;

use Slim\Slim;

class DeleteOne
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($plantId)
    {
        if (!is_numeric($plantId)) {
            $this->app->response->setStatus(400);

            return;
        }
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $sql = 'UPDATE plants SET is_deleted=1 WHERE id='.$plantId;
        $result = $db->query($sql);
        if (!$result) {
            $response = ['error' => 'No plant found', 'debug' => $sql];
            $this->app->response->setBody(json_encode($response));
            $this->app->response->setStatus(404);

            return;
        }
        $response = json_encode(['status' => 'ok']);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
