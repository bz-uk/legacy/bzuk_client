<?php

namespace botgarApi\image;

use Slim\Slim;

class Get
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function createName($file)
    {
        $name = uniqid('img-' . date('Ymd') . '-');
        if ($file['type'] === 'image/jpeg' || substr($file['url'], strlen($file['url']) - 3) === 'jpg') {
            $name .= '.jpg';
        }
        if ($file['type'] === 'image/png' || substr($file['url'], strlen($file['url']) - 3) === 'png') {
            $name .= '.png';
        }
        if ($file['type'] === 'image/gif' || substr($file['url'], strlen($file['url']) - 3) === 'gif') {
            $name .= '.gif';
        }
        return $name;
    }

    private function resample($imageName, $maxWidth = null) {
        $resizedImageName = substr($imageName, 0, strlen($imageName) - 4) . '_max_w_' . $maxWidth . substr($imageName, strlen($imageName) - 4);
        $sizeInfo = getimagesize($imageName);
        if (strtolower(substr($imageName, strrpos($imageName, '.'))) === '.jpg') {
            $image = imagecreatefromjpeg($imageName);
        } else {
            $image = imagecreatefrompng($imageName);
        }
        if ($maxWidth === null) {
            $maxWidth = $sizeInfo[0];
            $maxHeight = $sizeInfo[1];
        } else {
            $ratio = $sizeInfo[1] / $sizeInfo[0]; //h/w
            $maxHeight = $maxWidth * $ratio;
        }
        $resizedImage = imagecreatetruecolor($maxWidth, $maxHeight);
        imagealphablending($resizedImage, false);
        imagesavealpha($resizedImage, true);
        $transparent = imagecolorallocatealpha($resizedImage, 255, 255, 255, 127);
        imagefilledrectangle($resizedImage, 0, 0, $maxWidth, $maxHeight, $transparent);
        imagecopyresampled($resizedImage, $image, 0, 0, 0, 0, $maxWidth, $maxHeight, $sizeInfo[0], $sizeInfo[1]);
        if (strtolower(substr($imageName, strrpos($imageName, '.'))) === '.jpg') {
            imagejpeg($resizedImage, $resizedImageName, 90);
        } else {
            imagepng($resizedImage, $resizedImageName, 9);
        }
        return $resizedImageName;
    }

    private function prepareWebp($imageName)
    {
        try {
            $cmd = 'cwebp -q 80 ' . $imageName . ' -o ' . substr($imageName, 0, strrpos($imageName, '.')) . '.webp > /dev/null &';
            $outputfile = '/dev/null';
            $pidfile = '/dev/null';
            $execCmd = sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile);
            exec($execCmd);
        } catch (\Exception $e) {
            //well... oka
        }
    }

    public function run($imageId, $maxWidth = null, $webp = false)
    {
        $sizes = [100, 150, 200, 250, 384, 512, 640, 800, 1024, 1280, 2048];
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $sql = "select * from settings where name='baseUrl'";
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setBody(json_encode(['error' => 'unable to get baseurl', 'debug' => $sql]));
            $this->app->response->setStatus(400);
            return;
        }
        $row = $result->fetch();
        $baseUrl = $row['value'];

        $sql = 'SELECT * FROM image WHERE id=' . $imageId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setBody(json_encode(['error' => 'unable to get image', 'debug' => $sql]));
            $this->app->response->setStatus(404);

            return;
        }

        $fileInfo = $result->fetch();

        if ($fileInfo['path'] == null) { //compatibility
            if (file_exists('../data/img/' . $fileInfo['name'])) {
                $response = file_get_contents('../data/img/' . $fileInfo['name']);
                $newName = $this->createName($fileInfo);
                file_put_contents('../data/img/' . $newName, $response);
                $sizeInfo = getimagesize('../data/img/' . $newName);
                $fileSize = filesize('../data/img/' . $newName);
                $type = 'image/' . substr($newName, strlen($newName) - 3);
                unlink('../data/img/' . $fileInfo['name']);
                $sql = 'UPDATE image SET path="../data/img/' . $newName . '", type="' . $type . '",size=' . $fileSize . ', height=' . $sizeInfo[1] . ', width=' . $sizeInfo[0] . ', name="' . $newName . '", url="//' . $baseUrl . 'api/api/images/' . $imageId . '" WHERE id=' . $imageId;
                $result = $db->query($sql);
                if (!$result) {
                    error_log('Unable to resave image id ' . $imageId . ': ' . $sql);
                }
            } else {
                $response = file_get_contents($fileInfo['url']);
                $newName = $this->createName($fileInfo);
                file_put_contents('../data/img/' . $newName, $response);
                $sizeInfo = getimagesize('../data/img/' . $newName);
                $fileSize = filesize('../data/img/' . $newName);
                $type = 'image/' . substr($newName, strlen($newName) - 3);
                $sql = 'UPDATE image SET path="../data/img/' . $newName . '", type="' . $type . '",size=' . $fileSize . ',height=' . $sizeInfo[1] . ', width=' . $sizeInfo[0] . ', name="' . $newName . '", url="//' . $baseUrl . 'api/api/images/' . $imageId . '" WHERE id=' . $imageId;
                $result = $db->query($sql);
                if (!$result) {
                    error_log('Unable to resave image id ' . $imageId . ': ' . $sql);
                }
            }
        } else {
            $name = $fileInfo['path'];
            $sizeInfo = getimagesize($fileInfo['path']);
            if ($maxWidth !== null && is_numeric($maxWidth) && in_array($maxWidth, $sizes)) {
                if ($maxWidth < $sizeInfo[0]) {
                    $webpImageName = '../data/img/' . substr($fileInfo['name'], 0, strpos($fileInfo['name'], '.')) . '_max_w_' . $maxWidth . '.webp';
                    $imageName = $this->resample($name, $maxWidth);
                } else {
                    $webpImageName = '../data/img/' . substr($fileInfo['name'], 0, strpos($fileInfo['name'], '.')) . '.webp';
                    $imageName = $fileInfo['path'];
                }
                if ($webp && file_exists($webpImageName)) {
                    $response = file_get_contents($webpImageName);
                    $size = strlen($response);
                    $this->app->response->header('Content-Type', 'content-type: image/webp');
                    $this->app->response->header('Content-Length', $size);
                    $this->app->response->header('Cache-Control', 'max-age=10');
                    echo $response;
                    return;
                }
                if (file_exists($imageName)) {
                    if ($maxWidth < 1024 && !file_exists($webpImageName)) {
                        $this->prepareWebp($imageName);
                    }
                    $response = file_get_contents($imageName);
                    $size = strlen($response);
                    $this->app->response->header('Content-Type', 'content-type: ' . $fileInfo['type']);
                    $this->app->response->header('Content-Length', $size);
                    $this->app->response->header('Cache-Control', 'max-age=10');
                    echo $response;
                    return;
                }
            }
            $webpName = substr($name, 0, strrpos($name, '.')) . '.webp';
            if ($webp && file_exists($webpName)) {
                $response = file_get_contents($webpName);
                $type = 'image/webp';
            } else {
                if ($sizeInfo[0] < 1024 && !file_exists($webpName)) {
                    $this->prepareWebp($name);
                }
                $response = file_get_contents($name);
                $type = $fileInfo['type'];
            }
            $this->app->response->header('Content-Type', $type);
            $this->app->response->header('Cache-Control', 'max-age=10');
            $size = strlen($response);
            $this->app->response->header('Content-Length', $size);
            echo $response;
            return;
        }
    }
}
