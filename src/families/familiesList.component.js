'use strict';
var familiesListController = function ($log, familiesService, $mdDialog, $scope) {
    var ctrl = this;
    ctrl.model = {
        families: familiesService.allPlantsFamilies,
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }

    ctrl.getFamilies = function (force) {
        ctrl.model.promise = familiesService.getAllFamiliesAsync(ctrl.query, force).then(function(families) {ctrl.model.families = families;})
    }

    ctrl.deleteFamily = function (ev, family) {
        var confirm = $mdDialog.confirm()
            .title('Potvrzeni')
            .textContent('Čeleď "' + family.name + '" bude natrvalo odebrána ze všech roslitn a smazána. Pokračovat?')
            .ariaLabel('Potvrzení')
            .targetEvent(ev)
            .ok('Ano')
            .cancel('Zrušit');
        $mdDialog.show(confirm).then(function () {
            familiesService.deleteFamily(family);
        }, function () {
            //
        });
    }

    ctrl.editFamily = function (ev, family) {
        $mdDialog.show({
            controller: 'editFamilyDialogController',
            templateUrl: 'plants/editFamilyDialog.template.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            resolve: {
                family: function () {
                    return angular.copy(family)
                },
            },
            clickOutsideToClose: true,
            fullscreen: true
        })
            .then(function () {
                $log.debug('family saved!')
                ctrl.getFamilies(true);
            }, function () {
                $log.debug('You cancelled the dialog.');
            });
    }

    ctrl.selected = [];
    ctrl.limitOptions = [5, 10, 15];
    ctrl.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    ctrl.getFamilies(true);
}

angular.module('adminApp').component('familiesList', {
    templateUrl: 'families/familiesList.template.html',
    controller: familiesListController,
    bindings: {}
});