<?php

namespace botgarApi\plants;

use Slim\Slim;

class GetAllTags
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run()
    {
      error_log('Getting all tags');
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        //TODO: do not use join!
        $sql = 'SELECT * FROM tags WHERE type=1';

        $result = $db->query($sql);
        if ($result) {
            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                $row['system'] = $row['system'] + 0;
                $tags[] = $row;
            }
        } else {
            $response = ['error' => 'Problem getting plant tags', 'debug' => $sql];
            $this->app->response->setBody(json_encode($response));
            $this->app->response->setStatus(400);
        }

        $response = json_encode($tags);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
