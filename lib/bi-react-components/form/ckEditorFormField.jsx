'use strict';
import React from 'react';
import FormField from './formField';

export default class CkEditorFormField extends FormField {
    getWrappedElement() {
        return React.cloneElement(this.props.children, {
            defaultValue: this.state.value,
            errorText: this.state.error,
            onChange: this.onChange
        });
    }
}