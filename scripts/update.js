const process = require('process');
const umzug = require('./../lib/migration/umzug');

umzug.pending()
.then((updates) => {
    if (updates.length === 0) {
        console.log('Application is up to date');
        process.exit(0);
    }
    console.log('There are ' + updates.length + ' updates waiting...');
    return umzug.up();
})
.then((result) => {
    console.log(result.length + ' update steps finished');
    process.exit(0);
})

