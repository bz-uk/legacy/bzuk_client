'use strict'
//controller
var singleBlockPlantContent = function ($mdDialog, $log, $scope) {
    $log.debug('blocks edit', this.block, this.blockIdx)
    this.editMode = false;
    this.editBlockContent = ''
    this.editBlockContentEnglish = ''
    this.editBlockColor = ''

    var ctrl = this;

    ctrl.cancelEdit = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Edit cancel confirmation')
            .textContent('Are you sure you want to cancel editation? All changed content will be lost.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            ctrl._clearTempVariables()
            ctrl.editMode = false;
            $scope.$parent.$emit('edit_enabled', false);
            $scope.$emit('show-mode-change', 'show')
        });
    };
    ctrl._clearTempVariables = function () {
        ctrl.editBlockContent = '';
        ctrl.editBlockContentEnglish = '';
        ctrl.editBlockColor = '';
    }
    ctrl.editBlock = function () {
        ctrl._clearTempVariables();
        ctrl.editBlockContent = ctrl.block.content;
        ctrl.editBlockContentEnglish = ctrl.block.content_en
        ctrl.editBlockColor = ctrl.block.backgroud_color;
        ctrl.editMode = true
        $scope.$emit('show-mode-change', 'edit')
        $scope.$parent.$emit('edit_enabled', true);
    }
    ctrl.saveBlock = function () {
        ctrl.block.content = ctrl.editBlockContent
        ctrl.block.content_en = ctrl.editBlockContentEnglish
        ctrl.block.backgroud_color = ctrl.editBlockColor
        ctrl._clearTempVariables()
        ctrl.editMode = false;
        $scope.$parent.$emit('edit_enabled', false);
        $scope.$emit('show-mode-change', 'show')
    }
    ctrl.moveUp = function() {
        $scope.$emit('move-item-up', ctrl.blockIdx)
    }
    ctrl.moveDown = function() {
        $scope.$emit('move-item-down', ctrl.blockIdx)
    }
    ctrl.deleteBlock = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Delete block confirmation')
            .textContent('Are you sure you want to delete this block? All block content will be lost.')
            .ariaLabel('Warning')
            .targetEvent(ev !== undefined ? ev : null)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            $scope.$emit('delete-item', ctrl.blockIdx)
        });
    }
}
//definition
angular.module('adminApp').component('singleBlockPlantContent', {
    templateUrl: 'plants/content/singleBlockPlantContent.template.html',
    controller: singleBlockPlantContent,
    bindings: {
        block: '=',
        blockIdx: '=',
        isFirst: '=',
        isLast: '=',
        showLanguage: '=',
    }
});