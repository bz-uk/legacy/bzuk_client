'use strict'
//controller
var singleBlockPlantContent = function ($mdDialog, $log, $scope, settingsService) {
    var ctrl = this;
    ctrl.editMode = false;
    ctrl.editBlockImages = '';
    ctrl.visibleIndex = 0;
    ctrl.imgViewStyle = {
        'object-position': '50% 50%',
        'position': 'relative',
        'object-fit': 'cover',
        'height': '300px',
        'width': '100%'
    }
    ctrl.pictureViewStyle = {
        'position': 'relative',
        'width': '100%'
    }

    settingsService.load(function (data) {
        $scope.globalSettings = data
    })

    //ng-class="{'plant-edit-thumb-container-contain':$ctrl.block.display==='contain','plant-edit-thumb-container-cover':$ctrl.block.display==='cover'||$ctrl.block.display===undefined||$ctrl.block.display===''||$ctrl.block.display===null,'plant-edit-thumb-container-none':$ctrl.block.display==='none'}"
    console.log('imgs block ', this.block);

    ctrl.applyImgStyle = function () {
        console.log('DDDD', this.block);
        if (!this.block) {
            return;
        }
        if (this.block.display === 'cover' || this.block.display === 'none' || this.block.display === 'contain') {
            this.imgViewStyle['object-fit'] = this.block.display;
        }
        if (ctrl.block.images[ctrl.visibleIndex].display === 'cover' || ctrl.block.images[ctrl.visibleIndex].display === 'none' || ctrl.block.images[ctrl.visibleIndex].display === 'contain') {
            this.imgViewStyle['object-fit'] = ctrl.block.images[ctrl.visibleIndex].display;
        }

        if (this.block.display === 'contain' || ctrl.block.images[ctrl.visibleIndex].display === 'contain') {
            this.pictureViewStyle['margin-left'] = 'auto';
            this.pictureViewStyle['margin-right'] = 'auto';
        }
        if (ctrl.block.images[ctrl.visibleIndex].display !== this.block.display) {
            this.pictureViewStyle.width = ctrl.block.images[ctrl.visibleIndex].display;
        }
        if (this.block.display_vertical_offset >= 0 && this.block.display_vertical_offset <= 100) {
            this.imgViewStyle['object-position'] = '50% ' + this.block.display_vertical_offset + '%';
        }
        if ((ctrl.block.images[ctrl.visibleIndex].display === 'cover' || ctrl.block.images[ctrl.visibleIndex].display === 'none') &&
            ctrl.block.images[ctrl.visibleIndex].display_vertical_offset >= 0 && ctrl.block.images[ctrl.visibleIndex].display_vertical_offset <= 100) {
            this.imgViewStyle['object-position'] = '50% ' + ctrl.block.images[ctrl.visibleIndex].display_vertical_offset + '%';
        }
    }
    setTimeout(function() {
        ctrl.applyImgStyle();
    }, 10);

    ctrl.cancelEdit = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Edit cancel confirmation')
            .textContent('Are you sure you want to cancel editation? All changed content will be lost.')
            .ariaLabel('Warning')
            .targetEvent(ev)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            ctrl._clearTempVariables()
            ctrl.editMode = false;
            $scope.$parent.$emit('edit_enabled', false);
            $scope.$emit('show-mode-change', 'show')
        });
    };
    ctrl._clearTempVariables = function () {
        ctrl.editBlockImages = [];
    }
    ctrl.editBlock = function () {
        ctrl._clearTempVariables();
        ctrl.editBlockImages = ctrl.block.images
        ctrl.editMode = true
        $scope.$parent.$emit('edit_enabled', true);
        $scope.$emit('show-mode-change', 'edit')
    }
    ctrl.saveBlock = function () {
        ctrl.block.images = angular.copy(ctrl.editBlockImages)
        ctrl._clearTempVariables();
        ctrl.applyImgStyle();
        ctrl.editMode = false;
        $scope.$parent.$emit('edit_enabled', false);
        $scope.$emit('show-mode-change', 'show')
    }
    ctrl.nextSlide = function () {
        ctrl.visibleIndex++;
        ctrl.applyImgStyle();
    }
    ctrl.prevSlide = function () {
        ctrl.visibleIndex--;
        ctrl.applyImgStyle();
    }
    ctrl.moveUp = function () {
        $scope.$emit('move-item-up', ctrl.blockIdx)
    }
    ctrl.moveDown = function () {
        $scope.$emit('move-item-down', ctrl.blockIdx)
    }
    ctrl.getPreviewUrl = function(url) {
        return url.replace(/\{\$BASE_URL\}/, $scope.globalSettings.baseUrl);
    }
    ctrl.deleteBlock = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Delete block confirmation')
            .textContent('Are you sure you want to delete this block? All block content will be lost.')
            .ariaLabel('Warning')
            .targetEvent(ev !== undefined ? ev : null)
            .ok('Ok')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            $scope.$emit('delete-item', ctrl.blockIdx)
        });
    }
}
//definition
angular.module('adminApp').component('singleBlockPlantImages', {
    templateUrl: 'plants/content/singleBlockPlantImages.template.html',
    controller: singleBlockPlantContent,
    bindings: {
        block: '=',
        blockIdx: '=',
        isFirst: '=',
        isLast: '=',
    }
});