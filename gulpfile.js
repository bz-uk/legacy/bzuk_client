/* File: gulpfile.js */
'use strict';

// grab our gulp packages
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat');

var rename = require('gulp-rename');
var replace = require('gulp-replace');
var babelify = require('babelify')
var browserify = require('browserify')
var vinylSourceStream = require('vinyl-source-stream')
var vinylBuffer = require('vinyl-buffer')
var sourcemaps = require('gulp-sourcemaps')
var debug = require('gulp-debug');
const webpack = require('webpack');
const plumber = require('gulp-plumber');
const paths = require('./build.paths');
const webpackStream = require('webpack-stream');

//consts
var jsMergedFileName = 'app_merged.js'
//

var templateCache = require('gulp-angular-templatecache');

gulp.task('templates', ['js'], function () {
    return gulp.src('src/**/*.template.html').pipe(templateCache({
        module: 'adminApp'
    })).pipe(gulp.dest('public'));
});

gulp.task('styles', ['js'], function () {
    var stream = gulp.src('./src/sass/**/*.scss')
    .pipe(sass({
        outFile: 'styles.css'
    }).on('error', sass.logError))
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('./public'));
    return stream
});

function onWarning(error) {
    gutil.log(error.message);
}

gulp.task('libs', function () {
    return gulp.src(['bower_components/jquery/dist/jquery.min.js',
        'jqueryui/jquery-ui.min.js',
        'node_modules/angular/angular.js',
        'node_modules/angular-resource/angular-resource.js',
        'node_modules/angular-material-icons/angular-material-icons.js',
        'node_modules/angular-animate/angular-animate.js',
        'node_modules/angular-aria/angular-aria.js',
        'node_modules/angular-route/angular-route.js',
        'node_modules/angular-ui-sortable/dist/sortable.min.js',
        'resources/js/lodash.min.js',
        'node_modules/angular-simple-logger/dist/angular-simple-logger.min.js',
        'node_modules/angular-google-maps/dist/angular-google-maps.min.js',
        'node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js',
        'node_modules/ng-file-upload/dist/ng-file-upload.min.js',
        'node_modules/angular-sanitize/angular-sanitize.min.js',
        'node_modules/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js',
        'node_modules/bluebird/js/browser/bluebird.min.js',
        'public/qrcode.js',
        'node_modules/angular-qrcode/angular-qrcode.js',
        'node_modules/angular-ckeditor/angular-ckeditor.js',
        'node_modules/angular-material/angular-material.js',
        'node_modules/angular-material-data-table/dist/md-data-table.min.js',
        'node_modules/angular-audio/app/angular.audio.js',
        'node_modules/angular-cookies/angular-cookies.min.js'])
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('./public/'))
    .on('error', onWarning);
})

gulp.task('js', function () {
    var stream = gulp.src(['./src_init/*.js', './src/**/*.js'])
    .pipe(concat(jsMergedFileName))
    .pipe(gulp.dest('./public/'))
    console.log('JS sources merged into ' + jsMergedFileName)
    return stream
});

gulp.task('js_version', ['js_compile'], function () {
    var time = new Date().getTime();
    gulp.src('./public/app.min.js')
    .pipe(rename('app_' + time + '.min.js'))
    .pipe(gulp.dest('./public'));
    gulp.src('./index.template.php')
    .pipe(rename('index.php'))
    .pipe(replace(/\{\{timestamp\}\}/g, time))
    .pipe(gulp.dest(''));
})

gulp.task('js_compile', ['js', 'styles', 'templates'], function () {
    var sources = browserify({
        entries: ['public/' + jsMergedFileName, 'public/templates.js'],
        debug: false
    })
    .transform(babelify.configure())

    return sources.bundle()
    .pipe(vinylSourceStream('app.min.js'))
    .pipe(vinylBuffer())
    // Do stuff to the output file
    .pipe(gulp.dest('public'));
});

gulp.task('build', ['js', 'templates', 'styles', 'js_version', 'libs']);
