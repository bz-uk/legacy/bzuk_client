"use strict";
const process = require('process');
const sequelize = require('./../lib/database/local').sequelize;
const _ = require('lodash');
const knex = require('./../lib/database/local').knex;

let regenerate = async function () {
    try {
        await sequelize.transaction(async function (t) {
            let query = 'SELECT plants.id as plant_id, tags.* from plants left join plant_tags on plants.id=plant_tags.plant_id left join tags on tags.id=plant_tags.tag_id where tags.id is not null';
            let rows = await sequelize.query(query, {type: sequelize.QueryTypes.SELECT, transaction: t});
            let plants = {};
            rows.map((row) => {
                if (!plants[row.plant_id]) {
                    console.log('create for ', row);
                    plants[row.plant_id] = [];
                }
                row.code = _.snakeCase(row.name);
                plants[row.plant_id].push(row);
                delete row.plant_id;
            })
            let queue = Promise.resolve();
            Object.keys(plants).map((plantId) => {
                let query = 'UPDATE plants SET tags=? WHERE plants.id=?';
                queue = queue.then(() => {
                    return sequelize.query(query, {
                        replacements: [JSON.stringify(plants[plantId]), plantId],
                        transaction: t
                    });
                })
            })
            await queue;
        });
    } catch (e) {
        console.error('Unable to regenerate plants tags: ', e)
    }
};

regenerate()
.then(() => {
    process.exit(0);
});

