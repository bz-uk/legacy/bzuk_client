'use strict';
module.exports = class NotFound extends Error {
    constructor(entityName) {
        super(entityName + ' not found');
        this.entityName = entityName;
    }
}