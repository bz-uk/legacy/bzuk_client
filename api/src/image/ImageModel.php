<?php
namespace botgarApi\image;

/**
 * Created by PhpStorm.
 * User: juraj
 * Date: 21.11.15
 * Time: 22:28
 */

class ImageModel
{
    private $id;
    private $name;
    private $content;
    private $author;
    private $dateCreated;

    public function loadFromXml(\SimpleXMLElement $xml)
    {
        $this->id = (String)$xml->id;
        $this->name = (String)$xml->name;
        $this->author = (String)$xml->author;
        $this->dateCreated = (String)$xml->dateCreated;
    }

    /*
    * @return array
    */
    public function asArray()
    {
        return array(
            'id' => (String)$this->id,
            'name' => (String)$this->name,
            'content' => $this->content
        );
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }
}