'use strict';
import _ from 'lodash';
import BaseValidator from './baseValidator';
import isEmail from 'validator/lib/isEmail';

export default class EmailValidator extends BaseValidator {
    constructor(options) {
        let defaultOptions = {errorMessage: 'Value is not valid email'}
        super(_.assign(defaultOptions, options));
    }
    
    isValid(value) {
        return !!value && isEmail(value);
    }
}