'use strict';
import React from 'react';
import Screen from '../common/screen';

class MyProfileScreenComponent extends React.Component {
    render() {
        return (
            <div>about meee</div>
        )
    }
}

export default class MyProfileScreen extends Screen{
    constructor(options) {
        super(options)
    }

    getTitle() {
        return 'My profile'
    }

    getIconClass() {
        return 'icon-user';
    }

    getComponent() {
        return MyProfileScreenComponent;
    }
}