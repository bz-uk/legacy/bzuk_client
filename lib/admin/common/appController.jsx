'use strict';
import React from 'react';
import {Router, Route, IndexRoute, hashHistory} from 'react-router'
import _ from 'lodash';
import eventbus from './../eventBus';
import LoggedUser from './../services/loggedUser'

export default class AppController {
    constructor() {
        this.screens           = {};
        this.defaultScreenCode = null;
        this.activeScreen = null;
    }

    registerScreen(screen) {
        this.screens[screen.getCode()] = screen;
    }

    getScreens() {
        return this.screens;
    }


    getRouterComponent() {
        return (
            <Router history={hashHistory}>
                <Route path="/" component={this.layout.getComponent(this)}>
                    <IndexRoute onEnter={(routeCode, redirect) => {this.onEnterRoute(this.screens[this.defaultScreenCode], redirect)}} component={this.screens[this.defaultScreenCode].getComponent()}/>
                    {this.getRoutes()}
                </Route >
            </Router >
        )
    }

    onEnterRoute(screen, replace) {
        if (screen.requirePermission() && !LoggedUser.hasPermission(screen.requirePermission())) {
            console.warn('User does not have sufficient permission to screen ', screen.getCode());
            eventbus.emit('error-message', 'Insufficient permissions to open screen ' + screen.getTitle());
            console.log('replace', replace);
            return replace('/');
        }
        eventbus.emit('screen-activated', screen);
        this.activeScreen = screen;
    }

    getRoutes() {
        let routes = [];
        _.forEach(this.screens, (screen, screenCode) => {
            routes.push(<Route onEnter={(routeCode, redirect) => {this.onEnterRoute(screen, redirect)}} key={screenCode} path={'/' + screenCode} component={screen.getComponent()}/>)
        });
        return routes;
    }

    setDefaultScreenCode(code) {
        let screenFound;
        _.forEach(this.screens, (screen, screenCode) =>{
            if (screenCode === code) {
                screenFound = true;
            }
        });
        if (screenFound) {
            this.defaultScreenCode = code;
            return
        }
        throw new Error(`Screen with code ${code} not found`);
    }

    setLayout(layout) {
        this.layout = layout;
    }
}