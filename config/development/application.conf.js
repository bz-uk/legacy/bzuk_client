let configuration = module.exports = {
    listen: {
        port: 9001
    },
    baseUrl: 'https://admin.bz-uk.roamingowl.local/',
    storage: {
        mysql: {
            host: '127.0.0.1',
            user: 'root',
            password: 'root',
            database: 'bzuk',
            tablePrefix: 'ro2_'
        },
        memcache: {
            host: '127.0.0.1:11211'
        }
    },
    session: {
        secret: 'yBGlXsWx6c3x1Dg4zAF8QHtlfrGG4slLGSr11GnWpbFftPT58LcoWuu8TqjHukDC',
        resave: true,
        saveUninitialized: false,
        cookie: {}
    }
}
