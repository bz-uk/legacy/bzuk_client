<?php
namespace botgarApi\circuit;

use Slim\Slim;

class GetOnePrimaryPoint {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($circuitId, $pointId) {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT * FROM circuit where id=" . $circuitId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            $this->app->response->setBody(['error' => 'unable to load circuit']);
            return;
        }
        $circuit = $result->fetch(\PDO::FETCH_ASSOC);

        $sql = "SELECT * FROM circuit_primary_points where circuit_id=" . $circuitId . ' and id=' . $pointId;

        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            //$err = var_export($db->errorInfo(), true);
            $this->app->response->setBody(json_encode(['error' => 'unable to load point', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }
        $point = $result->fetch(\PDO::FETCH_ASSOC);
        error_log(var_export($point, true));
        //image
        if ($point['image_id'] != null) {
          $sql = "SELECT * FROM image where id=" . $point['image_id'];
          $result = $db->query($sql);
          $imgRow = $result->fetch(\PDO::FETCH_ASSOC);
          if ($result) {
              $point['image'] = $imgRow;
          }
        }
        //parts
        $point['parts'] = [];
        $sql = 'select * from primary_point_parts where primary_point_id=' . $pointId;
        $result = $db->query($sql);
        if ($result) {
          while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $point['parts'][] = $row;
          }
        }
        //markers
        $point['markers'] = [];
        if (isset($point['map_marker_id']) && $point['map_marker_id'] > 0) {
          $sql = 'select * from map_marker where id='.$point['map_marker_id'];
          $result = $db->query($sql);
          if ($result) {
            $point['markers'][] = $result->fetch(\PDO::FETCH_ASSOC);
          }
        }        

        $response = json_encode($point);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
