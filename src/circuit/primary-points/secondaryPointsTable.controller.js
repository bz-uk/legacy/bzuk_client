'use strict';
angular.module('adminApp').controller('secondaryPointsTableController', ['$scope', '$rootScope', '$log', '$mdDialog', '$mdMedia', 'circuitsService', '$timeout', '$routeParams', 'uiGmapIsReady', 'settingsService', 'plantsService', 'Upload', '$location',
    function($scope, $rootScope, $log, $mdDialog, $mdMedia, circuitsService, $timeout, $routeParams, uiGmapIsReady, settingsService, plantsService, Upload, $location) {
      $log.debug('loading sec. points for ' + $scope.circuitId + '/' + $scope.primaryPointId)
      //start: models
      $scope.mode = 'list'
      $scope.filter = {
          category: undefined,
          family: undefined,
          tags: [],
          search: undefined
      }
      $scope.secondaryPointsTable = {
          points: [],
          query: {
              order: 'id',
              limit: 10,
              page: 1
          },
          selected: [],
      }
      $scope.plantsTable = {
          plants: [],
          query: {
              order: 'id',
              limit: 10,
              page: 1
          },
          selected: [],
      }
      $scope.categories = []
      $scope.families = []
      //end: models
      //start listeners
      circuitsService.onAllSecondaryPointsLoaded($scope, function(event, secondaryPointsList) {
        $log.debug('secondary points loaded: ' + secondaryPointsList.tableData.length)
        $scope.secondaryPointsTable.points = secondaryPointsList.tableData
        //secondaryPointsList.markers??
      })
      circuitsService.onSecondaryPointDeleted($scope, function(event, data) {
          $scope.getPoints()
      })
      circuitsService.onSecondaryPointSaved($scope, function(event, data) {
          $scope.getPoints()
      })
      plantsService.onPlantsLoaded($scope, function(event, markers, plants) {
        $scope.plantsTable.plants = plants
      })
      circuitsService.onSecondaryPointAdded($scope, function(data) {
        $scope.mode = 'list'
        $scope.getPoints()
      })
      //end listeners
      //start: helpers
      $scope.search = function() {
        $scope.getPlants()
      }
      $scope.getAddButtonLabel = function() {
          if ($scope.plantsTable.selected.length === 0) {
            return 'Add'
          } else {
            return 'Add ' +  $scope.plantsTable.selected.length + ' plants'
          }
      }
      $scope.addNew = function() {
        $scope.mode = 'add'
        $scope.getPlants()
      }
      $scope.cancelAdd = function() {
        $scope.mode = 'list'
        $scope.getPoints()
      }
      $scope.getPoints = function() {
          $scope.secondaryPointsTable.promise = circuitsService.loadAllSecondaryPoints($scope.circuitId, $scope.primaryPointId, $scope.secondaryPointsTable.query).$promise
      }
      $scope.deletePoint = function(point) {
          circuitsService.removeSecondaryPoint($scope.circuitId, $scope.primaryPointId, point.id)
      }
      $scope.publish = function(point) {
        point.active = !point.active
        circuitsService.updateSecondaryPoint($scope.circuitId, $scope.primaryPointId, point.id, point)
      }
      $scope.getPlants = function() {
          var filter = angular.copy($scope.filter)
          $scope.plantsTable.promise = plantsService.getAllPlantsAndSendEvent($scope.plantsTable.query, filter).$promise;
      }
      $scope.onSearchKeyPress = function() {
        $scope.getPlants()
      }
      $scope.add = function() {
        circuitsService.addSecondaryPoints($scope.circuitId, $scope.primaryPointId, $scope.plantsTable.selected)
      }
      //end: helpers
      //start: loading data
      $scope.getPoints()
      plantsService.getCategories(function(categories) {
          $scope.categories = categories
      })
      plantsService.getFamilies(function(families) {
          $scope.families = families
      })
      //end: loading data
    }
]);
