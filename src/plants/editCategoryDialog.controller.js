angular.module('adminApp').controller('editCategoryDialogController', ['$scope', '$mdDialog', 'plantsService', 'category', '$log', function($scope, $mdDialog, plantsService, category, $log) {
    $log.debug('D', category)
    $scope.model = category

    //start: listeners
    plantsService.onPlantCategorySaved($scope, function(event, category) {
      $scope.hide({relaod: true})
    })
    //end: listeners

    $scope.save = function() {
      plantsService.saveCategory($scope.model.id, $scope.model)
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);
