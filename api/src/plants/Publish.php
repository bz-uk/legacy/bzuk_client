<?php

namespace botgarApi\plants;

use Slim\Slim;

class Publish
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message)
    {
        $response = ['status' => 'error', 'message' => $message];
        $this->app->response->setStatus(400);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }

    public function run()
    {
        $json = $this->app->request->getBody();
        $data = json_decode($json);

        var_dump($data);

        //validation
        if (!isset($data->ids)) {
            return $this->returnInvalidResponse('No plants specified');
        }

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $db->beginTransaction();


        foreach ($data->ids as $plantId => $published) {
            if ($published === true) {
                $published = 1;
            } else {
                $published = 0;
            }
            $now = strftime('%Y-%m-%d %H:%M:%S');
            $sql = 'UPDATE plants SET is_active=' . $published . ', modified_Date="' . $now . '" where id=' . $plantId;
            $result = $db->query($sql);
            if (!$result) {
                return $this->returnInvalidResponse('Unable to find plant: ' . $sql);
            }
        }
        $db->commit();
        $response = ['status' => 'ok', 'message' => 'plant published'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
