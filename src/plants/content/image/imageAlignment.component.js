'use strict'
//controller
var imageAlignmentcomponent = function ($mdDialog, $log, $scope, plantsService) {

}
//definition single-block-plant-more-info
angular.module('adminApp').component('imageAlignment', {
    templateUrl: 'plants/content/image/imageAlignment.template.html',
    controller: imageAlignmentcomponent,
    bindings: {
        display: '=',
        displayVerticalOffset: '=',
        parentType: '@'
    }
});