'use strict';
import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {ToastContainer} from 'react-toastify';

import LoginScren from './loginScreen';

injectTapEventPlugin();

export default class Application extends React.Component {
    componentDidMount() {
        let loadingPlaceholder = document.querySelector('#loading-placeholder');
        loadingPlaceholder.classList.add('loading-invisible');
        setTimeout(() => {
            let appElement = document.querySelector('#entry-point');
            appElement.classList.remove('login-invisible');
            loadingPlaceholder.parentNode.removeChild(loadingPlaceholder);
        }, 500);
    }

    render() {
        // if (this.state.webp !== undefined) {
        return (
            <MuiThemeProvider>
                <div>
                    <LoginScren />
                    <ToastContainer/>
                </div>
            </MuiThemeProvider>
        );
    }
}

ReactDOM.render(<Application/>, document.getElementById('entry-point'));