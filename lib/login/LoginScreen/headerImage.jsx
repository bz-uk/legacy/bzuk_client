import React from 'react';

export default function(props) {
    let styles = {
        logoImageStyle: {
            maxWidth: '100%'
        },
    }
    return (
        <picture>
            <source style={styles.logoImageStyle} srcSet="img/login/logo.webp" type="image/webp"/>
            <source style={styles.logoImageStyle} srcSet="img/login/logo.png" type="image/png"/>
            <img style={styles.logoImageStyle} src="img/login/logo.png"/>
        </picture>
    );
}