// fucking not working
// class NewPlantsService extends baseService {
// 	constructor($log, $resource, $rootScope) {
// 		super($rootScope)
// 		this.$resource = $resource
// 		this.$log = $log
// 		this.allPlantsTags = []
// 		this.initResurces()
// 	}
//
// 	initResurces() {
// 		this.plantResource = this.$resource('/api/api/index.php/plants/:plantId', {}, {
// 			'save': {method: 'PUT'},
// 			'get': {method: 'GET'},
// 			'delete': {method: 'DELETE'}
// 		})
// 	}
//
// 	onPlantSaved(scope, callback) {
// 		this.registerHandler('plant-saved-event', scope, callback)
// 	}
//
// 	updateSinglePlant(plantModel){
// 		return this.plantResource.save({plantId: plantModel.id},plantModel, function(data) {
// 			this.emit('plant-saved-event', plantModel, 'Plant "'+plantModel.name+'" saved successfully')
// 		}, function(err) {
// 			console.log(err);
// 			var message = '';
// 			if (err.status === 500) {
// 				message = err.data;
// 			} else {
// 				message = err.data.message;
// 			}
// 			this.emit('plant-saved-error-event', plantModel);
// 		});
// 	}
// }
// NewPlantsService.$inject = ['$log', '$resource', '$rootScope'];

//angular.module('adminApp').factory('NewPlantsService', NewPlantsService)

angular.module('adminApp').factory('tagsService', ['$log', '$resource', '$rootScope', '$q', function ($log, $resource, $rootScope, $q) {
    var plantsTagsResource = $resource('/api/api/index.php/plants-tags', {}, {
        'getAll': {method: 'GET', isArray: true},
        'insert': {method: 'POST'}
    });

    var singleTagResource = $resource('/api/api/index.php/plants-tags/:tagId', {}, {
        'delete': {method: 'DELETE'}
    });

    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }

    var allPlantsTags = []
    return {
        //start: hanlders
        onTagsLoaded: function (scope, callback) {
            registerHandler('tags-loaded-event', scope, callback)
        },
        onTagDeleted: function (scope, callback) {
            registerHandler('tag-deleted-event', scope, callback)
        },
        //end: hanlders
        deleteTag: function(tag) {
            $log.debug('Deleting tag', tag);
            return singleTagResource.delete({tagId: tag.id}, tag, function(data) {
                $rootScope.$emit('tag-deleted-event', tag);
            });
        },
        getAllTagsAsync: function (query, force,successCallback) {
            if (allPlantsTags.length > 0 && true !== true) {
                return $q(function(resolve, reject) {
                    resolve(successCallback(allPlantsTags));
                }).promise;
            }
            return plantsTagsResource.getAll({}, {query: query}, function (tags) {
                allPlantsTags = angular.copy(tags)
                delete allPlantsTags.$promise;
                delete allPlantsTags.resolved;
                successCallback(allPlantsTags);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status === 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to get plant tags', err);
            }).$promise;
        },
        createTag: function (model, successCallback, errCallback) {
            return plantsTagsResource.insert(model, function (data) {
                console.log(data)
                var newTag = {
                    id: data.id,
                    name: model.name,
                    name_lowercase: model.name.toLowerCase()
                }
                allPlantsTags.push(newTag)
                successCallback(newTag);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status === 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to add new plant tag', err);
                errCallback(message);
            })
        }
    }
}]);
