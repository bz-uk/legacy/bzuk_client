'use strict';
const BaseRoute = require('./route');
const Promise = require('bluebird');
const debug = require('debug')('route-auth');

const RouteRedirect = require('./../error/routeRedirect');
const Unauthorized = require('./../error/unauthorized');
const User = require('./../appModels/user');
const Role = require('./../appModels/role');
const Permissions = require('./../models/app/permissions');
const UserRolePermission = require('./../appModels/compound/userRolePermission');
const UnauthorizedError = require('./../error/unauthorized');

module.exports = class Route extends BaseRoute {

    getConfig() {
        throw new Error('not implemented!')
    }

    constructor(options) {
        super(options);
    }

    /**
     *
     * @param req
     * @param res
     */
    authenticate(req, res) {
        var self = this;
        return new Promise(function (resolve, reject) {
            debug('Authenticating', req.session.user);
            if (req.session.user) {
                return UserRolePermission.fetchFor(req.session.user)
                .then(function (coumpound) {
                    if (!Permissions.has(coumpound.permissions, 'can_login')) {
                        return reject(new UnauthorizedError());
                    }
                    self.context.user = coumpound.user;
                    self.context.role = coumpound.role;
                    self.context.permissions = coumpound.permissions;
                    return resolve();
                })
            } else {
                if (self.options.unauthorizedRedirectRoute) {
                    return reject(new RouteRedirect(self.options.unauthorizedRedirectRoute));
                } else {
                    return reject(new Unauthorized());
                }
            }
        });
    }

    /**
     * @override
     * @param req
     * @param res
     */
    main(req, res) {
        throw new Error('not implemented!')
    }

    /**
     * @override
     * @returns {[*,*]}
     */
    getAllSteps() {
        return [
            this.authenticate,
            this.main
        ]
    }
}