'use strict';
import forEach from 'lodash.foreach';
import assign from 'lodash.assign';
export default class FieldValidator {
    constructor(validators, errorMessage, options) {
        let defaultOptions = {};
        this.options       = assign(defaultOptions, options);
        this.errorMessage  = errorMessage;
        this.validators    = validators;
        this.errors        = [];
        this.error         = '';
        this.valid         = null;
    }

    validate(value) {
        this.valid  = true;
        this.errors = [];
        this.error  = '';
        forEach(this.validators, (validator) => {
            let result = validator.validate(value);
            if (!result.valid) {
                this.errors.push(result.errorMessage);
                this.error = this.errorMessage.replace(/\{commaSeparatedErrors\}/i, this.errors.join(', '))
            }
            this.valid = this.valid && result.valid;
        });
        return this.valid;
    }
};