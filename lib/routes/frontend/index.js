'use strict';
const Promise = require('bluebird');
const debug   = require('debug')('frontendIndexRoute');
const _       = require('lodash');

const AuthenticatedFrontendRoute = require('./../../common/authenticatedFrontendRoute');
const RouteRedirect      = require('./../../error/routeRedirect');
const User               = require('./../../appModels/user');
const packageJson        = require('./../../../package.json');
const engDict            = require('./../../../lang/admin/en');
const csDict             = require('./../../../lang/admin/cs');

module.exports = class Route extends AuthenticatedFrontendRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/',
            name: 'index'
        }
    }

    main(req, res) {
        let templateVars = {
            appSettings:    JSON.stringify({
                csrf:        '',//req.csrfToken(),
                role:        _.omit(this.context.role, ['created_at', 'updated_at', 'deleted_at', 'locked']),
                permissions: this.context.permissions,
                version:     packageJson.version,
            }),
            user:           JSON.stringify(
                _.omit(this.context.user, ['password', 'deleted_at'])
            ),
            langDictionary: JSON.stringify(
                {
                    en: engDict,
                    cs: csDict
                }
            )
        };
        res.render('admin/index', templateVars);
        return Promise.resolve()
    }
};