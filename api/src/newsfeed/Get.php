<?php
namespace botgarApi\newsfeed;

use Slim\Slim;

/**
 * Class Get
 * @package botgarApi\newsfeed
 * @url http://192.168.33.99/botgar_api/api/news/[fromDate]
 * [fromDate] - YYYY-MM-DD HH:MM:SS (uri encoded)
 */
class Get {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {

        $this->app = $app;
    }

    /**
     * @param int $fromDate
     */
    public function run($fromDate = null) {
        $db = new \PDO('mysql:host=localhost;port=3306;dbname=botgar;charset=UTF8;','botgar','ooIKRCsJBYVo3MSwG3KO9oLyn');
        $sql = "SELECT * FROM news_item";
        if ($fromDate != '' && strtotime($fromDate) > 0) {
            $sql .= " WHERE date_created>'" . strftime('%F %T', strtotime($fromDate)) . "'";
        }
        $sql .= " ORDER BY date_created DESC";
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(500);
            return;
        }

	$output = [];
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $apiItem = new ItemModel();
            $apiItem->loadFromDbRow($row);
            $output[] = $apiItem->asArray();
        }
        $tags = [];
        $sql = "SELECT * FROM tags";
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(500);
            return;
        }
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $tags[] = ['id' => $row['id'], 'name' => $row['name'], 'image_id'=>$row['image_id']];
        }
        $response = json_encode(['news' => $output, 'tags' => $tags]);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);

    }
}
