'use strict';
const debug = require('debug')('image-upload');
const Busboy = require('busboy');
const sharp = require('sharp');
const process = require('process');
const path = require('path');
const fs = require('fs');
const os = require('os');
const mkdirp = require('mkdirp');
const crypto = require('crypto');

const configuration = require('./../../../common/config');
const knex = require('./../../../database/local').knex;
const BaseRoute = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const UnauthorizedError = require('./../../../error/unauthorized');
const Resizer = require('./../../../common/image/resizer');
const ERROR_CODES = require('./../../../error/codes');

const UPLAOD_TARGET_PATH = 'data/images/';
const ORIGINALS_PATH = 'originals';
const OFFLINE_MAX_WIDTH = 640;
//const RESIZE_WIDTHS = [100, 150, 200, 250, 384, 512, 640, 800, 1024, 1280, 2048]; //all formats
const RESIZE_WIDTHS = [250, 640, 1280];
const ADDITIONAL_OUTPUT_FORMATS = ['webp'];

module.exports = class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'post',
            path: '/upload/image',
            name: 'uploadImage'
        }
    }

    main(req, res) {
        let incommingFiles = [];
        let uploadedFiles = [];
        let transaction;
        let queue = Promise.resolve()
        .then(() => {
            return new Promise((resolve, reject) => {
                mkdirp(path.join(process.cwd(), UPLAOD_TARGET_PATH, ORIGINALS_PATH), (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve();
                })
            })
        })
        .then(() => {
            let busboy = new Busboy({headers: req.headers});
            busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
                console.log('MIME is ', mimetype);
                let serverFileName = 'img_' + Math.ceil(Math.random() * 8999999 + 1000000) + '_' + new Date().getTime() + '_' + crypto.createHash('md5').update(filename).digest("hex") + path.extname(filename);
                let fullServerFileName = path.join(path.join(UPLAOD_TARGET_PATH, ORIGINALS_PATH), serverFileName);
                let valid = true;
                if (mimetype === 'image/png' || mimetype === 'image/jpg' || mimetype === 'image/jpeg') {
                    file.pipe(fs.createWriteStream(fullServerFileName));
                } else {
                    file.resume();
                    valid = false;
                }
                file.on('end', () => {
                    debug('Got image in temp: ', fullServerFileName);
                    let info = {
                        name: serverFileName,
                        fullName: fullServerFileName,
                        originalName: filename,
                        path: path.dirname(fullServerFileName),
                        extension: path.extname(filename)
                    }
                    queue = queue.then(() => {
                        if (!valid) {
                            throw new Error(ERROR_CODES.admin.upload.unsupportedFormat);
                        }
                        return new Promise((resolve, reject) => {
                            fs.stat(fullServerFileName, (err, stats) => {
                                if (err) {
                                    reject(err);
                                }
                                resolve(stats);
                            });
                        })
                    })
                    .then((fsFileInfo) => {
                        info.size = fsFileInfo.size;
                        incommingFiles.push(info);
                        debug('file info stored');
                    })
                })
            });
            busboy.on('finish', function () {
                debug('upload finished');
                queue = queue.then(() => {
                    let subQueue = Promise.resolve();
                    debug('post-processing files', incommingFiles);
                    incommingFiles.map((imageData) => {
                        debug('processing ', imageData);
                        let context = {};
                        context.imageData = imageData;
                        subQueue = subQueue.then(() => {
                            return sequelize.transaction()
                        })
                        .then((t) => {
                            context.imageData.t = t;
                            return sequelize.models.ImageCompat.create({
                                name: context.imageData.name,
                                path: path.dirname(context.imageData.fullName),
                                width: 0,
                                height: 0,
                                url: '{baseUrl}/images/640/{imageFileName}',
                                original_name: context.imageData.originalName,
                                type: context.imageData.extension.substr(1),
                                size: context.imageData.size
                            }, {transaction: context.imageData.t})
                        })
                        .then((model) => {
                            debug('img added to db');
                            context.imageData.dataModel = model;
                            context.imageData.id = model.id;
                            return sharp(context.imageData.fullName)
                            .metadata()
                        })
                        .then((metadata) => {
                            debug('updating db item')
                            context.imageData.width = metadata.width;
                            context.imageData.height = metadata.height;
                            return context.imageData.dataModel.update({
                                preview_url: '{$BASE_URL}/images-preview/' + context.imageData.id,
                                width: context.imageData.width,
                                height: context.imageData.height
                            }, {transaction: context.imageData.t});
                        })
                        .then(() => {
                            debug('begin resizing process');
                            return Resizer.resize(context.imageData.fullName, 'png'/*context.imageData.extension*/, context.imageData.id, RESIZE_WIDTHS, UPLAOD_TARGET_PATH);
                        })
                        .then(() => {
                            debug('begin resizing process for webp');
                            return Resizer.resize(context.imageData.fullName, 'webp', context.imageData.id, RESIZE_WIDTHS, UPLAOD_TARGET_PATH);
                        })
                        .then(() => {
                            debug('imag stored default location. Preview webp?');
                            context.imageData.url = configuration.baseUrl.replace(/\/$/, '') + '/images-preview/' + context.imageData.id;
                            return context.imageData.t.commit();
                        })
                        .then(() => {
                            debug('sql data commited');
                            delete context.imageData.t;
                            uploadedFiles.push({url: context.imageData.url, id: context.imageData.id, created_at: context.imageData.dataModel.created_at});
                        })
                        .catch((e) => {
                            debug('upload error', e);
                            console.error('File ' + imageData.fullName + ' upload failed ', e);
                            return context.imageData.t.rollback();
                        })
                    });
                    return subQueue;
                });
                queue = queue.then(() => {
                    debug('all done!', uploadedFiles);
                    res.writeHead(200, {'Connection': 'close'});
                    res.end(JSON.stringify(uploadedFiles));
                })
                .catch((err) => {
                    res.writeHead(400, {'Connection': 'close'});
                    res.end(JSON.stringify({code: err.message, message: err.message}));
                })
            });
            req.pipe(busboy);
        })
        return queue;
    }
}