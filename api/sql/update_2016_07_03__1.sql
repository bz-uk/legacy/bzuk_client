ALTER TABLE `tags`
CHANGE COLUMN `name` `name` VARCHAR(50) NOT NULL ,
ADD COLUMN `name_en` VARCHAR(50) NULL AFTER `image_id`,
ADD COLUMN `type` INT NOT NULL DEFAULT 0 AFTER `name_en`;

ALTER TABLE `tags`
ADD COLUMN `color` VARCHAR(10) NULL AFTER `type`;

ALTER TABLE `tags`
ADD COLUMN `name_lowercase` VARCHAR(50) NULL AFTER `color`,
ADD COLUMN `name_en_lowercase` VARCHAR(50) NULL AFTER `name_lowercase`;

INSERT INTO `tags` (`name`, `image_id`, `name_en`, `type`, `color`, `name_lowercase`, `name_en_lowercase`) VALUES ('Kvete', '', 'Blooming', '1', '#ffffff', 'kvete', 'blooming');
INSERT INTO `tags` (`name`, `image_id`, `name_en`, `type`, `color`, `name_lowercase`, `name_en_lowercase`) VALUES ('Plodí', '', 'Bearing fruits', '1', '#ffffff', 'plodi', 'bearing fruits');

CREATE TABLE `plant_tags` (
  `plant_id` INT NOT NULL,
  `tag_id` INT NOT NULL,
  PRIMARY KEY (`plant_id`, `tag_id`));
