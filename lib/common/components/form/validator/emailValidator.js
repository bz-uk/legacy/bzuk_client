'use strict';
import assign from 'lodash.assign';
import BaseValidator from './baseValidator';
import isEmail from 'validator/lib/isEmail';
export default class EmailValidator extends BaseValidator {
    constructor(options) {
        let defaultOptions = {errorMessage: 'Value is not valid email'}
        super(assign(defaultOptions, options));
    }

    isValid(value) {
        return value && isEmail(value);
    }
}