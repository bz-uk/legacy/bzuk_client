<?php
namespace botgarApi\plants;

use Slim\Slim;

class DeleteTag
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message)
    {
        $response = ['status' => 'error', 'message' => $message];
        $this->app->response->setStatus(400);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }

    public function run($tagId)
    {
        if (!isset($tagId) || !is_numeric($tagId)) {
            return $this->returnInvalidResponse('Tag id must be numeric value');
        }

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $sql = 'SELECT * from tags t WHERE t.id=' . $tagId;
        $result = $db->query($sql);
        $row = $result->fetch(\PDO::FETCH_ASSOC);
        if ($row['system'] == 1) {
            return $this->returnInvalidResponse('System tag cannot be deleted');
        }

        $db->beginTransaction();

        $sql = 'SELECT DISTINCT pl.id from plant_tags pt inner join plants pl ON pt.plant_id=pl.id WHERE pt.tag_id=' . $tagId;
        $result = $db->query($sql);
        if ($result) {
            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                $tagsSerialized = [];
                $sql = 'select t.* from plant_tags pt inner join tags t where t.id=pt.tag_id AND pt.plant_id=' . $row['id'] . ' AND t.type=1';
                $tagRes = $db->query($sql);
                if ($tagRes) {
                    while ($t = $tagRes->fetch(\PDO::FETCH_ASSOC)) {
                        if ($t['id'] == $tagId) {
                            continue;
                        }
                        $tagsSerialized[] = $t;
                    }
                }
                $sql = 'update plants set tags=' . $db->quote(json_encode($tagsSerialized)) . ' where id=' . $row['id'];
                $db->query($sql);
            }
        }
        $sql = 'DELETE from plant_tags WHERE id='.$tagId;
        $db->query($sql);
        if (!$result) {
            $db->rollBack();
            return $this->returnInvalidResponse('Unable to delete tag from plants: ' . $sql);
        }
        $sql = 'DELETE from tags WHERE id='.$tagId;
        $db->query($sql);
        if (!$result) {
            $db->rollBack();
            return $this->returnInvalidResponse('Unable to delete tag: ' . $sql);
        }
        $db->commit();
        $response = ['status' => 'ok', 'message' => 'tag saved'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
