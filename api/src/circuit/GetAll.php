<?php
namespace botgarApi\circuit;

use Slim\Slim;

class GetAll {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run() {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT * FROM circuit";
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            return;
        }
        $output = [];
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
        	//add circuit
		        $output[] = $row;
        }
        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
