'use strict'
angular.module('adminApp').directive('circuitEdit', function() {
	return {
		restrict : 'E',
		templateUrl : 'circuit/circuitEdit.template.html',
		controller: 'circuitEditController'
	};
});
