'use strict';
import _ from 'lodash';
import React from 'react';
import FontIcon from 'material-ui/FontIcon';

export default class Screen {
    constructor(options) {
        this.options = options;
    }

    requirePermission() {
        return null;
    }

    getIconClass() {
        return 'icon-home';
    }

    getTitle() {
        throw new Error('not implemented')
    }

    getCode() {
        return _.kebabCase(this.getTitle());
    }

    getIcon() {
        let styles = {
            menuIcon:   {
                margin: '0 0 12px 12px'
            }
        }
        return (<FontIcon style={styles.menuIcon}
                          className={this.getIconClass()}>
        </FontIcon>)

    }

}