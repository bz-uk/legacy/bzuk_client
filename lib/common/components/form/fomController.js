'use strict';
import forIn from 'lodash.forin';
import EventEmitter from 'wolfy87-eventemitter';
import Promise from 'bluebird';
export default class FormController extends EventEmitter {
    constructor(options) {
        super();
        this.fields = {}
        this.valid  = null;
    }

    validate() {
        return new Promise((resolve) => {
            var valid = true;
            forIn(this.fields, (field) => {
                field.validate(); //TODO: allow promises here
                valid = valid && field.state.valid;
            })
            return resolve(valid);
        })
    }
}