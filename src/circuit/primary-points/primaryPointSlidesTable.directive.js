'use strict'
angular.module('adminApp').directive('primaryPointSlidesTable', function() {
	return {
		restrict : 'E',
		templateUrl : 'circuit/primary-points/primaryPointSlidesTable.template.html',
		controller: 'primaryPointSlidesTableController',
		scope: {
      circuitId: "@circuitId",
			primaryPointId: "@primaryPointId"
    }
	};
});
