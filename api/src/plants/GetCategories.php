<?php
namespace botgarApi\plants;

use Slim\Slim;

class GetCategories {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run() {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT * FROM plant_category order by name";
        $result = $db->query($sql);
        $items = [];
        while($row = $result->fetch(\PDO::FETCH_ASSOC)) {
          $items[] = $row;
        }
        $response = json_encode($items);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
