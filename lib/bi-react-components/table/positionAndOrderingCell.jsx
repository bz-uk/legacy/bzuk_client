'use strict';
import React from 'react';

var format = require('date-format');

export default class PositionAndOrderingCell extends React.Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        var styles = {
            cellStyle: {
                cursor: 'ns-resize',
            }
        }
        return (
            <span style={styles.cellStyle}>
                # {this.props.value}
            </span>
        
        );
    }
}