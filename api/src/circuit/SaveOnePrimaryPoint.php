<?php
namespace botgarApi\circuit;

use Slim\Slim;

class SaveOnePrimaryPoint {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message) {
      $response = ['status'=>'error', 'message'=> $message];
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run($circuitId, $pointId) {
        $json = $this->app->request->getBody();
        $data = json_decode($json);

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT * FROM circuit where id=" . $circuitId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            $this->app->response->setBody(['error' => 'unable to load circuit']);
            return;
        }
        $circuit = $result->fetch(\PDO::FETCH_ASSOC);

        $sql = "SELECT * FROM circuit_primary_points where id=" . $pointId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(404);
            $this->app->response->setBody(['error' => 'unable to load point']);
            return;
        }
        $point = $result->fetch(\PDO::FETCH_ASSOC);
        //
        $db->beginTransaction();
        //
        // //image
        $image_id = 'null';
        if (isset($data->image) && $data->image != null && $data->image->id > 0) {
          $image_id = $data->image->id;
        }
        //marker
        $mapMarkerId = 'null';
        if (isset($data->markers) && is_array($data->markers) && count($data->markers) > 0) {
          if (isset($point['map_marker_id']) && $point['map_marker_id'] != null) {
            $mapMarkerId = $point['map_marker_id'];
            $sql = 'update map_marker set latitude='.$data->markers[0]->latitude.', longitude='.$data->markers[0]->longitude.' where id='. $point['map_marker_id'];
            $result = $db->query($sql);
            if (!$result) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to update marker point: ' . $sql);
            }
          } else {
            $sql = 'insert into map_marker (latitude, longitude, marker_visible, title,contents) values('.$data->markers[0]->latitude.', '.$data->markers[0]->longitude.', 1, " ", "")';
            $result = $db->query($sql);
            if (!$result) {
                $db->rollback();
                return $this->returnInvalidResponse('Unable to insert marker point: ' . $sql);
            }
            $mapMarkerId = $db->lastInsertId();
          }
        }

        $sql = "UPDATE circuit_primary_points SET name=".$db->quote($data->name).", name_en=".$db->quote($data->name_en).", image_id=".$image_id.", map_marker_id=".$mapMarkerId." WHERE id=" . $pointId;

        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            //$err = var_export($db->errorInfo(), true);
            $this->app->response->setBody(json_encode(['error' => 'unable to update point', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }

        //parts
        foreach ($data->parts as $part) {
          $sql = "UPDATE primary_point_parts SET content=".$db->quote($part->content).", content_en=".$db->quote($part->content_en)." WHERE id=" . $part->id;
          $result = $db->query($sql);
          if (!$result) {
              $this->app->response->setStatus(400);
              //$err = var_export($db->errorInfo(), true);
              $this->app->response->setBody(json_encode(['error' => 'unable to update point part content', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
              return;
          }
        }



        $db->commit();

        $response = json_encode(['status' => 'ok', 'message' => 'point saved']);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
