'use strict';
const ExtendableError = require('./extendableError');

module.exports = class RouteRedirect extends ExtendableError {
    constructor(redirectUrl) {
        super('redirect');
        this.redirectUrl = redirectUrl;
    }
};