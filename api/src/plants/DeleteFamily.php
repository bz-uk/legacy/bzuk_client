<?php
namespace botgarApi\plants;

use Slim\Slim;

class DeleteFamily
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message)
    {
        $response = ['status' => 'error', 'message' => $message];
        $this->app->response->setStatus(400);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }

    public function run($familyId)
    {
        if (!isset($familyId) || !is_numeric($familyId)) {
            return $this->returnInvalidResponse('Family id must be numeric value');
        }

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $db->beginTransaction();

        $sql = 'UPDATE plants SET family_id=NULL WHERE family_id=' . $familyId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollBack();
            return $this->returnInvalidResponse('Unable to unassign family from plants: ' . $sql);
        }
        $sql = 'DELETE from plant_family WHERE id='.$familyId;
        $db->query($sql);
        if (!$result) {
            $db->rollBack();
            return $this->returnInvalidResponse('Unable to delete family: ' . $sql);
        }
        $db->commit();
        $response = ['status' => 'ok', 'message' => 'family deleted'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
