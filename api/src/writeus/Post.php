<?php
namespace botgarApi\writeus;

use Slim\Slim;

class Post
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run()
    {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);
    	if (!isset($data->message) || $data->message == null || trim($data->message) == null) {
    		$this->app->response->setStatus(400);
    		return;
    	}
    	
    	$replyTo = null;
    	if (isset($data->email)) {
    		$replyTo = $data->email;
    		
    		if (isset($data->name)) {
    			$replyTo = $data->name . '<' . $data->email . '>';
    		}
    	}
    	$headers = 'From: noreply@jedinecnakava.sk' . "\r\n";
    	$headers .= "MIME-Version: 1.0\r\n";
    	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    	if ($replyTo != null) {
    		$headers = $headers . 'Reply-To: ' . $replyTo . "\r\n";
    	}
    	$headers .= 'X-Mailer: PHP/' . phpversion();
    	 
    	mail('magdalena.hrdinova@natur.cuni.cz', 'Nova zprava z aplikace botanicke zahrady', '<b>Nova zprava</b><br>Name: ' . $data->name . '<br>Email: ' . $data->email . '<br>Message:' . $data->message, $headers) . '<br><br><i>Pozn: pokud navstevnik zadal taky svuj email muzete mu odpovedet primo na tento email.<i>';
    	
        $response = array('status' => 'ok');
        $this->app->response->header('Content-Length', strlen(json_encode($response)));
        $this->app->response->header('Content-Type: application/json');
        $this->app->response->setBody(json_encode($response));
    }
}