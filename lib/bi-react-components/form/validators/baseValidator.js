'use strict';
import _ from 'lodash';

/**
 * @class baseValidator
 */
export default class baseValidator {
    
    constructor(options) {
        let defaultOptions = {
            errorMessage: 'Value "{value}" is not valid'
        }
        
        this.options = _.assign(defaultOptions, options);
    }
    
    /**
     * @abstract
     */
    isValid(value) {
        throw new Error('abstract')
    }
    
    /**
     *
     * @param value
     * @return {Promise}
     */
    validate(value) {
        let validationResult = this.isValid(value);
        if (validationResult === undefined || validationResult === null) {
            validationResult = Promise.resolve(null);
        } else if (typeof(validationResult) === "boolean") {
            validationResult = Promise.resolve(validationResult);
        }
        return validationResult
        .then((valid) => {
            let result = {
                valid: valid,
                value: value
            }
            if (valid === false) {
                result.errorMessage = this.options.errorMessage.replace(/\{value\}/i, value);
            }
            return result;
        })
    }
}