angular.module('adminApp').controller('editAuthorDialogController', ['$scope', '$mdDialog', 'authorsService', 'author', '$log', function ($scope, $mdDialog, authorsService, author, $log) {
    $scope.model = author

    //start: listeners
    authorsService.onAuthorSaved($scope, function (event, author) {
        $scope.hide({relaod: true})
    })
    //end: listeners

    $scope.save = function () {
        if (($scope.model.name === undefined || $scope.model.name.length === 0)) {
            alert('Jméno nemuže být prázdné!')
            return
        }
        authorsService.saveAuthor($scope.model.id, $scope.model);
    }

    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);
