module.exports = {
    login: {
        wrongUsernameOrPass: 'wrong_username_or_pass'
    },
    admin: {
        upload: {
            unsupportedFormat: 'upload_unsupported_format'
        }
    }
}