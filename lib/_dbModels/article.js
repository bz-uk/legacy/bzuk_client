const connection = require('./../database/local');
const configuration = require('./../common/config');

var Article = connection.bookshelf.Model.extend({
    tableName:    configuration.storage.mysql.tablePrefix + 'articles',
    hasTimestamps: true,
});

module.exports = connection.bookshelf.model('Article', Article);