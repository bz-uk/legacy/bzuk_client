'use strict';
angular.module('adminApp').controller('articleEditController', ['$location', '$scope', '$mdDialog', '$mdMedia', 'articlesService', '$timeout', '$routeParams', 'settingsService', 'Upload',
    function ($location, $scope, $mdDialog, $mdMedia, articlesService, $timeout, $routeParams, settingsService, Upload) {
        //start: models
        $scope.saving = false
        $scope.model = {
            id: $routeParams.articleId
        }
        $scope.flashMessages = []
        $scope.settings = []
        $scope.isNew = $routeParams.articleId == '_';
        //end: models

        $scope.save = function () {
            $scope.saving = true
            if ($scope.isNew) {
                articlesService.addAndEmitEvent($scope.model);
            } else {
                articlesService.saveAndEmitEvent($scope.model);
            }
        }
        $scope.getSaveLabel = function () {
            if ($scope.isNew) {
                return $scope.saving ? 'Vyvářím...' : 'Vytvořit'
            } else {
                return $scope.saving ? 'Ukádám...' : 'Uložit'
            }
        }
        $scope.openArticles = function () {
            $location.url('/articles')
        }
        $scope.cancel = function () {
            $scope.openArticles()
        }
        //start: upload
        $scope.upload = function (file) { //TODO: to service??
            if (file === undefined) {
                return;
            }
            if (!file.$error) {
                var newImg = {
                    id: undefined,
                    url: 'https://bz-uk.cz/sites/default/files/styles/medium/public/images/abutilon_megapotamicum2.jpg',
                    status: 'uploading'
                }
                $scope.model.image = newImg
                Upload.upload({
                    url: '/api/api/index.php/images', //TODO: load from settings
                    data: {
                        file: file
                    }
                }).then(function (resp) {
                        console.log('upload complete')
                        console.log(resp)
                        $timeout(function () {
                            console.log('complete timeout')
                            if (resp.data.id !== undefined && parseInt(resp.data.id) > 0) {
                                console.log('done!')
                                newImg.url = resp.data.url
                                newImg.id = parseInt(resp.data.id)
                                newImg.status = 'done'
                            }
                            $scope.log = 'file: ' +
                                resp.config.data.file.name +
                                ', Response: ' + JSON.stringify(resp.data) +
                                '\n' + $scope.log;
                        });
                    },
                    function (resp) {
                        alert('Unable to uplaod image: ' + resp.data)
                        $scope.model.image = undefined
                    },
                    function (evt) {
                        var progressPercentage = parseInt(100.0 *
                            evt.loaded / evt.total);
                        newImg['progress'] = progressPercentage
                    });
            }

        }
        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        })
        $scope.deleteImage = function (image) {
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this image?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function () {
                $scope.model.image = undefined
            }, function () {
            });
        }
        //end: upload

        $scope.options = {
            language: 'en',
            format_tags: 'h1;h2',
            allowedContent: true,
            entities: false,
            removePlugins: 'templates'
            //width: '100%'
        };

        //start: listeners
        articlesService.onArticleSaved($scope, function (event) {
            $scope.saving = false;
            $location.url('/articles/');
        });
        articlesService.onArticleCreated($scope, function (event) {
            $scope.saving = false;
            $location.url('/articles/');
        });
        articlesService.onArticleCreateError($scope, function (event) {
            $scope.saving = false;
        });
        articlesService.onArticleSaveError($scope, function (event) {
            $scope.saving = false;
        });
        settingsService.onSettingsLoaded($scope, function (event, settings) {
            $scope.settings = settings
        })
        //end: listeners
        if (!$scope.isNew) {
            articlesService.loadOne($scope.model.id, function (articleModel) {
                $scope.model = articleModel;
            });
        }
        settingsService.loadAndEmitEvent()
    }]);
