<?php
namespace botgarApi\search;

use botgarApi\article\ArticleModel;
use botgarApi\image\ImageModel;
use Slim\Slim;

class Get
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {

        $this->app = $app;
    }

    public function run()
    {
        $items = simplexml_load_file('../data/articles.xml');
        $output = array();
        foreach ($items as $item) {
            $apiItem = new ArticleModel();
            $apiItem->loadFromXml($item);
            $output[] = array(
                'id' => $apiItem->getId(),
                'title' => $apiItem->getTitle(),
                'dateCreated' => $apiItem->getDateCreated(),
                'dateUpdated' => $apiItem->getDateUpdated(),
                'type' => $apiItem->getType(),
                'code' => $apiItem->getCode()
            );
        }
//        $items = simplexml_load_file('../data/images.xml');
//        foreach ($items as $item) {
//            $apiItem = new ImageModel();
//            $apiItem->loadFromXml($item);
//            $output[] = array(
//                'id' => $apiItem->getId(),
//                'title' => $apiItem->getName(),
//                'dateCreated' => $apiItem->getDateCreated(),
//                'author' => $apiItem->getAuthor(),
//                'type' => 'image'
//            );
//        }
        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}