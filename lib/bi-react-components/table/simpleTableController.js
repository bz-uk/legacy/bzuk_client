'use strict';
import axios from 'axios';
import _ from 'lodash';
import EventEmitter  from 'wolfy87-eventemitter';
import BaseDataProvider from './baseDataProvider';

import SimpleTableDataProvider from './simpleTableDataProvider.js';

const ROW_ORDER_ROUTE_URL = '/order'

export default class SimpleTableController extends EventEmitter {
    constructor(backendEndPointUrl, options) {
        super();
        let defaultOptions = {
            identifier: 'table',
            defaultSorting: {
                sortBy: 'id',
                sortOrder: 'asc'
            },
            pagination: {
                rowsPerPage: 20,
            },
            columns: {},
            rowActions: {},
            onReorderRequested: null
        }
        this.options       = _.assign(defaultOptions, options);
        if (!this.options.pagination.page) {
            this.options.pagination.page = 1;
        }
        if (backendEndPointUrl instanceof BaseDataProvider) {
            this.dataProvider = backendEndPointUrl;
        } else {
            this.dataProvider = new SimpleTableDataProvider(backendEndPointUrl, {
                eventBus: this.options.eventBus,
                logger: this.options.logger
            });
        }
        
        this.filter        = JSON.parse(localStorage.getItem(this.options.identifier + '_filter') || '{}');
        this.configuration = JSON.parse(localStorage.getItem(this.options.identifier) || '{}');
    }
    
    saveConfiguration(configuration) {
        this.configuration = configuration;
        localStorage.setItem(this.options.identifier, JSON.stringify(configuration));
    }
    
    getDefaultSorting() {
        return this.options.defaultSorting;
    }
    
    setPagination(page, rowsPerPage) {
        return this.options.pagination.page = page;
        return this.options.pagination.rowsPerPage = rowsPerPage;
    }
    
    getFilter() {
        return this.filter;
    }
    
    searchBy(filter) {
        this.setFilter(filter);
        this.reload();
    }
    
    setFilter(filter) {
        this.filter = filter;
        localStorage.setItem(this.options.identifier + '_filter', JSON.stringify(filter));
    }
    
    getPagination() {
        return this.options.pagination;
    }
    
    getColumns() {
        return this.options.columns
    }
    
    getRowActions() {
        return this.options.rowActions
    }
    
    bind() {
        //prepared here in case class would like to listen its own or data provider events
    }
    
    destroy() {
        this.removeAllListeners();
        this.dataProvider.destruct();
    }
    
    reload() {
        this.emit('reload');
    }
    
    sendChangeOrderRequest(orderedIdsArray) {
        if (!this.options.onReorderRequested) {
            return axios({
                method: 'put',
                xsrfCookieName: 'xsrf-token',
                url: this.dataProvider.backendEndPointUrl.replace(/\/$/, '') + ROW_ORDER_ROUTE_URL,
                data: {'ids': orderedIdsArray}
            })
            .then((result) => {
                this.emit('table-reordered');
            })
        } else {
            return this.options.onReorderRequested(orderedIdsArray);
        }
    }
    
    setDafaultSorting(defaultSorting) {
        this.options.defaultSorting = defaultSorting;
    }
}