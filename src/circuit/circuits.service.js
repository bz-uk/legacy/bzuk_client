angular.module('adminApp').factory('circuitsService', ['$log', '$resource', '$rootScope',
    function ($log, $resource, $rootScope) {
        var circuitsResource = $resource('/api/api/index.php/circuits', {}, {
            'get': {
                method: 'GET',
                isArray: true
            },
            'save': {
                method: 'PUT'
            },
            'create': {
                method: 'POST'
            }
        });
        var primaryPointsResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points', {}, {
            'get': {
                method: 'GET',
                isArray: true
            },
            'create': {
                method: 'POST'
            }
        });
        var primaryPointsOrderResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/order', {}, {
            'save': {
                method: 'POST'
            },
        });
        var primaryPointResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/:pointId', {}, {
            'getOne': {
                method: 'GET'
            },
            'save': {
                method: 'PUT'
            },
            'delete': {
                method: 'DELETE'
            }
        });
        var primaryPointSlidesResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/:pointId/slides', {}, {
            'get': {
                method: 'GET', isArray: true
            },
            'add': {
                method: 'POST'
            }
        });
        var primaryPointSlideResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/:pointId/slides/:slideId', {}, {
            'save': {
                method: 'PUT'
            },
            'delete': {
                method: 'DELETE'
            }
        });
        var secondaryPointsResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/:primaryPointId/secondary-points', {}, {
            'get': {
                method: 'GET',
            },
            'add': {
                method: 'POST'
            }
        });
        var secondaryPointResource = $resource('/api/api/index.php/circuits/:circuitId/primary-points/:primaryPointId/secondary-points/:secondaryPointId', {}, {
            'delete': {
                method: 'DELETE',
            },
            'save': {
                method: 'PUT'
            }
        });
        var circuitResource = $resource('/api/api/index.php/circuits/:circuitId', {}, {
            'get': {
                method: 'GET'
            },
            'save': {
                method: 'PUT'
            }
        });
        var registerHandler = function (eventName, scope, callback) {
            var handler = $rootScope.$on(eventName, callback);
            scope.$on('$destroy', handler);
        }
        return {
            //subscribs
            onError: function (scope, callback) {
                registerHandler('circuits-service-error-event', scope, callback)
            },
            onAllCircuitsLoaded: function (scope, callback) {
                registerHandler('all-circuits-loaded-event', scope, callback)
            },
            onAllPrimaryPointsLoaded: function (scope, callback) {
                registerHandler('all-circuit-primary-points-loaded-event', scope, callback)
            },
            onAllSecondaryPointsLoaded: function (scope, callback) {
                registerHandler('all-circuit-secondary-points-loaded-event', scope, callback)
            },
            onCircuitDataLoaded: function (scope, callback) {
                registerHandler('circuit-data-loaded-event', scope, callback)
            },
            onCircuitSaved: function (scope, callback) {
                registerHandler('circuit-saved-event', scope, callback)
            },
            onCircuitCreated: function (scope, callback) {
                registerHandler('circuit-created-event', scope, callback)
            },
            onPrimaryPointLoaded: function (scope, callback) {
                registerHandler('primary-point-loaded-event', scope, callback)
            },
            onPrimaryPointSaved: function (scope, callback) {
                registerHandler('primary-point-saved-event', scope, callback)
            },
            onPrimaryPointCreated: function (scope, callback) {
                registerHandler('primary-point-created-event', scope, callback)
            },
            onPrimaryPointsOrderSaved: function (scope, callback) {
                registerHandler('primary-points-order-saved-event', scope, callback)
            },
            onPrimaryPointDeleted: function (scope, callback) {
                registerHandler('primary-point-deleted-event', scope, callback)
            },
            onSecondaryPointDeleted: function (scope, callback) {
                registerHandler('secondary-point-deleted-event', scope, callback)
            },
            onSecondaryPointSaved: function (scope, callback) {
                registerHandler('secondary-point-saved-event', scope, callback)
            },
            onSecondaryPointAdded: function (scope, callback) {
                registerHandler('secondary-point-added-event', scope, callback)
            },
            onPrimaryPointSlidesLoaded: function (scope, callback) {
                registerHandler('primary-point-slides-loaded-event', scope, callback)
            },
            onPrimaryPointSlideDeleted: function (scope, callback) {
                registerHandler('primary-point-slide-deleted-event', scope, callback)
            },
            // onPrimaryPointSlideEdited: function(scope, callback) {
            // registerHandler('primary-point-slide-saved-event', scope, callback)
            // },
            // onSecondaryPointDeleted: function(scope, callback) {
            // registerHandler('primary-point-slide-deleted-event', scope, callback)
            // },
            onPrimaryPointSlideEdited: function (scope, callback) {
                registerHandler('primary-point-slide-saved-event', scope, callback)
            },
            //
            loadOne: function (circuitId) {
                return circuitResource.get({
                    circuitId: circuitId
                },
                    function (data) {
                        var circuitData = angular.copy(data);
                        delete circuitData.$promise;
                        delete circuitData.resolved;
                        $rootScope.$emit('circuit-data-loaded-event', circuitData);
                    },
                    function (err) {
                        $log.error('Unable to load primary points of circuit: ', err);
                        $rootScope.$emit('circuits-service-error-event', err);
                    })
            },
            loadAllPrimaryPoints: function (circuitId, query) {
                //TODO: implement query
                return primaryPointsResource.get({
                    circuitId: circuitId
                }, function (points) {
                    var pointsList = angular.copy(points);
                    delete pointsList.$promise;
                    delete pointsList.resolved;
                    //return successCallback(circuitsList);
                    $rootScope.$emit('all-circuit-primary-points-loaded-event', pointsList);
                }, function (err) {
                    $log.error('Unable to load primary points of circuit: ', err);
                    $rootScope.$emit('circuits-service-error-event', err);
                })
            },
            loadAllSecondaryPoints: function (circuitId, primaryPointId, query) {
                //TODO: implement query
                return secondaryPointsResource.get({
                    circuitId: circuitId,
                    primaryPointId: primaryPointId
                }, function (points) {
                    var pointsList = angular.copy(points);
                    delete pointsList.$promise;
                    delete pointsList.resolved;
                    //return successCallback(circuitsList);
                    $rootScope.$emit('all-circuit-secondary-points-loaded-event', pointsList);
                }, function (err) {
                    $log.error('Unable to load primary points of circuit: ', err);
                    $rootScope.$emit('circuits-service-error-event', err);
                })
            },
            loadAllCircuits: function (query) {
                //TODO: implement query
                return circuitsResource.get({}, function (circuits) {
                    var circuitsList = angular.copy(circuits);
                    delete circuitsList.$promise;
                    delete circuitsList.resolved;
                    //return successCallback(circuitsList);
                    $rootScope.$emit('all-circuits-loaded-event', circuitsList);
                }, function (err) {
                    $log.error('Unable to load circuits: ', err);
                    $rootScope.$emit('circuits-service-error-event', err);
                })
            },
            saveAndSendEvent: function (circuit) {
                return circuitResource.save({
                    circuitId: circuit.id
                }, circuit, function (data) {
                    $rootScope.$emit('circuit-saved-event', data);
                }, function (err) {
                    console.log(err);
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to save circuit: ', err);
                    $rootScope.$emit('circuit-save-error-event', err);
                });
            },
            save: function (circuit, successCallback, errCallback) {
                return circuitResource.save({
                    circuitId: circuit.id
                }, circuit, function (data) {
                    successCallback(data);
                }, function (err) {
                    console.log(err);
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to save circuit', err);
                    errCallback(message);
                });
            },
            createAndSendEvent: function (circuit) {
                return circuitsResource.create({}, circuit, function (data) {
                    $rootScope.$emit('circuit-created-event', data);
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to create circuit: ', err);
                    $rootScope.$emit('circuit-create-error-event', err);
                })
            },
            loadOnePrimaryPoint: function (circuitId, pointId) {
                return primaryPointResource.get({
                    circuitId: circuitId,
                    pointId: pointId
                }, {}, function (pointData) {
                    $rootScope.$emit('primary-point-loaded-event', pointData);
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to load primary point: ', err);
                    $rootScope.$emit('primary-point-loaded-error-event', err);
                })
            },
            createPrimaryPoint: function (circuitId, point) {
                return primaryPointsResource.create({
                    circuitId: circuitId
                }, point, function (data) {
                    $rootScope.$emit('primary-point-created-event', data);
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to create primary point: ', err);
                    $rootScope.$emit('primary-point-created-error-event', err);
                })
            },
            savePrimaryPoint: function (circuitId, point) {
                return primaryPointResource.save({
                    circuitId: circuitId,
                    pointId: point.id
                }, point, function (data) {
                    $rootScope.$emit('primary-point-saved-event', data);
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to create primary point: ', err);
                    $rootScope.$emit('primary-point-saved-error-event', err);
                })
            },
            savePrimaryPointsOrder: function (circuitId, newOrder, tableQuery) {
                var data = {
                    order: newOrder,
                    fromIndex: (tableQuery.page - 1) * tableQuery.limit
                }
                return primaryPointsOrderResource.save({
                    circuitId: circuitId
                }, data, function (data) {
                    $rootScope.$emit('primary-points-order-saved-event', data);
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data;
                    } else {
                        message = err.data.message;
                    }
                    $log.error('Unable to create primary point: ', err);
                    $rootScope.$emit('primary-points-order-save-error-event', err);
                })
            },
            removeSecondaryPoint: function (circuitId, primaryPointId, secondaryPointId) {
                return secondaryPointResource.delete({
                    circuitId: circuitId,
                    primaryPointId: primaryPointId,
                    secondaryPointId: secondaryPointId
                }, function (data) {
                    $rootScope.$emit('secondary-point-deleted-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to delete secondary point: ', err)
                    $rootScope.$emit('secondary-point-deleted-error-event', err)
                })
            },
            addSecondaryPoints: function (circuitId, primaryPointId, points) {
                return secondaryPointsResource.add({
                    circuitId: circuitId,
                    primaryPointId: primaryPointId
                }, points, function (data) {
                    $rootScope.$emit('secondary-point-added-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to add secondary points: ', err)
                    $rootScope.$emit('secondary-point-added-error-event', err)
                })
            },
            updateSecondaryPoint: function (circuitId, primaryPointId, secondaryPointId, newData) {
                return secondaryPointResource.save({
                    circuitId: circuitId,
                    primaryPointId: primaryPointId,
                    secondaryPointId: secondaryPointId
                }, newData, function (data) {
                    $rootScope.$emit('secondary-point-saved-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to save secondary point: ', err)
                    $rootScope.$emit('secondary-point-saved-error-event', err)
                })
            },
            deletePrimaryPoint: function (circuitId, point) {
                return primaryPointResource.delete({
                    circuitId: circuitId,
                    pointId: point.id
                },
                    function (data) {
                        $rootScope.$emit('primary-point-deleted-event', data)
                    },
                    function (err) {
                        var message = '';
                        if (err.status == 500) {
                            message = err.data
                        } else {
                            message = err.data.message
                        }
                        $log.error('Unable to delete primary point: ', err)
                        $rootScope.$emit('primary-point-deleted-error-event', err)
                    })
            },
            //start: primary point slides
            loadPrimaryPointSlides: function (circuitId, pointId, query) {
                return primaryPointSlidesResource.get({
                    circuitId: circuitId,
                    pointId: pointId
                }, query, function (data) {
                    $rootScope.$emit('primary-point-slides-loaded-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to load primary point slides: ', err)
                    $rootScope.$emit('primary-point-slides-loaded-error-event', err)
                })
            },
            deletePrimaryPointSlide: function (circuitId, pointId, slideId) {
                return primaryPointSlideResource.delete({
                    circuitId: circuitId,
                    pointId: pointId,
                    slideId: slideId
                }, function (data) {
                    $rootScope.$emit('primary-point-slide-deleted-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to delete slide: ', err)
                    $rootScope.$emit('primary-point-slide-deleted-error-event', err)
                })
            },
            updatePrimaryPointSlide: function (circuitId, pointId, slideId, slideData) {
                return primaryPointSlideResource.save({
                    circuitId: circuitId,
                    pointId: pointId,
                    slideId: slideId
                }, slideData, function (data) {
                    $rootScope.$emit('primary-point-slide-saved-event', data)
                }, function (err) {
                    var message = '';
                    if (err.status == 500) {
                        message = err.data
                    } else {
                        message = err.data.message
                    }
                    $log.error('Unable to save slide: ', err)
                    $rootScope.$emit('primary-point-slide-saved-error-event', err)
                })
            },
            //end: primary point slides
        }
    }
]);
