<?php
namespace botgarApi\article;

use botgarApi\image\ImageModel;
use Slim\Slim;

class Delete
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($articleId)
    {
        if ($articleId == null || !is_numeric($articleId) || $articleId === 0) {
            $response = ['status' => 'error', 'message' => 'Article id must be a number'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host=' . $conf['db:host'] . ';port=' . $conf['db:port'] . ';dbname=' . $conf['db:dbname'] . ';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $db->beginTransaction();
        $now = strftime('%Y-%m-%d %H:%M:%S');
        $sql = "UPDATE article SET deleted_at=" . $db->quote($now) . " where id=" . $articleId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            $response = ['status' => 'error', 'message' => 'Problem during deleting article'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }
        $db->commit();
        $response = ['status' => 'ok', 'message' => 'Article deleted'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
