'use strict'
/**
 * @desc czech language file for login app
 *
 */
module.exports = {
  "User name": "Přihlašovací jméno",
  "Password":  "Heslo",
  "remember":  "zapamatuj",
  "login":     "Přihlásit"
}
