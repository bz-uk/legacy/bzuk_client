// fucking not working
// class NewPlantsService extends baseService {
// 	constructor($log, $resource, $rootScope) {
// 		super($rootScope)
// 		this.$resource = $resource
// 		this.$log = $log
// 		this.allPlantsTags = []
// 		this.initResurces()
// 	}
//
// 	initResurces() {
// 		this.plantResource = this.$resource('/api/api/index.php/plants/:plantId', {}, {
// 			'save': {method: 'PUT'},
// 			'get': {method: 'GET'},
// 			'delete': {method: 'DELETE'}
// 		})
// 	}
//
// 	onPlantSaved(scope, callback) {
// 		this.registerHandler('plant-saved-event', scope, callback)
// 	}
//
// 	updateSinglePlant(plantModel){
// 		return this.plantResource.save({plantId: plantModel.id},plantModel, function(data) {
// 			this.emit('plant-saved-event', plantModel, 'Plant "'+plantModel.name+'" saved successfully')
// 		}, function(err) {
// 			console.log(err);
// 			var message = '';
// 			if (err.status === 500) {
// 				message = err.data;
// 			} else {
// 				message = err.data.message;
// 			}
// 			this.emit('plant-saved-error-event', plantModel);
// 		});
// 	}
// }
// NewPlantsService.$inject = ['$log', '$resource', '$rootScope'];

//angular.module('adminApp').factory('NewPlantsService', NewPlantsService)

angular.module('adminApp').factory('webpService', ['$log', '$q', function ($log, $q) {
    var hasWebP = false;

    var img = new Image();
    img.onload = function () {
        hasWebP = !!(img.height > 0 && img.width > 0);
        //ctrl.webp = ctrl.hasWebP ? '/webp' : '';
    };
    img.onerror = function () {
        hasWebP = false;
        //ctrl.webp = ctrl.hasWebP ? '/webp' : '';
    };
    img.src = 'data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=';

    return {
        supported: function () {
            return hasWebP;
        }
    }
}]);
