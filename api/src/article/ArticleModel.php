<?php
namespace botgarApi\article;

/**
 * Created by PhpStorm.
 * User: juraj
 * Date: 21.11.15
 * Time: 22:28
 */

class ArticleModel
{
    private $id;
    private $dateCreated;
    private $title;
    private $contents;
    private $dateUpdated;
    private $type;
    private $code;

    public function loadFromXml(\SimpleXMLElement $xml)
    {
        $this->id = (String)$xml->id;
        $this->title = (String)$xml->title;
        $this->type = (String)$xml->type;
        $this->dateCreated = (String)$xml->date_created;
        $this->dateUpdated = (String)$xml->date_updated;
        $this->contents = base64_encode(file_get_contents('../data/articles/' . $this->id . '.html')); //(String)$xml->content;
        $this->code = (String)$xml->code;
    }

    /*
    * @return array
    */
    public function asArray()
    {
        return array(
            'id' => (String)$this->id,
            'title' => (String)$this->title,
            'dateCreated' => (String)$this->dateCreated,
            'content' => (String)$this->contents,
            'code' => (String)$this->code
        );
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function getCode()
    {
        return $this->code;
    }
}