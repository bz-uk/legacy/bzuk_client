'use strict';
import React from 'react';

import StringUtil from './../util/stringUtil';

export default class CkEditor extends React.Component {
    
    constructor(props) {
        super(props);
        this.name  = 'editor_' + StringUtil.randomString(8);
        this.value = this.props.defaultValue || null;
    }
    
    componentDidMount() {
        setTimeout(() => { //needed because of ck editor. without this it is not able to recognise 'editor' textarea element
            this.initEditor();
        }, 0);
        if (this.props.stickyToolbar) {
            window.addEventListener("scroll", this.onScroll);
        }
    }
    
    componentWillUnmount() {
        if (this.props.stickyToolbar) {
            window.removeEventListener('scroll', this.onScroll);
        }
    }
    
    onScroll = () => {
        var editor = document.getElementById('cke_' + this.name);
        if (editor) {
            let editorPosition   = CKEDITOR.instances[this.name].container.$.getBoundingClientRect();
            let topToolbarHeight = document.getElementById('top-menu').clientHeight;
            let topPosition      = topToolbarHeight + Math.abs(editorPosition.top); //-10 = magical constanct (padding+margin of the header)
            if (editorPosition.top < 0) {
                document.getElementById(CKEDITOR.instances[this.name].id + '_top').classList.add('fixed-editor-toolbar');
                document.getElementById(CKEDITOR.instances[this.name].id + '_top').style.top = topPosition + 'px';
            } else {
                document.getElementById(CKEDITOR.instances[this.name].id + '_top').classList.remove('fixed-editor-toolbar');
                document.getElementById(CKEDITOR.instances[this.name].id + '_top').style.top = '';
            }
        }
    }
    
    initEditor = () => {
        var self = this;
        CKEDITOR.replace(this.name, this.props.configuration);
        CKEDITOR.instances[this.name].on('change', function () {
            let data = CKEDITOR.instances[this.name].getData();
            if (self.value === null) {
                self.value = data;
                return;
            }
            self.value = data;
            if (self.props.onChange) {
                self.props.onChange(self.value);
            }
        }.bind(this)); //??? bind?
        CKEDITOR.on('instanceReady', (evt) => {
            evt.editor.on('selectionChange', (elm) => {
                if (self.value === null) {
                    self.value = CKEDITOR.instances[this.name].getData();
                    return;
                }
                self.value = CKEDITOR.instances[this.name].getData();
                if (self.props.onChange) {
                    self.props.onChange(self.value);
                }
            })
        });
        CKEDITOR.instances[this.name].on('fileUploadRequest', function (evt) {
            var xhr = evt.data.fileLoader.xhr;
            
            function getCookie(name) {
                var value = "; " + document.cookie;
                var parts = value.split("; " + name + "=");
                if (parts.length == 2) return parts.pop().split(";").shift();
            }
            
            xhr.setRequestHeader('Cache-Control', 'no-cache');
            xhr.setRequestHeader('XSRF-TOKEN', getCookie('XSRF-TOKEN'));
        });
    }
    
    render() {
        var styles = {
            editorDiv: {
                position: 'relative',
                border: this.props.errorText ? '2px solid rgb(244, 67, 54)' : ''
            },
            editorErrorMessage: {
                paddingLeft: 10,
                color: 'rgb(244, 67, 54)'
            }
        }
        return (
            <div>
                <div>{this.props.label ?
                    this.props.label
                    :
                    ''}{this.props.errorText ?
                    <span style={styles.editorErrorMessage}>{this.props.errorText}</span>
                    :
                    ''}
                </div>
                <div style={styles.editorDiv}>
                                <textarea style={styles.editorDiv} name={this.name}
                                          cols={this.props.cols}
                                          rows={this.props.rows}
                                          defaultValue={this.props.defaultValue}></textarea>
                </div>
            </div>
        )
    }
}