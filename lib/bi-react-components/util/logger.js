'use strict';
/**
 * @class Logger
 */
export default class Logger {
    static LEVEL_NONE  = 0;
    static LEVEL_ERROR = 1;
    static LEVEL_WARN  = 2;
    static LEVEL_DEBUG = 3;
    
    /**
     *
     * @param {int} logLevel
     */
    constructor(logLevel) {
        this.logLevel = logLevel || Logger.LEVEL_NONE;
    }
    
    debug() {
        if (this.logLevel < Logger.LEVEL_DEBUG) {
            return;
        }
        console.log.apply(null, arguments);
    }
    
    warn() {
        if (this.logLevel < Logger.LEVEL_WARN) {
            return;
        }
        console.warn.apply(null, arguments);
    }
    
    error() {
        if (this.logLevel < Logger.LEVEL_ERROR) {
            return;
        }
        console.error.apply(null, arguments);
    }
}