'use strict'
const CronJob = require('cron').CronJob;

const TASK_NAME = 'imageResizer';

module.exports = function (timing, timeZone = 'America/Los_Angeles') {
    return new CronJob(timing, function () {
            //code
        }, function () {
            console.log('Task ' + TASK_NAME + ' stopped');
        },
        false, /* Start the job right now */
        timeZone /* Time zone of this job. */
    );
};
