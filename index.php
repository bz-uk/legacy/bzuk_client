<?php
function redirect($url, $statusCode = 303)
{
    header('Location: ' . $url, true, $statusCode);
    die();
}


include 'api/config/config.php';

$db = new \PDO('mysql:host='.$config['db:host'].';port='.$config['db:port'].';dbname='.$config['db:dbname'].';charset=UTF8;', $config['db:user'], $config['db:pass']);
//TODO load session from DB

if (!$_COOKIE['connect_sid']) {
    redirect('/login', 302);
    die();
} else {
    $sess_name = 'connect_sid';
    if (substr($_COOKIE[$sess_name], 0, 2) === "s:") {
        $_COOKIE[$sess_name] = substr($_COOKIE[$sess_name], 2);
    }
    $dot_pos = strpos($_COOKIE[$sess_name], ".");
    if ($dot_pos !== false) {
        $sessionId = substr($_COOKIE[$sess_name], 0,$dot_pos);
        session_start();
        $q = "SELECT * FROM ro2_sessions  WHERE  session_id = " . $db->quote($sessionId);
$result = $db->query($q);
if ($result) {
$row = $result->fetch(\PDO::FETCH_ASSOC);
if (time() > $row['expires']) { //TODO: test it
redirect('/login', 302);
die();
}
$sessionData = [];
$decodedSession = json_decode($row['data']);
foreach ($decodedSession as $itemName => $key) {
$_SESSION[$itemName] = $decodedSession->{$itemName};
}
} else {
redirect('/login', 302);
die();
}
}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Botanicka zahrada - admin</title>
        <base href="/">
        <base target="_blank">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="initial-scale=1"/>

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,400italic'>
        <link rel="stylesheet" href="bower_components/angular-material/angular-material.css"/>
        <link href="bower_components/md-color-picker/dist/mdColorPicker.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="public/styles.css"/>

        <link href="bower_components/angular-material-data-table/dist/md-data-table.min.css" rel="stylesheet" type="text/css"/>

        <meta http-equiv="cache-control" content="max-age=900" />
        <meta http-equiv="cache-control" content="public" />
        <meta http-equiv="pragma" content="public" />
    </head>

    <body layout="column" ng-app="adminApp">
        <div ng-view></div>
        <!--<script src="bower_components/jquery/dist/jquery.min.js"></script>-->
        <!--<script src="jqueryui/jquery-ui.min.js"></script>-->
        <!--<script src="node_modules/angular/angular.js"></script>-->
        <!--<script src="node_modules/angular-resource/angular-resource.js"></script>-->
        <!--<script src="node_modules/angular-material-icons/angular-material-icons.js"></script>-->
        <!--<script src="node_modules/angular-animate/angular-animate.js"></script>-->
        <!--<script src="node_modules/angular-aria/angular-aria.js"></script>-->
        <!--<script src="bower_components/angular-route/angular-route.js"></script>-->
        <!--<script src="bower_components/angular-ui-sortable/sortable.js"></script>-->
        <!--<script src="bower_components/lodash/dist/lodash.min.js"></script>-->
        <!--<script src="bower_components/angular-simple-logger/dist/angular-simple-logger.min.js"></script>-->
        <!--<script src="bower_components/angular-google-maps/dist/angular-google-maps.min.js"></script>-->
        <!--<script src="bower_components/ng-file-upload-shim/ng-file-upload-shim.min.js"></script>-->
        <!--<script src="bower_components/ng-file-upload/ng-file-upload.min.js"></script>-->
        <!--<script src="node_modules/angular-sanitize/angular-sanitize.min.js"></script>-->
        <!--<script src="node_modules/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js"></script>-->
        <!--<script src="bower_components/bluebird/js/browser/bluebird.min.js"></script>-->
        <!--<script src="public/qrcode.js"></script>-->
        <!--<script src="node_modules/angular-qrcode/angular-qrcode.js"></script>-->
        <script src="public/libs.js"></script>
        <script src="ckeditor/ckeditor.js"></script>
        <!--<script src="node_modules/angular-ckeditor/angular-ckeditor.js"></script>-->
        <!--<script type="text/javascript" src="bower_components/angular-material/angular-material.js"></script>-->
        <!--<script type="text/javascript" src="bower_components/angular-material-data-table/dist/md-data-table.min.js"></script>-->

        <script src="public/app_1503348013180.min.js"></script>
        
    </body>
</html>
