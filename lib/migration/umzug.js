const sequelize = require('./../database/local').sequelize;
const process = require('process');
const Umzug = require('umzug');
const path = require('path');
const CustomStorage = require('./customStorage');

let umzug = new Umzug({
    storage: new CustomStorage(),
    migrations: {
        params: [
            sequelize.getQueryInterface(), // queryInterface
            sequelize.constructor, // DataTypes
            function () {
                throw new Error('Migration tried to use old style "done" callback. Please upgrade to "umzug" and return a promise instead.');
            }
        ],
        path: path.resolve(process.cwd(), 'migrations'),
        pattern: /\.js$/
    },

    logging: function () {
        console.log.apply(null, arguments);
    },
});

function logUmzugEvent(eventName) {
    return function (name, migration) {
        console.log(`${ name } ${ eventName }`);
    }
}

umzug.on('migrating', logUmzugEvent('migrating'));
umzug.on('migrated', logUmzugEvent('migrated'));
umzug.on('reverting', logUmzugEvent('reverting'));
umzug.on('reverted', logUmzugEvent('reverted'));

module.exports = umzug;