var imagesService = function ($log, $resource, $rootScope) {
    var imageResource = $resource('/api/api/index.php/images/:imageId', {}, {
        'save': {
            method: 'PUT'
        },        
    });

    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }
    return {
        onImageSaveError: function (scope, callback) {
            registerHandler('image-save-error-event', scope, callback)
        },  
        /**
         * @return Promise
         */
        save: function (imageId, data) {            
        	var promise = imageResource.save({imageId: imageId}, data,
                function (data) {
                    $rootScope.$emit('image-saved-event');
                    return data;
                },
                function (err) {
                    $log.error('Unable to save image: ', err);
                    $rootScope.$emit('image-save-error-event', err);
                }).$promise;
        	return promise;
        },        
    }
}
imagesService.$inject = ['$log', '$resource', '$rootScope'];
angular.module('adminApp').factory('imagesService', imagesService);