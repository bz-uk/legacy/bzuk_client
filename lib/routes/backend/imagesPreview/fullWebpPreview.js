'use strict';
const debug = require('debug')('image-upload');
const sharp = require('sharp');
const process = require('process');
const path = require('path');
const fs = require('fs');
const os = require('os');
const mkdirp = require('mkdirp');
const HttpStatusCodes = require('http-status-codes');


const routeUtil = require('./../../../util/route');
const fsPromise = require('./../../../common/fs');
const configuration = require('./../../../common/config');
const knex = require('./../../../database/local').knex;
const BaseRoute = require('./../../../common/route');
const sequelize = require('./../../../database/local').sequelize;
const UnauthorizedError = require('./../../../error/unauthorized');
const Resizer = require('./../../../common/image/resizer');

const UPLAOD_TARGET_PATH = 'data/images/';
const ORIGINALS_PATH = 'originals';
const WIDTH = 2048;

module.exports = class Route extends BaseRoute {
    /**
     * @override >max-width/:maxWidth
     */
    getConfig() {
        return {
            method: 'get',
            path: '/images-preview/:imageId/webp',
            name: 'fullWidthWebpPreview'
        }
    }

    main(req, res) {
        debug('Params: ', req.params)
        if (!req.params.imageid) {
            res.status(HttpStatusCodes.BAD_REQUEST).json({errorCode: 'invalid_value', message: 'invalid value'});
            return Promise.resolve();
        }
        let fileName = req.params.imageid + '.webp';
        debug('Searching for file ', fileName);
        return fsPromise.fileExists(path.join(process.cwd(), UPLAOD_TARGET_PATH, WIDTH + '', fileName))
        .then((exists) => {
            if (!exists) {
                debug('file does not exists.')
                return sequelize.models.ImageCompat.findOne({where: {id: req.params.imageid}})
                .then((image) => {
                    debug('resizing...');
                    return Resizer.resize(path.join(process.cwd(), UPLAOD_TARGET_PATH, 'originals', image.name), 'webp', image.id, [WIDTH], UPLAOD_TARGET_PATH);
                })
            } else {
                debug('file already exists.')
            }
        })
        .then(() => {
            debug('returning image');
            routeUtil.serveFileInline(res, path.join(process.cwd(), UPLAOD_TARGET_PATH, WIDTH + '', req.params.imageid + '.webp'), 'webp');
        })
    }
}