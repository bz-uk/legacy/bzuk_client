'use strict';
angular.module('adminApp')
    .controller('printPlantsListController', ['$scope', '$location', '$mdDialog', '$mdMedia', 'articlesService', '$timeout', 'uiGmapIsReady', 'plantsService', 'settingsService', 'tableService', '$routeParams', '$log',
        function($scope, $location, $mdDialog, $mdMedia, articlesService, $timeout, uiGmapIsReady, plantsService, settingsService, tableService, $routeParams, $log) {
            $scope.settings = []
            settingsService.load(function(data) {
                $scope.settings = data
            })
            $scope.mapObject
            $scope.filterByMap = true
            $scope.$watch('filterByMap', function(newValue) {
                if (!newValue) {
                    $scope.plants = $scope.allPlants
                } else {
                    $scope.filterPlantsByBounds($scope.mapObject.getBounds())
                }
            })            
            $scope.plants = []
            $scope.tableModel = {}
            $scope.visibleColumns = {
                name_lat: true,
                name: true,
                family_lat: true,
                family: true,
                category: true
            }
            $scope.filter = {
                category: undefined,
                family: undefined,
                tags: [],
                search: undefined
            }
            $scope.openMenu = function($mdOpenMenu, ev) {
                $mdOpenMenu(ev);
            };

            $scope.categories = []
            $scope.families = []
            $scope.getCategoryName = function(categoryId) {
                for (var i = 0; i < $scope.categories.length; i++) {
                    // console.log($scope.categories[i].id)
                    if ($scope.categories[i].id == categoryId) {
                        // console.log('have cat')
                        return $scope.categories[i].name
                    }
                }
            }
            $scope.getFamilyName = function(familyId) {
                for (var i = 0; i < $scope.families.length; i++) {
                    if ($scope.families[i].id == familyId) {
                        return $scope.families[i].name
                    }
                }
            }
            plantsService.getCategories(function(categories) {
                $scope.categories = categories
            })
            plantsService.getFamilies(function(families) {
                $scope.families = families
            })
            $scope.markers = []
            $scope.tableModel = {
                limitOptions: [5, 10, 15],
                query: {
                    order: 'name_lat',
                    limit: 10,
                    page: 1
                },
                selected: []
            }
            $scope.allPlants = []
            $scope.plants = []
            $scope.$watch('tableModel', function() {
                // console.log('CH=====================')
                // console.log($scope.tableModel)
            })

            
            $scope.getPlants = function() {
                var filter = angular.copy($scope.filter)
                if (!$scope.filter.expanded) {
                    filter.category = undefined
                    filter.family = undefined
                    filter.is_blooming = false
                    filter.is_growing = false
                }
                plantsService.getAllPlants($scope.tableModel.query, filter, function(markers, rows) {
                    // console.log(markers)
                    // console.log(rows)
                    $scope.markers = markers
                    $scope.allPlants = angular.copy(rows)
                    $scope.plants = rows             
                })
            }
            $scope.filterPlantsByBounds = function(bounds) {
                //??!?!?
                var visiblePlantIds = []
                for (var i = 0; i < $scope.markers.length; i++) {
                    var pos = new google.maps.LatLng($scope.markers[i].latitude, $scope.markers[i].longitude)
                    if (bounds.contains(pos)) {
                        visiblePlantIds.push(parseInt($scope.markers[i].plant_id))
                    }
                }
                $scope.plants = []
                for (var i = 0; i < $scope.allPlants.length; i++) {
                    var plantId = $scope.allPlants[i].id + 0
                    //console.log(visiblePlantIds)                    
                    if (visiblePlantIds.indexOf(plantId) > 0) {
                        //console.log('found!')
                        $scope.plants.push($scope.allPlants[i])
                    }
                }                
            } 
            tableService.load($routeParams.filterCode, function(filter) {
                $scope.filter = JSON.parse(filter.filter_json)
                // console.log($scope.filter)
                $scope.getPlants()
            })

            $scope.map = {
                markerOptions: {
                    draggable: false,
                },
                markerEvents: {},
                center: {
                    latitude: 0,
                    longitude: 0
                },
                zoom: 18,
                bounds: {
                    northeast: {
                        latitude: 50.0689551446,
                        longitude: 14.4179141521
                    },
                    southwest: {
                        latitude: 50.0733622808,
                        longitude: 14.4247806072
                    }
                }
            }


            $scope.getNormalizedCoord = function(coord, zoom) {
                var y = coord.y;
                var x = coord.x;

                // tile range in one direction range is dependent on zoom level
                // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
                var tileRange = 1 << zoom;

                // don't repeat across y-axis (vertically)
                if (y < 0 || y >= tileRange) {
                    return null;
                }

                // repeat across x-axis
                if (x < 0 || x >= tileRange) {
                    x = (x % tileRange + tileRange) % tileRange;
                }

                return {
                    x: x,
                    y: y
                };
            }

            $scope.initMap = function() {
                $scope.moonMapType = new google.maps.ImageMapType({
                    getTileUrl: function(coord, zoom) {
                        // console.log(getProjection())
                        var normalizedCoord = $scope.getNormalizedCoord(coord, zoom);
                        if (!normalizedCoord) {
                            return null;
                        }

                        var bound = Math.pow(2, zoom);
                        var url = '//' + $scope.settings.baseUrl + 'api/map2/' + zoom + '/' + normalizedCoord.x + '/' + normalizedCoord.y + '.png'
                        return url;
                    },
                    tileSize: new google.maps.Size(256, 256),
                    maxZoom: 18,
                    minZoom: 17,
                    radius: 1738000,
                    name: 'Moon'
                });
            }

            uiGmapIsReady.promise(1).then(function(instances) {
                instances.forEach(function(inst) {
                    var map = inst.map
                    $scope.mapObject = inst.map
                    var uuid = map.uiGmap_id
                    var mapInstanceNumber = inst.instance // Starts at 1.
                    console.log('ready!')
                        //map.addTileOverlay(new TileOverlayOptions().tileProvider());
                    $scope.initMap()
                    $scope.map.center = {
                        latitude: parseFloat($scope.settings.adminMapCenterLat),
                        longitude: parseFloat($scope.settings.adminMapCenterLon)
                    }
                    map.overlayMapTypes.push($scope.moonMapType);
                    map.addListener('bounds_changed', function() {
                        // console.log('bbbbbb')                        
                    if ($scope.filterByMap) {
                        $scope.filterPlantsByBounds(map.getBounds())
                    }
                    })
                    // var infoWindows = []
                    var markers = []
                    for (var i = 0; i < $scope.markers.length; i++) {
                        var div = document.createElement('DIV');
                        var window_id = i
                        div.innerHTML = '<div class="numbered-marker-bubble"><div class="number">' + $scope.markers[i].plant_id + '</div><div class="bottom-left-corner"></div></div>';
                        markers[i] = new RichMarker({
                            map: map,
                            position: new google.maps.LatLng($scope.markers[i].latitude, $scope.markers[i].longitude),
                            draggable: false,
                            flat: true,
                            anchor: RichMarkerPosition.BOTTOM_LEFT,
                            content: div,
                            // window_id: window_id
                        });
                        // var contentString = '<div id="content">#' + $scope.markers[i].plant_id
                        // if ($scope.markers[i].title !== undefined) {
                          // contentString += ': ' + $scope.markers[i].title
                          // if ($scope.markers[i].title_lat !== undefined) {
                            // contentString += '<br/>'
                          // }
                        // }
                        // if ($scope.markers[i].title_lat !== undefined) {
                          // contentString += '<i>'+$scope.markers[i].title_lat+'</i>'
                        // }
                        // infoWindows[i] = new google.maps.InfoWindow({
                            // content: contentString
                        // });
                        // google.maps.event.addListener(markers[i], 'click', function(e) {
                          // console.log(this)
                          // infoWindows[this.window_id].open(map, markers[this.window_id])
                        // })
                      }
                })
            })
        }
    ])
