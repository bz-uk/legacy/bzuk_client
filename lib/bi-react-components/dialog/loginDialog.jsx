'use strict';
import React from 'react';
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import _ from 'lodash';
import FormField from './../form/formField';
import FormController from './../form/formController.js';
import RequiredValidator from './../form/validators/requiredValidator.js';

/**
 * @deprecated
 * this component is deprecated. Use component/loginDialog
 * @class LoginDialog
 */
export default class LoginDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state          = {
            model: {
                username: '',
                password: '',
                remembers: false,
            }
        }
        this.formController = new FormController();
    }
    
    componentWillUnmount() {
        this.formController.off('field-change', this.onFormFieldChange);
    }
    
    componentDidMount() {
        this.formController.on('field-change', this.onFormFieldChange);
    }
    
    onFormFieldChange = (fieldName, fieldValue) => {
        this.state.model[fieldName] = fieldValue;
        this.state.valid            = this.formController.valid;
        this.setState(this.state);
    }
    
    onLoginButtonClick = () => {
        this.props.onLoginCallback(this.state.model);
    }
    
    getUsernameValidators() {
        if (this.props.usernameValidators && _.isArray(this.props.usernameValidators)) {
            return this.props.usernameValidators;
        }
        return [new RequiredValidator({errorMessage: 'is empty'})]
    }
    
    handleRememberChange = (event, checked) => {
        this.state.model.remembers = checked;
        this.setState(this.state);
    }
    
    render() {
        let styles = {
            CardStyle: _.assign({
                backgroundColor: '#f7f7f7'
            }, this.props.cardStyle),
            CardActionsStyle: {
                textAlign: 'right'
            },
            ContainerStyle: this.props.containerStyle || {}
        };
        return (
            <div style={styles.ContainerStyle}>
                <Card style={styles.CardStyle}>
                    <CardTitle title="Login"/>
                    <CardText>
                        <FormField value={this.state.model.username} formController={this.formController}
                                   validators={this.getUsernameValidators()}
                                   errorMessage="Username {commaSeparatedErrors}" name="username">
                            <TextField hintText={this.props.usernameHint || "valid username"}
                                       floatingLabelText={this.props.usernameLabel || "User name"}
                                       type={this.props.usernameType || ''}
                            />
                        </FormField>
                        <br/>
                        <FormField value={this.state.model.password} formController={this.formController}
                                   validators={[new RequiredValidator({errorMessage: 'is empty'})]}
                                   errorMessage="Password {commaSeparatedErrors}" name="password">
                            <TextField value={this.state.password} onChange={this.handlePasswordChange}
                                       hintText="password"
                                       floatingLabelText="Password"
                                       type="password"
                            />
                        </FormField>
                        <br/>
                        <Checkbox defaultChecked={this.state.model.remembers} onCheck={this.handleRememberChange}
                                  label="remember"
                        />
                    </CardText>
                    <CardActions style={styles.CardActionsStyle}>
                        <RaisedButton disabled={!this.state.valid} onClick={this.onLoginButtonClick} label="Login"/>
                    </CardActions>
                </Card>
            </div>
        );
    }
}