'use strict'
angular.module('adminApp').controller('circuitEditController', ['$scope', '$rootScope', '$log', '$mdDialog', '$mdMedia', 'circuitsService', '$timeout', '$routeParams', 'uiGmapIsReady', 'settingsService', 'Upload', '$location', '$q',
    function($scope, $rootScope, $log, $mdDialog, $mdMedia, circuitsService, $timeout, $routeParams, uiGmapIsReady, settingsService, Upload, $location, $q) {
        //start:models
        $scope.circuit = {}

        $scope.settings = []

        $scope.files //for uploads

        $scope.saving = false
        $scope.isNew = $routeParams.circuitId == '_'

        $scope.primaryPointsTable = {
            points: [],
            query: {
                order: 'order',
                limit: 10,
                page: 1
            },
            selected: [],
        }
        $scope.secondaryPointsTable = {
                points: [],
                query: {
                    order: 'id',
                    limit: 10,
                    page: 1
                },
                selected: [],
        }
        $scope.options = { //for mce
            language: 'cz',
            format_tags: 'h1;h2',
            allowedContent: true,
            entities: false,
            removePlugins: 'templates'
            //width: '100%'
        };
        $scope.map = {
            infoWindowTemplate: 'plants/plantEditMapMarkerInfoWindow.template.html',
            markerOptions: {
                draggable: true,
                animation: 'DROP'
            },
            markerEvents: {
                dragend: function(markerModel, eventName, markerObject) {
                  //
                }
            },
            center: {
                latitude: 50.0708767569985,
                longitude: 14.4208672642708
            },
            zoom: 18,
            bounds: {
                northeast: {
                    latitude: 50.0689551446,
                    longitude: 14.4179141521
                },
                southwest: {
                    latitude: 50.0733622808,
                    longitude: 14.4247806072
                }
            },
            events: {
                click: function(mapModel, eventName, originalEventArgs) {
                    $scope.$apply(function() {
                        //console.log('click!')

                        var e = originalEventArgs[0];
                        //$scope.model.markers = []
                        $scope.markersIdCounter++
                        $scope.model.markers.push({
                            latitude: e.latLng.lat(),
                            longitude: e.latLng.lng(),
                            id: $scope.markersIdCounter,
                            title: $scope.model.name
                        })
                        console.log($scope.model.markers)
                    });
                }
            }
        }
        $scope.primaryTableDeferred
        $scope.sortableOptions = {
            stop: function(e, ui) {
                var order = [];
                for (var i = 0; i < $scope.primaryPointsTable.points.length; i++) {
                    order.push($scope.primaryPointsTable.points[i].id)
                }
                console.log(order)
                $scope.primaryTableDeferred = $q.defer();
                $scope.primaryPointsTable.promise = $scope.primaryTableDeferred.promise
                circuitsService.savePrimaryPointsOrder($scope.circuit.id, order, $scope.primaryPointsTable.query)
            },
            helper: "clone",
            forceHelperSize: true,
            forcePlaceholderSize: true
        }

        //end:models
        //start: upload
        $scope.upload = function(file) { //TODO: to service??
          if (file === undefined) {
            return;
          }
            if (!file.$error) {
                var newImg = {
                    id: undefined,
                    url: 'https://bz-uk.cz/sites/default/files/styles/medium/public/images/abutilon_megapotamicum2.jpg',
                    status: 'uploading'
                }
                $scope.circuit.image = newImg
                console.log($scope.circuit)
                Upload.upload({
                    url: '/api/api/index.php/images', //TODO: load from settings
                    data: {
                        file: file
                    }
                }).then(function(resp) {
                        console.log('upload complete')
                        console.log(resp)
                        $timeout(function() {
                            console.log('complete timeout')
                            if (resp.data.id !== undefined && parseInt(resp.data.id) > 0) {
                                console.log('done!')
                                newImg.url = resp.data.url
                                newImg.id = parseInt(resp.data.id)
                                newImg.status = 'done'
                            }
                            $scope.log = 'file: ' +
                                resp.config.data.file.name +
                                ', Response: ' + JSON.stringify(resp.data) +
                                '\n' + $scope.log;
                        });
                    },
                    function(resp) {
                        alert('Unable to uplaod image: ' + resp.data)
                        $scope.circuit.image = undefined
                    },
                    function(evt) {
                        var progressPercentage = parseInt(100.0 *
                            evt.loaded / evt.total);
                        newImg['progress'] = progressPercentage
                    });
            }

        }
        $scope.$watch('files', function() {
            $scope.upload($scope.files);
        })
        $scope.deleteImage = function(image) {
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this image?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                $scope.circuit.image = undefined
            }, function() {});
        }
        //end: upload
        //start: listeners
        settingsService.onSettingsLoaded($scope, function(event, settings){
          $scope.settings = settings
        })
        circuitsService.onAllPrimaryPointsLoaded($scope, function(event, pointsData) {
            $scope.primaryPointsTable.points = pointsData
            console.log($scope.primaryPointsTable.points)
            if ($scope.primaryTableDeferred !== undefined) {
              $scope.primaryTableDeferred.resolve()
            }
            event.stopPropagation()
        })
        circuitsService.onPrimaryPointsOrderSaved($scope, function(event, data) {
          $scope.getPrimaryPoints()
          event.stopPropagation()
        })
        circuitsService.onAllSecondaryPointsLoaded($scope, function(event, pointsData) {
            $scope.secondaryPointsTable.points = pointsData
            event.stopPropagation()
        })
        circuitsService.onCircuitDataLoaded($scope, function(event, circuitData) {
                $scope.circuit = circuitData
                console.log('circuit loaded!')
                event.stopPropagation()
            })
        circuitsService.onCircuitSaved($scope, function(event, circuitData) {
                //$scope.circuit = circuitData
                alert('Curcuit saved');
                event.stopPropagation()
            })
        circuitsService.onPrimaryPointDeleted($scope, function(event, data) {
            $scope.getPrimaryPoints() //??
            event.stopPropagation()
        })
        circuitsService.onCircuitCreated($scope, function(event, circuitData) {
          // $scope.circuit = circuitData.id
          // $scope.isNew = false
          alert('Curcuit created');
          $location.url('circuits/')
          event.stopPropagation()
        })
        //end: listeners
        //start: map logic
        $scope.getNormalizedCoord = function(coord, zoom) {
            var y = coord.y;
            var x = coord.x;

            // tile range in one direction range is dependent on zoom level
            // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
            var tileRange = 1 << zoom;

            // don't repeat across y-axis (vertically)
            if (y < 0 || y >= tileRange) {
                return null;
            }

            // repeat across x-axis
            if (x < 0 || x >= tileRange) {
                x = (x % tileRange + tileRange) % tileRange;
            }

            return {
                x: x,
                y: y
            };
        }
        $scope.initMap = function() {
            $scope.moonMapType = new google.maps.ImageMapType({
                getTileUrl: function(coord, zoom) {
                    // console.log(getProjection())
                    var normalizedCoord = $scope.getNormalizedCoord(coord, zoom);
                    if (!normalizedCoord) {
                        return null;
                    }

                    var bound = Math.pow(2, zoom);
                    var url = '//' + $scope.settings.baseUrl + 'api/map2/' + zoom + '/' + normalizedCoord.x + '/' + normalizedCoord.y + '.png'
                    return url;
                },
                tileSize: new google.maps.Size(256, 256),
                maxZoom: 18,
                minZoom: 17,
                radius: 1738000,
                name: 'Moon'
            });
        }
        uiGmapIsReady.promise(1).then(function(instances) {
            $log.log('map is ready')
            instances.forEach(function(inst) {
                var map = inst.map
                var uuid = map.uiGmap_id
                var mapInstanceNumber = inst.instance // Starts at 1.
                $scope.initMap()
                map.overlayMapTypes.push($scope.moonMapType);

                console.log('points: ' + $scope.circuit.markers.length)
                var markers = []
                var linePoints = []
                var lineSymbol = {
                  path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
                };
                var icons = []
                var offset = 0;
                for (var i = 0; i < $scope.circuit.markers.length; i++) {
                    var div = document.createElement('DIV');
                    //var window_id = i
                    var title = ''
                    if (i == 0) {
                      title = ': start'
                    } else if (i == $scope.circuit.markers.length - 1) {
                      title = ': end'
                    }
                    div.innerHTML = '<div class="numbered-marker-bubble"><div class="number">' + $scope.circuit.markers[i].id + title + '</div><div class="bottom-left-corner"></div></div>';
                    markers[i] = new RichMarker({
                        map: map,
                        position: new google.maps.LatLng($scope.circuit.markers[i].latitude, $scope.circuit.markers[i].longitude),
                        draggable: true,
                        flat: true,
                        anchor: RichMarkerPosition.BOTTOM_LEFT,
                        content: div,
                        //window_id: window_id
                    });
                    linePoints.push(new google.maps.LatLng($scope.circuit.markers[i].latitude, $scope.circuit.markers[i].longitude))
                    offset +=  100 / $scope.circuit.markers.length;
                    icons.push({
                      offset: offset + '%',
                      icon: lineSymbol,
                    })
                    // var contentString = '<div id="content">#' + $scope.markers[i].plant_id
                    // if ($scope.markers[i].title !== undefined) {
                    //   contentString += ': ' + $scope.markers[i].title
                    //   if ($scope.markers[i].title_lat !== undefined) {
                    //     contentString += '<br/>'
                    //   }
                    // }
                    // if ($scope.markers[i].title_lat !== undefined) {
                    //   contentString += '<i>'+$scope.markers[i].title_lat+'</i>'
                    // }
                    // infoWindows[i] = new google.maps.InfoWindow({
                    //     content: contentString
                    // });
                    // google.maps.event.addListener(markers[i], 'click', function(e) {
                    //   console.log(this)
                    //   infoWindows[this.window_id].open(map, markers[this.window_id])
                    // })
                }
                var flightPath = new google.maps.Polyline({
                  path: linePoints,
                  geodesic: true,
                  strokeColor: '#FF0000',
                  strokeOpacity: 0.7,
                  strokeWeight: 5,
                  icons: icons,
                });
                console.log(icons)
                flightPath.setMap(map);
            })
        })

        //end: map logic
        //start:helpers
        $scope.openCircuits = function() {
            //TODO: save check?!?
            $location.url('/circuits')
        }
        $scope.deletePrimaryPoint = function(point) {
          var confirm = $mdDialog.confirm()
              .title('Delete')
              .textContent('Do you really want to delete this primary point?')
              .ariaLabel('Delete')
              .ok('Yes')
              .cancel('Cancel');

          $mdDialog.show(confirm).then(function() {
              circuitsService.deletePrimaryPoint($scope.circuit.id,point)
          }, function() {});
        }
        $scope.save = function() {
          if ($scope.isNew) {
            circuitsService.createAndSendEvent($scope.circuit)
          } else {
            circuitsService.saveAndSendEvent($scope.circuit)
          }
        }
        $scope.getSaveLabel = function() {
            if ($scope.isNew) {
                return $scope.saving ? 'Adding...' : 'Add'
            } else {
                return $scope.saving ? 'Saving...' : 'Save'
            }
        }
        $scope.getPrimaryPoints = function() {
            $scope.primaryPointsTable.promise = circuitsService.loadAllPrimaryPoints($routeParams.circuitId, $scope.primaryPointsTable.query).$promise;
        }
        $scope.addPrimaryPoint = function() {
          console.log('add')
          $location.url('/circuits/' + $routeParams.circuitId + '/primary-points/_')
        }
        $scope.editPrimaryPoint = function(point) {
          console.log('epp!');
          $location.url('/circuits/' + $routeParams.circuitId + '/primary-points/' + point.id)
        }
        //end: helpers
        //starts:load data
        if (!$scope.isNew) {
            circuitsService.loadOne($routeParams.circuitId)
            $scope.getPrimaryPoints()
        }
        settingsService.loadAndEmitEvent()
        //end:load data
    }
]);
