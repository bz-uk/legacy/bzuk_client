CREATE TABLE `plant_block_image` (
  `plant_block_id` INT NOT NULL,
  `image_id` INT NOT NULL,
  PRIMARY KEY (`plant_block_id`, `image_id`));
ALTER TABLE `plant_block_image` 
ADD COLUMN `order` INT NOT NULL AFTER `image_id`;
