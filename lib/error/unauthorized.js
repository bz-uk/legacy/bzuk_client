'use strict';
const ExtendableError = require('./extendableError');

module.exports = class Unauthorized extends ExtendableError {
    constructor(redirectUrl) {
        super('unauthorized');
    }
};