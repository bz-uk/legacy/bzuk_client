'use strict';
const connection = require('./../database/local');
const UserRole   = require('./userRole');
const configuration = require('./../common/config');

var User = connection.bookshelf.Model.extend({
    tableName:     configuration.storage.mysql.tablePrefix + 'users',
    hasTimestamps: true,
});

module.exports = connection.bookshelf.model('User', User);