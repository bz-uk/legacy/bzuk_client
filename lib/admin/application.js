'use strict';
import AppController from './common/appController.jsx';
import AboutScreen from './screen/aboutScreen.jsx';
import HomeScreen from './screen/dashboardScreen.jsx';
import MyProfile from './screen/myProfile.jsx';
//import RegistryScreen from './screen/registryScreen.jsx';
import ArticlesScreen from './screen/articlesScreen.jsx';
import RootPanel from './layout/rootPanel.jsx';

let appController = new AppController();

appController.registerScreen(new AboutScreen());
//appController.registerScreen(new RegistryScreen());
appController.registerScreen(new ArticlesScreen());
let homeScren = new HomeScreen();
appController.registerScreen(homeScren);
appController.registerScreen(new MyProfile());
appController.setDefaultScreenCode(homeScren.getCode());

appController.setLayout(new RootPanel(appController.getScreens()));

export default appController;