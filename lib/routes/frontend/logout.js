'use strict';
const Promise = require('bluebird');
const debug   = require('debug')('front-logout');

const AuthenticatedRoute = require('./../../common/authenticatedFrontendRoute');
const RouteRedirect      = require('./../../error/routeRedirect');

module.exports = class Route extends AuthenticatedRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/logout',
            name: 'frontLogout'
        }
    }

    main(req, res) {
        return new Promise((resolve, reject) => {
            req.session.destroy((err) => {
                debug('session destroyed!');
                return reject(new RouteRedirect('/login'));
            })
        })
    }
};