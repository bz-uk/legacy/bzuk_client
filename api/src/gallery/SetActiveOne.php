<?php
namespace botgarApi\gallery;

use Slim\Slim;

class SetActiveOne
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($galleryId)
    {
        $conf = $this->app->container->get('configuration');

        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $json = $this->app->request->getBody();
        $data = json_decode($json);

        if (!isset($data->active)) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(["success"=>false, 'code'=>'validationError', 'messagge' => 'Status can not be empty']));
            return;
        }

        $isActive = $data->active?1:0;
        $sql = 'UPDATE drupal_gallery_widgets SET active='.$isActive.' WHERE id=' . $galleryId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(["success"=>false, 'code'=>'unableToUpdate']));
            return;
        }
        //TODO: drop update cache if inactive/build if active
//        $redis = new \Redis();
//        $redis->connect('127.0.0.1', 6379);
//        $redis->select(0);
//        $redis->delete("bzukpr_drwidget_gallery_cache_" . $galleryId);
        $response = json_encode(['success'=>true]);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
