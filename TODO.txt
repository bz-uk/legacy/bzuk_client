sql:
ALTER TABLE `botgar`.`image`
CHANGE COLUMN `created_date` `created_at` DATETIME NULL DEFAULT NULL ,
ADD COLUMN `updated_at` DATETIME NULL AFTER `display_vertical_offset`,
ADD COLUMN `deleted_at` DATETIME NULL AFTER `updated_at`;
