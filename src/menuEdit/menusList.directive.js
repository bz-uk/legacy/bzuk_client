'use strict';
angular.module('adminApp').directive('menusList', function() {
	return {
		restrict : 'E',
		templateUrl : 'menuEdit/menusList.template.html',
		controller: 'menusListController'
	};
});