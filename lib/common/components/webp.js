'use strict';
import Promise from 'bluebird';
//TODO: move to lib
export default class Webp {
    /**
     * @return Promise
     */
    static available() {
        return new Promise(function (resolve) {
            var img     = new Image();
            img.onload  = () => {
                return resolve(!!(img.height > 0 && img.width > 0));
            };
            img.onerror = () => {
                return resolve(false);
            };

            img.src = 'data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=';
        })
    }
}