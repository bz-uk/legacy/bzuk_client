'use strict';

export default class StringUtil {
    static randomString(strLength) {
        let text     = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        if (!strLength) {
            strLength = 8;
        }
        
        for (var i = 0; i < strLength; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        
        return text;
    }
}