'use strict';
const Promise       = require('bluebird');
const BaseRoute     = require('./../../common/route');
const RouteRedirect = require('./../../error/routeRedirect');
const engDict = require('./../../../lang/login/en.js');
const csDict = require('./../../../lang/login/cs.js');

module.exports = class Route extends BaseRoute {
    /**
     * @override
     */
    getConfig() {
        return {
            method: 'get',
            path: '/login',
            name: 'frontLogin'
        }
    }

    main(req, res) {
        if (req.session.user) { //special case, not using authenticated route
            return Promise.reject(new RouteRedirect('/'));
        }
        let templateVars = {
            appSettings: JSON.stringify({
                csrf: ''//req.csrfToken()
            }),
            langDictionary: JSON.stringify({ //TODO: make loader for autoload all lang files
                en: engDict,
                cs: csDict
            })
        };
        res.render('login/index', templateVars);
        return Promise.resolve()
    }
};