<?php
namespace botgarApi\menu;

use Slim\Slim;

class Put {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($menuId) {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);
    	if (!isset($data->name) || $data->name == "") {
    		$response = ['status'=>'error', 'message'=> 'Name can not be empty'];
    		$this->app->response->setStatus(400);
    		$response = json_encode($response);
    		$this->app->response->header('Content-Length', strlen($response));
    		$this->app->response->setBody($response);
    		return;
    	}
    	
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $db->beginTransaction();
        $sql = "UPDATE menu SET name=".$db->quote($data->name)." where id=" . $menuId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            $response = ['status'=>'error', 'message'=> 'Problem during saving menu'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }
        $db->commit();
        $response = ['status'=> 'ok', 'message' => 'Menu saved'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
