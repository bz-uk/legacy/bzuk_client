angular.module('adminApp').factory('articlesService', ['$log', '$resource', '$rootScope', function ($log, $resource, $rootScope) {
    var articlesResource = $resource('/api/api/index.php/articles', {}, {
        'get': {method: 'GET', isArray: true},
        'save': {method: 'PUT'},
        'create': {method: 'POST'}
    });
    var articleResource = $resource('/api/api/index.php/articles/:articleId', {}, {
        'save': {method: 'PUT'},
        'get': {method: 'GET'},
        'delete': {method: 'DELETE'}
    });
    var publishArticlesResource = $resource('/api/api/index.php/articles/publish', {}, {
        'publish': {method: 'POST'},
    });
    var registerHandler = function (eventName, scope, callback) {
        var handler = $rootScope.$on(eventName, callback);
        scope.$on('$destroy', handler);
    }
    return {
        onArticleDeleted: function (scope, callback) {
            registerHandler('article-deleted', scope, callback)
        },
        onArticleDeleteError: function (scope, callback) {
            registerHandler('article-delete-error', scope, callback)
        },
        onArticleCreated: function (scope, callback) {
            registerHandler('article-created', scope, callback)
        },
        onArticleCreateError: function (scope, callback) {
            registerHandler('article-create-error', scope, callback)
        },
        onArticleSaved: function (scope, callback) {
            registerHandler('article-saved', scope, callback)
        },
        onArticleSaveError: function (scope, callback) {
            registerHandler('article-save-error', scope, callback)
        },
        publishOneArticle: function (articleId, published) {
            var data = {};
            data[articleId] = published;
            return publishArticlesResource.publish({}, {ids: data}, function (data) {
                $rootScope.$emit('article-published', articleId, published);
                if (published) {
                    $rootScope.$emit('ok-message', 'Článek by publikován');
                }else{
                    $rootScope.$emit('ok-message', 'Článek by deaktivován');
                }
            }, function (err) {
                $rootScope.$emit('error-message', 'Publikování článku zlyhalo');
                $log.error(err);
                alert(err);
            })
        },
        deleteArticleAndEmitEvent: function (article) {
            return articleResource.delete({articleId: article.id}, function (data) {
                $rootScope.$emit('article-deleted', article);
                $rootScope.$emit('ok-message', 'Článek by smazán');
            }, function (err) {
                $rootScope.$emit('article-delete-error', err);
                $rootScope.$emit('error-message', 'Smazání článku zlyhalo');
                alert(err.message);
            });
        },
        load: function (query, successCallback) {
            //TODO: implement query
            return articlesResource.get({}, function (articles) {
                var articlesList = angular.copy(articles);
                delete articlesList.$promise;
                delete articlesList.resolved;
                console.log('okay got ' + articlesList.length + ' items');
                return successCallback(articlesList);
            }, function (err) {
                $log.error('Unable to load articles: ', err);
            })
        },
        saveAndEmitEvent: function(article) {
            return articleResource.save({articleId: article.id}, article, function (data) {
                $rootScope.$emit('article-saved', data);
                $rootScope.$emit('ok-message', 'Článek uložen');
            }, function(err) {
                $rootScope.$emit('article-save-error', err);
                $rootScope.$emit('error-message', 'Článek neuložen: ' + err.message);
            })
        },
        addAndEmitEvent: function(article) {
            return articlesResource.create({}, article, function (data) {
                $rootScope.$emit('article-created', data);
                $rootScope.$emit('ok-message', 'Článek vytvořen');
            }, function(err) {
                $rootScope.$emit('article-create-error', err);
                $rootScope.$emit('error-message', 'Článek nebyl vytvořen: ' + err.message);
            })
        },
        /** @deprecated */
        save: function (article, successCallback, errCallback) {
            return articleResource.save({articleId: article.id}, article, function (data) {
//				delete data.$promise;
//				delete data.resolved;
                successCallback(data);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status == 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to save article', err);
                errCallback(message);
            });
        },
        loadOne: function (articleId, successCallback, errCallback) {
            return articleResource.get({articleId: articleId}, function (data) {
                delete data.$promise;
                delete data.resolved;
                successCallback(data);
            }, function (err) {
                console.log(err);
                var message = '';
                if (err.status == 500) {
                    message = err.data;
                } else {
                    message = err.data.message;
                }
                $log.error('Unable to load article', err);
                errCallback(message);
            });
        }
    }
}]);
