'use strict';
import _ from 'lodash';
import BaseValidator from './baseValidator';

export default class RequiredValidator extends BaseValidator {
    constructor(options) {
        let defaultOptions = {errorMessage: 'Value is required'}
        super(_.assign(defaultOptions, options));
    }
    
    isValid(value) {
            return value !== undefined && value !== null && value !== '';
    }
}