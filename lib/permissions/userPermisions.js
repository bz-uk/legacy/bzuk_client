'use strict';
const _ = require('lodash');

module.exports = class UserPermissions {
    constructor(permissions) {
        this.permissions = {};
        _.forEach(permissions, (permission) => {
            this.permissions[permission.code] = permission;
        })
    }

    has(permissionName, level, ownership) {
        if (this.permissions[permissionName]) {
            return true;
        }
        return false;
    }

    toJSON() {
        return this.permissions;
    }
}