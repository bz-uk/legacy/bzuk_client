'use strict';
import React from 'react';
import axios from 'axios';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import MapsLocalFlorist from 'material-ui/svg-icons/maps/local-florist';
import ServerRequest from '../../common/serverRequest';
import I18n from './../../i18n/i18n';
import FlatButton from 'material-ui/FlatButton';
import glamorous from 'glamorous';
import ContentLink from 'material-ui/svg-icons/content/link';

const {A, Table} = glamorous;

const HeaderLink = glamorous.a(
    {
        color: 'black',
        textDecoration: 'none',
        ':hover': {
            textDecoration: 'underline',
        }
    });

export default class ArticlesCountWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            plantsCount: null,
            publishedCount: null,
        };
    }

    getWidgetContent() {
        return (
            <div>
                <Table css={{border: 0}}>
                    <tbody>
                    <tr>
                        <td>{I18n.t('Total plants:')}</td>
                        <td>{this.state.plantsCount}</td>
                    </tr>
                    <tr>
                        <td>{I18n.t('Published:')}</td>
                        <td>{this.state.publishedCount}</td>
                    </tr>
                    </tbody>
                </Table>
            </div>
        )
    }

    onCreate = () => {
        window.location = '/plants/_';
    }

    render() {
        let styles = {
            widgetTitle: {
                marginLeft: '.5rem'
            },
            card: {
                opacity: this.state.plantsCount !== null ? '1' : '.1'
            },
            titleRow: {
                display: 'flex',
                alignItems: 'center'
            },
            flexAuto: {
                flex: 'auto'
            }
        }
        return (
            <Card style={styles.card}>
                <CardTitle
                    title={<div style={styles.titleRow}><MapsLocalFlorist/><HeaderLink href="/plants"><span
                        style={styles.widgetTitle}>{I18n.t('Plants')}</span></HeaderLink>
                        <div style={styles.flexAuto}/>
                        <FlatButton label={I18n.t('Create')} onTouchTap={this.onCreate} primary={true}/>
                    </div>}/>
                <CardText>
                    {this.state.plantsCount === null ?
                        <div>{I18n.t('Loading...')}</div>
                        :
                        this.getWidgetContent()
                    }
                </CardText>
            </Card>

        );
    }

    componentDidMount() {
        let req = new ServerRequest('widget-data/plants-count');
        req.get()
        .then((response) => {
            this.state.plantsCount = response.plantsCount;
            this.state.publishedCount = response.publishedCount;
            this.setState(this.state);
        });
    }
}