angular.module('adminApp').controller('addTagDailogController', ['$scope', '$mdDialog', 'plantsService', function($scope, $mdDialog, plantsService) {

    $scope.model = {
      name: ''
    }

    $scope.create = function() {
      plantsService.createTag($scope.model, function(newTag) {
        $mdDialog.hide(newTag);
      }, function (err) {
        alert(err)
      })
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);
