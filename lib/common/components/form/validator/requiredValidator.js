'use strict';
import assign from 'lodash.assign';
import BaseValidator from './baseValidator';
export default class RequiredValidator extends BaseValidator {
    constructor(options) {
        let defaultOptions = {errorMessage: 'Value is required'}
        super(assign(defaultOptions, options));
    }

    isValid(value) {
        return value !== undefined && value !== null && value !== '';
    }
}