'use strict';
import React from 'react';
import ReactDOM from 'react-dom';
import EventEmitter  from 'wolfy87-eventemitter';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class DialogComponent extends React.Component {
    constructor(props) {
        super(props);
    }
    
    componentDidMount() {
        this.emitter = new EventEmitter();
    }
    
    componentWillUnmount() {
        this.emitter.removeAllListeners();
        this.emitter = null;
    }
    
    on(eventName, handler) {
        return this.emitter.on(eventName, handler);
    }
    
    cancelAndClose = () => {
        this.emitter.emit('cancel');
    }
    
    onConfirm = () => {
        this.emitter.emit('confirm');
    }
    
    render() {
        return (
            <MuiThemeProvider>
                <Dialog title={this.props.title}
                        actions={[<FlatButton
                            label="Cancel"
                            primary={true}
                            onTouchTap={this.props.onCancel}
                        />,
                            <FlatButton
                                label={this.props.confirmLabel}
                                primary={true}
                                keyboardFocused={true}
                                onTouchTap={this.props.onConfirm}
                            />]}
                        modal={false}
                        open={true}
                        onRequestClose={this.props.onCancel}
                >
                    {this.props.message}
                </Dialog>
            </MuiThemeProvider>
        );
    }
}

export default class ConfirmationDialog extends EventEmitter {
    constructor(dialogOptions) {
        super();
        this.dialogOptions           = dialogOptions;
        this.dialogOptions.onCancel  = this.onCancel;
        this.dialogOptions.onConfirm = this.onConfirm;
    }
    
    show() {
        this.wrapper = document.body.appendChild(document.createElement('div'));
        ReactDOM.render(<DialogComponent {...this.dialogOptions}/>, this.wrapper);
    }
    
    onCancel = () => {
        this.emit('cancel');
        this.cleanUp();
    }
    
    onConfirm = () => {
        this.emit('confirm');
        this.cleanUp();
    }
    
    cleanUp() {
        var self = this;
        ReactDOM.unmountComponentAtNode(this.wrapper);
        return setTimeout(function () {
            return self.wrapper.remove();
        });
    }
}