angular.module('adminApp').factory('tableService', ['$resource', '$log',function ($resource, $log) {
	var tablefiltersResource = $resource('/api/api/index.php/table-filters', {}, {
		'post':  {method:'POST'}
	});
	var tablefilterResource = $resource('/api/api/index.php/table-filters/:filterId', {}, {
		'get':  {method:'GET'}
	});
  return {
		save: function(tableName, filter , successCallback, errCallback) {
			return tablefiltersResource.post({}, {tableName: tableName, filter: filter}, function(data) {
				console.log(data)
				successCallback(data.filter_code)
			}, function(err) {
				console.log(err);
				var message = '';
				if (err.status == 500) {
					message = err.data;
				} else {
					message = err.data.message;
				}
				$log.error('Unable to save filter', err);
				errCallback(message);
			});
		},
		load: function(filterId, successCallback, errCallback) {
				return tablefilterResource.get({filterId: filterId}, function(data) {
	        var filter = angular.copy(data)
	        delete filter.$prmoise
	        delete filter.resolved
					successCallback(filter)
				}, function(err) {
					console.log(err);
					var message = '';
					if (err.status == 500) {
						message = err.data;
					} else {
						message = err.data.message;
					}
					$log.error('Unable to load filter', err);
					errCallback(message);
				});
			},
	}
}]);
