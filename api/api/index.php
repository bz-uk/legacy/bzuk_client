<?php
//TODO: ged rid of this PHP code!
require 'bootstrap.php';
require '../config/config.php';

//START: session
//$db = null;
$db = new \PDO('mysql:host=' . $config['db:host'] . ';port=' . $config['db:port'] . ';dbname=' . $config['db:dbname'] . ';charset=UTF8;', $config['db:user'], $config['db:pass']);

$app = new \Slim\Slim ();
$app->container->singleton('configuration', function () use ($config) {
    return $config;
});

function redirect($url, $statusCode = 303)
{
    header('Location: ' . $url, true, $statusCode);
    die();
}

if (!$_COOKIE['connect_sid']) {
    var_export($_COOKIE);
    echo json_encode(['error' => 'unauthorized']);
    header("Status: 401 Unauthorized");
    die();
} else {
    $sess_name = 'connect_sid';
    if (substr($_COOKIE[$sess_name], 0, 2) === "s:") {
        $_COOKIE[$sess_name] = substr($_COOKIE[$sess_name], 2);
    }
    $dot_pos = strpos($_COOKIE[$sess_name], ".");
    if ($dot_pos !== false) {
        $sessionId = substr($_COOKIE[$sess_name], 0,$dot_pos);
        session_start();
        $q = "SELECT * FROM ro2_sessions  WHERE  session_id = " . $db->quote($sessionId);
        $result = $db->query($q);
        if ($result) {
            $row = $result->fetch(\PDO::FETCH_ASSOC);
            if (time() > $row['expires']) { //TODO: test it
                echo json_encode(['error' => 'unauthorized']);
                header("Status: 401 Unauthorized");
                die();
            }
            $sessionData = [];
            $decodedSession = json_decode($row['data']);
            foreach ($decodedSession as $itemName => $key) {
                $_SESSION[$itemName] = $decodedSession->{$itemName};
            }
        } else {
            echo json_encode(['error' => 'unauthorized']);
            header("Status: 401 Unauthorized");
            die();
        }
    }
}

botgarApi\Logger::log('Processing api call: ' . var_export($_REQUEST, true) . "\n" . var_export($_SERVER, true) . "\n");
$app->response->headers->set('Content-Type', 'application/json');
$app->put('/articles/:id', function ($id) use ($app) {
    $updater = new botgarApi\article\Put ($app);
    $updater->run($id);
});
$app->delete('/articles/:id', function ($id) use ($app) {
    $updater = new botgarApi\article\Delete ($app);
    $updater->run($id);
});
$app->post('/articles', function () use ($app) {
    $updater = new botgarApi\article\Post ($app);
    $updater->run();
});
$app->post('/articles/publish', function () use ($app) {
    $updater = new botgarApi\article\Publish($app);
    $updater->run();
});
$app->get('/authors', function () use ($app) {
    $updater = new botgarApi\authors\GetAll ($app);
    $updater->run();
});
$app->put('/authors/:id', function ($id) use ($app) {
    $updater = new botgarApi\authors\PutOne($app);
    $updater->run($id);
});
$app->delete('/authors/:id', function ($id) use ($app) {
    $updater = new botgarApi\authors\DeleteOne($app);
    $updater->run($id);
});
$app->post('/authors', function () use ($app) {
    $updater = new botgarApi\authors\CreateOne ($app);
    $updater->run();
});
// del author?
$app->get('/articles/:id', function ($id) use ($app) {
    $updater = new botgarApi\article\Get ($app);
    $updater->run($id);
});
$app->get('/articles', function () use ($app) {
    $getter = new botgarApi\article\GetAll ($app);
    $getter->run();
});
$app->put('/circuits/:id', function ($id) use ($app) {
    $updater = new botgarApi\circuit\Put ($app);
    $updater->run($id);
});
$app->get('/circuits/:id', function ($id) use ($app) {
    $updater = new botgarApi\circuit\GetOne ($app);
    $updater->run($id);
});
$app->post('/circuits', function () use ($app) {
    $updater = new botgarApi\circuit\Post ($app);
    $updater->run();
});
$app->get('/circuits', function () use ($app) {
    $getter = new botgarApi\circuit\GetAll ($app);
    $getter->run();
});
$app->get('/circuits/:circuitId/primary-points', function ($circuitId) use ($app) {
    $getter = new botgarApi\circuit\GetAllPrimaryPoints ($app);
    $getter->run($circuitId);
});
$app->post('/circuits/:circuitId/primary-points', function ($circuitId) use ($app) {
    $getter = new botgarApi\circuit\CreateOnePrimaryPoint ($app);
    $getter->run($circuitId);
});
$app->post('/circuits/:circuitId/primary-points/order', function ($circuitId) use ($app) {
    $getter = new botgarApi\circuit\SetPrimaryPointsOrder ($app);
    $getter->run($circuitId);
});
$app->get('/circuits/:circuitId/primary-points/:pointId', function ($circuitId, $pointId) use ($app) {
    $getter = new botgarApi\circuit\GetOnePrimaryPoint ($app);
    $getter->run($circuitId, $pointId);
});
$app->delete('/circuits/:circuitId/primary-points/:pointId', function ($circuitId, $pointId) use ($app) {
    $getter = new botgarApi\circuit\DeleteOnePrimaryPoint ($app);
    $getter->run($circuitId, $pointId);
});
$app->put('/circuits/:circuitId/primary-points/:pointId', function ($circuitId, $pointId) use ($app) {
    $getter = new botgarApi\circuit\SaveOnePrimaryPoint ($app);
    $getter->run($circuitId, $pointId);
});
$app->get('/circuits/:circuitId/primary-points/:pointId/secondary-points', function ($circuitId, $pointId) use ($app) {
    $getter = new botgarApi\circuit\GetAllSecondaryPoints ($app);
    $getter->run($circuitId, $pointId);
});
$app->get('/circuits/:circuitId/primary-points/:pointId/slides', function ($circuitId, $pointId) use ($app) {
    $getter = new botgarApi\circuit\GetPrimaryPointSlides ($app);
    $getter->run($circuitId, $pointId);
});
$app->put('/circuits/:circuitId/primary-points/:pointId/slides/:slideId', function ($circuitId, $pointId, $slideId) use ($app) {
    $getter = new botgarApi\circuit\SaveOnePrimaryPointSlide ($app);
    $getter->run($circuitId, $pointId, $slideId);
});
$app->delete('/circuits/:circuitId/primary-points/:pointId/slides/:slideId', function ($circuitId, $pointId, $slideId) use ($app) {
    $getter = new botgarApi\circuit\DeleteOnePrimaryPointSlide ($app);
    $getter->run($circuitId, $pointId, $slideId);
});
$app->post('/circuits/:circuitId/primary-points/:pointId/secondary-points', function ($circuitId, $pointId) use ($app) {
    $getter = new botgarApi\circuit\AddSecondaryPoints ($app);
    $getter->run($circuitId, $pointId);
});
$app->delete('/circuits/:circuitId/primary-points/:pointId/secondary-points/:secondaryPointId', function ($circuitId, $pointId, $secondaryPointId) use ($app) {
    $getter = new botgarApi\circuit\DeleteOneSecondaryPoint ($app);
    $getter->run($circuitId, $pointId, $secondaryPointId);
});
$app->put('/circuits/:circuitId/primary-points/:pointId/secondary-points/:secondaryPointId', function ($circuitId, $pointId, $secondaryPointId) use ($app) {
    $getter = new botgarApi\circuit\SaveOneSecondaryPoint ($app);
    $getter->run($circuitId, $pointId, $secondaryPointId);
});
$app->put('/menu(/:id)', function ($id) use ($app) {
    $updater = new botgarApi\menu\Put ($app);
    $updater->run($id);
});
$app->get('/menu/:id', function ($id) use ($app) {
    $updater = new botgarApi\menu\Get ($app);
    $updater->run($id);
});
$app->get('/menu/:id/items', function ($id) use ($app) {
    $updater = new botgarApi\menu\GetItems ($app);
    $updater->run($id);
});
$app->get('/settings', function () use ($app) {
    $updater = new botgarApi\settings\GetAll ($app);
    $updater->run();
});
$app->post('/table-filters', function () use ($app) {
    $updater = new botgarApi\tableFilters\Create ($app);
    $updater->run();
});
$app->get('/table-filters/:filterCode', function ($filterCode) use ($app) {
    $updater = new botgarApi\tableFilters\GetOne ($app);
    $updater->run($filterCode);
});
$app->post('/menu/:menuId/items/order', function ($menuId) use ($app) {
    $updater = new botgarApi\menu\SetItemsOrder ($app);
    $updater->run($menuId);
});
$app->post('/plants', function () use ($app) {
    $updater = new botgarApi\plants\addPlant ($app);
    $updater->run();
});
$app->post('/plants/tags', function () use ($app) {
    $updater = new botgarApi\plants\AddTags ($app);
    $updater->run();
});
$app->put('/plants/:plantId/tags', function ($plantId) use ($app) {
    $updater = new botgarApi\plants\UpdateTags($app);
    $updater->run($plantId);
});
$app->post('/plants/publish', function () use ($app) {
    $updater = new botgarApi\plants\Publish($app);
    $updater->run();
});
$app->put('/plants/tags', function () use ($app) {
    $updater = new botgarApi\plants\DeleteTags ($app);
    $updater->run();
});
$app->post('/plants/table-data', function () use ($app) {
    $updater = new botgarApi\plants\GetAll ($app);
    $updater->run();
});
$app->get('/plants/:plantId', function ($plantId) use ($app) {
    $updater = new botgarApi\plants\GetOne ($app);
    $updater->run($plantId);
});
$app->delete('/plants/:plantId', function ($plantId) use ($app) {
    $updater = new botgarApi\plants\DeleteOne ($app);
    $updater->run($plantId);
});
$app->get('/plants-tags', function () use ($app) {
    $updater = new botgarApi\plants\GetAllTags ($app);
    $updater->run();
});
$app->post('/plants-tags', function () use ($app) {
    $updater = new botgarApi\plants\CreateTag ($app);
    $updater->run();
});
$app->put('/plants-tags/:tagId', function ($tagId) use ($app) {
    $updater = new botgarApi\plants\EditTag ($app);
    $updater->run($tagId);
});
$app->delete('/plants-tags/:tagId', function ($tagId) use ($app) {
    $updater = new botgarApi\plants\DeleteTag ($app);
    $updater->run($tagId);
});

$app->put('/plants/:plantId', function ($plantId) use ($app) {
    $updater = new botgarApi\plants\PutOne ($app);
    $updater->run($plantId);
});
$app->delete('/plants/:plantId/tags/:tagId', function ($plantId, $tagId) use ($app) {
    $updater = new botgarApi\plants\RemoveTag ($app);
    $updater->run($plantId, $tagId);
});
$app->get('/widgets/dr-galleries', function () use ($app) {
    $updater = new botgarApi\gallery\GetAll($app);
    $updater->run();
});
$app->get('/widgets/dr-galleries/:galleryId', function ($galleryId) use ($app) {
    $updater = new botgarApi\gallery\GetOne($app);
    $updater->run($galleryId);
});
$app->put('/widgets/dr-galleries/:galleryId', function ($galleryId) use ($app) {
    $updater = new botgarApi\gallery\PutOne($app);
    $updater->run($galleryId);
});
$app->put('/widgets/dr-galleries/:galleryId/active', function ($galleryId) use ($app) {
    $updater = new botgarApi\gallery\SetActiveOne($app);
    $updater->run($galleryId);
});
$app->post('/widgets/dr-galleries/images', function () use ($app) {
    $updater = new botgarApi\gallery\GetImages($app);
    $updater->run();
});
$app->get('/plants-categories', function () use ($app) {
    $updater = new botgarApi\plants\GetCategories ($app);
    $updater->run();
});
$app->delete('/plants-categories/:categoryId', function ($categoryId) use ($app) {
    $updater = new botgarApi\plants\DeleteCategory($app);
    $updater->run($categoryId);
});
$app->put('/plants-categories/:categoryId', function ($categoryId) use ($app) {
    $updater = new botgarApi\plants\EditCategory ($app);
    $updater->run($categoryId);
});
$app->get('/plants-families', function () use ($app) {
    $updater = new botgarApi\plants\GetFamilies ($app);
    $updater->run();
});
$app->put('/plants-families/:familyId', function ($familyId) use ($app) {
    $updater = new botgarApi\plants\EditFamily ($app);
    $updater->run($familyId);
});
$app->delete('/plants-families/:familyId', function ($familyId) use ($app) {
    $updater = new botgarApi\plants\DeleteFamily($app);
    $updater->run($familyId);
});
$app->post('/files', function () use ($app) {
    $updater = new botgarApi\files\Post ($app);
    $updater->run();
});
$app->get('/files/:fileId', function ($fileId) use ($app) {
    $updater = new botgarApi\files\Get ($app);
    $updater->run($fileId);
});
//$app->post('/images', function () use ($app) {
//    $updater = new botgarApi\image\Post ($app);
//    $updater->run();
//});
$app->get('/images/:imageId', function ($imageId) use ($app) {
    $updater = new botgarApi\image\Get ($app);
    $updater->run($imageId);
});
$app->get('/images/:imageId/webp', function ($imageId) use ($app) {
    $updater = new botgarApi\image\Get ($app);
    $updater->run($imageId, null, true);
});
$app->get('/images/:imageId/max-width/:maxWidth', function ($imageId, $maxWidth) use ($app) {
    $updater = new botgarApi\image\Get ($app);
    $updater->run($imageId, $maxWidth);
});
$app->get('/images/:imageId/max-width/:maxWidth/webp', function ($imageId, $maxWidth) use ($app) {
    $updater = new botgarApi\image\Get ($app);
    $updater->run($imageId, $maxWidth, true);
});
$app->put('/images/:imageId', function ($imageId) use ($app) {
    $updater = new botgarApi\image\Put ($app);
    $updater->run($imageId);
});

$app->put('/menu/:id/item/:itemId', function ($id, $itemId) use ($app) {
    $updater = new botgarApi\menu\PutItem ($app);
    $updater->run($id, $itemId);
});
$app->get('/menu/:id/item/:itemId', function ($id, $itemId) use ($app) {
    $updater = new botgarApi\menu\GetItem ($app);
    $updater->run($id, $itemId);
});

$app->get('/menus', function () use ($app) {
    $getter = new botgarApi\menu\GetAll ($app);
    $getter->run();
});
$app->run();
