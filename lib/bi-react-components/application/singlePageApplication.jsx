'use strict';
import React from 'react';
import _ from 'lodash';
import {Router, Route, hashHistory, IndexRoute, browserHistory} from 'react-router'
import Logger from './../util/logger.js';

export default class SinglePageApplication extends React.Component {
    
    constructor(props) {
        super(props);
        
        this.logger = this.props.logger || Logger;
    }
    
    render() {
        return (
            <Router history={hashHistory}>
                <Route path="/" appSettings={this.props.appSettings} screens={this.props.screens}
                       eventBus={this.props.eventBus}
                       logger={this.props.logger} default={this.props.default} component={this.props.rootPanel}>
                    <IndexRoute component={this.getDeafultScreen().getComponent()}/>
                    {this.getRoutes(this.props.screens)}
                </Route>
            </Router>
        )
    }
    
    
    /* {this.getRoutes(this.props.screens)} */
    getRoutes(screens) {
        let routes = screens.map((routeInstance, index) => {
            return (<Route key={index} path={'/' + routeInstance.getCode()}
                           component={routeInstance.getComponent()}></Route>)
        });
        return routes;
    }
    
    getDeafultScreen() {
        for (let i = 0; i < this.props.screens.length; i++) {
            let screenName = this.props.screens[i].getCode();
            if (screenName === this.props.default) {
                return this.props.screens[i];
            }
        }
        this.logger.warn('There is no default screen');
        return null;
    }
}