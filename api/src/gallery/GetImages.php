<?php
namespace botgarApi\gallery;

use Slim\Slim;

class GetImages {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run() {
        $conf = $this->app->container->get('configuration');

        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $json = $this->app->request->getBody();
        $data = json_decode($json);

        if (!isset($data->filter)) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(["success"=>false, 'code'=>'invalid', 'message'=>'Filter can not be empty!']));
            return;
        }
        $filter = $data->filter;

        $sql = "SELECT i.*, pl.id as plantId, pl.name as plantName, pl.name_en as plantNameEn, pl.name_lat as plantNameLat, plf.name as family_name, plf.name_en as family_name_en, plf.name_lat as family_name_lat  FROM plants pl left join plant_family plf on pl.family_id=plf.id ";
        $filterQuery = [];
        $filterQuery[] = 'pl.is_deleted=0';
        $filterQuery[] = 'pl.is_active=1';
        if (isset($filter->tags) && is_array($filter->tags) && count($filter->tags) > 0) {
            $tagIds = $filter->tags;
            $sql .= ' inner join plant_tags plt on plt.plant_id=pl.id AND plt.tag_id in (' . join(',', $tagIds) . ')';
        }
        if (isset($filter->categories) && is_array($filter->categories) && count($filter->categories) > 0) {
            $categoryIds = $filter->categories;
            $filterQuery[] = 'pl.category_id IN (' . join(',', $categoryIds) . ')';
        }
        if (count($filterQuery) > 0) {
            $plantsSql = $sql .
                "left join plant_image pi on pi.plant_id=pl.id left join image i on pi.image_id=i.id " .
                ' WHERE ' . join($filterQuery, ' and ');
        }
        $plantsSql .= ' ORDER BY pl.id,pi.`order`';
        $result = $db->query($plantsSql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(["success"=>false, 'code'=>'noResult']));
            return;
        }
        $rows = [];
        //get plant images
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $this->processRow($row, $rows);
        }
        //get block images
        $filterQuery = [];
        $sql = "SELECT i.*, pl.id as plantId, pl.name as plantName, pl.name_en as plantNameEn, pl.name_lat as plantNameLat, plf.name as family_name, plf.name_en as family_name_en, plf.name_lat as family_name_lat  FROM plants pl left join plant_family plf on pl.family_id=plf.id
";
        if (isset($filter->tags) && is_array($filter->tags) && count($filter->tags) > 0) {
            $tagIds = $filter->tags;
            $sql .= ' inner join plant_tags plt on plt.plant_id=pl.id AND plt.tag_id in (' . join(',', $tagIds) . ')';
        }
        if (isset($filter->categories) && is_array($filter->categories) && count($filter->categories) > 0) {
            $categoryIds = $filter->categories;
            $filterQuery[] = 'pl.category_id IN (' . join(',', $categoryIds) . ')';
        }
        $plantsBlocksSql = $sql . ' inner join plant_block pb on pb.plant_id=pl.id AND pb.type=1 left JOIN plant_block_image pbi ON pb.id=pbi.plant_block_id
left JOIN image i on i.id=pbi.image_id ';
        if (count($filterQuery) > 0) {
            $plantsBlocksSql .= ' WHERE ' . join($filterQuery, ' and ');
        }
        $plantsBlocksSql .= ' ORDER BY pl.id, pb.`order`, pbi.`order`';
        $result = $db->query($plantsBlocksSql);
        if ($result) {
            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                $this->processRow($row, $rows);
            }
        }

        $response = json_encode(['plants' => $rows]);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }

    private function processRow($row, &$rows) { //TODO: ugly hell with '&'
        if (!isset($rows[$row['plantId']])) {
            $rows[$row['plantId']] = [
                'id' =>  $row['plantId'] + 0,
                'name' => $row['plantName'],
                'name_en' => $row['plantNameEn'],
                'name_lat' => $row['plantNameLat'],
                'family_name' => $row['family_name'],
                'family_name_en' => $row['family_name_en'],
                'family_name_lat' => $row['family_name_lat']
            ];
        }
        if (!isset($rows[$row['plantId']]['images'])) {
            $rows[$row['plantId']]['images'] = [];
        }
        if (!isset($rows[$row['plantId']]['images'][$row['id']])) {
            if (!is_numeric($row['id'])) {
                return;
            }
            $rows[$row['plantId']]['images'][$row['id']] = [
                'id' => $row['id'] + 0,
                'name' => $row['name'],
                'width' => $row['width'] + 0,
                'height' => $row['height'] + 0,
                'type' => $row['type'],
                'size' => $row['size'] + 0,
                'url' => $row['url'],
                'path' => $row['path']
            ];
        }
    }
}
