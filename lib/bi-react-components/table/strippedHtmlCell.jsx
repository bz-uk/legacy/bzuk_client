'use strict';
import React from 'react';
import striptags from 'striptags';

export default class StrippedHtmlCell extends React.Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <div>{striptags(this.props.value)}</div>
        );
    }
}