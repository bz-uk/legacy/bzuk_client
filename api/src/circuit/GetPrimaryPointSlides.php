<?php

namespace botgarApi\circuit;

use Slim\Slim;

class GetPrimaryPointSlides
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($circuitId, $primaryPointId)
    {
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;', $conf['db:user'], $conf['db:pass']);

        $sql = 'SELECT * FROM primary_point_parts where primary_point_id='.$circuitId.' AND is_deleted=0 order by `order` ASC';
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setBody(json_encode(['error' => $sql, 'err' => var_export($db->errorInfo(), true)]));
            $this->app->response->setStatus(400);

            return;
        }
        $output = [];
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $row['order'] += 0;
            $row['id'] += 0;
            $row['active'] = $row['active'] === '1' ? true : false;
            $output[] = $row;
        }
        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
