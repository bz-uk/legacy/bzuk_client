'use strict';
const sequelize = require('./../database/local').sequelize;

module.exports = function (rawQueryString, options) {
    return sequelize.query(rawQueryString, Object.assign({type: sequelize.QueryTypes.SELECT}, options))
}