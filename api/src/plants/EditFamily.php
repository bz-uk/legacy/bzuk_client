<?php
namespace botgarApi\plants;

use Slim\Slim;

class EditFamily {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message) {
      $response = ['status'=>'error', 'message'=> $message];
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run($familyId) {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);

      error_log('data: ' . var_export($json, true));
      //validation
      if (!isset($data->name) && !isset($data->name_en) && !isset($data->name_lat)) {
        return $this->returnInvalidResponse('Family name can not be empty. enter at least one');
    	}
      //ok now update

      $conf = $this->app->container->get('configuration');
      $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

      $db->beginTransaction();

      //category
        $sql = 'UPDATE plant_family set name='.$db->quote($data->name).', name_en='.$db->quote($data->name_en).',name_lat='.$db->quote($data->name_lat).' where id='.$familyId;
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            return $this->returnInvalidResponse('Unable to update family: ' . $sql);
        }
      $db->commit();
      $response = ['status'=> 'ok', 'message' => 'family saved'];
      $this->app->response->setStatus(200);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
      return;
    }
}
