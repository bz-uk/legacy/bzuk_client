'use strict'
//controller
var sourcesBlockController = function ($log, settingsService) {
    var ctrl = this;
    ctrl.options = settingsService.getFullCkEditorSettings();
    if (ctrl.showLanguage === 'cz') {
        ctrl.selectedTabIdx = 1;
    } else {
        ctrl.selectedTabIdx = 0;
    }
}
//definition
angular.module('adminApp').component('sourcesBlock', {
    templateUrl: 'plants/sources/sourcesBlock.template.html',
    controller: sourcesBlockController,
    bindings: {
        showLanguage: '@',
        content: '=',
        contentEn: '='
    }
});