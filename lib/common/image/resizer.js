'use strict';
const fs = require('fs');
const debug = require('debug')('image-resizer');
const path = require('path');
const sharp = require('sharp');
const mkdirp = require('mkdirp');
const process = require('process');
const fsPromise = require('./../fs');

const UPLAOD_TARGET_PATH = 'data/images/';

function checkTargetDirDir(maxWidth) {
    return new Promise((resolve, reject) => {
        let targetPath = path.join(process.cwd(), UPLAOD_TARGET_PATH, maxWidth + '');
        debug('Checking dir ', targetPath);
        fs.mkdir(targetPath, (err) => {
            if (err) {
                if (err.code === 'EEXIST') {
                    return resolve();
                }
                debug('Directory create err', err);
                return reject(err);
            }
            debug('Directory ', targetPath, ' OK');
            return resolve();
        });
    })
}

function resizeImage(imageFullServerName, targetFileName, maxWidth) {
    debug('Resizing ', imageFullServerName, ' to ', targetFileName, ' with max width: ', maxWidth);
    return fsPromise.fileExists(imageFullServerName)
    .then((sourceExists) => {
        if (!sourceExists) {
            debug('Warning: source file does not exists!');
            return;
        }
        return sharp(imageFullServerName)
        .resize(parseInt(maxWidth))
        .withoutEnlargement()
        .toFile(targetFileName)
        .then(() => {
            debug('sharp finished');
        })
    })
    .catch((err) => {
        debug('Unable to resize ' + imageFullServerName + ' to ' + maxWidth, err);
    })
}

let resize = function (imageFullServerName, targetImageExtension, imageId, sizes, targetPath) {
    debug('resizing image ', imageFullServerName, ' to sizes ', sizes);
    let queue = Promise.resolve();
    //for (let size of sizes) {
    sizes.map((size) => {
            let maxWidth = parseInt(size);
            debug('Expected with ', maxWidth);
            let targetFileName = path.join(process.cwd(), targetPath, maxWidth + '', imageId + (targetImageExtension.indexOf('.') > -1 ? targetImageExtension : '.' + targetImageExtension));
            queue = queue.then(() => {
                let targetFileName = path.join(process.cwd(), targetPath, maxWidth + '', imageId + (targetImageExtension.indexOf('.') > -1 ? targetImageExtension : '.' + targetImageExtension));
                return checkTargetDirDir(maxWidth);
            })
            .then(() => {
                return fsPromise.fileExists(targetFileName);
            })
            .then((fileExists) => {
                if (fileExists) {
                    debug('Image with size ' + maxWidth + ' already exists');
                    return;
                }
                debug('Image with size ' + maxWidth + ' does not exists');
                return resizeImage(imageFullServerName, targetFileName, maxWidth);
            });
        }
    )
    return queue;
}

module.exports.resize = resize;