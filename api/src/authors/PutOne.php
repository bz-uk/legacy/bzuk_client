<?php
namespace botgarApi\authors;

use Slim\Slim;

class PutOne {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message, $debug = null) {
      $response = ['status'=>'error', 'message'=> $message];
      if ($debug !== null) {
        $response['debug'] = $debug;
      }
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run($id) {
        $json = $this->app->request->getBody();
        $data = json_decode($json);
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);
                
        if (!is_numeric($id)) {
            return $this->returnInvalidResponse('invalid id');
        }
        if (!isset($data->name) || $data->name == null) {
            return $this->returnInvalidResponse('name can not be empty');
        }
        $sql = 'UPDATE authors SET name='.$db->quote($data->name).' WHERE id=' . $id;
        $result = $db->query($sql);
        if (!$result) {
            return $this->returnInvalidResponse('Unable to update author');
        }        
        $response = ['status'=> 'ok', 'message' => 'Author updated'];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
