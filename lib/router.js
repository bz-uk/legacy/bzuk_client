'use strict';
const Promise = require('bluebird');
const debug   = require('debug')('router');
const proxy = require('express-http-proxy');

const BaseRoute     = require('./common/route');
const RouteRedirect = require('./error/routeRedirect');
const localDb       = require('./database/local');
const glob          = require('glob');
const process       = require('process');

/**
 * @class Router
 *
 */
module.exports = class Router {
    constructor(expressApp) {
        this.expressApp = expressApp;
        this.routes     = [];
    }

    getRouteInstance(routeDefinition, options) {
        let routeInstance = new routeDefinition(options);
        return routeInstance;
    }

    initRoutes() {
        //proxies
        glob(process.cwd() + '/lib/routes/**/*.js', {}, (err, files) => {
            if (err) {
                throw err;
            }
            for (let file of files) {
                this.loadRoute(require(file));
            }
            console.log('Routes list:');
            this.printRoutes();
        })
    }

    processRoute(routeInstance, req, res) {
        let stepIndex  = 0;
        let routeSteps = routeInstance.getAllSteps();
        debug('route "'+routeInstance.getConfig().name+'" has ' + routeSteps.length + ' steps');

        function loop() {
            if (stepIndex >= routeSteps.length - 1) {
                return;
            }
            stepIndex++;
            return Promise.resolve(routeSteps[stepIndex].call(routeInstance, req, res)).then(loop);
        }

        return routeSteps[stepIndex].call(routeInstance, req, res).then(loop);
    }

    loadRoute(routeDefinition, options) {
        let instance = new routeDefinition(options);
        this.addRoute(instance.getConfig().method, instance.getConfig().path, instance, {name: instance.getConfig().name});
    }

    /**
     * @method addRoute
     * @param type
     * @param url
     * @param routeInstance
     * @param options
     */
    addRoute(type, url, routeInstance, options) {
        console.log()
        if (!(routeInstance instanceof BaseRoute)) {
            throw new Error('Route must be instance of BaseRoute!');
        }
        this.routes.push({type: type, url: url.toLowerCase(), instance: routeInstance, options: options});
        this.expressApp[type.toLowerCase()](url.toLowerCase(), (req, res, next) => {
            let routeContext  = {};
            routeContext.db   = localDb.bookshelf;
            routeContext.name = options.name || '';
            routeInstance.setContext(routeContext);
            this.processRoute(routeInstance, req, res)
            .then(function () {
                debug('route finished');
                if (!res.statusCode) {
                    throw new Error('no response');
                }
                return null;
            })
            .catch(function (err) { //TODO: extract to error handler
                debug('route error', err);
                if (err instanceof RouteRedirect) { //TODO do not handle redirect by throwing errors - it is ugly as hell
                    debug('Redirecting to ' + err.redirectUrl);
                    res.redirect(err.redirectUrl)
                } else {
                    debug('Route error', err.code);
                    res.status(400);
                    res.json({success: false, errorCode: err.statusCode, message: err.message, stack: err.stack, code: err.code});
                }
            })
        });
    }

    printRoutes() {
        this.routes.forEach((route) => {
            let name = (route.options && route.options.name) ? '@' + route.options.name : '';
            console.log(`${route.type.toUpperCase()}: ${route.url} ${name}`);
        });
    }
}