'use strict';
const ExtendableError = require('./extendableError');
const ErrorCodes = require('./codes');

module.exports = class WrongUsernameOrPassError extends ExtendableError {
    constructor() {
        super('Wrong username or password');
        this.code = ErrorCodes.login.wrongUsernameOrPass;
    }
};