'use strict';
var galleriesListController = function ($log, $location, galleriesService, tagsService, categoriesService, $mdDialog, $scope) {
    var ctrl = this;
    ctrl.model = {
        galleries: [],
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }
    ctrl.tags = {};
    ctrl.categories = {}

    ctrl.activateGallery = function (gallery) {
        galleriesService.setActive(gallery.id, !gallery.active);
    }

    ctrl.editGallery = function (evt, gallery) {
        $location.path('/galleries/' + gallery.id);
    }

    ctrl.getGalleries = function () {
        ctrl.model.promise = galleriesService.getAllGalleries(ctrl.query).then(function (galleries) {
            ctrl.model.galleries = galleries;
        })
    }

    ctrl.selected = [];
    ctrl.limitOptions = [5, 10, 15];
    ctrl.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };

    ctrl.getFilterVerbose = function (filter) {
        var output = [];
        if (filter.tags) {
            var tagsNames = [];
            _.forEach(filter.tags, function (tagId) {
                if (ctrl.tags[tagId]) {
                    tagsNames.push(ctrl.tags[tagId]);
                }
            })
            output.push('Tagy: ' + tagsNames.join(', '));
        }
        if (filter.categories) {
            var names = [];
            _.forEach(filter.categories, function (id) {
                if (ctrl.categories[id]) {
                    names.push(ctrl.categories[id]);
                }
            })
            if (names.length > 0) {
                output.push('Kategorie: ' + names.join(', '));
            }
        }
        return output.join(', ');
    }

    ctrl.processTags = function (tags) {
        for (var i = 0; i < tags.length; i++) {
            ctrl.tags[parseInt(tags[i].id)] = tags[i].name;
        }
    }

    ctrl.processCategories = function (categories) {
        for (var i = 0; i < categories.length; i++) {
            ctrl.categories[parseInt(categories[i].id)] = categories[i].name;
        }
    }

    tagsService.getAllTagsAsync(null, null, function () {
    })
        .then(function (tags) {
            ctrl.processTags(tags);
            return categoriesService.getAllCategoriesAsync(null, null);
        })
        .then(function (categories) {
            ctrl.processCategories(categories);
            ctrl.getGalleries(true);
        })
}

angular.module('adminApp').component('galleriesList', {
    templateUrl: 'gallery/galleriesList.template.html',
    controller: galleriesListController,
    bindings: {}
});