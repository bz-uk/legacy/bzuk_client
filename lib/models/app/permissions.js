'use strict'
module.exports = class Permissions {
    static has(permissions, permissionCode) {
        for (let i = 0; i < permissions.length; i++) {
            if (permissions[i].code === permissionCode) {
                return true;
            }
        }
        return false;
    }
}