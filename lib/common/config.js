'use strict';
const path = require('path');
const debug = require('debug')('config');

const CONFIG_DIR = 'config';

let configFileName = path.join('./../../', CONFIG_DIR, (process.env.NODE_ENV ? (process.env.NODE_ENV === 'production' ? '' : process.env.NODE_ENV) : 'development'), '/application.conf.js');
debug('Got env ', process.env.NODE_ENV, ', congif file resolved: ', configFileName);

module.exports = require(configFileName);