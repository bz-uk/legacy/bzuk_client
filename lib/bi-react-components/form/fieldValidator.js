'use strict';

export default class FieldValidator {
    constructor(validators, errorMessage, options) {
        let defaultOptions     = {};
        this.options           = Object.assign(defaultOptions, options);
        this.errorMessage      = errorMessage;
        this.validators        = validators;
        this.errors            = [];
        this.error             = '';
        this.valid             = null;
        this.validationPromise = null;
    }
    
    validate(value) {
        this.valid  = true;
        this.errors = [];
        this.error  = '';
        
        let localValid = true;
        let localErrors = [];
        
        this.validationPromise = Promise.resolve();
        for (let i = 0; i < this.validators.length; i++) {
            this.validationPromise = this.validationPromise.then(() => {
                return this.validators[i].validate(value)
                .then((response) => {
                    if (response.valid === false) {
                        localErrors.push(response.errorMessage);
                    }
                    localValid = localValid && response.valid;
                })
            });
        }
        return this.validationPromise.then(() => {
            if (!localValid) {
                this.errors = localErrors;
                this.error = this.errorMessage.replace(/\{commaSeparatedErrors\}/i, this.errors.join(', '))
            }
            this.valid = localValid;
            return this.valid;
        })
    }
};