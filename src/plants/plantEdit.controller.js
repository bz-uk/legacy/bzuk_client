'use strict'
angular.module('adminApp').controller('plantEditController', ['$location', '$rootScope', '$scope', '$mdDialog', '$mdMedia', 'menusService', '$timeout', '$routeParams', 'uiGmapIsReady', 'Upload', 'plantsService', 'settingsService', '$log', '$q', '$window', 'leftMenuService', 'ngAudio', '$interval',
    function ($location, $rootScope, $scope, $mdDialog, $mdMedia, menusService, $timeout, $routeParams, uiGmapIsReady, Upload, plantsService, settingsService, $log, $q, $window, leftMenuService, ngAudio, $interval) {
        $scope.markersIdCounter = 0
        $scope.modelChanged = false;
        // $scope.authorSearchText;
        // $scope.selectedAuthor;
        $scope.options = { //for mce
            language: 'en',
            format_tags: 'h1;h2',
            allowedContent: true,
            entities: false,
            removePlugins: 'templates'
            //width: '100%'
        };
        $scope.contentLanguageSelectorVisible = true;
        $scope.saveEnabled = true;
        $scope.formSettings = {
            selectedContentLanguage: 'cs'
        }
        $scope.$on('$locationChangeStart', function (event, next, current) {
            $log.debug('URL ', next);
            if ($scope.modelChanged === true) {
                var confirm = $mdDialog.confirm()
                    .title('Otevřít registr')
                    .textContent('Rostlina obsahuje neuložené změny. Přejít na registr?')
                    .ariaLabel('Otevřít registr')
                    .ok('Ano')
                    .cancel('Ne');

                $mdDialog.show(confirm).then(function () {
                    $scope.modelChanged = false;
                    leftMenuService.clearOpenConfirmation();
                    $location.url('/plants');
                }, function () {
                });
                event.preventDefault();
                return false;
            }
        });
        $rootScope.$on('marker_deleted', function (event, data) {
            for (var i = 0; i < $scope.model.markers.length; i++) {
                if ($scope.model.markers[i].id == data.markerId) {
                    $scope.model.markers.splice(i, 1)
                    event.stopPropagation();
                    return
                }
            }
            event.stopPropagation();
        })
        $scope.$on('save_enabled', function (event, enabled) {
            $scope.saveEnabled = enabled;
        });

        $scope.settings = []

        $scope.map = {
            infoWindowTemplate: 'plants/plantEditMapMarkerInfoWindow.template.html',
            markerOptions: {
                draggable: true,
                animation: 'DROP'
            },
            center: {
                latitude: 0,
                longitude: 0
            },
            zoom: 18,
            bounds: {
                northeast: {
                    latitude: 50.0689551446,
                    longitude: 14.4179141521
                },
                southwest: {
                    latitude: 50.0733622808,
                    longitude: 14.4247806072
                }
            },
            events: {
                click: function (mapModel, eventName, originalEventArgs) {
                    $scope.$apply(function () {
                        var e = originalEventArgs[0];
                        //$scope.model.markers = []
                        $scope.markersIdCounter++
                        $scope.model.markers.push({
                            latitude: e.latLng.lat(),
                            longitude: e.latLng.lng(),
                            id: $scope.markersIdCounter,
                            title: $scope.model.name
                        })
                    });
                }
            }
        }

        $scope.$on('$destroy', $scope.$on('show-mode-change', function (event, mode) {
            if (mode === 'show') {
                $scope.contentLanguageSelectorVisible = true;
            }
            if (mode === 'edit') {
                $scope.contentLanguageSelectorVisible = false;
            }
            event.stopPropagation();
        }));
        // authorsService.onAllAuthorsLoaded($scope, function (event, authors) {
        //     $scope.allAuthors = authors;
        // })

        //start: tags
        $scope.allTags = []

        $scope.showAllTags = function () {
            var tagNames = []
            for (var i = 0; i < $scope.allTags.length; i++) {
                tagNames.push($scope.allTags[i].name);
            }
            alert("Existing tags: \n" + tagNames.join(",\n") + "\n\nNote: this dialog will be nicer in the future")
        }

        $scope.reloadTags = function (okCallback) {
            plantsService.getAllTags(function (allTags) {
                $scope.allTags = allTags
                if (okCallback !== undefined) {
                    okCallback()
                }
            }, function (err) {
                $log.error(err)
            })
        }
        $scope.reloadTags()

        $scope.addTag = function (ev) {
            $mdDialog.show({
                controller: 'addTagDailogController',
                templateUrl: 'plants/addTagDialog.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                resolve: {},
                clickOutsideToClose: true,
                fullscreen: true
            })
                .then(function (newTag) {
                    console.log('tag created!')
                    $scope.reloadTags(function (data) {
                        console.log('collection reloaded')
                        $scope.model.tags.push(newTag)
                    })
                }, function () {
                    console.log('You cancelled the dialog.');
                });
        }

        $scope.filterSelected = true;
        $scope.tagQuerySearch = function (criteria) {
            console.log($scope.allTags)
            return $scope.allTags.filter($scope.createFilterFor(criteria))
        }

        $scope.createFilterFor = function (query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(tag) {
                console.log(tag.name_lowercase + ', pos:' + tag.name_lowercase.indexOf(lowercaseQuery))
                return (tag.name_lowercase.indexOf(lowercaseQuery) != -1);
                ;
            }
        }

        $scope.getQrCode = function () {
            return $scope.settings['registryMapUrl'] + '?plant=' + $scope.model.id;
        }
        //start: tags

        $scope.getNormalizedCoord = function (coord, zoom) {
            var y = coord.y;
            var x = coord.x;

            // tile range in one direction range is dependent on zoom level
            // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
            var tileRange = 1 << zoom;

            // don't repeat across y-axis (vertically)
            if (y < 0 || y >= tileRange) {
                return null;
            }

            // repeat across x-axis
            if (x < 0 || x >= tileRange) {
                x = (x % tileRange + tileRange) % tileRange;
            }

            return {
                x: x,
                y: y
            };
        }

        $scope.getLocationFromMap = function () {
            if ($scope.model.markers === undefined || $scope.model.markers.length == 0) {
                return 'not set'
            }
            return $scope.model.markers.length + ' markers'
        }

        $scope.initMap = function () {
            $scope.moonMapType = new google.maps.ImageMapType({
                getTileUrl: function (coord, zoom) {
                    // console.log(getProjection())
                    var normalizedCoord = $scope.getNormalizedCoord(coord, zoom);
                    if (!normalizedCoord) {
                        return null;
                    }

                    var bound = Math.pow(2, zoom);
                    // console.log('x: ' + coord.x)
                    // console.log('y:' + coord.y)
                    var url = '//' + $scope.settings.baseUrl + 'api/map2/' + zoom + '/' + normalizedCoord.x + '/' + normalizedCoord.y + '.png'
                    //var url = 'http://botgar.local/api/map/' + zoom + '/' + normalizedCoord.x + '/' + (bound - normalizedCoord.y - 1) + '.png'
                    // console.log(url)
                    return url;
                },
                tileSize: new google.maps.Size(256, 256),
                maxZoom: 18,
                minZoom: 17,
                radius: 1738000,
                name: 'Moon'
            });
        }

        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        })
        $scope.deleteImage = function (image) {
            var confirm = $mdDialog.confirm()
                .title('Delete')
                .textContent('Do you really want to delete this image?')
                .ariaLabel('Delete')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function () {
                image.status = 'deleted'
            }, function () {
            });
        }

        $scope.getPublishedLabel = function () {
            return $scope.model.is_active ? 'Publikován' : 'Nepublikován';
        }

        $scope.removeCzAudio = function () {
            $scope.model.audio_cz = null;
            $scope.model.audio_cz_url = null;
        }

        $scope.uploadCzAudioFile = function (file, errFiles) {
            //$scope.f = file;
            //$scope.errFile = errFiles && errFiles[0];
            $scope.audioCzUpload = true;
            if (file) {
                file.upload = Upload.upload({
                    url: '/upload/audio',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        var data = response.data[0];
                        //file.result = response.data;
                        $scope.model.audio_cz = data.id;
                        $scope.model.audio_cz_url = data.url;
                        $scope.audioCzUpload = false;
                        var audioUrl = $scope.model.audio_cz_url.replace(/\{\$BASE_URL\}/, '//' + $scope.settings.baseUrl.replace(/\/$/, ''));
                        console.log('Got audio: ', audioUrl);
                        $scope.sound_cz = ngAudio.load(audioUrl);
                    },100);
                }, function (response) {
                    if (resp.data.code === 'upload_unsupported_format') {
                        alert('Unable to upload image: unsupported format.');
                    }
                    $scope.audioCzUpload = false;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                    $scope.audioCzUploadProgress = file.progress;
                });
            }
        }

        uiGmapIsReady.promise(1).then(function (instances) {
            instances.forEach(function (inst) {
                var map = inst.map
                var uuid = map.uiGmap_id
                var mapInstanceNumber = inst.instance // Starts at 1.
                console.log('ready!')
                //map.addTileOverlay(new TileOverlayOptions().tileProvider());
                $scope.initMap()
                $scope.map.center = {
                    latitude: parseFloat($scope.settings.adminMapCenterLat),
                    longitude: parseFloat($scope.settings.adminMapCenterLon)
                };
                map.overlayMapTypes.push($scope.moonMapType);
                // map.mapTypes.set('moon', $scope.moonMapType);
                // map.setMapTypeId('moon');
            })
        })

        $scope.families = []
        $scope.categories = []
        // $scope.allAuthors = []

        plantsService.getCategories(function (categories) {
            $scope.categories = categories
        })

        plantsService.getFamilies(function (families) {
            $scope.families = families
        })

        // authorsService.loadAll()


        $scope.saving = false
        $scope.isNew = $routeParams.plantId == '_'

        settingsService.load(function (data) {
            $scope.settings = data;

            if ($scope.isNew) {
                $scope.model = {
                    new_family: 'none',
                    occurrence: '',
                    name: '',
                    name_en: '',
                    name_lat: '',
                    name_de: '',
                    name_sk: '',
                    new_category: 'none',
                    authors: [],
                    is_active: false,
                    blocks: [{
                        content: 'ukazkovej text',
                        content_en: 'example text',
                        backgroud_color: '#ffffff',
                        type: 0
                    }],
                    delete_blocks: [],
                    markers: [{
                        id: 1,
                        latitude: parseFloat($scope.settings.adminMapCenterLat),
                        longitude: parseFloat($scope.settings.adminMapCenterLon),
                        title: $scope.model.name
                    }],
                    images: [],
                    tags: []
                }
            } else {
                $scope.model = {
                    id: $routeParams.plantId
                }
            }

            if (!$scope.isNew) {
                $scope.loadPlant($scope.model.id);
                $scope.$watch('model', function (newVal, oldVal) {
                    if ($scope.originalModel === undefined) {
                        $scope.originalModel = _.cloneDeep(newVal.toJSON());
                        return;
                    }
                    $scope.modelChanged = !_.isEqual(JSON.parse(angular.toJson(newVal)), $scope.originalModel);
                    if ($scope.modelChanged) {
                        leftMenuService.setOpenConfirmation('Opravdu odejít? Všechny změny budou ztracenny.')
                    } else {
                        leftMenuService.clearOpenConfirmation();
                    }
                }, true);
            } else {
                $scope.markersIdCounter = 1
                //$scope.originalModel = _.cloneDeep($scope.model);
                $scope.$watch('model', function (newVal, oldVal) {
                    if ($scope.originalModel === undefined) {
                        $scope.originalModel = _.cloneDeep(JSON.parse(angular.toJson($scope.model)));
                        return;
                    }
                    $log.debug($scope.originalModel);
                    $log.debug(JSON.parse(angular.toJson(newVal)));
                    $scope.modelChanged = !_.isEqual(JSON.parse(angular.toJson(newVal)), $scope.originalModel);
                    if ($scope.modelChanged) {
                        leftMenuService.setOpenConfirmation('Opravdu odejít? Všechny změny budou ztraceny.')
                    } else {
                        leftMenuService.clearOpenConfirmation();
                    }
                }, true);
            }

        });

        // $scope.onMapClicked = function(event) {
        //     $scope.$apply(function() {
        //
        //         var point = event.latlng
        //         $scope.markersIdCounter++
        //         $scope.model.markers.push({
        //             latitude: point.lat,
        //             longitude: point.lng,
        //             id: $scope.markersIdCounter,
        //             title: 'hello'
        //         })
        //         $log.log($scope.model.markers)
        //     })
        // }

        $scope.flashMessages = []


        $scope.saveAndClose = function () {
            $scope.save(function () {
                $scope.openPlantsList();
            })
        }

        $scope.save = function (okCallback) {
            if (($scope.model.name === '' || $scope.model.name === undefined) &&
                ($scope.model.name_en === '' || $scope.model.name_en === undefined) &&
                ($scope.model.name_lat === '' || $scope.model.name_lat === undefined)) {
                alert('Please enter at least one name is some language')
                return
            }
            $scope.saving = true;
            if ($scope.isNew) {
                plantsService.insertOne($scope.model, function (updatedPlant) {
                    $log.debug('Plant created', updatedPlant);
                    $rootScope.$emit('ok-message', 'Rostlina byla vytvořena');
                    $scope.isNew = false;
                    if (okCallback) {
                        $scope.modelChanged = false;
                        return okCallback()
                    }
                    $scope.loadPlant(updatedPlant.id).then(function () {
                        $scope.saving = false
                        leftMenuService.clearOpenConfirmation();
                        $scope.modelChanged = false;
                        $scope.originalModel = _.cloneDeep(JSON.parse(angular.toJson($scope.model)));
                    })
                }, function (err) {
                    $scope.saving = false
                    $rootScope.$emit('error-message', 'Vložení nové rostliny zlyhalo.');
                });
            } else {
                plantsService.updateOne($scope.model, function (updatedPlant) {
                    $rootScope.$emit('ok-message', 'Rostlina byla uložena');
                    leftMenuService.clearOpenConfirmation();
                    if (okCallback) {
                        $scope.modelChanged = false;
                        okCallback();
                    }
                    $scope.loadPlant($scope.model.id).then(function () {
                        $scope.saving = false;
                        leftMenuService.clearOpenConfirmation();
                        $scope.modelChanged = false;
                        $scope.originalModel = _.cloneDeep(JSON.parse(angular.toJson($scope.model)));
                    })
                }, function (err) {
                    $scope.saving = false
                    alert('Unable to save plant: ' + err)
                });
            }
        }

        $scope.openPlantsList = function () {
            if ($scope.modelChanged === true) {
                var confirm = $mdDialog.confirm()
                    .title('Přejít na seznam rostlin')
                    .textContent('Rostlina obsahuje neuložené změny. Přejít na seznam rostlin?')
                    .ariaLabel('Přejít na seznam rostlin')
                    .ok('Ano')
                    .cancel('Ne');
                $mdDialog.show(confirm).then(function () {
                    leftMenuService.clearOpenConfirmation();
                    $location.url('/plants/')
                }, function () {
                });
            } else {
                $location.url('/plants/')
            }
        }
        $scope.opnRegistryLink = function (link) {
            if ($scope.modelChanged === true) {
                var confirm = $mdDialog.confirm()
                    .title('Otevřít registr')
                    .textContent('Rostlina obsahuje neuložené změny. Přejít na registr?')
                    .ariaLabel('Otevřít registr')
                    .ok('Ano')
                    .cancel('Ne');

                $mdDialog.show(confirm).then(function () {
                    $window.open(link, '_blank');
                }, function () {
                });
            } else {
                $window.open(link, '_blank');
            }
        }
        $scope.cancel = function () {
            $scope.openPlantsList();
        }
        $scope.getSaveLabel = function () {
            if ($scope.isNew) {
                return $scope.saving ? 'Vytvářím...' : 'Vytvořit'
            } else {
                return $scope.saving ? 'Ukládám...' : 'Uložit'
            }
        }

        $scope.getSaveAndCloseLabel = function () {
            if ($scope.isNew) {
                return $scope.saving ? 'Vytvážím...' : 'Vytvořit a zavřít'
            } else {
                return $scope.saving ? 'Ukládám...' : 'Uložit a zavřít'
            }
        }

        $scope.getHhMmSs = function(seconds) {
            var h = Math.floor(seconds / 3600);
            var m = Math.floor(seconds % 3600 / 60);
            var s = Math.floor(seconds % 3600 % 60);

            return (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s);
        }

        $scope.togglePlayAudioCz = function () {
            if ($scope.audio_cz_timer) {
                $interval.cancel($scope.audio_cz_timer);
            }
            $scope.audio_cz_timer = $interval(function () {
                $scope.audio_cz_progress = $scope.getHhMmSs($scope.sound_cz.currentTime);
                $scope.audio_cz_total = $scope.getHhMmSs($scope.sound_cz.currentTime + $scope.sound_cz.remaining);
            }, 100);

            if ($scope.audio_cz_status && $scope.audio_cz_status === 'playing') {
                $interval.cancel($scope.audio_cz_timer);
                $scope.sound_cz.pause();
                $scope.audio_cz_status = 'pause';
                return;
            }
            $scope.sound_cz.play();
            $scope.audio_cz_status = 'playing';
        }

        $scope.resetAudioCzPlay = function () {
            $interval.cancel($scope.audio_cz_timer);
            $scope.sound_cz.stop();
            $scope.audio_cz_status = null;
        }

        $scope.loadPlant = function (plantId) {
            return plantsService.getPlant(plantId, function (plantModel) {
                delete plantModel.$promise;
                delete plantModel.$resolved;
                plantModel.delete_blocks = [];
                var maxId = 0
                for (var i = 0; i < plantModel.markers.length; i++) {
                    if (plantModel.markers[i].id > maxId) {
                        maxId = plantModel.markers[i].id
                    }
                }
                $scope.markersIdCounter = maxId
                $scope.model = plantModel;
                if ($scope.model.audio_cz_url) {
                    var audioUrl = $scope.model.audio_cz_url.replace(/\{\$BASE_URL\}/, '//' + $scope.settings.baseUrl.replace(/\/$/, ''));
                    console.log('Audio url ', audioUrl);
                    $scope.sound_cz = ngAudio.load(audioUrl);
                }
            });
        }


    }
]);
angular.module('adminApp').controller('infoWindowCtrl', function ($scope, $rootScope) {
    $scope.markerNote = ''
    $scope.deleteMarker = function () {
        console.log($scope.$parent.model.id) //marker id fucker!
        $rootScope.$emit('marker_deleted', {markerId: $scope.$parent.model.id})
    }
});
