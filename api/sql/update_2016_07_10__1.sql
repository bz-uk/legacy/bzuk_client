ALTER TABLE `circuit_primary_points`
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `primary_point_parts` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;
