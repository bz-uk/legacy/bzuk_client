'use strict';
const process = require('process');
const path = require('path');
const program = require('commander');
const debug = require('debug')(__filename);

const configuration = require('./../lib/common/config');
const sequelize = require('./../lib/database/local').sequelize;
const knex = require('./../lib/database/local').knex;
const Resizer = require('./../lib/common/image/resizer');

const RESIZE_WIDTHS = [250, 640, 1280]; //all formats const RESIZE_WIDTHS = [100, 150, 200, 250, 384, 512, 640, 800, 1024, 1280, 2048]; //all formats
const IMAGES_TARGET_PATH = 'data/images/';

program
.version('0.1.0')
.option('-t, --test', 'Do not do anything, just print out results')
.parse(process.argv);

if (program.test) {
    console.log('TEST MODE!');
}

let run = async function () {
    console.log('Checking images...');
    let blockImages = await getAllActivePlantsBLockImages()
    console.log('Found ' + blockImages.length);
    let plantImages = await getAllDirectPlantsImages()
    console.log('Found ' + plantImages.length);
    let articleImages = await getAllDirectArticlesImages();
    console.log('Found ' + articleImages.length + ' images. Regenerating...');
    let images = blockImages.concat(plantImages).concat(articleImages);
    try {
        await convertImages(images);
    } catch (e) {
        console.error('Unable to convert images ', e);
        process.exit(1);
    }
    console.log('Regeneration complete');
    process.exit(0);
};

let convertImages = function (images) {
    let queue = Promise.resolve()
    //let image = images[0];
    images.map((image) => {
        let sourceImage = image;
        let serverFileName;
        if (image.path) {
            serverFileName = path.join(process.cwd(), 'api/data/img', image.path && image.path.replace('../data/img/', ''));
        } else {
            serverFileName = path.join(process.cwd(), 'api/data/img', image.name);
        }
        queue = queue.then(() => {
            debug('Resizing image ', sourceImage.id, ' from location ', serverFileName);
            let imageExtension = serverFileName.indexOf('.jpg') > -1 ? 'jpg' : sourceImage.type.indexOf('.jpeg') > -1 ? 'jpeg' : 'png';
            if (program.test) {
                return;
            }
            return Resizer.resize(serverFileName, imageExtension, sourceImage.id, RESIZE_WIDTHS, IMAGES_TARGET_PATH);
        })
        .then(() => {
            debug('resize complete');
            if (program.test) {
                return;
            }
            return Resizer.resize(serverFileName, 'webp', sourceImage.id, RESIZE_WIDTHS, IMAGES_TARGET_PATH);
        })
        .then(() => {
            debug('webp resize complete');
        })
    })
    return queue;
}

let getAllDirectArticlesImages = function () {
    let query = knex.select('image.*')
    .from('article')
    .leftJoin('image', function () {
        this.on('image.id', '=', 'article.image_id')
    })
    .whereNotNull('image.id')
    .where('article.deleted_at', null)
    .toString();
    return sequelize.query(
        query
        , {type: sequelize.QueryTypes.SELECT})
}

let getAllDirectPlantsImages = function () {
    let query = knex.select('image.*')
    .from('plants')
    .leftJoin('plant_image', function () {
        this.on('plants.id', '=', 'plant_image.plant_id')
    })
    .leftJoin('image', function () {
        this.on('image.id', '=', 'plant_image.image_id')
    })
    .whereNotNull('image.id')
    .where('plants.is_deleted', 0)
    .toString();
    return sequelize.query(
        query
        , {type: sequelize.QueryTypes.SELECT})
}

let getAllActivePlantsBLockImages = function () {
    let query = knex.select('image.*')
    .from('plants')
    .leftJoin('plant_block', function () {
        this.on('plants.id', '=', 'plant_block.plant_id').andOn('plant_block.type', 1).onNull('plant_block.deleted_at')
    })
    .leftJoin('plant_block_image', 'plant_block_image.plant_block_id', 'plant_block.id')
    .leftJoin('image', 'image.id', 'plant_block_image.image_id')
    .where('plants.is_deleted', 0)
    .whereNotNull('image.id')
    .toString();
    debug('Query: ', query);
    return sequelize.query(
        query
        , {type: sequelize.QueryTypes.SELECT})
};

run();
