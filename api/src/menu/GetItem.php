<?php
namespace botgarApi\menu;

use Slim\Slim;

class GetItem {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($menuId, $menuItemId) {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT * FROM menu where id=" . $menuId;
        $result = $db->query($sql);
        if (!$result) {
            $response = ['status'=>'error', 'message'=> 'Problem during menu properties laod'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }
        $response = $result->fetch(\PDO::FETCH_ASSOC);
        $publishable = $response['publishable_items'];

        $sql = "SELECT * FROM menu_item where id=" . $menuItemId;
        $result = $db->query($sql);
        if (!$result) {
            $response = ['status'=>'error', 'message'=> 'Problem during menu item laod'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }
        $response = $result->fetch(\PDO::FETCH_ASSOC);
        $this->app->response->setStatus(200);
        $response['publishable'] = $publishable;
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
