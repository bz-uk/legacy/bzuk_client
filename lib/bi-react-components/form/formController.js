'use strict';
import EventEmitter from 'wolfy87-eventemitter';

export default class FormController extends EventEmitter {
    constructor(options) {
        super();
        this.fields = {}
        this.valid  = null;
    }
    
    validate() {
        var formFalid = true;
        let promise   = Promise.resolve(true);
        _.forIn(this.fields, (field) => {
            promise = promise.then((valid) => {
                formFalid = formFalid && valid;
                return field.validate()
            })
        })
        return promise
        .then((valid) => {
            formFalid = formFalid && valid;
            return formFalid;
        })
    }
}