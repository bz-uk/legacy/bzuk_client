'use strict';
module.exports = {
    "Logout": "Logout", //top right user menu item
    "Dashboard": "Přehled",
    "Plants": "Rostliny",
    "Loading...": "Načítám...",
    "Total plants:": "Rostlin celkově:",
    "Published:": "Publikováno:",
    "Articles:": "Článků:",
    "Articles": "Články",
    "Create": "Vytvoř"
}