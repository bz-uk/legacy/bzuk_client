'use strict';
angular.module('adminApp').controller('menuItemsListController', ['$scope', '$mdDialog', '$mdMedia', 'menusService', '$timeout', '$location', function ($scope, $mdDialog, $mdMedia, menusService, $timeout, $location) {
  $scope.orderable = false
  $scope.publishable = false
    $scope.model = {
        query: {
            order: 'name',
            limit: 10,
            page: 1
        },
        selected: []
    }
    function success(menuItems) {
      console.log(menuItems)
      $scope.orderable = menuItems.orderable === '1'?true:false
      $scope.publishable = menuItems.publishable === '1'?true:false
      $scope.enableOrdering($scope.orderable)
        for(var i = 0; i<menuItems.items.length;i++) {
            if (menuItems.items[i].published === "1") {
                menuItems.items[i].published = true;
            } else {
                menuItems.items[i].published = false;
            }
        }
        $scope.items = menuItems.items;
    }
    $scope.publicationEnabled = function() {
      return $scope.publishable
    }
    $scope.getMenuItems = function () {
        $scope.model.promise = menusService.loadMenuItems($scope.menuId, $scope.query, success).$promise;
    }
    $scope.editMenuItem = function (menuItem, ev) {
        $location.url('menus/' + $scope.menuId + '/menu-items/' + menuItem.id)
    }
    $scope.publishItem = function(menuItem) {
        $timeout(function() {
            menusService.saveMenuItem($scope.menuId, menuItem, function(data) {
                var isPublished = menuItem.published?true:false;
                $scope.onItemPublished({published: isPublished})
                $scope.getMenuItems($scope.menuId);
            });
        }, 100);
    }
    $scope.selected = [];
    $scope.limitOptions = [5, 10, 15];
    $scope.enableOrdering = function(enable) {
      if (!enable) {
          $scope.sortableOptions = {
            disabled: true
          };
          return;
      }
      $scope.sortableOptions = {
          stop: function(e, ui) {
              console.log($scope.items);
              var order = [];
              for (var i = 0; i < $scope.items.length; i++) {
                  order.push($scope.items[i].id)
              }
              menusService.saveItemsOrder($scope.menuId, order, $scope.model.query, function() {
                  $scope.onItemsReordered({})
              })
          },
          helper: "clone",
          forceHelperSize: true,
          forcePlaceholderSize: true
      }
    }

    $scope.options = {
        rowSelection: false,
        multiSelect: false,
        autoSelect: false,
        decapitate: false,
        largeEditDialog: true,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    $scope.getMenuItems($scope.menuId);
}]);
