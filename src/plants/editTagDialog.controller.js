angular.module('adminApp').controller('editTagDialogController', ['$scope', '$mdDialog', 'plantsService', 'tag', '$log', function($scope, $mdDialog, plantsService, tag, $log) {
    $scope.model = tag

    //start: listeners
    plantsService.onPlantTagSaved($scope, function(event, tag) {
      $scope.hide({relaod: true})
    })
    //end: listeners

    $scope.save = function() {
      plantsService.savePlantTag($scope.model.id, $scope.model)
    }

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false,
        width: 600
    };
}]);
