<?php
namespace botgarApi\circuit;

use botgarApi\image\ImageModel;
use Slim\Slim;

class Post {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run() {
    	$json = $this->app->request->getBody();
    	$data = json_decode($json);
      //validation
    	if (!isset($data->title) || $data->title == "") {
    		$response = ['status'=>'error', 'message'=> 'Title can not be empty'];
    		$this->app->response->setStatus(400);
    		$response = json_encode($response);
    		$this->app->response->header('Content-Length', strlen($response));
    		$this->app->response->setBody($response);
    		return;
    	}
    	if (!isset($data->content) || $data->content == "") {
    		$response = ['status'=>'error', 'message'=> 'Content can not be empty'];
    		$this->app->response->setStatus(400);
    		$response = json_encode($response);
    		$this->app->response->header('Content-Length', strlen($response));
    		$this->app->response->setBody($response);
    		return;
    	}

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $db->beginTransaction();

        $content = '';
        if (isset($data->content)) {
          $content = $data->content;
        }
        $content_en = '';
        if (isset($data->content_en)) {
          $content = $data->content;
        }
        //image
        $image_id = 'null';
        if (isset($data->image) && $data->image != null && $data->image->id > 0) {
          $image_id = $data->image->id;
        }
        //circuit
        $titleEn = '';
        if (isset($data->title_en)) {
            $titleEn = $data->title_en;
        }
        $sql = "INSERT INTO circuit(color, title, title_en, content, content_en, image_id) values(".$db->quote($data->color).", ".$db->quote($data->title).", ".$db->quote($titleEn).", ".$db->quote($data->content).", ".$db->quote($data->content_en).", ".$image_id.")";
        $result = $db->query($sql);
        if (!$result) {
            $db->rollback();
            $response = ['status'=>'error', 'message'=> 'Problem during creating circuit'];
            $this->app->response->setStatus(400);
            $response = json_encode($response);
            $this->app->response->header('Content-Length', strlen($response));
            $this->app->response->setBody($response);
            return;
        }
        $id = $db->lastInsertId();
        $db->commit();
        $response = ['status'=> 'ok', 'message' => 'Circuit created', 'id' => $id];
        $this->app->response->setStatus(200);
        $response = json_encode($response);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
        return;
    }
}
