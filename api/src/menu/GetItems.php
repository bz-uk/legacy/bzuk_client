<?php
namespace botgarApi\menu;

use Slim\Slim;

class GetItems {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($menuId) {
        if (!is_numeric($menuId)) {
            $this->app->response->setStatus(400);
            return;
        }
        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $sql = "SELECT * FROM menu WHERE id=" . $menuId;
        $result = $db->query($sql);
        $menu = $result->fetch(\PDO::FETCH_ASSOC);
        $orderable = $menu['orderable'];
        $publishable = $menu['publishable_items'];

        $sql = "SELECT * FROM menu_item WHERE menu_id=" . $menuId . " order by `order`";
        $result = $db->query($sql);
        if (!$result) {
            $response = ['error' => 'No items found', 'debug' => $sql];
            $this->app->response->setBody(json_encode($response));
            $this->app->response->setStatus(404);
            return;
        }
        $output = ['items' => [], 'orderable' => $orderable, 'publishable' => $publishable];
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            //add menu item
            $item = [
                'id' => $row['id'],
                'title' => $row['title'],
                'title_en' => $row['title_en'],
                'subtitle' => $row['subtitle'],
                'subtitle_en' => $row['subtitle_en'],
                'published' => $row['published'],
                'order' => $row['order'],
            ];

            $output['items'][] = $item;
        }
        $response = json_encode($output);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
