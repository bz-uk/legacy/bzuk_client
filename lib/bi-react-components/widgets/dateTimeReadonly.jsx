'use strict';
import React from 'react';

var format = require('date-format');

const DEFAULT_DATETIME_FORMAT = 'dd.MM.yyyy hh:mm';

export default class DateTimeReadonly extends React.Component {
    constructor(props) {
        super(props);
        this.format = DEFAULT_DATETIME_FORMAT;
    }
    
    getHumanReadableDiffString(date) {
        var now   = new Date();
        var delta = Math.round((now.getTime() - date.getTime()) / 1000);
        
        var minute = 60,
            hour   = minute * 60,
            day    = hour * 24,
            week   = day * 7;
        
        var fuzzy;
        if (delta < 30) {
            fuzzy = 'just now';
        } else if (delta < minute) {
            fuzzy = delta + ' seconds ago';
        } else if (delta < 2 * minute) {
            fuzzy = 'a minute ago'
        } else if (delta < hour) {
            fuzzy = Math.floor(delta / minute) + ' minutes ago';
        } else if (Math.floor(delta / hour) == 1) {
            fuzzy = '1 hour ago'
        } else if (delta < day) {
            fuzzy = Math.floor(delta / hour) + ' hours ago';
        } else if (delta < day * 2) {
            fuzzy = 'yesterday';
        } else if (delta < day * 7) {
            fuzzy = Math.floor(delta / day) + ' days ago';
        } else if (delta < day * 8) {
            fuzzy = 'a week ago';
        } else if (delta < week * 4) {
            fuzzy = Math.floor(delta / week) + ' weeks ago';
        } else {
            fuzzy = format(this.format, date);
        }
        return fuzzy;
    }
    
    render() {
        if (this.props.date instanceof Date) {
            let formatedDate = format(this.format, this.props.date);
            return (<span title={formatedDate}>
                {this.getHumanReadableDiffString(this.props.date)}
            </span>);
        } else {
            return (<span></span>);
        }
    }
}