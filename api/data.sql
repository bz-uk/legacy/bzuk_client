-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL,
  `title` varchar(1024) NOT NULL,
  `code` varchar(10) NOT NULL,
  `author` varchar(1024) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_changed` datetime NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  `content` text NOT NULL,
  `map_marker_id` bigint(20) NOT NULL,
  `map_overlay_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `article` (`id`, `type`, `title`, `code`, `author`, `date_created`, `date_changed`, `image_id`, `tag_id`, `content`, `map_marker_id`, `map_overlay_id`) VALUES
(1,	0,	'Vystava orchideji',	'',	'Botanicka zahrada',	'2015-12-27 18:03:01',	'2015-12-27 18:03:01',	2,	1,	'Lorem ipsum',	0,	0),
(2,	0,	'Vanocni vystava',	'',	'Botanicka zahrada',	'2015-12-27 18:03:06',	'2015-12-27 18:03:06',	2,	1,	'lorem ipsum',	0,	0),
(3,	0,	'Oteviraci doba a cennik',	'',	'Botanicka zahrada',	'2016-02-04 06:22:01',	'2016-02-04 06:22:01',	0,	0,	'',	0,	0),
(4,	0,	'Historie zahrady',	'',	'Botanicka zahrada',	'2016-02-04 06:22:01',	'2016-02-04 06:22:01',	0,	0,	'',	0,	0),
(5,	0,	'Jak se k nám dostanete',	'',	'Botanicka zahrada',	'2016-02-04 06:22:01',	'2016-02-04 06:22:01',	0,	0,	'',	0,	0),
(6,	0,	'Komentované prohlídky',	'',	'Botanicka zahrada',	'2016-02-04 06:22:01',	'2016-02-04 06:22:01',	0,	0,	'',	0,	0),
(7,	0,	'Kalendar akcii',	'',	'Botanicka zahrada',	'2016-02-05 06:52:17',	'2016-02-05 06:52:17',	0,	0,	'',	0,	0),
(8,	0,	'kaktury a ostatni sukulenty',	'',	'Botanicka zahrada',	'2016-02-05 20:27:13',	'2016-02-05 20:27:13',	0,	0,	'',	0,	0);

DROP TABLE IF EXISTS `article_block`;
CREATE TABLE `article_block` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `backgroud_color` varchar(10) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `article_block` (`id`, `article_id`, `content`, `backgroud_color`, `order`) VALUES
(1,	1,	'lorem ipsum 1',	'FFFFFF',	0),
(2,	1,	'and here is second one...',	'88FFFF',	1),
(3,	2,	'vanoce lorem 1',	'FFFFFF',	0),
(4,	3,	'<b>Unor-březen</b><br>\r\nSkleníky: 10-16 h<br>\r\nExteriéry: 10-17 h<br>\r\n',	'',	10),
(5,	3,	'<b>Duben-srpen</b><br>\r\nSkleníky: 0-17 h<br>\r\nExteriéry: 10-19,30 h<br>\r\n',	'',	20),
(6,	3,	'<b>Září-říjen</b> (do změny času)<br>\r\nSkleníky: 10-17 h<br>\r\nExteriéry: 10-18 h<br>',	'',	30),
(7,	3,	'<b>Listopad-leden</b><br>\r\nSkleníky: 10-15,30 h<br>\r\nExteriéry: 10-16 h<br>',	'',	40),
(8,	3,	'Otevírací doba skleníků se může během výstav měnit.<br>\r\n<br>\r\nPokladna se uzavírá 30 minut před uzavřením skleníku.<br>\r\n<br>\r\nSkleníky i exteriéry jsou otevřené denně po celý rok.<br>\r\n<br>',	'',	50),
(9,	4,	'Univerzitní zahrada byla založena před 230 lety, r. 1775 v Praze na Smíchově. Kolem r. 1840 se v ní pěstovalo téměř 13 000 druhů a odrůd domácích i cizokrajných rostlin.\r\n<br><br>\r\nPro časté záplavy byla zahrada přemístěna na pozemek odkoupený od „Společnosti pro zvelebování zahrad“, podporované mj. knížetem Rohanem. Na současném místě byla otevřena spolu s novými botanickými ústavy české i německé univerzity v roce 1898 (budovy Benátská 2 a Viničná 5).\r\n<br><br>\r\nSbírky rostlin ze Smíchova byly rozděleny mezi obě univerzity rovným dílem. Převzaty byly rovněž sbírky ze skleníků „Společnosti pro zvelebování zahad“; část z nich se dochovala dodnes, proto je stáří některých palem, cykasů a dalších rostlin odhadováno na více než 130 let.\r\n<br><br>\r\nPři náletu 14. února. 1945 byly silně poškozeny a později strženy skleníky německé univerzity. Po osvobození převzala obě části zahrady jako celek Univerzita Karlova. Do osmdesátých let minulého století byla zahrada formálně propojena s Katedrou botaniky, od té doby je samostatnou správní jednotkou.\r\n<br><br>',	'FFFFFF',	0),
(10,	5,	'<b>Adresa</b><br>\r\n<br>\r\nBotanická zahrada Přírodovědecké fakulty UK v Praze<br>\r\n<br>\r\nNa Slupi 16<br>\r\n<br>\r\n128 01  Praha 2<br>\r\n<br>\r\nGPS: 50°4’15.328”N, 14°25’12.432”E<br>\r\n<br>\r\n<b>Dopravní spojení</b><br>\r\n<br>\r\nTramvaj: Linky 6, 18 a 24, zastávka “Botanická zahrada”<br>\r\n<br>\r\nMetro: Linka B, zastávka “Karlovo náměstí”<br>',	'FFFFFF',	0),
(11,	6,	'<b>EXterieary</b><br>\r\n<br>\r\nV exteriérech botanické zahrady naleznete v každém ročním období mnoho pozoruhodných rostlin. Naši průvodci pro vás vyberou nejzajímavější rostliny, které lze v daném ročním období spatřit nebo přizpůsobí prohlídku vašim individuálním požadavkům. Prohlídka trvá přibližně hodinu.\r\n',	'',	0),
(12,	6,	'<b>Skleníky</b><br>\r\n</br>\r\nPokud máte zájem zapojit při návštěvě skleníků i jiné smysly než zrak, objednejte si komentovanou prohlídku skleníků. Prohlídka trvá přibližně hodinu a seznámí vás s nejpoutavějšími rostlinami našich skleníků. Pro mateřské školy máme připraven speciální program přizpůsobený potřebám dětí, které ještě neumějí číst.\r\n',	'',	1),
(13,	6,	'<b>Ceny komentovaných prohlídek</b><br>\r\n<br>\r\n<b>Exteriéry - školy</b>\r\n<ul>\r\n<li>35 Kč na žáka (zahrnuje vstupné do skleníků), pedagogický doprovod zdarma</li>\r\n<li>20 Kč na žáka (nezahrnuje vstupné do skleníků), pedagogický doprovod zdarma</li>\r\n</ul><br>\r\n<b>Exteriéry - ostatní</b><br>\r\n200 Kč na skupinu (max 25 osob)<br>\r\n<br>\r\n<b>Skleníky - školy<b><br>\r\n35 Kč na žáka (zahrnuje vstupné do skleníků), pedagogický doprovod zdarma<br>\r\n<br>\r\n<b>Skleníky - ostatní</b><br>\r\n200 Kč na skupinu (max 15 osob) + vstupné do skleníků',	'',	2),
(14,	6,	'<b>Jak se objednat?</b><br>\r\n<br>\r\nProhlídky objednávejte na botazah@natur.cuni.cz (link sends e-mail). Do předmětu emailu napište “Komentovaná prohlídka - Exteriéry/Skleníky” a do textu uveďte (i) datum a čas prohlídky, (ii) počet osob a orientační věkové složení a (iii) kontaktní osobu s telefonním číslem.<br>\r\n<br>\r\nŠkoly mohou prohlídku objednat i přes projekt Přírodovědci.cz<br>\r\n<br>',	'',	3),
(15,	3,	'<b>Vstupné</b><br>\r\n<br>\r\nVstup do exteriérů je zdarma.<br>\r\n<br>\r\nVstupné do skleníků:<br>\r\n<ul>\r\n<li>Dospělí: 55 Kč</li>\r\n<li>Děti, studenti (do 26 let), senioři:	30 Kč</li>\r\n<li>Rodinné vstupné (2 dospělí, maximálně 3 děti): 130 Kč</li>\r\n<li>ZTP, děti do 5 let, pedagogický doprovod, studenti a zaměstnanci PřF UK: zdarma</li>\r\n</ul>\r\n<br>\r\nPozn: Na výstavy se vybírá zvláštní vstupné.',	'',	9),
(16,	7,	'<b>Výstavy v roce 2016</b><br>\r\n<b>11. 1. – 14. 2.</b> Malířská inspirace botani(c)kou<br>\r\n<b>30. 4. – 22. 5.</b> Pohádkové krajinky se sukulenty<br>\r\n<b>18. 5.</b> Den fascinace rostlinami – objevy ze světa rostlin (10:00–17:00)<br>\r\n<b>28. 5. – 15. 6.</b> Kaktusy a sukulenty – jarní etapa<br>\r\n<b>11. 6.</b> Pražská muzejní noc<br>\r\n<b>10. 6. – 19. 6.</b> Masožravé rostliny',	'',	0),
(17,	8,	'Sukulentní skleník je věnován velké trvalé expozici sukulentních rostlin, která je navíc neustále doplňována.  Botanická zahrada má v současnosti největší sbírku kaktusů ze všech botanických  zahrad v České republice.<br>\r\n<br>\r\nSbírka kaktusů (čeleď Cactaceae, od vchodu napravo) je ve výstavních vitrínách rozdělena podle kontinentů, kterým odpovídá i zvolená hornina, ve které jsou kaktusy umístěny. Ve volné expozici jsou nepřehlédnutelné například letité exempláře rodu Echinocactus, které jsou lidově snad téměř v každém jazyce nazývány “sedátko pro tchýni”.<br>\r\n<br>\r\nV levé části skleníku nalezneme ostatní sukulenty z čeledí aloovitých, agávovitých a mnoha dalších. Příkladem atraktivních a na podzim bohatě kvetoucích sukulentů mohou být zástupci rodu Lithops, tzv. živé kameny z čeledi kosmatcovitých. Zajímavou skupinou jsou také převislé kaktusy rostoucí v zadní vitríně – zástupci rodu Rhipsalis, kteří se přirozeně vyskytují v tropických lesích. Mají sice sukulentní stonky, ale nepotřebují trny bránící se okusu, neboť rostou jako epifity na dřevinách.<br>',	'',	0);

-- 2016-02-05 20:43:02

