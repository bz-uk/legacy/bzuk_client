<?php
namespace botgarApi\gallery;

use Slim\Slim;

class PutOne
{
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    public function run($galleryId)
    {
        $conf = $this->app->container->get('configuration');

        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $json = $this->app->request->getBody();
        $data = json_decode($json);

        if (!$data->name) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(["success"=>false, 'code'=>'validationError', 'messagge' => 'Name can not be empty']));
            return;
        }

        $isActive = $data->active?1:0;
        $show_plant_category = $data->show_plant_category?1:0;
        $sort_plants_order = $data->sort_plants_order;
        $sort_categories_order = $data->sort_categories_order;
        $show_category_headings = $data->show_category_headings?1:0;
        $sort_by_category = $data->sort_by_category?1:0;
        $plants = json_encode($data->plants);
        $filter = json_encode($data->filter);
        $sql = 'UPDATE drupal_gallery_widgets SET sort_by_category='.$sort_by_category.', show_category_headings='.$show_category_headings.', sort_categories_order='.$db->quote($sort_categories_order).', sort_plants_order='.$db->quote($sort_plants_order).', show_plant_category='.$show_plant_category.', name='.$db->quote($data->name).', active='.$isActive.', plants='.$db->quote($plants).',filter='.$db->quote($filter).' WHERE id=' . $galleryId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(["success"=>false, 'code'=>'unableToUpdate', 'sql' => $sql]));
            return;
        }
        //TODO: drop update cache if inactive/build if active
        $response = json_encode(['success'=> true]);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
