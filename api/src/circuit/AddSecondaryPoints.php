<?php
namespace botgarApi\circuit;

use Slim\Slim;

class AddSecondaryPoints {
    /**
     * @var Slim
     */
    private $app;

    public function __construct(Slim $app)
    {
        $this->app = $app;
    }

    private function returnInvalidResponse($message) {
      $response = ['status'=>'error', 'message'=> $message];
      $this->app->response->setStatus(400);
      $response = json_encode($response);
      $this->app->response->header('Content-Length', strlen($response));
      $this->app->response->setBody($response);
    }

    public function run($circuitId, $pointId) {
        $json = $this->app->request->getBody();
        $plants = json_decode($json);

        $conf = $this->app->container->get('configuration');
        $db = new \PDO('mysql:host='.$conf['db:host'].';port='.$conf['db:port'].';dbname='.$conf['db:dbname'].';charset=UTF8;',$conf['db:user'],$conf['db:pass']);

        $db->beginTransaction();

        foreach ($plants as $plant) {
          $sql = "INSERT INTO circuit_secondary_points (circuit_id, primary_point_id, plant_id, active) values(".$circuitId.", ".$pointId.", ".$plant->id.", 1)";
          $result = $db->query($sql);
          if (!$result) {
              $err = $db->errorCode();
              if ($err != "23000") {
                $db->rollback();
                $this->app->response->setStatus(400);
                $this->app->response->setBody(json_encode(['error' => 'unable to update secondary point', 'sql' => $sql, 'sql_err' => $db->errorInfo(), 'sql_code' => $db->errorCode()]));
                return;
              } else {
                continue;
              }
          }
        }

        //secondary points count
        $sql = 'select count(*) as cnt from circuit_secondary_points where primary_point_id=' . $pointId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(['error' => 'unable to count secondary points', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }
        $row = $result->fetch(\PDO::FETCH_ASSOC);
        $secondaryPointsCount = $row['cnt'];

        $sql = 'UPDATE circuit set secondary_count=' . $secondaryPointsCount . ' where id=' . $circuitId;
        $result = $db->query($sql);
        if (!$result) {
            $this->app->response->setStatus(400);
            $this->app->response->setBody(json_encode(['error' => 'unable to update circuit', 'sql' => $sql, 'sql_err' => $db->errorInfo()]));
            return;
        }

        $db->commit();

        $response = json_encode(['status' => 'ok', 'message' => 'point saved']);
        $this->app->response->header('Content-Length', strlen($response));
        $this->app->response->setBody($response);
    }
}
