'use strict';
import React from 'react';
import {black} from 'material-ui/styles/colors';
import FontIcon from 'material-ui/FontIcon';

const styles = {
    nowrap: {
        whiteSpace: 'nowrap'
    },
    header: {
        display: 'flex',
        alignItems: 'center'
    },
    headerActions: {
        marginLeft: '1rem'
    }
}


//TODO: check if props.actions is a/element using React.isValidElement OR array of elements
export default function (props) {
    let actions = null;
    if (React.isValidElement(props.actions)) {
        actions = props.actions;
    } else {
        if (props.actions.length > 0) {
            actions = [];
            for (let i = 0; i < props.actions.length; i++) {
                if (React.isValidElement(props.actions[i])) {
                    actions.push(props.actions[i]);
                } else {
                    console.log('Warning: action is not a valid react element!')
                }
            }
        }
    }
    return (
        <div style={styles.header}>
            <div>
                <h2 style={styles.nowrap}>
                    {props.icon ?
                        <FontIcon color={black} className="icon-puzzle"></FontIcon>
                        :
                        ''}
                    {props.title}
                </h2>
            </div>
            <div style={styles.headerActions}>
                {actions}
            </div>
        </div>
    );
}