CREATE TABLE `plant_codes` (
  `plant_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`plant_id`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
